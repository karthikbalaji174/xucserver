import play.sbt.PlayImport._
import sbt._

object Version {
  val asteriskjava       = "2.0.4.c.XIVOCC"
  val akka               = "2.6.21"
  val xivojavactilib     = "2018.05.08"
  val mockito            = "1.10.19"
  val playauthentication = "2023.08.00-play2.8"
  val metrics            = "4.2.19"
  val dbunit             = "2.7.3"
  val scalatestplay      = "5.1.0"
  val postgresql         = "42.6.0"
  val nScalaTime         = "2.30.0"
  val rabbitmq           = "5.18.0"
  val quartz             = "2.3.2"
  val play               = "2.8.7"
  val playws             = "2.1.11"
  val http4sVersion      = "0.21.34"
}

object Library {
  val asteriskjava = ("org.asteriskjava" % "asterisk-java" % Version.asteriskjava
    exclude ("javax.jms", "jms") exclude ("com.sun.jdmk", "jmxtools") exclude ("com.sun.jmx", "jmxri"))
  val xivojavactilib = "org.xivo" % "xivo-javactilib" % Version.xivojavactilib
  val playauthentication =
    "solutions.xivo" %% "play-authentication" % Version.playauthentication
  val metrics    = "io.dropwizard.metrics" % "metrics-core" % Version.metrics
  val metricsJvm = "io.dropwizard.metrics" % "metrics-jvm"  % Version.metrics
  val metricsJmx = "io.dropwizard.metrics" % "metrics-jmx"  % Version.metrics
  val metricsLogback =
    "io.dropwizard.metrics" % "metrics-logback" % Version.metrics
  val akkaTestkit = "com.typesafe.akka" %% "akka-testkit" % Version.akka
  val akkaStreamTestkit =
    "com.typesafe.akka" %% "akka-stream-testkit" % Version.akka
  val akkaActor  = "com.typesafe.akka" %% "akka-actor"       % Version.akka
  val akkaStream = "com.typesafe.akka" %% "akka-stream"      % Version.akka
  val akkaSlf4j  = "com.typesafe.akka" %% "akka-slf4j"       % Version.akka
  val akkaTyped  = "com.typesafe.akka" %% "akka-actor-typed" % Version.akka
  val akkaJackson =
    "com.typesafe.akka" %% "akka-serialization-jackson" % Version.akka
  val mockito    = "org.mockito"    % "mockito-all" % Version.mockito
  val dbunit     = "org.dbunit"     % "dbunit"      % Version.dbunit
  val postgresql = "org.postgresql" % "postgresql"  % Version.postgresql
  val nScalaTime =
    "com.github.nscala-time" %% "nscala-time" % Version.nScalaTime
  val rabbitmq = "com.rabbitmq"         % "amqp-client" % Version.rabbitmq
  val quartz   = "org.quartz-scheduler" % "quartz"      % Version.quartz
  val xerces   = "xerces"               % "xercesImpl"  % "2.12.1"

  lazy val scalaReflect =
    "org.scala-lang" % "scala-reflect" % Dependencies.scalaVersion
  lazy val decline    = "com.monovore"  %% "decline-effect" % "1.4.0"
  lazy val catscore   = "org.typelevel" %% "cats-core"      % "2.9.0"
  lazy val catseffect = "org.typelevel" %% "cats-effect"    % "2.5.5"
  lazy val scalaTest  = "org.scalatest" %% "scalatest"      % "3.2.16"
  lazy val http4sdsl  = "org.http4s"    %% "http4s-dsl"     % Version.http4sVersion
  lazy val http4sclient =
    "org.http4s" %% "http4s-blaze-client" % Version.http4sVersion
  lazy val http4scirce = "org.http4s" %% "http4s-circe" % Version.http4sVersion
  // Optional for auto-derivation of JSON codecs
  lazy val circegeneric = "io.circe" %% "circe-generic"        % "0.14.2"
  lazy val circeextras  = "io.circe" %% "circe-generic-extras" % "0.14.2"
  // Optional for string interpolation to JSON model
  lazy val circeliteral = "io.circe"    %% "circe-literal" % "0.14.2"
  lazy val circeparser  = "io.circe"    %% "circe-parser"  % "0.14.2"
  lazy val oslib        = "com.lihaoyi" %% "os-lib"        % "0.9.1"
}

object PlayLibrary {
  val playws = "com.typesafe.play" %% "play-ahc-ws-standalone" % Version.playws
  val playwsjson =
    "com.typesafe.play" %% "play-ws-standalone-json" % Version.playws
  val anorm        = "org.playframework.anorm" %% "anorm"          % "2.6.10"
  val jwt          = "com.github.jwt-scala"    %% "jwt-play-json"  % "8.0.2"
  val playjsonjoda = "com.typesafe.play"       %% "play-json-joda" % "2.9.4"
  val scalatestplay =
    "org.scalatestplus.play" %% "scalatestplus-play" % Version.scalatestplay
  val scalatestmock = "org.scalatestplus" %% "mockito-3-4" % "3.2.10.0"
}

object CliDependencies {
  import Library._

  val libraries = Seq(
    catscore,
    catseffect,
    decline,
    http4sdsl,
    http4sclient,
    http4scirce,
    circegeneric,
    circeextras,
    circeparser,
    circeliteral,
    scalaReflect,
    oslib,
    scalaTest % Test
  )
}

object Dependencies {

  import Library._
  import PlayLibrary._

  val scalaVersion = "2.13.10"
  val play         = "2.8.7"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/")
  )

  val runDep = run(
    asteriskjava,
    xivojavactilib,
    playauthentication,
    playws,
    playwsjson,
    javaWs,
    metrics,
    metricsJvm,
    metricsJmx,
    metricsLogback,
    postgresql,
    jdbc,
    anorm,
    guice,
    ws,
    nScalaTime,
    jwt,
    filters,
    rabbitmq,
    quartz,
    xerces,
    playjsonjoda,
    catscore,
    akkaActor,
    akkaTyped,
    akkaStream,
    akkaSlf4j,
    akkaJackson
  )

  val testDep = test(
    akkaTestkit,
    mockito,
    dbunit,
    scalatestplay,
    scalatestmock,
    akkaStreamTestkit
  )

  def run(deps: ModuleID*): Seq[ModuleID]  = deps
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
