#!/usr/bin/python

from psycopg2 import connect

def main():
    conn = connect(host='localhost', database='asterisk', user='asterisk', password='proformatique')
    cur = conn.cursor()
    cur.execute('SELECT id, number FROM agentfeatures')
    agents = cur.fetchall()
    for agent in agents:
        print '[agent-%d]' % agent[0]
        print 'agent_%d = 100' % agent[0]
        print 'agent_no_%s = 100' % agent[1]
        print 'genagent = 100'
    conn.close()

if __name__ == '__main__':
    main()

