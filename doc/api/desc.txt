connect
--------
curl -v -H "Content-Type: application/json" -XPOST http://localhost:9000/xuc/api/1.0/connect/avencall.com/bruce/ -d '{"password":"bruce"}'

dial
-----

curl -v -H "Content-Type: application/json" -XPOST http://localhost:9000/xuc/api/1.0/dial/avencall.com/bruce/ -d '{"number":"1012"}'

dnd
---

curl -v -H "Content-Type: application/json" -XPOST http://localhost:9000/xuc/api/1.0/dnd/avencall.com/bruce/ -d '{"state":true}'

forward
-------
curl -v -H "Content-Type: application/json" -XPOST http://localhost:9000/xuc/api/1.0/uncForward/avencall.com/bruce/ -d '{"state":true,"destination":"06223598"}'

curl -v -H "Content-Type: application/json" -XPOST http://localhost:9000/xuc/api/1.0/naForward/avencall.com/bruce/ -d '{"state":true,"destination":"77777"}'

curl -v -H "Content-Type: application/json" -XPOST http://localhost:9000/xuc/api/1.0/busyForward/avencall.com/bruce/ -d '{"state":true,"destination":"99999"}'
