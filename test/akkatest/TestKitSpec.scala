package akkatest

import akka.actor._
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.BeforeAndAfterAll
import scala.concurrent.duration._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

abstract class TestKitSpec(name: String)
    extends TestKit(ActorSystem(name))
    with AnyWordSpecLike
    with Matchers
    with BeforeAndAfterAll
    with ImplicitSender {

  val expectMsgTimeout = 500.milliseconds

  override def afterAll(): Unit = {
    system.terminate()
  }

  def createParentActor(
      childActor: => Actor,
      childName: String
  ): TestParentActor =
    new TestParentActor(childActor, childName)

  def createActorProxy(
      ref: ActorRef,
      stopRefOnExit: Boolean = false
  ): TestActorProxy =
    new TestActorProxy(ref, stopRefOnExit)
}

class TestParentActor(childActor: => Actor, childName: String) extends Actor {
  override def preStart(): Unit = {
    context.actorOf(Props(childActor), childName)
  }
  def receive = Actor.emptyBehavior
}

class TestActorProxy(ref: ActorRef, stopRefOnExit: Boolean = false)
    extends Actor {

  override def postStop() = {
    if (stopRefOnExit)
      ref ! PoisonPill
  }

  def receive = { case o =>
    ref.forward(o)
  }
}
