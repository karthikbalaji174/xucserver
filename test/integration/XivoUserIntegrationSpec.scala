package integration

import models.{XivoUser, XivoUserDao}
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import xuctest.{BaseTest, IntegrationTest}

class XivoUserIntegrationSpec
    extends BaseTest
    with GuiceOneAppPerSuite
    with ScalaFutures {

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .build()

  "xuc" should {

    "be able to get a list of users from xivo" taggedAs (IntegrationTest) in {
      val xivoUserDao = app.injector.instanceOf[XivoUserDao]
      val users       = xivoUserDao.all()
      users.length should be > 0
    }

    "be able to get a user from xivo" taggedAs (IntegrationTest) in {
      val xivoUserDao = app.injector.instanceOf[XivoUserDao]
      val fResult     = xivoUserDao.getCtiUser(2)

      fResult.futureValue should be(
        Some(
          XivoUser(
            2,
            None,
            None,
            "bruce",
            Some("William"),
            Some("bruce"),
            Some("0000"),
            None,
            Some(1)
          )
        )
      )
    }
  }
}
