package services

import akka.actor.{Actor, ActorRef, Kill, Props, Terminated}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.{DynamicFilter, OperatorEq, Queue => _, _}
import org.json.JSONObject
import org.mockito.Mockito.{never, stub, verify, when}
import org.mockito.{ArgumentCaptor, Matchers}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentStatusUpdate, IpbxCommandResponse}
import org.xivo.cti.model.Endpoint.EndpointType
import org.xivo.cti.model.{Endpoint, QueueStatRequest}
import play.api.libs.json.Json
import services.XucEventBus.{Topic, TopicType}
import services.agent.AgentAction
import services.calltracking.DeviceConferenceAction._
import services.calltracking.SipDriver
import services.chat.model._
import services.config.ConfigDispatcher._
import services.config.{ConfigRepository, ConfigServerRequester}
import services.line.PhoneController
import services.request.PhoneRequest.GetCurrentCallsPhoneEvents
import services.request._
import services.user.CtiUserAction
import services.video.model._
import services.voicemail.{SubscribeToVoiceMail, UnsubscribeFromVoiceMail, VoiceMailStatusUpdate}
import xivo.directory.PersonalContactRepository
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._
import xivo.models._
import xivo.network.CtiLinkKeepAlive.StartKeepAlive
import xivo.network.LoggedOn
import xivo.phonedevices.{OutboundDial, RequestToMds}
import xivo.websocket.LinkState.{down, up}
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.{WsContent, WsMessageEvent}
import xivo.websocket.{LinkStatusUpdate, WebSocketEvent, WsBus, WsConferenceCommandError}
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import java.time.OffsetDateTime
import java.util.{Date, UUID}
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters._

class CtiRouterSpec
    extends TestKitSpec("CtiRouterSpec")
    with GuiceOneAppPerSuite
    with MockitoSugar
    with XucUserHelper {

  import xivo.models.LineHelper.makeLine

  var lastId: Int                    = 0
  implicit lazy val executionContext = app.injector.instanceOf[ExecutionContext]

  class Helper {
    val parent                 = TestProbe()
    val testCtiLink            = TestProbe()
    val testCtiFilter          = TestProbe()
    val configDispatcher       = TestProbe()
    val agentConfig            = TestProbe()
    val agentActionService     = TestProbe()
    val wsBus                  = mock[WsBus]
    val eventBus               = mock[XucEventBus]
    val statEventBus           = mock[XucStatsEventBus]
    val amiBusConnector        = TestProbe()
    val messageFactory         = mock[MessageFactory]
    val callHistoryManager     = TestProbe()
    val xivoDirectoryInterface = TestProbe()
    val callbackMgrInterface   = TestProbe()
    val voiceMailManager       = TestProbe()
    val flashTextService       = TestProbe()
    val clientLogger           = TestProbe()
    val configServerRequester  = mock[ConfigServerRequester]
    val videoEventManager      = TestProbe()
    val videoService           = TestProbe()
    val phoneControllerFactory = new PhoneController.Factory {
      def apply(phoneNb: String, line: Line, xivoUser: XivoUser): Actor =
        createActorProxy(phoneController.ref)
    }
    val userPreferenceService = TestProbe()

    val ctiFilterFactory = new CtiFilter.Factory {
      def apply(
          user: XucUser,
          myRouter: ActorRef,
          personalContactRepo: ActorRef,
          configRepository: ConfigRepository
      ): Actor =
        createActorProxy(testCtiFilter.ref, stopRefOnExit = true)
    }
    val personalContactRepoFactory = mock[PersonalContactRepository.Factory]
    val agentActionFactory         = mock[AgentAction.Factory]
    val phoneController            = TestProbe()
    val queueDispatcher            = TestProbe()
    val configRepo                 = mock[ConfigRepository]
    val xucConfig                  = app.injector.instanceOf[XucBaseConfig]

    when(configRepo.getLineForAgent(Matchers.anyLong())).thenReturn(None)

    val mForward    = mock[(ForwardRequest) => {}]
    val mUserAction = mock[(UserActionRequest, Long) => {}]

    class TestUserActionService(link: ActorRef)
        extends CtiUserAction(
          link,
          new MessageFactory(),
          configServerRequester
        ) {

      override def doUserAction(request: UserActionRequest, userId: Long) = {
        mUserAction(request, userId)
      }
    }

    def actor(user: XucUser) = {
      lastId = lastId + 1
      val actorName = s"testCtiRouter$lastId"
      val a = TestActorRef[CtiRouter](
        Props(
          new CtiRouter(
            wsBus,
            eventBus,
            statEventBus,
            messageFactory,
            phoneControllerFactory,
            ctiFilterFactory,
            personalContactRepoFactory,
            configRepo,
            agentActionFactory,
            configServerRequester,
            callbackMgrInterface.ref,
            configDispatcher.ref,
            callHistoryManager.ref,
            xivoDirectoryInterface.ref,
            amiBusConnector.ref,
            queueDispatcher.ref,
            agentConfig.ref,
            voiceMailManager.ref,
            flashTextService.ref,
            videoService.ref,
            clientLogger.ref,
            videoEventManager.ref,
            userPreferenceService.ref,
            xucConfig,
            user
          ) {

            override def preStart() = {
              super.preStart()
              ctiUserAction = new TestUserActionService(ctiLink)
            }

            override def createCtiLink(username: String): ActorRef =
              testCtiLink.ref
          }
        ),
        parent.ref,
        actorName
      )
      a.underlyingActor.agentActionService = agentActionService.ref
      a.underlyingActor.phoneController = Some(phoneController.ref)
      (a, a.underlyingActor)
    }
  }

  "A CtiRouter actor" should {

    """ answer Started when asked IsReady""" in new Helper {
      val user             = getXucUser("isready", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.phoneController = Some(TestProbe().ref)

      ref ! IsReady

      expectMsg(Started())
      ctiRouter.isReadyRequester should not contain (self)
    }

    """If logon message exists
        Send new client connected to ctiFilter on wsconnected
        update router user field
        subscribe to voicemail""" in new Helper {

      val user             = getXucUser("wsconnected", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val updatedUser = getXucUser("yoip", "pwd", Some("2006"))

      val wsActor = TestProbe()

      ref ! WsConnected(wsActor.ref, updatedUser)

      testCtiFilter.expectMsgAllOf(
        ClientConnected(wsActor.ref, LoggedOn(user, "34"))
      )
      ctiRouter.user should be(updatedUser)
      voiceMailManager.expectMsg(SubscribeToVoiceMail(updatedUser.xivoUser))

    }

    "prevent the ctirouter from counting twice the same actor" in new Helper {
      val user             = getXucUser("wsconnected", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val updatedUser = getXucUser("yoip", "pwd", Some("2006"))

      val wsActor = TestProbe()

      ctiRouter.nbOfClients.getCount() should be(0)

      ref ! WsConnected(wsActor.ref, updatedUser)
      ctiRouter.nbOfClients.getCount() should be(1)

      ref ! WsConnected(wsActor.ref, updatedUser)
      ctiRouter.nbOfClients.getCount() should be(1)
    }

    "connect flash text user if user is logged on" in new Helper {
      val user     = getXucUser("loggedON", "sdf78d", Some("2006"))
      val (ref, _) = actor(user)

      val loggedOn = LoggedOn(user, "34")

      ref ! loggedOn

      testCtiFilter.expectMsg(loggedOn)
      flashTextService.expectMsg(ConnectFlashTextUser(user.xivoUser))
    }

    "stop self and ctilink when ctilink terminates" in new Helper {
      val user             = getXucUser("shre", "sdf78d", Some("2002"))
      val (ref, ctiRouter) = actor(user)
      val watcher          = TestProbe()

      watcher.watch(testCtiFilter.ref)
      watcher.watch(ref)

      testCtiLink.ref ! Kill

      watcher.expectMsgAllClassOf(classOf[Terminated], classOf[Terminated])
      flashTextService.expectMsg(DisconnectFlashTextUser(user.xivoUser))
    }

    "forward ipbxcommandmessage to agent service" in new Helper {
      val (ref, _) = actor(getXucUser("ipbxcommand", "sdf78d", Some("2006")))
      val ipbxcommandMessage =
        new IpbxCommandResponse("agent_login_invalid_exten", new Date)

      ref ! ipbxcommandMessage

      agentActionService.expectMsg(ipbxcommandMessage)
    }
    "forward Cti messages to the ctiFilter" in new Helper {
      val (ref, ctiRouter) = actor(getXucUser("tata", "sdf78d", Some("2006")))
      val ctiMessage       = new AgentStatusUpdate(34, null)

      ref ! ctiMessage

      testCtiFilter.expectMsg(ctiMessage)
    }

    "forward config request to configDispatcher" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("cfgRequest", "sdf78d", Some("2006")))

      val requestConfig = RequestConfig(self, GetConfig("agent"))

      ref ! requestConfig

      configDispatcher.expectMsg(requestConfig)
    }

    "forward config change request to config dispatcher " in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("cfgRequest", "sdf78d", Some("2006")))

      val cfgChangeRequest = ConfigChangeRequest(ref, SetAgentQueue(32, 21, 7))

      ref ! cfgChangeRequest

      configDispatcher.expectMsg(cfgChangeRequest)

    }

    "forward agent status request to configDispatcher and do not start keepalive" in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypeAgent

      val (ref, ctiRouter) =
        actor(getXucUser("cfgRequest", "sdf78d", Some("2006")))

      val requestStatus = RequestStatus(self, 32, TypeAgent)

      ref ! requestStatus

      configDispatcher.expectMsg(requestStatus)
    }

    "forward user config updated to dispatcher" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("cfgRequest", "sdf78d", Some("2006")))

      val userConfigUpdated = UserConfigUpdated(1L, None)

      ref ! userConfigUpdated

      configDispatcher.expectMsg(userConfigUpdated)
    }

    "send started message to creator when loggedon forward to filter and config dispatcher start keepalive" in new Helper {
      val user             = getXucUser("loggedON", "sdf78d", Some("2006"))
      val (ref, ctiRouter) = actor(user)
      val linkKeepAlive    = TestProbe()
      ctiRouter.ctiKeepAlive = linkKeepAlive.ref

      val loggedOn = LoggedOn(user, "34")

      ref ! loggedOn

      testCtiFilter.expectMsg(loggedOn)
      linkKeepAlive.expectMsg(
        StartKeepAlive(loggedOn.userId, ctiRouter.ctiLink)
      )
      flashTextService.expectMsg(ConnectFlashTextUser(user.xivoUser))
    }

    "Publish JsonNode messages to the web socket bus" in new Helper {
      val user             = getXucUser("moilp", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)

      val message = Json.parse("{\"hello\": \"test\"}")

      ref ! message

      verify(wsBus).publish(
        WsMessageEvent(WsBus.browserTopic(user.username), WsContent(message))
      )
    }

    "publish link status down message when CtiLink sends one and remove loggedon message" in new Helper {
      val user             = getXucUser("linkstatus", "sdf78d", Some("2006"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))

      ref ! LinkStatusUpdate(down)

      verify(wsBus).publish(
        WsMessageEvent(
          WsBus.browserTopic("linkstatus"),
          WsContent(WebSocketEvent.createEvent(LinkStatusUpdate(down)))
        )
      )
      ctiRouter.loggedOnMsg should be(None)
    }
    "send start message to cti link on link status up do not publish link status" in new Helper {
      val user             = getXucUser("linkstatusup", "sdf78d", Some("2006"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.ctiLink = testCtiLink.ref

      ref ! LinkStatusUpdate(up)

      testCtiLink.expectMsg(Start(user))

      verify(wsBus, never).publish(
        WsMessageEvent(
          WsBus.browserTopic("linkstatusup"),
          WebSocketEvent.createEvent(LinkStatusUpdate(down))
        )
      )
    }

    "receive a request success on request forwarded to ctilink" in new Helper {
      val user             = getXucUser("requestSucess", "password", Some("5023"))
      val (ref, ctiRouter) = actor(user)

      ref ! LoggedOn(user, "34")
      val ctiRequest = new MessageFactory().createDial(
        new Endpoint(EndpointType.PHONE, "3546")
      )
      ref ! ctiRequest
      expectMsgType[CtiReqSuccess]

    }

    "try to resend request if user not logged to the cti server while receiving a request" in new Helper {
      val user             = getXucUser("resendRequest", "password", Some("5023"))
      val (ref, ctiRouter) = actor(user)

      val ctiRequest = new MessageFactory().createDial(
        new Endpoint(EndpointType.PHONE, "3546")
      )
      ref ! ctiRequest
      ref ! LoggedOn(user, "34")
      expectMsgType[CtiReqSuccess]

    }

    "send an error on request retry if user not logged to the cti server" in new Helper {
      val user             = getXucUser("requestNotLogged", "password", Some("5023"))
      val (ref, ctiRouter) = actor(user)

      val ctiRequest = new MessageFactory().createDial(
        new Endpoint(EndpointType.PHONE, "3546")
      )
      ref ! ctiRequest
      expectMsgType[CtiReqError]
    }

    "Send update config request to config dispatcher" in new Helper {
      val (ref, _) =
        actor(getXucUser("UpdateConfigRequest", "sdf78d", Some("2006")))

      val updateConfigRequest = mock[UpdateConfigRequest]

      ref ! BaseRequest(self, updateConfigRequest)

      configDispatcher.expectMsg(ConfigChangeRequest(self, updateConfigRequest))

    }
    "Send move agent in group message to agent config service" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentConfigRequest", "sdf78d", Some("2006")))

      val agentConfigRequest = mock[AgentConfigRequest]

      ref ! BaseRequest(self, agentConfigRequest)

      agentConfig.expectMsg(BaseRequest(self, agentConfigRequest))
    }
    "send agent login request without any id to cti filter to be completed" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentLoginWithoutId", "sdf78d", Some("2006")))

      val loginReq = AgentLoginRequest(None, Some("2001"))

      ref ! BaseRequest(self, loginReq)

      testCtiFilter.expectMsg(loginReq)

    }
    "send agent logout request without any id to cti filter to be completed" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentLogoutWithoutId", "sdf78d", Some("2006")))

      val logoutReq = AgentLogoutRequest(None)

      ref ! BaseRequest(self, logoutReq)

      testCtiFilter.expectMsg(logoutReq)

    }
    "send agent pause request without any id to cti filter to be completed" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentPauseWithoutId", "sdf78d", Some("2006")))

      val pauseReq = AgentPauseRequest(None)

      ref ! BaseRequest(self, pauseReq)

      testCtiFilter.expectMsg(pauseReq)

    }
    "send agent unpause request without any id to cti filter to be completed" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentUnPauseWithoutId", "sdf78d", Some("2006")))

      val unpauseReq = AgentUnPauseRequest(None)

      ref ! BaseRequest(self, unpauseReq)

      testCtiFilter.expectMsg(unpauseReq)

    }
    "send agent listen request with no userid to ctifilter" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentListenRequest", "listen", Some("3006")))

      val agentListen = AgentListen(34)

      ref ! BaseRequest(self, agentListen)

      testCtiFilter.expectMsg(agentListen)

    }
    "Send agent listen request with a userid to agent action service" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentListenRequest", "listen", Some("3006")))

      val agentListen = AgentListen(34, Some(56))

      ref ! agentListen

      agentActionService.expectMsg(agentListen)

    }
    "send agent action requests to agent action service" in new Helper {
      val (ref, _) =
        actor(getXucUser("agentActionRequest", "sdf78d", Some("2006")))

      val agentActionRequest = mock[AgentActionRequest]

      ref ! BaseRequest(self, agentActionRequest)

      agentActionService.expectMsg(agentActionRequest)

    }

    "subscribe sender to agent events on request" in new Helper {
      val (ref, _) =
        actor(getXucUser("eventbusrequest", "sdf78d", Some("2006")))

      val subscribeToAgentEvents = SubscribeToAgentEvents

      val requester = TestProbe()

      ref ! BaseRequest(requester.ref, subscribeToAgentEvents)

      verify(eventBus).subscribe(
        requester.ref,
        Topic(TopicType.Event, ObjectType.TypeAgent)
      )
    }

    "send user status update request with no userid to ctifilter" in new Helper {
      val (ref, _) =
        actor(getXucUser("userStatusUpdateRequest", "lsdf99", Some("3006")))

      val userStatusUpdateReq = UserStatusUpdateReq(None, "backoffice")

      ref ! BaseRequest(self, userStatusUpdateReq)

      testCtiFilter.expectMsg(userStatusUpdateReq)

    }

    "send UserActionRequest to userActionService" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("userActionRequest", "pwd", Some("2104")))

      val userActionRequest = mock[UserActionRequest]

      ref ! BaseRequest(TestProbe().ref, userActionRequest)

      verify(mUserAction)(userActionRequest, 1)
    }

    "add phone number, forward config request from browser to configDispatcher and subscribe to event bus" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("configRequest", "sdf78d", Some("2006")))

      val requester = TestProbe()

      val requestConfig = BaseRequest(requester.ref, GetConfig("agent"))

      ref ! requestConfig

      configDispatcher.expectMsg(
        RequestConfigForUsername(
          requester.ref,
          GetConfig("agent"),
          Some("configRequest")
        )
      )
      verify(eventBus).subscribe(
        requester.ref,
        Topic(TopicType.Config, ObjectType.TypeAgent)
      )

    }
    "forward get list request from browser to configDispatcher and subscribe to event bus" in new Helper {

      import services.config.ConfigDispatcher.ObjectType._

      val (ref, ctiRouter) =
        actor(getXucUser("configRequest", "sdf78d", Some("2006")))

      val requester = TestProbe()

      val requestList = BaseRequest(requester.ref, GetList(TypeQueueMember))

      ref ! requestList

      configDispatcher.expectMsg(
        RequestConfig(requester.ref, GetList("queuemember"))
      )
      verify(eventBus).subscribe(
        requester.ref,
        Topic(TopicType.Config, ObjectType.TypeQueueMember)
      )

    }

    "forward monitor action from browser to phone controller" in new Helper {

      val (ref, ctiRouter) =
        actor(getXucUser("configRequest", "sdf78d", Some("2006")))

      val request = mock[MonitorActionRequest]

      ref ! BaseRequest(null, request)

      phoneController.expectMsg(request)

    }

    "forward outboundDial to amiBusconnector" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("configRequest", "sdf78d", Some("2006")))

      val request = OutboundDial(
        "123456789",
        85,
        "3000",
        Map("VAR" -> "Value"),
        "10.20.10.3",
        123L,
        SipDriver.SIP
      )

      ref ! request

      amiBusConnector.expectMsg(RequestToMds(request, None))

    }

    "forward RequestToAmi to amiBusConnector tagged with current line mds" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("configRequest", "sdf78d", Some("2006")))
      val line = makeLine(
        3,
        "default",
        "SIP",
        "ahjhjk",
        Some(XivoDevice("78", Some("10.10.0.2"), "Snom")),
        xivoIp = "",
        registrar = Some("mds1")
      )
      val lcf = LineConfig("12", "1000", Some(line))

      ref ! lcf

      val request = OutboundDial(
        "123456789",
        85,
        "3000",
        Map("VAR" -> "Value"),
        "10.20.10.3",
        123L,
        SipDriver.SIP
      )

      ref ! request

      amiBusConnector.expectMsg(RequestToMds(request, Some("mds1")))

    }

    """upon configQuery command received
      |- send configQuery request to config service""" in new Helper {
      val user             = getXucUser("configQuery", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)

      val requester = TestProbe()

      val configQuery   = mock[ConfigQuery]
      val requestConfig = BaseRequest(requester.ref, configQuery)

      ref ! requestConfig

      configDispatcher.expectMsg(RequestConfig(requester.ref, configQuery))
    }

    "Subscribe to queue statistics on request" in new Helper {
      val user             = getXucUser("subscribestats", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()

      val requestSubStat = BaseRequest(requester.ref, SubscribeToQueueStats)

      ref ! requestSubStat

      verify(statEventBus).subscribe(requester.ref, ObjectDefinition(Queue))
    }

    "Subscribe to agent statistics on request and get all statistics" in new Helper {
      val user             = getXucUser("subscribeagentstats", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()

      val requestSubStat = BaseRequest(requester.ref, SubscribeToAgentStats)

      ref ! requestSubStat

      configDispatcher.expectMsg(
        RequestConfig(requester.ref, GetAgentStatistics)
      )
      verify(eventBus).subscribe(
        requester.ref,
        Topic(TopicType.Stat, ObjectType.TypeAgent)
      )
    }

    "Subscribe to phoneHints on request" in new Helper {
      val user             = getXucUser("subscribephonehints", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()

      val subscribeToPhoneHints =
        BaseRequest(requester.ref, SubscribeToPhoneHints(List("1005", "1006")))

      ref ! subscribeToPhoneHints

      configDispatcher.expectMsg(
        MonitorPhoneHint(requester.ref, List("1005", "1006"))
      )
    }

    "unsubscribe from all phone hints on request" in new Helper {
      val user             = getXucUser("unsubscribefromallphonehints", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()

      ref ! BaseRequest(requester.ref, UnsubscribeFromAllPhoneHints)

      configDispatcher.expectMsg(MonitorPhoneHint(requester.ref, List()))
    }

    "subscribe to queue calls on request and get queue calls" in new Helper {
      val user             = getXucUser("subscribequeuecalls", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()
      val queueId          = 3

      val requestQCalls =
        BaseRequest(requester.ref, SubscribeToQueueCalls(queueId))

      ref ! requestQCalls

      configDispatcher.expectMsg(
        RequestConfig(requester.ref, GetQueueCalls(queueId))
      )
      verify(eventBus).subscribe(
        requester.ref,
        Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId))
      )
    }

    "unsubscribe to queue calls on request" in new Helper {
      val user             = getXucUser("unsubscribequeuecalls", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()
      val queueId          = 3

      val requestQCalls =
        BaseRequest(requester.ref, UnSubscribeToQueueCalls(queueId))

      ref ! requestQCalls

      verify(eventBus).unsubscribe(
        requester.ref,
        Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId))
      )
    }

    "transform the InviteConferenceRoom message and send the result to cti link" in new Helper {
      val userId           = 5
      val user             = getXucUser("InviteConferenceRoom", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val json             = new JSONObject()
      stub(messageFactory.createInviteConferenceRoom(userId)).toReturn(json)
      val requester = TestProbe()

      ref ! BaseRequest(requester.ref, InviteConferenceRoom(userId))

      verify(messageFactory).createInviteConferenceRoom(userId)
      testCtiLink.expectMsg(json)
    }
    import org.mockito.Matchers._

    "process queue statistics request" in new Helper {
      val queueId = 67
      val window  = 3600
      val xpos    = 60
      val req     = QueueStatisticsReq(queueId, window, xpos)

      val user             = getXucUser("QueueStatisticRequest", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val json             = new JSONObject()
      json.append("test", "QueueStatisticRequest")

      when(
        messageFactory.createGetQueueStatistics(
          List(any[QueueStatRequest]()).asJava
        )
      ).thenReturn(json)

      val arg =
        ArgumentCaptor.forClass(classOf[java.util.ArrayList[QueueStatRequest]])

      ref ! BaseRequest(TestProbe().ref, req)

      verify(messageFactory).createGetQueueStatistics(arg.capture)

      val queueStatRequest = arg.getValue.get(0)
      queueStatRequest.getQueueId shouldBe queueId.toString
      queueStatRequest.getWindow shouldBe window
      queueStatRequest.getXqos shouldBe xpos

      testCtiLink.expectMsg(json)

    }
    "send GetAgentCallHistory request to CallHistoryManager service" in new Helper {
      val userName         = "toto"
      val (ref, ctiRouter) = actor(getXucUser(userName, "pwd", Some("2104")))
      val historyRequest   = GetAgentCallHistory(5)
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      callHistoryManager.expectMsg(
        BaseRequest(testProbe.ref, AgentCallHistoryRequest(5, userName))
      )
    }

    "send FindCustomerCallHistoryRequest to CallHistoryManager service" in new Helper {
      val userName         = "toto"
      val (ref, ctiRouter) = actor(getXucUser(userName, "pwd", Some("2104")))
      val criteria = FindCustomerCallHistoryRequest(
        List(DynamicFilter("src_num", Some(OperatorEq), Some("1234")))
      )
      val historyRequest = CustomerCallHistoryRequestWithId(1, criteria)
      val testProbe      = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      callHistoryManager.expectMsg(
        BaseRequest(
          testProbe.ref,
          CustomerCallHistoryRequestWithId(1, criteria)
        )
      )
    }

    "send GetQueueCallHistory request to CallHistoryManager service" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("queueCallHistory", "pwd", Some("2104")))
      val queue          = "bl"
      val size           = 5
      val historyRequest = GetQueueCallHistory(queue, size)
      val testProbe      = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      callHistoryManager.expectMsg(
        BaseRequest(testProbe.ref, GetQueueCallHistory(queue, size))
      )
    }

    "send DirectoryLookUp request to xivoDirectory interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, DirectoryLookUp("term"))

      xivoDirectoryInterface.expectMsg(
        UserBaseRequest(testProbe.ref, DirectoryLookUp("term"), user)
      )
    }

    "send GetFavorites request to xivoDirectory interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, GetFavorites)

      xivoDirectoryInterface.expectMsg(
        UserBaseRequest(testProbe.ref, GetFavorites, user)
      )
    }

    "send SetFavorite request to xivoDirectory interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, AddFavorite("123", "xivou"))

      xivoDirectoryInterface.expectMsg(
        UserBaseRequest(testProbe.ref, AddFavorite("123", "xivou"), user)
      )
    }

    "send RemoveFavorite request to xivoDirectory interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, RemoveFavorite("34", "xivou"))

      xivoDirectoryInterface.expectMsg(
        UserBaseRequest(testProbe.ref, RemoveFavorite("34", "xivou"), user)
      )
    }

    "send call back requests to callback management service" in new Helper {
      val (ref, _) =
        actor(getXucUser("callbackMgrRequest", "ssdf77", Some("2029")))

      val callbackRequest = mock[CallbackMgrRequest]

      ref ! BaseRequest(self, callbackRequest)

      callbackMgrInterface.expectMsg(BaseRequest(self, callbackRequest))
    }

    "transfer GetCallbackLists to callbackMgr interface and subscribe to callback events" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, GetCallbackLists)

      callbackMgrInterface.expectMsg(
        BaseRequest(testProbe.ref, GetCallbackLists)
      )
      verify(eventBus).subscribe(
        testProbe.ref,
        XucEventBus.callbackListsTopic()
      )
    }

    "transfer FindCallbackRequestWithId to callbackMgr interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()
      val rq               = FindCallbackRequestWithId(1, FindCallbackRequest(List.empty))

      ref ! BaseRequest(testProbe.ref, rq)

      callbackMgrInterface.expectMsg(BaseRequest(testProbe.ref, rq))
    }

    "transfer TakeCallback to callbackMgr interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()
      val uuid             = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, TakeCallback(uuid.toString))

      callbackMgrInterface.expectMsg(
        TakeCallbackWithAgent(uuid.toString, "toto")
      )
    }

    "transfer ReleaseCallback to callbackMgr interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()
      val uuid             = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, ReleaseCallback(uuid.toString))

      callbackMgrInterface.expectMsg(ReleaseCallback(uuid.toString))
    }

    "enrich StartCallback with the username and transfer to the callbackMgr interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()
      val uuid             = UUID.randomUUID()

      ref ! BaseRequest(testProbe.ref, StartCallback(uuid.toString, "3322154"))

      callbackMgrInterface.expectMsg(
        BaseRequest(
          testProbe.ref,
          StartCallbackWithUser(uuid.toString, "3322154", "toto")
        )
      )
    }

    "transfer PhoneRequests to the phoneController" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val request          = new PhoneRequest {}

      ref ! BaseRequest(self, request)

      phoneController.expectMsg(UserBaseRequest(self, request, user))
    }

    "transfer UpdateCallbackTicket to the callbackMgr interface" in new Helper {
      val user             = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val testProbe        = TestProbe()
      val uuid             = UUID.randomUUID()

      ref ! BaseRequest(
        testProbe.ref,
        UpdateCallbackTicket(
          uuid.toString,
          Some(CallbackStatus.Answered),
          Some("Small comment")
        )
      )

      callbackMgrInterface.expectMsg(
        BaseRequest(
          testProbe.ref,
          UpdateCallbackTicket(
            uuid.toString,
            Some(CallbackStatus.Answered),
            Some("Small comment")
          )
        )
      )
    }

    "stop the previous phoneController and initialize a new one when receiving a LineConfig" in new Helper {
      val user               = getXucUser("toto", "pwd", Some("2104"))
      val (ref, ctiRouter)   = actor(user)
      val previousController = TestProbe()
      ctiRouter.phoneController = Some(previousController.ref)
      val line = makeLine(
        3,
        "default",
        "SIP",
        "ahjhjk",
        Some(XivoDevice("78", Some("10.10.0.2"), "Snom")),
        xivoIp = ""
      )
      val lcf = LineConfig("12", "1000", Some(line))
      watch(previousController.ref)
      ctiRouter.isReadyRequester = Set(self)

      ref ! lcf

      expectTerminated(previousController.ref)
      phoneController.expectMsg(PhoneController.Start)
      phoneController.expectMsg(
        UserBaseRequest(ref, GetCurrentCallsPhoneEvents, user)
      )
      expectMsg(Started())
    }

    "inform connected websocket when receiving a new LineConfig" in new Helper {
      val user             = getXucUser("wsconnected", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val updatedUser = getXucUser("yoip", "pwd", Some("2006"))
      val line1 = makeLine(
        3,
        "default",
        "SIP",
        "abcde",
        Some(XivoDevice("78", Some("10.10.0.2"), "Yealink")),
        xivoIp = ""
      )
      val lcf1 = LineConfig("12", "1000", Some(line1))

      val line2 = makeLine(
        7,
        "default",
        "SIP",
        "fghij",
        Some(XivoDevice("89", Some("10.10.0.5"), "Snom")),
        xivoIp = ""
      )
      val lcf2 = LineConfig("15", "1001", Some(line2))

      val wsActor = TestProbe()

      ref ! WsConnected(wsActor.ref, updatedUser)

      ref ! lcf1
      wsActor.expectMsg(lcf1)

      ref ! lcf2
      wsActor.expectMsg(lcf2)
    }

    "inform all connected websockets when receiving a new LineConfig" in new Helper {
      val user             = getXucUser("wsconnected", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val updatedUser = getXucUser("yoip", "pwd", Some("2006"))
      val line = makeLine(
        3,
        "default",
        "SIP",
        "abcde",
        Some(XivoDevice("78", Some("10.10.0.2"), "Yealink")),
        xivoIp = ""
      )
      val lcf = LineConfig("12", "1000", Some(line))

      val wsActor1 = TestProbe()
      val wsActor2 = TestProbe()

      ref ! WsConnected(wsActor1.ref, updatedUser)
      ref ! WsConnected(wsActor2.ref, updatedUser)

      ref ! lcf
      wsActor1.expectMsg(lcf)
      wsActor2.expectMsg(lcf)

    }

    "unsubscribe from XucEventBus when client leaves" in new Helper {

      val user             = getXucUser("wsconnected", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val updatedUser = getXucUser("yoip", "pwd", Some("2006"))

      val wsActor = TestProbe()

      ref ! WsConnected(wsActor.ref, updatedUser)

      ref ! BaseRequest(wsActor.ref, SubscribeToAgentStats)

      ref ! Leave(wsActor.ref)

      verify(eventBus).unsubscribe(wsActor.ref)

    }

    """Stop ctilink when no more  ws actor is connected
      update flash text status
      unsubscribe voicemail""" in new Helper {

      val user             = getXucUser("StopCtilink", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))

      val wsActor1     = TestProbe()
      val wsActor2     = TestProbe()
      val ctiLinkProbe = TestProbe()
      ctiLinkProbe watch testCtiLink.ref
      ref ! WsConnected(wsActor1.ref, user)
      voiceMailManager.expectMsg(SubscribeToVoiceMail(user.xivoUser))
      ref ! WsConnected(wsActor2.ref, user)
      ref ! Leave(wsActor1.ref)
      ref ! Leave(wsActor2.ref)
      flashTextService.expectMsg(DisconnectFlashTextUser(user.xivoUser))
      voiceMailManager.expectMsg(UnsubscribeFromVoiceMail(user.xivoUser))
      videoEventManager.expectMsg(
        UserBaseRequest(wsActor2.ref, VideoEvent("videoEnd"), user)
      )
      ctiLinkProbe.expectTerminated(testCtiLink.ref)

    }

    """send a logout agent for webrtc agents""" in new Helper {

      val line = makeLine(
        3,
        "default",
        "SIP",
        "ahjhjk",
        None,
        xivoIp = "",
        webRTC = true,
        number = Some("2004"),
        registrar = Some("mds1")
      )
      val user = getXucUser("StopCtilink", "4etr,wqd", Some("2004"))
      when(configRepo.getLineForAgent(1L)).thenReturn(Some(line))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))

      val wsActor1     = TestProbe()
      val ctiLinkProbe = TestProbe()
      ctiLinkProbe watch testCtiLink.ref
      ref ! WsConnected(wsActor1.ref, user)
      ctiRouter.gracefulAgentLogout(user.xivoUser.agentId.get)
      user.phoneNumber.foreach(phone => {
        ctiRouter.gracefulAgentLogout(user.xivoUser.agentId.get)
        agentActionService.expectMsg(AgentLogoutRequest(user.xivoUser.agentId))
      })
    }

    "decrement correctly the number of connected user" in new Helper {

      val user             = getXucUser("StopCtilink", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val updatedUser = getXucUser("newuser", "pwd", Some("2006"))

      val wsActor1 = TestProbe()
      val wsActor2 = TestProbe()

      ctiRouter.nbOfClients.getCount() should be(0)

      ref ! WsConnected(wsActor1.ref, updatedUser)
      ctiRouter.nbOfClients.getCount() should be(1)

      ref ! WsConnected(wsActor2.ref, updatedUser)
      ctiRouter.nbOfClients.getCount() should be(2)

      ref ! Leave(wsActor1.ref)
      ctiRouter.nbOfClients.getCount() should be(1)

      ref ! Leave(wsActor1.ref)
      ctiRouter.nbOfClients.getCount() should be(
        1
      ) // Same actor leaving, no decrement

      ref ! Leave(wsActor2.ref)
      ctiRouter.nbOfClients.getCount() should be(0)
    }

    "send DialFromQueue to queueDispatcher" in new Helper {
      val (ref, ctiRouter) = actor(getXucUser("tata", "sdf78d", Some("2006")))
      val message = DialFromQueue(
        destination = "123456789",
        queueId = 123,
        callerIdName = "Jim",
        callerIdNumber = "223344",
        variables = Map("foo" -> "bar"),
        "default"
      )
      ctiRouter.receive(BaseRequest(self, message))
      queueDispatcher.expectMsg(message)
    }

    "forward UserCallHistory with size to the CtiFilter" in new Helper {

      val userName         = "myName"
      val (ref, ctiRouter) = actor(getXucUser(userName, "pwd", Some("0000")))
      val historyRequest   = GetUserCallHistory(HistorySize(5))
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      testCtiFilter.expectMsg(BaseRequest(testProbe.ref, historyRequest))

    }

    "forward UserCallHistory with days to the CtiFilter" in new Helper {

      val userName         = "myName"
      val (ref, ctiRouter) = actor(getXucUser(userName, "pwd", Some("0000")))
      val historyRequest   = GetUserCallHistoryByDays(HistoryDays(5))
      val testProbe        = TestProbe()

      ref ! BaseRequest(testProbe.ref, historyRequest)

      testCtiFilter.expectMsg(BaseRequest(testProbe.ref, historyRequest))

    }

    "send conference command error to bus" in new Helper {

      val user             = getXucUser("moilp", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)
      val errors = Map(
        NotAMember          -> "NotAMember",
        NotOrganizer        -> "NotOrganizer",
        CannotKickOrganizer -> "CannotKickOrganizer"
      )
      errors.foreach { case (error, errorString) =>
        ref ! error
        val event =
          WebSocketEvent.createEvent(WsConferenceCommandError(error))
        verify(wsBus).publish(
          WsMessageEvent(WsBus.browserTopic(user.username), WsContent(event))
        )
      }
    }

    "forward toggle phone device change request to configDispatcher" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("cfgRequest", "sdf78d", Some("2006")))

      val requestConfig = RequestConfigForUsername(
        ctiRouter.ctiFilter,
        ToggleUniqueAccountDevice(None, TypePhoneDevice),
        Some("cfgRequest")
      )

      ref ! BaseRequest(self, ToggleUniqueAccountDevice(None, TypePhoneDevice))

      configDispatcher.expectMsg(requestConfig)
    }

    "forward toggle webrtc device change request to configDispatcher" in new Helper {
      val (ref, ctiRouter) =
        actor(getXucUser("cfgRequest", "sdf78d", Some("2006")))

      val requestConfig = RequestConfigForUsername(
        ctiRouter.ctiFilter,
        ToggleUniqueAccountDevice(None, TypeDefaultDevice),
        Some("cfgRequest")
      )

      ref ! BaseRequest(
        self,
        ToggleUniqueAccountDevice(None, TypeDefaultDevice)
      )

      configDispatcher.expectMsg(requestConfig)
    }

    "forward UserPreferenceRequest to the userPreference service" in new Helper {
      val userName = "myName"
      val (ref, _) = actor(getXucUser(userName, "pwd", Some("0000")))
      val userPreferenceRequest = SetUserPreferenceRequest(
        None,
        UserPreferenceKey.PreferredDevice,
        "DevicePhoneType",
        UserPreferenceType.StringType
      )
      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, userPreferenceRequest)

      userPreferenceService.expectMsg(
        userPreferenceRequest.copy(userId = Some(1))
      )

    }

    "forward UserPreferenceDisconnect when last wsActor disconnect" in new Helper {
      val userName              = "myName"
      val user                  = getXucUser(userName, "pwd", Some("0000"))
      val (ref, _)              = actor(user)
      val userPreferenceRequest = UserPreferenceDisconnect(Some(1))
      val testProbe             = TestProbe()

      val wsActor1 = testProbe
      val wsActor2 = testProbe

      ref ! WsConnected(wsActor1.ref, user)
      ref ! WsConnected(wsActor2.ref, user)
      ref ! Leave(wsActor1.ref)
      ref ! Leave(wsActor2.ref)

      userPreferenceService.expectMsg(
        UserPreferenceDisconnect(Some(1))
      )

    }

    "forward UnregisterMobileApp to the userPreference service" in new Helper {
      val userName            = "myName"
      val (ref, _)            = actor(getXucUser(userName, "pwd", Some("0000")))
      val unregisterMobileApp = UnregisterMobileApp(None)

      val testProbe = TestProbe()

      ref ! BaseRequest(testProbe.ref, unregisterMobileApp)

      userPreferenceService.expectMsg(
        unregisterMobileApp.copy(username = Some(userName))
      )
    }
  }

  "On Video request / event" should {
    "forward video start event with requester username to the video event manager" in new Helper {
      val user             = getXucUser("ahonnet", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)

      val videoEvent = VideoEvent("videoStart")
      val testProbe  = TestProbe()

      ref ! BaseRequest(testProbe.ref, videoEvent)

      videoEventManager.expectMsg(
        UserBaseRequest(testProbe.ref, videoEvent, user)
      )
    }

    "inform connected websocket when receiving a new video status event" in new Helper {
      val user             = getXucUser("lami", "4etr,wqd", Some("2004"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))
      val wsActor = TestProbe()

      ref ! WsConnected(wsActor.ref, user)

      val vse = VideoStatusEvent("jmgo", VideoEvents.Busy)

      ref ! vse

      wsActor.expectMsg(vse)
    }

    "forward video invitation enriched with source user" in new Helper {
      val user     = getXucUser("cpunk", "pwd", Some("2077"))
      val (ref, _) = actor(user)

      val videoInvite = InviteToMeetingRoom(1, "some.to.ken", "bwillis")
      val testProbe   = TestProbe()

      ref ! BaseRequest(testProbe.ref, videoInvite)

      videoEventManager.expectMsg(
        UserBaseRequest(testProbe.ref, videoInvite, user)
      )
    }

    "forward video invitation events to browser" in new Helper {
      val user             = getXucUser("cpunk", "pwd", Some("2077"))
      val (ref, ctiRouter) = actor(user)
      ctiRouter.loggedOnMsg = Some(LoggedOn(user, "34"))

      val videoInvitationAck =
        MeetingRoomInviteAckReply(
          42,
          "bwillis",
          "Bruce Willis",
          VideoInviteAck.ACK
        )
      val wsActor = TestProbe()

      ref ! WsConnected(wsActor.ref, user)

      ref ! videoInvitationAck

      wsActor.expectMsg(
        videoInvitationAck
      )
    }
  }

  "On flash text" should {
    "Forward direct message to flash text service and add displayName" in new Helper {
      val user             = getXucUser("directFlashText", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)

      val message = "Hello"
      val seq     = 456
      val to      = "Bob"

      val request = BrowserSendDirectMessage(to, message, seq)
      ref ! BaseRequest(TestProbe().ref, BrowserRequestEnvelope(request))

      flashTextService.expectMsg(
        SendDirectMessage("directFlashText", to, message, seq)
      )
    }

    "Publish response to the bus" in new Helper {
      val user             = getXucUser("FlashResponse", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)

      val resp = Message(
        ChatUser(
          user.username,
          Some("1456"),
          user.xivoUser.username,
          None
        ),
        ChatUser("Alice", Some("1000"), Some("Alice Sample"), None),
        "This is my answer",
        OffsetDateTime.now(),
        18
      )

      ref ! resp

      verify(wsBus).publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(WebSocketEvent.createEvent(BrowserEventEnvelope(resp)))
        )
      )
    }

    "Publish message history to the bus" in new Helper {
      val user             = getXucUser("FlashResponse", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)

      val ft1 =
        ChatUser(user.username, Some("1456"), user.xivoUser.username, None)
      val ft2 = ChatUser("Alice", Some("1000"), Some("Alice Sample"), None)

      val resp = MessageHistory(
        (ft1, ft2),
        List(
          Message(
            ChatUser(
              user.username,
              Some("1456"),
              user.xivoUser.username,
              None
            ),
            ChatUser("Alice", Some("1000"), Some("Alice Sample"), None),
            "This is my answer",
            OffsetDateTime.now(),
            18
          ),
          Message(
            ChatUser(
              user.username,
              Some("1456"),
              user.xivoUser.username,
              None
            ),
            ChatUser("Alice", Some("1000"), Some("Alice Sample"), None),
            "This is my answer",
            OffsetDateTime.now(),
            18
          )
        ),
        0
      )

      ref ! resp

      verify(wsBus).publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(WebSocketEvent.createEvent(BrowserEventEnvelope(resp)))
        )
      )
    }

    "Publish unread message notification to the bus" in new Helper {
      val user             = getXucUser("FlashResponse", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)

      val resp = MessageUnreadNotification(
        List(
          Message(
            ChatUser(
              user.username,
              Some("1456"),
              user.xivoUser.username,
              None
            ),
            ChatUser("Alice", Some("1000"), Some("Alice Sample"), None),
            "This is my answer",
            OffsetDateTime.now(),
            18
          ),
          Message(
            ChatUser(
              user.username,
              Some("1456"),
              user.xivoUser.username,
              None
            ),
            ChatUser("Alice", Some("1000"), Some("Alice Sample"), None),
            "This is my answer",
            OffsetDateTime.now(),
            18
          )
        ),
        0
      )

      ref ! resp

      verify(wsBus).publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(WebSocketEvent.createEvent(BrowserEventEnvelope(resp)))
        )
      )
    }

    "Retrieve a user display name from username" in new Helper {
      val user      = getXucUser("jbond", "98dd", Some("1234"))
      val (ref, _)  = actor(user)
      val requester = TestProbe()

      val requestConfig = DisplayNameLookup("jbond")

      ref ! BaseRequest(requester.ref, requestConfig)

      configDispatcher.expectMsg(RequestConfig(requester.ref, requestConfig))
    }

    "unsubscribe from all video status on request" in new Helper {
      val user             = getXucUser("ahonnet", "pwd", Some("2104"))
      val (ref, ctiRouter) = actor(user)
      val requester        = TestProbe()

      ref ! BaseRequest(requester.ref, UnsubscribeFromAllVideoStatus)

      configDispatcher.expectMsg(MonitorVideoStatus(requester.ref, List()))
    }
  }

  "On voicemail" should {

    "Publish VoiceMailStatus to the bus" in new Helper {
      val user             = getXucUser("voicemailUser", "98dd", Some("1456"))
      val (ref, ctiRouter) = actor(user)

      val resp = VoiceMailStatusUpdate(4, 1)

      ref ! resp

      verify(wsBus).publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(WebSocketEvent.createEvent(resp))
        )
      )
    }
  }
}
