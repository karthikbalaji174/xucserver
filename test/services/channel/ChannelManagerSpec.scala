package services.channel

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.asteriskjava.live.AsteriskChannel
import org.asteriskjava.manager.action.{
  GetVarAction,
  PauseMonitorAction,
  SetVarAction,
  UnpauseMonitorAction
}
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.{GetVarResponse, ManagerResponse}
import org.mockito.Matchers.any
import org.mockito.Mockito.{times, verify, when}
import org.mockito.{ArgumentCaptor, ArgumentMatcher, Matchers}
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus._
import services.calltracking.SipDriver
import services.channel.ChannelManager.Channels
import services.channel.ChannelRequestProc.ChannelActionRequest
import services.{AmiEventHelper, XucAmiBus}
import xivo.xuc.XucConfig
import xivo.xucami.models._
import xuctest.ChannelGen

import java.beans.PropertyChangeEvent
import java.util
import scala.collection.immutable.HashMap

class ChannelManagerSpec
    extends TestKitSpec("ChannelManagerSpec")
    with MockitoSugar
    with ChannelGen
    with AmiEventHelper
    with ChannelHelper {

  class Helper() {
    val agentCalled = new AgentCalledEvent("")
    agentCalled.setAgentCalled("Local/id-6@agentcallback")
    agentCalled.setMemberName("Agent/2200")
    agentCalled.setUniqueId("1416910889.110")
    agentCalled.setQueue("irs")
    agentCalled.setCallerIdName("hawkeye")
    agentCalled.setCallerIdNum("1002")

    val amiBus      = mock[XucAmiBus]
    val channelRepo = mock[ChannelRepository]
    val config      = mock[XucConfig]

    val getVarResponse = new GetVarResponse()
    val attr           = new util.HashMap[String, Object]()
    val getVarAction   = new GetVarAction()

    trait TestEventProcessor extends EventProcessor {
      override val processor = mock[AmiEventProcessor]
    }

    trait TestChannelRequestProcessor extends ChannelRequestProcessor {
      override val channelProcessor = mock[ChannelRequestProc]
    }

    def actor(channelRepo: ChannelRepository = channelRepo) = {
      val sa = TestActorRef(
        new ChannelManager(amiBus, channelRepo = channelRepo, config)
          with TestEventProcessor
          with TestChannelRequestProcessor
      )
      (sa, sa.underlyingActor)
    }
  }

  "ChannelManager" should {

    "subscribe to the ami bus for AmiEvents, AmiResponses and ChannelActionRequests" in new Helper() {
      val (ref, a) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiResponse)
      verify(amiBus).subscribe(ref, AmiType.ChannelActionRequest)
    }

    """in new SIP channel event:
      - add a channel in repo
      - publish added channel from this repo
      - request SIP CallId variable""".stripMargin in new Helper() {
      val interface = "SIP/uwert"
      val peerChan =
        bchan(interface).copy(variables = Map("USR_DATA1" -> "val1"))
      val secondChan      = bchan(interface)
      val chans: Channels = HashMap(peerChan.id -> peerChan)
      val (ref, a)        = actor()

      val secondChanEvent = new NewChannelEvent("")
      secondChanEvent.setUniqueId(secondChan.id)
      secondChanEvent.setChannel(secondChan.name)
      secondChanEvent.setCallerIdName(secondChan.callerId.name)
      secondChanEvent.setCallerIdNum(secondChan.callerId.number)
      secondChanEvent.setChannelState(ChannelState.RINGING.id)

      when(channelRepo.addNewChannel(Channel(secondChanEvent))).thenReturn(
        ChannelRepository(
          HashMap(
            secondChan.id -> secondChan.copy(variables =
              Map("USR_DATA1" -> "val1")
            )
          )
        )
      )

      when(config.sipDriver).thenReturn(SipDriver.SIP)

      ref ! AmiEvent(secondChanEvent)

      verify(amiBus).publish(
        ChannelEvent(secondChan.copy(variables = Map("USR_DATA1" -> "val1")))
      )

      class GetVarActionValuesMatcher(
          channel: String,
          variable: String,
          chanId: String
      ) extends ArgumentMatcher[AmiAction] {
        def matches(o: Object): Boolean = {
          o match {
            case a: AmiAction =>
              a.message match {
                case gv: GetVarAction =>
                  (gv.getChannel == channel && gv.getVariable == variable && a.reference == Some(
                    chanId
                  ))
                case _ => false
              }
            case _ => false
          }
        }
      }
      verify(amiBus).publish(
        Matchers.argThat(
          new GetVarActionValuesMatcher(
            secondChan.name,
            "SIPCALLID",
            secondChan.id
          )
        )
      )
    }

    """in new PJSIP channel event:
      - add a channel in repo
      - publish added channel from this repo
      - request SIP CallId variable""".stripMargin in new Helper() {
      val interface = "PJSIP/uwert"
      val peerChan =
        bchan(interface).copy(variables = Map("USR_DATA1" -> "val1"))
      val secondChan      = bchan(interface)
      val chans: Channels = HashMap(peerChan.id -> peerChan)
      val (ref, a)        = actor()
      val mdsName         = "default"

      val secondChanEvent = new NewChannelEvent("")
      secondChanEvent.setUniqueId(secondChan.id)
      secondChanEvent.setChannel(secondChan.name)
      secondChanEvent.setCallerIdName(secondChan.callerId.name)
      secondChanEvent.setCallerIdNum(secondChan.callerId.number)
      secondChanEvent.setChannelState(ChannelState.RINGING.id)

      when(channelRepo.addNewChannel(Channel(secondChanEvent))).thenReturn(
        ChannelRepository(
          HashMap(
            secondChan.id -> secondChan.copy(variables =
              Map("USR_DATA1" -> "val1")
            )
          )
        )
      )

      when(config.sipDriver).thenReturn(SipDriver.PJSIP)

      ref ! AmiEvent(secondChanEvent, mdsName)

      verify(amiBus).publish(
        ChannelEvent(secondChan.copy(variables = Map("USR_DATA1" -> "val1")))
      )

      class GetVarActionValuesMatcher(
          channel: String,
          variable: String,
          chanId: String,
          targetMds: Option[String] = None
      ) extends ArgumentMatcher[AmiAction] {
        def matches(o: Object): Boolean = {
          o match {
            case a: AmiAction =>
              a.message match {
                case gv: GetVarAction =>
                  gv.getChannel == channel && gv.getVariable == variable && a.reference
                    .contains(chanId) && a.targetMds == targetMds
                case _ => false
              }
            case _ => false
          }
        }
      }
      verify(amiBus).publish(
        Matchers.argThat(
          new GetVarActionValuesMatcher(
            secondChan.name,
            "CHANNEL(pjsip,call-id)",
            secondChan.id,
            Some(mdsName)
          )
        )
      )
    }

    "process ami events" in new Helper {
      val (ref, a)      = actor()
      val newStateEvent = AmiEvent(new NewStateEvent(""))

      ref ! newStateEvent

      verify(a.processor).process(newStateEvent, channelRepo)
    }

    "process channel requests" in new Helper {
      val (ref, a) = actor()

      val channelRequest = ChannelRequest(mock[ChannelActionRequest])

      ref ! channelRequest

      verify(a.channelProcessor).processChannelReq(channelRequest, channelRepo)
    }

    "publish a channel event on a PropertyChangeEvent with monitored=true" in new Helper() {
      val (ref, a) = actor()
      val initialChannel = Channel(
        "646654546.56",
        "SIP/wert",
        CallerId("eddie", "2014"),
        "646654546.56",
        ChannelState.BUSY
      )
      val channel = mock[AsteriskChannel]
      when(channel.getName()).thenReturn("SIP/wert")
      when(channel.getId()).thenReturn("35700")
      val event = new PropertyChangeEvent(channel, "monitored", false, true)
      a.process(ChannelRepository(HashMap("35700" -> initialChannel)))(event)

      val expectedChannel = new Channel(
        "646654546.56",
        "SIP/wert",
        CallerId("eddie", "2014"),
        "646654546.56"
      ).withChannelState(ChannelState.BUSY)
      expectedChannel.monitored = MonitorState.ACTIVE
      verify(amiBus).publish(ChannelEvent(expectedChannel))
    }

    "publish a channel event on successful pauseMonitor" in new Helper() {
      val (ref, a) = actor()
      val response = new ManagerResponse()
      response.setResponse("Success")
      val request = new PauseMonitorAction()
      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("7789")))))
      val channel =
        new Channel("7789", "SIP/uwert", CallerId("eddie", "2014"), "7789")
      channel.monitored = MonitorState.ACTIVE
      a.process(ChannelRepository(HashMap("7789" -> channel)))(richResponse)

      channel.monitored = MonitorState.PAUSED
      verify(amiBus).publish(ChannelEvent(channel))
    }

    "publish a channel event on successful unpauseMonitor" in new Helper() {
      val (ref, a) = actor()
      val response = new ManagerResponse()
      response.setResponse("Success")
      val request = new UnpauseMonitorAction()
      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")
      channel.monitored = MonitorState.PAUSED
      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      channel.monitored = MonitorState.ACTIVE
      verify(amiBus).publish(ChannelEvent(channel))
    }

    "publish a channel event with monitor state paused on variable MONITOR_PAUSED" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")

      attr.put("variable", "MONITOR_PAUSED")
      attr.put("value", "true")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      channel.monitored.shouldEqual(MonitorState.PAUSED)
      verify(amiBus).publish(ChannelEvent(channel))

    }

    "publish a channel event with SIP CallId variable on variable SIPCALLID" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")

      attr.put("variable", "SIPCALLID")
      attr.put("value", "454-654-654")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel.addVariables(Map("SIPCALLID" -> "454-654-654"))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on variable DIALSTATUS and UP" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("ANSWER")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.UP
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> "ANSWER"))
        .copy(direction = Some(ChannelDirection.OUTGOING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on variable DIALSTATUS and RINGING" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("ANSWER")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.RINGING
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> "ANSWER"))
        .copy(direction = Some(ChannelDirection.INCOMING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on variable DIALSTATUS and ORIGINATING" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("ANSWER")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.ORIGINATING
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> "ANSWER"))
        .copy(direction = Some(ChannelDirection.OUTGOING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on variable DIALSTATUS and DIALING" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("ANSWER")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.DIALING
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> "ANSWER"))
        .copy(direction = Some(ChannelDirection.OUTGOING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on empty DIALSTATUS and UP" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.UP
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> ""))
        .copy(direction = Some(ChannelDirection.INCOMING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on empty DIALSTATUS and ORIGINATING" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.ORIGINATING
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> ""))
        .copy(direction = Some(ChannelDirection.OUTGOING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "publish and update a channel event with dial status variable and direction on empty DIALSTATUS and RINGING" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Success")

      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.RINGING
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val expected = channel
        .addVariables(Map("DIALSTATUS" -> ""))
        .copy(direction = Some(ChannelDirection.INCOMING))
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "update channel repository on MONITOR_PAUSED true" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")

      attr.put("variable", "MONITOR_PAUSED")
      attr.put("value", "true")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")

      val expected: Channel = channel.copy(monitored = MonitorState.PAUSED)

      val oldChannels: Channels = HashMap(channel.id -> channel)

      a.process(ChannelRepository(oldChannels))(richResponse)

      verify(amiBus).publish(ChannelEvent(expected))
    }

    "not update channel repository on MONITOR_PAUSED false" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")

      attr.put("variable", "MONITOR_PAUSED")
      attr.put("value", "false")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")

      val expected: Channel = channel

      val oldChannels: Channels = HashMap(channel.id -> channel)
      a.process(ChannelRepository(oldChannels))(richResponse)

      verify(amiBus, times(0)).publish(ChannelEvent(expected))
    }

    "update channel repository on SIPCALLID" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")

      attr.put("variable", "SIPCALLID")
      attr.put("value", "454-654-654")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "SIP/uwert", CallerId("eddie", "2014"), "8899")

      val expected: Channel =
        channel.addVariables(Map("SIPCALLID" -> "454-654-654"))
      val newChannels: HashMap[String, Channel] =
        HashMap(channel.id -> expected)

      val oldChannels: Channels = HashMap(channel.id -> channel)

      a.process(ChannelRepository(oldChannels))(richResponse)
      verify(amiBus).publish(ChannelEvent(expected))
    }

    "update channel repository on PJSIP call-id response" in new Helper {
      val (ref, a) = actor()
      val response = new GetVarResponse()
      response.setResponse("Success")

      attr.put("variable", "CHANNEL(pjsip,call-id)")
      attr.put("value", "454-654-654")
      response.setAttributes(attr)
      val request = new GetVarAction()

      val richResponse =
        AmiResponse((response, Some(AmiAction(request, Some("8899")))))
      val channel =
        new Channel("8899", "PJSIP/uwert", CallerId("eddie", "2014"), "8899")

      val expected: Channel =
        channel.addVariables(Map("SIPCALLID" -> "454-654-654"))

      val oldChannels: Channels = HashMap(channel.id -> channel)

      a.process(ChannelRepository(oldChannels))(richResponse)

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus, times(2)).publish(arg.capture)

      arg.getAllValues
        .get(0)
        .message
        .asInstanceOf[SetVarAction]
        .getVariable shouldBe "XIVO_SIPCALLID"
      arg.getAllValues
        .get(0)
        .message
        .asInstanceOf[SetVarAction]
        .getChannel shouldBe "PJSIP/uwert"
      arg.getAllValues
        .get(0)
        .message
        .asInstanceOf[SetVarAction]
        .getValue shouldBe "454-654-654"

      arg.getAllValues.get(1).asInstanceOf[ChannelEvent] shouldBe ChannelEvent(
        expected
      )
    }

    "update channel repository on DIALSTATUS" in new Helper {
      val (ref, a) = actor()

      getVarResponse.setResponse("Success")
      attr.put("variable", "DIALSTATUS")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("ANSWER")

      val richResponse = AmiResponse(
        (getVarResponse, Some(AmiAction(getVarAction, Some("8899"))))
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.UP
      )

      val expected: Channel = channel
        .addVariables(Map("DIALSTATUS" -> "ANSWER"))
        .copy(direction = Some(ChannelDirection.OUTGOING))
      val newChannels: HashMap[String, Channel] =
        HashMap(channel.id -> expected)

      val oldChannels: Channels = HashMap(channel.id -> channel)
      a.process(ChannelRepository(oldChannels))(richResponse)

      verify(amiBus).publish(ChannelEvent(expected))
    }

    "republish AmiRequest if get PJSIP callId failed and channel exists" in new Helper {
      val (ref, a) = actor()
      getVarResponse.setResponse("Error")

      attr.put("variable", "CHANNEL(pjsip,call-id")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("")
      getVarResponse.setUniqueId("8899")

      getVarAction.setVariable("CHANNEL(pjsip,call-id)")
      getVarAction.setChannel("SIP/uwert")
      getVarAction.setActionId("8899")

      val richResponse = AmiResponse(
        (
          getVarResponse,
          Some(AmiAction(getVarAction, Some("8899"), None, Some("defaultMDS")))
        )
      )
      val channel = new Channel(
        "8899",
        "SIP/uwert",
        CallerId("eddie", "2014"),
        "8899",
        state = ChannelState.RINGING
      )

      a.process(ChannelRepository(HashMap("8899" -> channel)))(richResponse)

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(amiBus).publish(arg.capture)
      val action: AmiAction = arg.getValue
      action.message match {
        case gv: GetVarAction =>
          gv.getVariable shouldBe "CHANNEL(pjsip,call-id)"
        case _ =>
          fail("Wrong message type")
      }
      action.targetMds shouldBe Some("defaultMDS")

    }

    "DO NOT republish get PJSIP call id of the channel does not exist" in new Helper() {
      val (ref, a) = actor()
      getVarResponse.setResponse("Error")

      attr.put("variable", "CHANNEL(pjsip,call-id")
      getVarResponse.setAttributes(attr)
      getVarResponse.setValue("")
      getVarResponse.setUniqueId("8899")

      getVarAction.setVariable("CHANNEL(pjsip,call-id)")
      getVarAction.setChannel("SIP/uwert")
      getVarAction.setActionId("8899")

      val richResponse = AmiResponse(
        (
          getVarResponse,
          Some(AmiAction(getVarAction, Some("8899"), None, Some("defaultMDS")))
        )
      )

      a.process(ChannelRepository(HashMap()))(richResponse)

      verify(amiBus, times(0)).publish(any[AmiAction]())

    }

    "ignore failed Responses" in new Helper() {
      val (ref, a) = actor()
      val response = new ManagerResponse()
      response.setResponse("Error")
      val request = new PauseMonitorAction()
      request.setChannel("7789")
      val richResponse = AmiResponse((response, Some(AmiAction(request))))
      val channel =
        new Channel("7789", "SIP/uwert", CallerId("eddie", "2014"), "7789")
      channel.monitored = MonitorState.ACTIVE

      val oldChannels: Channels = HashMap(channel.id -> channel)
      a.process(ChannelRepository(oldChannels))(richResponse)

      verify(amiBus, times(0)).publish(any[ChannelEvent]())
    }

  }

}
