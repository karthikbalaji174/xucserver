package services

import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.mockito.Mockito.{stub, verify}
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigRepository
import services.video.model.{VideoEvents, VideoStatusEvent}
import xivo.events.{PhoneEvent, PhoneEventType}
import xivo.websocket.WebSocketEvent

class EventPublisherSpec
    extends TestKitSpec("EventPublisher")
    with MockitoSugar {

  class Helper {
    val restPublisher = mock[RestPublisher]
    val eventBus      = mock[XucEventBus]
    val userRepo      = mock[ConfigRepository]
    def actor() = {
      val sa = TestActorRef(
        new EventPublisher(restPublisher, eventBus, userRepo)
      )
      (sa, sa.underlyingActor)
    }
  }

  "An event publisher" should {
    "subscribe to event bus phone events" in new Helper {
      val (ref, a) = actor()
      verify(eventBus).subscribe(ref, XucEventBus.allPhoneEventsTopic)
    }
    "publish phone event for a user" in new Helper {
      val (ref, a) = actor()
      stub(userRepo.userNameFromPhoneNb("1001")).toReturn(Some("jlang"))

      val phoneEvent = PhoneEvent(
        PhoneEventType.EventRinging,
        "1001",
        "0675432130",
        "Billy The Kid",
        "1003456.7",
        "1003456.1"
      )

      ref ! phoneEvent

      verify(restPublisher).publish(
        "jlang",
        WebSocketEvent.createEvent(phoneEvent)
      )
    }
    "subscribe to event bus video events" in new Helper {
      val (ref, a) = actor()
      verify(eventBus).subscribe(ref, XucEventBus.allVideoStatusTopic)
    }
    "publish video event for a user" in new Helper {
      val (ref, a) = actor()

      val videoStatusEvent = VideoStatusEvent("ahonnet", VideoEvents.Busy)

      ref ! videoStatusEvent

      verify(restPublisher).publish(
        "ahonnet",
        WebSocketEvent.createEvent(videoStatusEvent)
      )
    }
  }
}
