package services.video

import akkatest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import akka.testkit.TestProbe
import akka.testkit.TestActorRef
import services.request.UserBaseRequest
import models.XucUser
import models.XivoUser
import services.video.model.{
  InviteToMeetingRoom,
  MeetingRoomInvite,
  MeetingRoomInviteAccept,
  MeetingRoomInviteAck,
  MeetingRoomInviteAckReply,
  MeetingRoomInviteReject,
  MeetingRoomInviteResponse,
  UserVideoEvent,
  VideoEvent,
  VideoInviteAck,
  VideoInviteResponse
}

trait XucUserHelper {
  def getXucUser(
      username: String,
      pwd: String,
      phone: Option[String] = None,
      mobilePhone: Option[String] = None
  ): XucUser = {
    XucUser(
      username,
      XivoUser(
        1,
        None,
        None,
        "John",
        Some("Doe"),
        Some(username),
        Some(pwd),
        mobilePhone,
        None
      ),
      phone
    )
  }
}

class VideoEventManagerSpec
    extends TestKitSpec("VideoEventManagerSpec")
    with MockitoSugar
    with XucUserHelper {

  class Helper {
    val configDispatcher = TestProbe()
    val chatService      = TestProbe()

    def actor() = {
      val a = TestActorRef(
        new VideoEventManager(configDispatcher.ref, chatService.ref)
      )
      (a, a.underlyingActor)
    }
  }

  "A VideoEventManager actor" should {
    "receive start video request and convert them into events" in new Helper {
      val user      = getXucUser("ahonnet", "pwd", Some("1100"))
      val (self, _) = actor()
      self ! UserBaseRequest(self, VideoEvent("busy"), user)
      configDispatcher.expectMsg(UserVideoEvent("ahonnet", "busy"))
      self ! UserBaseRequest(self, VideoEvent("available"), user)
      configDispatcher.expectMsg(UserVideoEvent("ahonnet", "available"))
    }

    "receive invite video request and forward it to chat service " in new Helper {
      val userSource = getXucUser("jduff", "pwd", Some("1100"))
      val (self, _)  = actor()

      val videoInvite = InviteToMeetingRoom(1, "some.to.ken", "bwillis")

      self ! UserBaseRequest(self, videoInvite, userSource)
      chatService.expectMsg(
        MeetingRoomInvite(
          videoInvite.requestId,
          videoInvite.token,
          videoInvite.username,
          userSource.username,
          userSource.xivoUser.fullName
        )
      )
    }

    "receive invite ack, enrich it and forward it to chat service" in new Helper {
      val userSource = getXucUser("jduff", "pwd", Some("1100"))
      val (self, _)  = actor()

      val videoAck = MeetingRoomInviteAck(1, "bwillis")

      self ! UserBaseRequest(self, videoAck, userSource)
      chatService.expectMsg(
        MeetingRoomInviteAckReply(
          videoAck.requestId,
          videoAck.username,
          userSource.xivoUser.fullName,
          VideoInviteAck.ACK
        )
      )
    }

    "receive invite accept, enrich it and forward it to chat service" in new Helper {
      val userSource = getXucUser("jduff", "pwd", Some("1100"))
      val (self, _)  = actor()

      val videoResponseAccept = MeetingRoomInviteAccept(1, "bwillis")

      self ! UserBaseRequest(self, videoResponseAccept, userSource)
      chatService.expectMsg(
        MeetingRoomInviteResponse(
          videoResponseAccept.requestId,
          videoResponseAccept.username,
          userSource.xivoUser.fullName,
          VideoInviteResponse.ACCEPT
        )
      )
    }

    "receive invite reject, enrich it and forward it to chat service" in new Helper {
      val userSource = getXucUser("jduff", "pwd", Some("1100"))
      val (self, _)  = actor()

      val videoResponseReject = MeetingRoomInviteReject(1, "bwillis")

      self ! UserBaseRequest(self, videoResponseReject, userSource)
      chatService.expectMsg(
        MeetingRoomInviteResponse(
          videoResponseReject.requestId,
          videoResponseReject.username,
          userSource.xivoUser.fullName,
          VideoInviteResponse.REJECT
        )
      )
    }
  }

}
