package services.callhistory

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models.{DynamicFilter, OperatorEq}
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.mockito.Mockito.stub
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.{JsArray, Json}
import services.config.ConfigRepository
import services.request._
import xivo.models._
import xivo.network._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CallHistoryManagerSpec
    extends TestKitSpec("CallHistoryManagerSpec")
    with MockitoSugar
    with BeforeAndAfterEach {

  val format                   = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  var requester: TestProbe     = null
  var requesterRef: ActorRef   = null
  var recordingWs: RecordingWS = null
  var historyMgr: ActorRef     = null
  var repo: ConfigRepository   = null

  override def beforeEach(): Unit = {
    requester = TestProbe()
    requesterRef = requester.ref
    recordingWs = mock[RecordingWS]
    repo = mock[ConfigRepository]
    historyMgr = TestActorRef[CallHistoryManager](
      Props(new CallHistoryManager(recordingWs, repo))
    )
  }

  "A CallHistoryManager" should {
    "send agent call history to the requester" in {
      val agent = Agent(12, "John", "Doe", "1002", "default")
      val rq    = BaseRequest(requesterRef, AgentCallHistoryRequest(10, "toto"))
      stub(repo.agentFromUsername("toto")).toReturn(Some(agent))
      val json = JsArray(
        List(
          Json.obj(
            "start"    -> "2014-01-01 08:00:00",
            "duration" -> "00:14:23",
            "agent"    -> "1002",
            "queue"    -> "test_queue",
            "src_num"  -> "3354789",
            "dst_num"  -> "44485864"
          )
        )
      )
      stub(recordingWs.getAgentCallHistory(HistorySize(10), "1002"))
        .toReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(
        CallHistory(
          List(
            CallDetail(
              format.parseDateTime("2014-01-01 08:00:00"),
              Some(new Period(0, 14, 23, 0)),
              "3354789",
              Some("44485864"),
              CallStatus.Answered
            )
          )
        )
      )
    }

    "send user call history to the requester" in {
      val param     = HistorySize(10)
      val rq        = BaseRequest(requesterRef, UserCallHistoryRequest(param, "toto"))
      val interface = "SIP/agbef"
      stub(repo.interfaceFromUsername("toto")).toReturn(Some(interface))
      val json = JsArray(
        List(
          Json.obj(
            "start"    -> "2014-01-01 08:00:00",
            "duration" -> "00:14:23",
            "status"   -> "emitted",
            "src_num"  -> "3354789",
            "dst_num"  -> "44485864"
          )
        )
      )
      stub(recordingWs.getUserCallHistory(param, interface))
        .toReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(
        CallHistory(
          List(
            CallDetail(
              format.parseDateTime("2014-01-01 08:00:00"),
              Some(new Period(0, 14, 23, 0)),
              "3354789",
              Some("44485864"),
              CallStatus.Emitted
            )
          )
        )
      )
    }

    "send customer call history to the requester" in {
      val criteria = FindCustomerCallHistoryRequest(
        List(DynamicFilter("src_num", Some(OperatorEq), Some("1234")))
      )
      val rq = BaseRequest(
        requesterRef,
        CustomerCallHistoryRequestWithId(22, criteria)
      )

      var response = FindCustomerCallHistoryResponse(
        1,
        List(
          CustomerCallDetail(
            format.parseDateTime("2014-01-01 08:00:00"),
            Some(new Period(0, 14, 23, 0)),
            Some(new Period(0, 10, 0, 0)),
            Some("James Bond"),
            Some("1000"),
            Some("Services"),
            Some("3000"),
            CallStatus.Answered
          )
        )
      )

      stub(recordingWs.findCustomerCallHistory(criteria))
        .toReturn(Future(response))

      historyMgr ! rq

      requester.expectMsg(CustomerCallHistoryResponseWithId(22, response))
    }

    "send queue call history to the requester" in {
      val queue = "bl"
      val size  = 8
      val rq    = BaseRequest(requesterRef, GetQueueCallHistory(queue, size))

      val json = JsArray(
        List(
          Json.obj(
            "start"    -> "2014-01-01 08:00:00",
            "duration" -> "00:14:23",
            "agent"    -> "1002",
            "queue"    -> "test_queue",
            "src_num"  -> "3354789",
            "dst_num"  -> "44485864"
          )
        )
      )
      stub(recordingWs.getQueueCallHistory(queue, size))
        .toReturn(Future(HistoryServerResponse(json)))

      historyMgr ! rq

      requester.expectMsg(
        CallHistory(
          List(
            CallDetail(
              format.parseDateTime("2014-01-01 08:00:00"),
              Some(new Period(0, 14, 23, 0)),
              "3354789",
              Some("44485864"),
              CallStatus.Answered
            )
          )
        )
      )
    }
  }
}
