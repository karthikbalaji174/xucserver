package services.request

import play.api.libs.json.{JsError, JsSuccess, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class DialToQueueSpec extends AnyWordSpec with Matchers {
  "A DialToQueue" should {
    "be created from json" in {
      val json = Json.parse(
        """|{
           |  "destination": "0123456789",
           |  "queueName": "std_notaire",
           |  "callerIdNumber": "0472727272",
           |  "variables": {
           |    "varname1": "varval1",
           |    "varname2": "varval2"
           |  }
           |}""".stripMargin
      )
      DialToQueue.validate(json) match {
        case JsSuccess(o, _) =>
          o shouldEqual DialToQueue(
            "0123456789",
            "std_notaire",
            "0472727272",
            Map("varname1" -> "varval1", "varname2" -> "varval2")
          )
        case JsError(e) => fail()
      }
    }
  }
}
