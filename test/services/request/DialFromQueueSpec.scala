package services.request

import play.api.libs.json.{JsError, JsSuccess, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class DialFromQueueSpec extends AnyWordSpec with Matchers {
  "A DialFromQueue" should {
    "be created from json" in {
      val json = Json.parse(
        """{"destination": "123456789", "queueId": 123, "callerIdName": "Thomas", "callerIdNumber": "998877", "variables": {"foo": "bar"}}"""
      )
      DialFromQueue.validate(json) match {
        case JsSuccess(o, _) =>
          o shouldEqual DialFromQueue(
            "123456789",
            123,
            "Thomas",
            "998877",
            Map("foo" -> "bar"),
            "default"
          )
        case JsError(e) => fail()
      }
    }
    "be created from minimal json" in {
      val json = Json.parse("""{"destination": "123456789", "queueId": 123}""")
      DialFromQueue.validate(json) match {
        case JsSuccess(o, _) =>
          o shouldEqual DialFromQueue("123456789", 123, "", "", Map(), "default")
        case JsError(e) => fail()
      }
    }
    "not be created from json where callerIdName contains quotation mark" in {
      val json = Json.parse(
        """{"destination": "123456789", "queueId": 123, "callerIdName": "Thom\"as", "callerIdNumber": "998877", "variables": {"foo": "bar"}}"""
      )
      DialFromQueue.validate(json) match {
        case e: JsError if e.errors.exists(_._1.toString == "/callerIdName") =>
        case _                                                               => fail()
      }
    }
    "not be created from json where callerIdNumber contains character <" in {
      val json = Json.parse(
        """{"destination": "123456789", "queueId": 123, "callerIdName": "Thomas", "callerIdNumber": "998<877", "variables": {"foo": "bar"}}"""
      )
      DialFromQueue.validate(json) match {
        case e: JsError
            if e.errors.exists(_._1.toString == "/callerIdNumber") =>
        case _ => fail()
      }
    }
    "not be created from json where callerIdNumber contains character >" in {
      val json = Json.parse(
        """{"destination": "123456789", "queueId": 123, "callerIdName": "Thomas", "callerIdNumber": "998877>", "variables": {"foo": "bar"}}"""
      )
      DialFromQueue.validate(json) match {
        case e: JsError
            if e.errors.exists(_._1.toString == "/callerIdNumber") =>
        case _ => fail()
      }
    }
  }
}
