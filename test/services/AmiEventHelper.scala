package services

import org.asteriskjava.manager.event.ManagerEvent

trait AmiEventHelper {
  def AmiEvent(m: ManagerEvent)            = XucAmiBus.AmiEvent(m, "default")
  def AmiEvent(m: ManagerEvent, s: String) = XucAmiBus.AmiEvent(m, s)
}
