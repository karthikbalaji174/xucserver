package services.directory

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models._
import org.scalatestplus.mockito.MockitoSugar
import DirectoryTransformer.{EnrichDirectoryResult, RawDirectoryResult}
import xivo.services.XivoDirectory.{Action, DirLookupResult, FavoriteUpdated}
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent
import org.mockito.Mockito.when

class DirectoryTransformerSpec
    extends TestKitSpec("DirectoryTransformerSpec")
    with MockitoSugar {

  class Helper {
    val testCtiLink            = TestProbe()
    val testCtiFilter          = TestProbe()
    val configDispatcher       = TestProbe()
    val agentConfig            = TestProbe()
    val amiBusConnector        = TestProbe()
    val callHistoryManager     = TestProbe()
    val xivoDirectoryInterface = TestProbe()
    val callbackMgrInterface   = TestProbe()

    val logic = mock[DirectoryTransformerLogic]
    def actor() = {
      val a = TestActorRef(
        new DirectoryTransformer(logic, configDispatcher.ref)
      )
      (a, a.underlyingActor)
    }
  }

  "DirectoryTransformer" should {
    """transform xivoDirectory search result to RichDirectoryResult with presence information
      |and send it to the asking actor""".stripMargin in new Helper() {
      val (ref, _)     = actor()
      val requester    = TestProbe()
      val searchResult = mock[DirSearchResult]
      val richResult   = new RichDirectoryResult(List("testHeader"))
      when(logic.enrichDirResult(searchResult, 42)).thenReturn(richResult)

      ref ! RawDirectoryResult(
        requester.ref,
        DirLookupResult(searchResult),
        42
      )

      requester.expectMsg(WsContent(WebSocketEvent.createEvent(richResult)))
    }
    "transform xivoDirFavorite result to Websocket event and send it to the asking actor" in new Helper() {
      val (ref, _)  = actor()
      val requester = TestProbe()
      val searchResult =
        FavoriteUpdated(Action.Added, "contactId", "sourceDirectory")

      ref ! RawDirectoryResult(requester.ref, searchResult, 42)

      requester.expectMsg(WsContent(WebSocketEvent.createEvent(searchResult)))
    }
    "transform DirSearchResult to RichDirectoryResult and sent it to the asking actor" in new Helper() {
      val (ref, _)                      = actor()
      val searchResult: DirSearchResult = mock[DirSearchResult]
      val richResult                    = new RichDirectoryResult(List("testHeader"))
      when(logic.enrichDirResult(searchResult, 42)).thenReturn(richResult)

      ref ! EnrichDirectoryResult(searchResult, 42)
      expectMsg(richResult)
    }
  }

}
