package services.config

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import command.{AgentInitLoggedIn, AgentInitLoggedOut}
import org.joda.time.DateTime
import org.mockito.Mockito.{stub, verify}
import org.scalatestplus.mockito.MockitoSugar
import services.AgentActorFactory
import xivo.ami.AgentCallUpdate
import xivo.events._
import xivo.models.Agent
import xivo.xucami.models.MonitorState

import scala.concurrent.duration._

class AgentManagerSpec
    extends TestKitSpec("AgentManagerSpec")
    with MockitoSugar {

  class Helper {
    val agFSMFactory = mock[AgentActorFactory]
    def actor = {
      val a = TestActorRef[AgentManager](Props(new AgentManager(agFSMFactory)))
      (a, a.underlyingActor)
    }
    def actorWithFsm(agentId: Agent.Id) = {
      val (ref, agentManager) = actor
      val agentFSM            = TestProbe()
      agentManager.agFsms += (agentId -> agentFSM.ref)
      (ref, agentManager, agentFSM)
    }
  }

  "Agent Manager" should {

    "send agent call update to the FSM" in new Helper {
      val agentId: Agent.Id   = 665
      val (ref, agentManager) = actor
      val agentFSM            = TestProbe()
      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))

      val acu = AgentCallUpdate(agentId, MonitorState.ACTIVE)

      ref ! acu

      agentFSM.expectMsg(acu)
    }

    "forward EventAgentLogin to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54
      val agentEvent          = EventAgentLogin(agentId, phoneNumber, agentNumber)
      val agentFSM            = TestProbe()

      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }

    "forward EventAgentPause to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54L
      val agentEvent          = EventAgentPause(agentId)
      val agentFSM            = TestProbe()

      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }
    "forward EventAgentUnPause to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54L
      val agentEvent          = EventAgentUnPause(agentId)
      val agentFSM            = TestProbe()

      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }
    "forward EventAgentWrapup to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54L
      val agentEvent          = EventAgentWrapup(agentId)
      val agentFSM            = TestProbe()

      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }

    "forward EventAgentLogout to the agent FSM matching the agent's id" in new Helper {
      val (ref, agentManager) = actor
      val agentNumber         = "2001"
      val agentId             = 55
      val agentEvent          = EventAgentLogout(agentId, agentNumber)
      val agentFSM            = TestProbe()

      stub(agFSMFactory.get(agentId)).toReturn(Some(agentFSM.ref))
      ref ! agentEvent

      verify(agFSMFactory).get(agentId)
      agentFSM.expectMsg(agentEvent)
    }

    "get or create agent FSM and agent stat on init login state received and send event" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54
      val agentLoggedIn =
        AgentInitLoggedIn(agentId, agentNumber, phoneNumber, new DateTime())
      val loginEvent         = EventAgentLogin(agentId, phoneNumber, agentNumber)
      val agentFSM           = TestProbe()
      val agentStatCollector = TestProbe()

      stub(agFSMFactory.getOrCreate(agentId, agentNumber, agentManager.context))
        .toReturn(agentFSM.ref)
      stub(
        agFSMFactory.getOrCreateAgentStatCollector(
          agentId,
          agentManager.context
        )
      ).toReturn(agentStatCollector.ref)

      ref ! agentLoggedIn

      verify(agFSMFactory).getOrCreate(
        agentId,
        agentNumber,
        agentManager.context
      )
      agentFSM.expectMsg(loginEvent)
    }
    "get or create agent FSM and agent stat on init logout state received and send event" in new Helper {
      val (ref, agentManager) = actor
      val phoneNumber         = "1000"
      val agentNumber         = "2000"
      val agentId             = 54
      val agentInitLoggedOut =
        AgentInitLoggedOut(agentId, agentNumber, new DateTime())
      val agentFSM           = TestProbe()
      val agentStatCollector = TestProbe()

      stub(agFSMFactory.getOrCreate(agentId, agentNumber, agentManager.context))
        .toReturn(agentFSM.ref)
      stub(
        agFSMFactory.getOrCreateAgentStatCollector(
          agentId,
          agentManager.context
        )
      ).toReturn(agentStatCollector.ref)

      ref ! agentInitLoggedOut

      verify(agFSMFactory).getOrCreate(
        agentId,
        agentNumber,
        agentManager.context
      )
      agentFSM.expectNoMessage(200.millis)
    }

  }
}
