package services.config

import _root_.xivo.events.{CurrentCallsPhoneEvents, PhoneEvent, PhoneEventType, PhoneHintStatusEvent}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import models._
import org.asteriskjava.manager.event.{ExtensionStatusEvent, QueueEntryEvent}
import org.joda.time.{DateTime, Period}
import org.json.JSONObject
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentConfigUpdate => _, QueueConfigUpdate => _, UserConfigUpdate => _, _}
import org.xivo.cti.model._
import services.XucAmiBus.{AmiExtensionStatusEvent, AmiFailure, AmiType}
import services.XucEventBus.XucEvent
import services.XucStatsEventBus.{Stat, StatUpdate}
import services.agent.{AgentStatistic, StatPeriod, Statistic}
import services.calltracking.{DevicesTracker, SipDriver}
import services.config.ConfigDispatcher.ObjectType._
import services.config.ConfigDispatcher.{ObjectType, _}
import services.config.ConfigManager.PublishUserPhoneStatuses
import services.config.ConfigRepository.WaitingCallsStatistics
import services.config.ExtensionManager.GetExtensionStatus
import services.request._
import services.video.model.{UserVideoEvent, VideoEvents, VideoStatusEvent}
import services.{GetRouter, Router, XucAmiBus, XucEventBus}
import xivo.events.AgentState.{AgentOnPause, AgentReady}
import xivo.models.XivoObject.{ObjectDefinition, ObjectType => StatObjectType}
import xivo.models._
import xivo.websocket._
import xivo.xuc.XucConfig
import xivo.xucami.models._

import java.util.Date
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.jdk.CollectionConverters._

class ConfigDispatcherSpec
    extends TestKitSpec("ConfigDispatcherSpec")
    with MockitoSugar {

  class Helper {
    val configRepository: ConfigRepository = mock[ConfigRepository]
    val configServerRequester: ConfigServerRequester =
      mock[ConfigServerRequester]
    val defaultMembershipRepo   = TestProbe()
    val eventBus                = mock[XucEventBus]
    val amiBus                  = mock[XucAmiBus]
    val agentManager            = TestProbe()
    val agentQueueMemberFactory = mock[AgentQueueMemberFactory]
    val messageFactory          = mock[MessageFactory]
    val agentGroupFactory       = mock[AgentGroupFactory]
    val userLineNumber          = mock[UserLineNumberFactory]
    val mUpdate                 = mock[(Int, String, Double) => Unit]
    val statAgreggator          = TestProbe()
    val devicesTracker          = TestProbe()
    val configServerManager     = TestProbe()
    val statusPublish           = TestProbe()
    val extensionsManager       = TestProbe()
    val agentLoginStatusDao     = mock[AgentLoginStatusDao]
    val config                  = mock[XucConfig]
    val extensionPattern        = mock[ExtensionPatternFactory]
    val userPreferenceService   = TestProbe()
    val ctiRouterFactory        = TestProbe()

    val dbUser = new XivoUserDao {
      def getCtiUsers(): Future[List[XivoUser]] = Future.successful(Nil)
      def getCtiUser(userId: Long): Future[Option[XivoUser]] =
        Future.successful(None)
      def getCtiUserByLogin(login: String): Future[XivoUser] =
        Future.failed(
          new NoSuchElementException(s"$login not found (ConfigDispatcherSpec)")
        )
      def all(): List[XivoUser] = List.empty
    }

    val dbWebServiceUser = new WebServiceUserDao {
      def getWebServiceUsers(): Future[List[WebServiceUser]] =
        Future.successful(Nil)
      def getWebServiceUser(login: String): Future[Option[WebServiceUser]] =
        Future.successful(None)
    }

    when(config.statsMetricsRegistryName).thenReturn("testRegistry")
    when(configRepository.configServerRequester).thenReturn(
      configServerRequester
    )
    when(extensionPattern.getAll()).thenReturn(Future.successful(List.empty))

    trait MetricUpdateTest extends MetricUpdate {
      def updateOrCreateMetric(
          queueId: Int,
          statName: String,
          statValue: Double
      ) = mUpdate(queueId, statName, statValue)
    }

    def actor = {
      val a = TestActorRef(
        new ConfigDispatcher(
          configRepository,
          agentManager.ref,
          defaultMembershipRepo.ref,
          statAgreggator.ref,
          dbUser,
          dbWebServiceUser,
          eventBus,
          amiBus,
          agentQueueMemberFactory,
          agentGroupFactory,
          userLineNumber,
          devicesTracker.ref,
          configServerManager.ref,
          statusPublish.ref,
          extensionsManager.ref,
          agentLoginStatusDao,
          extensionPattern,
          config,
          userPreferenceService.ref,
          ctiRouterFactory.ref
        ) {
          override def updateOrCreateMetric(
              queueId: Int,
              statName: String,
              statValue: Double
          ) = mUpdate(queueId, statName, statValue)
        }
      )
      (a, a.underlyingActor)
    }
  }

  "A config dispatcher actor" should {

    "subscribe to eventBus and AmiBus on start" in new Helper {
      val (ref, _) = actor

      verify(eventBus).subscribe(ref, XucEventBus.allAgentsEventsTopic)
      verify(eventBus).subscribe(ref, XucEventBus.allAgentsStatsTopic)

      verify(amiBus).subscribe(ref, AmiType.QueueEvent)
      verify(amiBus).subscribe(ref, AmiType.AmiService)
      verify(amiBus).subscribe(ref, AmiType.TransferEvent)
      verify(amiBus).subscribe(ref, AmiType.ExtensionStatusEvent)

    }

    "send back userPhoneStatus on PublishUserPhoneStatuses request" in new Helper {
      val (ref, _) = actor

      val uph = UserPhoneStatus(
        "user1",
        PhoneHintStatus.getHintStatus(Integer.valueOf(0))
      )
      stub(configRepository.getAllUserPhoneStatuses).toReturn(List(uph))

      ref ! PublishUserPhoneStatuses

      expectMsg(uph)
    }

    "send back queues on getconfig(queue) message received" in new Helper {
      val (ref, _)           = actor
      val gcf                = GetConfig("queue")
      val queueConfigRequest = new JSONObject()

      val qConfig1 = QueueConfigUpdate(
        123,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      val qConfig2 = QueueConfigUpdate(
        456,
        "q2",
        "Queue Two",
        "222",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val requester = TestProbe()

      stub(configRepository.getQueues()).toReturn(List(qConfig1, qConfig2))

      ref ! RequestConfigForUsername(requester.ref, gcf, Some("username"))

      requester.expectMsgAllOf(qConfig1, qConfig2)
    }

    "update config repository on queue received and publish on bus" in new Helper {
      val (ref, a) = actor

      val qConfig = QueueConfigUpdate(
        123,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      stub(configRepository.getQueue(123)).toReturn(Some(qConfig))

      ref ! qConfig
      verify(configRepository).updateQueueConfig(qConfig)
      verify(configRepository).getQueue(123)
      verify(eventBus).publish(qConfig)
    }

    "update config repository on queue entry and publish on bus" in new Helper {
      val (ref, a) = actor

      val event = new QueueEntryEvent("test")
      event.setCallerIdName("User Two")
      event.setCallerIdNum("2002")
      event.setQueue("switchboard_hold")
      event.setUniqueId("123456789.123")
      event.setChannel("Local/1016@default-00000036;1")

      val queueCall = QueueCall(
        1,
        Some("User One"),
        "2001",
        new DateTime(),
        "Local/1016@default-00000036;1",
        "main"
      )
      val expectedQueueCall =
        queueCall.copy(name = Some("User Two"), number = "2002")

      stub(
        configRepository.updateQueueCallCallerId(
          "switchboard_hold",
          "Local/1016@default-00000036;1",
          "123456789.123",
          "2002",
          "User Two"
        )
      )
        .toReturn(Some(expectedQueueCall))

      stub(configRepository.getQueueCalls("switchboard_hold"))
        .toReturn(Some(QueueCallList(1L, List(expectedQueueCall))))

      ref ! event

      verify(configRepository).updateQueueCallCallerId(
        "switchboard_hold",
        "Local/1016@default-00000036;1",
        "123456789.123",
        "2002",
        "User Two"
      )
      verify(eventBus).publish(QueueCallList(1L, List(expectedQueueCall)))
    }

    "send back agents on getconfig(agent) message received" in new Helper {
      val (ref, _)           = actor
      val gcf                = GetConfig("agent")
      val agentConfigRequest = new JSONObject()
      val ag1                = Agent(1, "John", "Doe", "2250", "default")
      val ag2                = Agent(1, "Bill", "Door", "2260", "sales")
      stub(configRepository.getAgents()).toReturn(List(ag1, ag2))

      val requester = TestProbe()

      ref ! RequestConfigForUsername(requester.ref, gcf, Some("username"))

      requester.expectMsgAllOf(ag1, ag2)
    }
    "send back queue members on getconfig(queuemember) message received" in new Helper {
      val (ref, _) = actor
      val gcf      = GetConfig("queuemember")
      val aqmc     = AgentQueueMember(55, 77, 12)
      stub(configRepository.getAgentQueueMembers()).toReturn(List(aqmc))

      val requester = TestProbe()

      ref ! RequestConfigForUsername(requester.ref, gcf, Some("username"))

      requester.expectMsgAllOf(aqmc)
    }
    "send back user's line config on getconfig(line) message received" in new Helper {
      val (ref, _) = actor
      val glc      = GetConfig("line")
      val lineByNo = LineConfigQueryByNb("1")
      val line     = mock[Line]
      val lineCfg  = LineConfig("1", "1000", Some(line))
      stub(configRepository.getLineForUser("username")).toReturn(Some(line))
      stub(line.id).toReturn(3)
      stub(configRepository.getLineConfig(LineConfigQueryById(3)))
        .toReturn(Some(lineCfg))
      val requester = TestProbe()

      ref ! RequestConfigForUsername(requester.ref, glc, Some("username"))

      requester.expectMsg(lineCfg)
    }

    "send back user's updated line config on getconfig(line) message received" in new Helper {
      val (ref, a)       = actor
      val link           = TestProbe()
      val getPhoneStatus = mock[JSONObject]

      a.messageFactory = messageFactory
      a.link = link.ref

      val xivoUser = mock[XivoUser]
      val glc      = GetConfig("line")
      val lineByNo = LineConfigQueryByNb("1")
      val line     = mock[Line]
      val lineCfg  = LineConfig("1", "1000", Some(line))

      stub(configRepository.getLineForUser("username")).toReturn(Some(line))
      stub(line.id).toReturn(3)

      stub(configRepository.getLineConfig(LineConfigQueryById(3)))
        .toReturn(None)
        .toReturn(Some(lineCfg))

      stub(xivoUser.id).toReturn(1)
      stub(configRepository.getCtiUser("username")).toReturn(Some(xivoUser))
      stub(userLineNumber.get(1)).toReturn(Some(UserLineNumber(1, 3, "1000")))
      stub(messageFactory.createGetPhoneStatus("3")).toReturn(getPhoneStatus)

      val requester = TestProbe()

      ref ! RequestConfigForUsername(requester.ref, glc, Some("username"))

      requester.expectMsg(lineCfg)
    }

    "send back user services when asked" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      stub(configRepository.getUserServices(123)).toReturn(Some(services))

      ref ! RequestConfig(requester.ref, GetUserServices(123))

      requester.expectMsg(services)
    }

    "forward user services request to configmgt when not cached" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      stub(configRepository.getUserServices(123)).toReturn(None)
      stub(configServerRequester.getUserServices(123))
        .toReturn(Future.successful(services))

      ref ! RequestConfig(requester.ref, GetUserServices(123))

      verify(configServerRequester).getUserServices(123)
      requester.expectMsg(services)
    }

    "update configrepository when user services updated" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      ref ! UserServicesUpdated(123, services)

      verify(configRepository).onUserServicesUpdated(123, services)
    }

    "publish to ami when user services updated" in new Helper {
      val patterns = List(
        ExtensionPattern(ExtensionName.ProgrammableKey, "_*735."),
        ExtensionPattern(ExtensionName.DND, "*25"),
        ExtensionPattern(ExtensionName.UnconditionalForward, "_*21."),
        ExtensionPattern(ExtensionName.NoAnswerForward, "_*22."),
        ExtensionPattern(ExtensionName.BusyForward, "_*23.")
      )

      val fkeys = List(
        FunctionKey("*735123***225", KeyInUse),
        FunctionKey("*735123***223*1234", KeyNotInUse),
        FunctionKey("*735123***222*4567", KeyNotInUse),
        FunctionKey("*735123***221*7890", KeyNotInUse)
      )

      val (ref, _) = actor

      patterns.foreach(exten => {
        when(configRepository.getExtensionPattern(exten.name))
          .thenReturn(Some(exten))
      })

      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      when(configRepository.getUserServices(123)).thenReturn(None)
      reset(amiBus)

      ref ! UserServicesUpdated(123, services)

      fkeys
        .map(_.toAmi)
        .foreach(msg => {
          verify(amiBus, timeout(500)).publish(msg)
        })
    }

    "publish to event bus when user services updated" in new Helper {
      val (ref, _) = actor

      val services = UserServices(
        dndEnabled = true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )
      ref ! UserServicesUpdated(1, services)
      verify(eventBus).publish(UserServicesUpdated(1, services))
    }

    "publish to event bus when user config is requested" in new Helper {
      val (ref, _) = actor
      val userId   = 1
      val line     = mock[Line]
      val services = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "1234"),
        UserForward(false, "1234")
      )
      val xivoUser =
        XivoUser(userId, None, None, "John", Some("Doe"), None, None, None, None)

      when(configRepository.getCtiUser(userId)).thenReturn(Some(xivoUser))
      when(configRepository.getUserServices(userId)).thenReturn(Some(services))
      when(configRepository.getAgentByUserId(userId)).thenReturn(None)
      when(line.id).thenReturn(1)
      when(configRepository.getLineForUser(userId)).thenReturn(Some(line))

      val request = GetUserConfig(userId)
      ref ! RequestConfig(testActor, request)

      val expected = UserConfigUpdate(
        userId,
        "John",
        "Doe",
        "John Doe",
        0,
        true,
        false,
        "1234",
        false,
        "1234",
        false,
        "1234",
        "",
        List(1),
        0,
        false
      )

      verify(eventBus).publish(expected)
    }

    "send list of queues on getList(queue) message received" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val qConfig = QueueConfigUpdate(
        1,
        "q1",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      stub(configRepository.getQueues()).toReturn(List(qConfig))

      ref ! RequestConfig(requester.ref, GetList("queue"))

      requester.expectMsg(QueueList(List(qConfig)))

    }

    "get agent by user id" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val userId    = 14
      val agent     = Agent(1, "John", "Doe", "2250", "default", 0, userId)

      stub(configRepository.getAgentByUserId(userId)).toReturn(Some(agent))

      ref ! RequestConfig(requester.ref, GetAgentByUserId(userId))

      requester.expectMsg(agent)

    }

    "send list of agents on getList(agent) message received" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val agents    = List(Agent(1, "John", "Doe", "2250", "default"))

      stub(configRepository.getAgents()).toReturn(agents)

      ref ! RequestConfig(requester.ref, GetList("agent"))

      requester.expectMsg(AgentList(agents))

    }
    "send list of agents on getAgents for groupId, queueId, penalty reveiced" in new Helper {
      val (ref, _)                    = actor
      val requester                   = TestProbe()
      val agents                      = List(Agent(1, "John", "Doe", "2250", "default"))
      val (groupId, queueId, penalty) = (5, 25, 6)

      stub(configRepository.getAgents(groupId, queueId, penalty))
        .toReturn(agents)

      ref ! RequestConfig(requester.ref, GetAgents(groupId, queueId, penalty))

      requester.expectMsg(AgentList(agents))

    }
    "send list of agent on get agents from a group not in queue received" in new Helper {
      val (ref, _)                    = actor
      val requester                   = TestProbe()
      val agents                      = List(Agent(7, "Bob", "Marley", "7540", "basic"))
      val (groupId, queueId, penalty) = (8, 32, 2)

      stub(configRepository.getAgentsNotInQueue(groupId, queueId))
        .toReturn(agents)

      ref ! RequestConfig(requester.ref, GetAgentsNotInQueue(groupId, queueId))

      requester.expectMsg(AgentList(agents))

    }
    "send list of queue members on getList(queuemember) message received" in new Helper {
      val (ref, _)     = actor
      val requester    = TestProbe()
      val queueMembers = List(AgentQueueMember(55, 77, 12))

      stub(configRepository.getAgentQueueMembers()).toReturn(queueMembers)

      ref ! RequestConfig(requester.ref, GetList("queuemember"))

      requester.expectMsg(AgentQueueMemberList(queueMembers))

    }
    "send back agent status on request status for agent" in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypeAgent
      import xivo.events.AgentState.AgentReady

      val (ref, _)  = actor
      val requester = TestProbe()
      val rs        = RequestStatus(requester.ref, 37, TypeAgent)
      val agentState =
        AgentReady(3, new DateTime(), "1001", List(), agentNb = "2000")
      stub(configRepository.getAgentState(37)).toReturn(Some(agentState))

      ref ! rs

      requester.expectMsg(agentState)
    }

    "send back user status on request status for user" in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypeUser

      val (ref, _)  = actor
      val requester = TestProbe()
      val rs        = RequestStatus(requester.ref, 59, TypeUser)

      val userStatusUpdate = new UserStatusUpdate

      stub(configRepository.getUserStatus(59)).toReturn(Some(userStatusUpdate))

      ref ! rs

      requester.expectMsg(userStatusUpdate)
    }
    "send back phone status on request status for phone" in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypePhone

      val (ref, _)  = actor
      val requester = TestProbe()
      val rs        = RequestStatus(requester.ref, 12, TypePhone)

      val phoneStatusUpdate = new PhoneStatusUpdate

      stub(configRepository.getPhoneStatus(12))
        .toReturn(Some(phoneStatusUpdate))

      ref ! rs

      requester.expectMsg(phoneStatusUpdate)
    }

    "update config repository agent status on agent state received" in new Helper {
      val (ref, _) = actor
      val agentState =
        AgentReady(34, new DateTime, "3200", List(), agentNb = "2000")
      ref ! agentState
      verify(configRepository).onAgentState(agentState)
    }

    """on MobilePushTokenAdded requests new line config for the ctirouter""" in new Helper {
      val user: XivoUser = XivoUser(
        14,
        None,
        None,
        "Laurent",
        Some("DLC"),
        Some("ldlc"),
        None,
        None,
        None
      )
      val xucUser: XucUser = XucUser("ldlc", user)
      val line: Line = Line(
        14,
        "default",
        "SIP",
        "someline",
        None,
        None,
        "192.168.56.2",
        false,
        None,
        None,
        false,
        CallerId("abc", "123"),
        SipDriver(1),
        true
      )
      val userRouter: TestProbe = TestProbe()
      val (ref, a)              = actor

      when(configRepository.getCtiUser(user.id))
        .thenReturn(Some(user))

      when(configRepository.getLineForUser("ldlc"))
        .thenReturn(Some(line))

      when(configRepository.getLineConfig(LineConfigQueryById(line.id)))
        .thenReturn(Some(LineConfig("14", "1234", Some(line))))

      ref ! MobilePushTokenAdded(14)

      ctiRouterFactory.expectMsg(3.seconds, GetRouter(xucUser))
      ctiRouterFactory.reply(Router(xucUser, userRouter.ref))

      verify(configRepository).getCtiUser(user.id)

      userRouter.expectMsg(LineConfig("14", "1234", Some(line)))
    }

    """on MobilePushTokenDeleted requests new line config for the ctirouter""" in new Helper {
      val user: XivoUser = XivoUser(
        14,
        None,
        None,
        "Laurent",
        Some("DLC"),
        Some("ldlc"),
        None,
        None,
        None
      )
      val xucUser: XucUser = XucUser("ldlc", user)
      val line: Line = Line(
        14,
        "default",
        "SIP",
        "someline",
        None,
        None,
        "192.168.56.2",
        false,
        None,
        None,
        false,
        CallerId("abc", "123"),
        SipDriver(1),
        true
      )
      val userRouter: TestProbe = TestProbe()
      val (ref, a)              = actor

      when(configRepository.getCtiUser(user.id))
        .thenReturn(Some(user))

      when(configRepository.getLineForUser("ldlc"))
        .thenReturn(Some(line))

      when(configRepository.getLineConfig(LineConfigQueryById(line.id)))
        .thenReturn(Some(LineConfig("14", "1234", Some(line))))

      ref ! MobilePushTokenDeleted(14)

      ctiRouterFactory.expectMsg(3.seconds, GetRouter(xucUser))
      ctiRouterFactory.reply(Router(xucUser, userRouter.ref))

      verify(configRepository).getCtiUser(user.id)

      userRouter.expectMsg(LineConfig("14", "1234", Some(line)))
    }

    """on phone config message
       - apply it by the config repository
       - ask for phone status""" in new Helper {
      val (ref, a) = actor
      val link     = TestProbe()
      a.link = link.ref
      a.messageFactory = messageFactory
      val getPhoneStatus = mock[JSONObject]
      stub(messageFactory.createGetPhoneStatus("130")).toReturn(getPhoneStatus)
      val phoneConfigUpdate = new PhoneConfigUpdate
      when(userLineNumber.get(13))
        .thenReturn(Some(UserLineNumber(13, 130, "1300")))
      phoneConfigUpdate.setUserId(13)

      ref ! phoneConfigUpdate
      link.expectMsg(getPhoneStatus)
      verify(configRepository).updatePhoneLine(130, "1300", 13)
    }

    """on LineConfigUpdate request send
       - apply it by the config repository
       - send updated line config to the requester
       - ask for phone status""" in new Helper {
      val (ref, a)  = actor
      val requester = TestProbe()
      val link      = TestProbe()
      a.link = link.ref
      a.messageFactory = messageFactory
      val getPhoneStatus = mock[JSONObject]
      stub(messageFactory.createGetPhoneStatus("140")).toReturn(getPhoneStatus)
      val lineConfig = LineConfig("140", "1000")
      when(userLineNumber.get(14))
        .thenReturn(Some(UserLineNumber(14, 140, "1400")))
      when(configRepository.getLineConfig(LineConfigQueryById(140)))
        .thenReturn(Some(lineConfig))
      ref ! ConfigChangeRequest(requester.ref, UpdateLineForUser(14))

      link.expectMsg(getPhoneStatus)
      verify(configRepository).updatePhoneLine(140, "1400", 14)
      requester.expectMsg(lineConfig)
    }

    "Monitor number when asked" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()

      when(configRepository.getPhoneHintStatusEventByNumber("1000")).thenReturn(None)
      when(configRepository.getPhoneHintStatusEventByNumber("1002")).thenReturn(None)

      ref ! MonitorPhoneHint(requester.ref, List("1000", "1002"))

      verify(eventBus).unsubscribe(
        requester.ref,
        XucEventBus.allPhoneHintEventTopic
      )
      verify(eventBus).subscribe(
        requester.ref,
        XucEventBus.phoneHintEventTopic("1000")
      )
      verify(eventBus, timeout(1500))
        .subscribe(requester.ref, XucEventBus.phoneHintEventTopic("1002"))
    }

    "publish phoneHints when monitoring starts" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()

      when(configRepository.getPhoneHintStatusEventByNumber("1005")).thenReturn(
        Some(PhoneHintStatusEvent("1005", PhoneHintStatus.AVAILABLE))
      )
      when(configRepository.getPhoneHintStatusEventByNumber("1006"))
        .thenReturn(Some(PhoneHintStatusEvent("1006", PhoneHintStatus.BUSY)))

      ref ! MonitorPhoneHint(requester.ref, List("1005", "1006"))

      verify(eventBus).publish(
        PhoneHintStatusEvent("1005", PhoneHintStatus.AVAILABLE)
      )
      verify(eventBus).publish(
        PhoneHintStatusEvent("1006", PhoneHintStatus.BUSY)
      )

    }

    "Send back line configuration on line config request by number if exists" in new Helper {
      val (ref, _)   = actor
      val lcr        = LineConfigQueryByNb("9000")
      val lineConfig = LineConfig("32", "9000")

      val requester = TestProbe()

      stub(configRepository.getLineConfig(lcr)).toReturn(Some(lineConfig))

      ref ! RequestConfig(requester.ref, lcr)

      requester.expectMsg(lineConfig)
    }

    "Send back line configuration on line config request by id if exists" in new Helper {
      val (ref, _)   = actor
      val lcr        = LineConfigQueryById(55)
      val line       = mock[Line]
      val lineConfig = LineConfig("55", "9041", Some(line))

      val requester = TestProbe()

      stub(configRepository.getLineConfig(lcr)).toReturn(Some(lineConfig))

      ref ! RequestConfig(requester.ref, lcr)

      devicesTracker.expectMsg(DevicesTracker.EnsureTrackerFor(line))

      requester.expectMsg(lineConfig)
    }

    "Do not send anything if line config doest not exists on request" in new Helper {
      val (ref, _) = actor
      val lcr      = LineConfigQueryByNb("9000")

      val requester = TestProbe()

      stub(configRepository.getLineConfig(lcr)).toReturn(None)

      ref ! RequestConfig(requester.ref, lcr)

      requester.expectNoMessage(100.millis)

    }

    "send agent directory on request get agent directory" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      stub(configRepository.getAgentDirectory).toReturn(List())

      ref ! RequestConfig(requester.ref, GetAgentDirectory)

      verify(configRepository).getAgentDirectory

      requester.expectMsg(AgentDirectory(List()))
    }

    "send agent states on get agent states request" in new Helper {
      val (ref, _) = actor
      val agentStateA =
        AgentOnPause(54, null, "98978", List(), agentNb = "2000")

      val agentStateB = AgentReady(32, null, "9898", List(), agentNb = "2000")

      val requester = TestProbe()

      stub(configRepository.getAgentStates).toReturn(
        List(agentStateA, agentStateB)
      )

      ref ! RequestConfig(requester.ref, GetAgentStates)

      verify(configRepository).getAgentStates

      requester.expectMsgAllOf(agentStateA, agentStateB)
    }

    "send a list of Meetme when GetList(meetme) message received" in new Helper {
      val (ref, _) = actor
      val meetmeA = new Meetme(
        "test",
        "4000",
        true,
        new Date(),
        List[MeetmeMember]().asJava
      )
      val meetmeB = new Meetme(
        "test2",
        "4002",
        true,
        new Date(),
        List[MeetmeMember]().asJava
      )
      val requester = TestProbe()
      stub(configRepository.getMeetmeList).toReturn(List(meetmeA, meetmeB))

      ref ! RequestConfig(requester.ref, GetList("meetme"))

      verify(configRepository).getMeetmeList
      requester.expectMsg(MeetmeList(List(meetmeA, meetmeB)))
    }

    "get agent config when create on agent" in new Helper {

      import services.config.ConfigDispatcher.ObjectType._
      import services.config.ConfigDispatcher._

      val msg = GetConfig("agent")
      msg should be(GetConfig(TypeAgent))
    }
    """set agent to queue on config change request
       and publish new queue member to bus
    """ in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypeQueueMember

      val (ref, configDispatcher) = actor

      stub(agentQueueMemberFactory.setAgentQueue(14, 210, 4))
        .toReturn(Some(AgentQueueMember(14, 210, 4)))

      ref ! ConfigChangeRequest(ref, SetAgentQueue(14, 210, 4))

      verify(agentQueueMemberFactory).setAgentQueue(14, 210, 4)

      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(TypeQueueMember),
          AgentQueueMember(14, 210, 4)
        )
      )

    }
    "remove agent from queue on config change request" in new Helper {
      val (ref, configDispatcher) = actor

      ref ! ConfigChangeRequest(ref, RemoveAgentFromQueue(12, 56))

      verify(agentQueueMemberFactory).removeAgentFromQueue(12, 56)

    }
    "Get agent groups" in new Helper {
      val (ref, _)    = actor
      var agentGroups = List(AgentGroup(Some(1), "group1"))
      val requester   = TestProbe()

      stub(agentGroupFactory.all()).toReturn(agentGroups)

      ref ! RequestConfig(requester.ref, GetList("agentgroup"))

      requester.expectMsg(AgentGroupList(agentGroups))

    }
    "update received queue statistics, filter-out LongestWaitTime, update metrics and forward it to the aggregator" in new Helper {
      val (ref, _) = actor
      val msg      = new QueueStatistics()
      msg.setQueueId(1)
      msg.addCounter(new Counter(StatName.EWT, 2))
      msg.addCounter(new Counter(StatName.LongestWaitTime, 123))

      val statUpdate = StatUpdate(
        ObjectDefinition(StatObjectType.Queue, Some(1)),
        List(Stat("EWT", 2))
      )

      ref ! msg

      statAgreggator.expectMsg(statUpdate)
      verify(mUpdate)(1, "EWT", 2.0)
    }

    "on agentQueueMember if not exists in repo, update repo and publish to bus" in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypeQueueMember

      val (ref, _)      = actor
      val agQueueMember = AgentQueueMember(1, 2, 3)

      stub(configRepository.queueMemberExists(agQueueMember)).toReturn(false)

      ref ! agQueueMember

      verify(configRepository).updateOrAddQueueMembers(agQueueMember)
      verify(eventBus).publish(
        XucEvent(XucEventBus.configTopic(TypeQueueMember), agQueueMember)
      )

    }

    "do not publish or update queue member if already received" in new Helper {

      import services.config.ConfigDispatcher.ObjectType.TypeQueueMember

      val (ref, _)      = actor
      val agQueueMember = AgentQueueMember(1, 2, 3)

      stub(configRepository.queueMemberExists(agQueueMember)).toReturn(true)

      ref ! agQueueMember

      verify(configRepository, never()).updateOrAddQueueMembers(agQueueMember)
      verify(eventBus, never()).publish(
        XucEvent(XucEventBus.configTopic(TypeQueueMember), agQueueMember)
      )
    }

    "update config repository meetme list and publish on the bus on meetme list received" in new Helper {
      val (ref, _) = actor
      val meetmeA = new Meetme(
        "test",
        "4000",
        true,
        new Date(),
        List[MeetmeMember]().asJava
      )
      val meetmeB = new Meetme(
        "test2",
        "4002",
        true,
        new Date(),
        List[MeetmeMember]().asJava
      )

      ref ! new MeetmeUpdate(List(meetmeA, meetmeB).asJava)

      verify(configRepository).onMeetmeUpdate(List(meetmeA, meetmeB))
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(TypeMeetme),
          MeetmeList(List(meetmeA, meetmeB))
        )
      )
    }

    "update config repository on agent statistics received" in new Helper {

      val (ref, _) = actor

      val agStat = AgentStatistic(34, List(Statistic("stat1", StatPeriod(1))))

      ref ! agStat

      verify(configRepository).updateAgentStatistic(agStat)

    }

    "send back statistics on request" in new Helper {
      val agStat1 = AgentStatistic(34, List(Statistic("stat1", StatPeriod(1))))
      val agStat2 = AgentStatistic(53, List(Statistic("stat2", StatPeriod(34))))

      val stats = List(agStat1, agStat2)
      stub(configRepository.getAgentStatistics).toReturn(stats)

      val (ref, _)  = actor
      val requester = TestProbe()

      ref ! RequestConfig(requester.ref, GetAgentStatistics)

      requester.expectMsgAllOf(agStat1, agStat2)
    }

    "send back outbound queue" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val queueIds  = List(2L, 7L, 9L)
      val outboundQueue = QueueConfigUpdate(
        123,
        "outqueue",
        "Queue Outbound",
        "3000",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        None,
        None,
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      stub(configRepository.getOutboundQueue(queueIds))
        .toReturn(Some(outboundQueue))

      ref ! RequestConfig(requester.ref, OutboundQueueQuery(queueIds))

      requester.expectMsg(OutboundQueue(outboundQueue))
    }

    "add a queue call in repository and publish on the bus when EnterQueue event received" in new Helper {
      val (ref, _) = actor
      val queueCall = QueueCall(
        2,
        Some("bar"),
        "3331545",
        new DateTime(),
        "123456.789",
        "main"
      )
      val queueCalls = QueueCallList(12, List(queueCall))
      val enterQueue =
        EnterQueue("foo", "123456.789", queueCall, "SIP/abcd-00001")
      stub(configRepository.getQueueCalls("foo")).toReturn(Some(queueCalls))

      val qConfig = QueueConfigUpdate(
        123,
        "foo",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      stub(configRepository.getQueue("foo")).toReturn(Some(qConfig))
      stub(configRepository.findWaitingCallsStatistics("foo")).toReturn(
        Some(
          WaitingCallsStatistics(1, new DateTime().minus(Period.millis(1234)))
        )
      )

      ref ! enterQueue

      verify(configRepository).onQueueCallReceived(
        "foo",
        "123456.789",
        queueCall
      )
      verify(eventBus).publish(queueCalls)
      val statUpdate = StatUpdate(
        ObjectDefinition(StatObjectType.Queue, Some(123)),
        List(Stat("WaitingCalls", 1.0d), Stat("LongestWaitTime", 1.0d))
      )
      statAgreggator.expectMsg(statUpdate)
    }

    "remove a queue call from the repository and publish on the bus when LeaveQueue event received" in new Helper {
      val (ref, _) = actor
      val queueCall = QueueCall(
        2,
        Some("bar"),
        "3331545",
        new DateTime(),
        "123456.789",
        "main"
      )
      val queueCalls = QueueCallList(12, List(queueCall))
      val leaveQueue =
        LeaveQueue("foo", "123456.789", new DateTime(), "SIP/abcd-00001")
      stub(configRepository.getQueueCalls("foo")).toReturn(Some(queueCalls))

      val qConfig = QueueConfigUpdate(
        123,
        "foo",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      stub(configRepository.getQueue("foo")).toReturn(Some(qConfig))
      stub(configRepository.findWaitingCallsStatistics("foo")).toReturn(None)

      ref ! leaveQueue

      verify(configRepository).onQueueCallFinished("foo", "123456.789")
      verify(eventBus).publish(queueCalls)
      val statUpdate = StatUpdate(
        ObjectDefinition(StatObjectType.Queue, Some(123)),
        List(Stat("WaitingCalls", 0.0d))
      )
      statAgreggator.expectMsg(statUpdate)
    }

    "update configRepository when AttendedTransferFinished event received and publish queue stats on updated queue" in new Helper {
      val (ref, _) = actor
      val queueCall = QueueCall(
        2,
        Some("bar"),
        "3331545",
        new DateTime(),
        "123456.789",
        "main"
      )
      val queueCalls = QueueCallList(12, List(queueCall))
      stub(configRepository.getQueueCalls("foo")).toReturn(Some(queueCalls))
      val event = AttendedTransferFinished("123456.789", "987654.321")
      stub(
        configRepository.onQueueCallTransferred(
          event.fromUniqueId,
          event.toUniqueId
        )
      ).toReturn(List("foo"))

      val qConfig = QueueConfigUpdate(
        123,
        "foo",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      stub(configRepository.getQueue("foo")).toReturn(Some(qConfig))
      stub(configRepository.findWaitingCallsStatistics("foo")).toReturn(
        Some(
          WaitingCallsStatistics(1, new DateTime().minus(Period.millis(1234)))
        )
      )

      ref ! event

      verify(configRepository).onQueueCallTransferred(
        event.fromUniqueId,
        event.toUniqueId
      )
      val statUpdate = StatUpdate(
        ObjectDefinition(StatObjectType.Queue, Some(123)),
        List(Stat("WaitingCalls", 1.0d), Stat("LongestWaitTime", 1.0d))
      )
      statAgreggator.expectMsg(statUpdate)
    }

    "remove all queue calls when ami failure is received and publish empty list" in new Helper {

      val queueCalls = QueueCallList(2, List())

      val qConfig = QueueConfigUpdate(
        2,
        "q1",
        "Queue One",
        "333",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      stub(configRepository.getQueues()).toReturn(List(qConfig))
      stub(configRepository.getQueueCalls("q1")).toReturn(Some(queueCalls))
      val (ref, _) = actor

      ref ! AmiFailure("Ami Failed", "main")

      verify(configRepository).removeQueueCallsFromMds("main")
      verify(eventBus).publish(queueCalls)
    }

    "send back queue calls" in new Helper {
      val (ref, _)  = actor
      val requester = TestProbe()
      val queueId   = 3
      val (c1, c2) = (
        QueueCall(
          1,
          Some("John Doe"),
          "335687",
          new DateTime(),
          "123456.789",
          "main"
        ),
        QueueCall(
          2,
          Some("Jack Smith"),
          "4458796",
          new DateTime(),
          "123456.789",
          "main"
        )
      )
      stub(configRepository.getQueueCalls(queueId))
        .toReturn(QueueCallList(15, List(c1, c2)))

      ref ! RequestConfig(requester.ref, GetQueueCalls(queueId))

      requester.expectMsg(QueueCallList(15, List(c1, c2)))
    }

    "reload the agent on agent config update" in new Helper {
      val (ref, _) = actor
      val agentId  = 32
      val agent    = Agent(agentId, "Isabel", "Smith", "1010", "default", 1)
      val agentMember = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val agCfg = AgentConfigUpdate(
        agentId,
        "Isabel",
        "Smith",
        "1010",
        "default",
        List(agentMember),
        1L,
        Some(0L)
      )

      val aqm            = List(AgentQueueMember(32, 1L, 1))
      val previousMember = List(AgentQueueMember(32, 2L, 1))

      stub(configRepository.getAgent(agentId)).toReturn(Some(agent))
      stub(configRepository.convertAgentConfigUpdateToAgent(agCfg))
        .toReturn(agent)
      stub(configRepository.convertAgentConfigUpdateToQueueMember(agCfg))
        .toReturn(aqm)
      stub(configRepository.getAgentQueueMembersToRemove(aqm))
        .toReturn(previousMember)
      stub(configRepository.agents).toReturn(Map.empty)

      ref ! agCfg

      verify(configRepository).addAgent(agent)
      verify(configRepository).getAgentQueueMembersToRemove(aqm)
      verify(configRepository).updateOrAddQueueMembers(
        AgentQueueMember(32, 1L, 1)
      )
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(32, 1L, 1)
        )
      )
      verify(configRepository).loadAgent(agentId)
      verify(eventBus).publish(agent)
      verify(configRepository).removeQueueMember(AgentQueueMember(32, 2L, 1))
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(32, 2L, -1)
        )
      )
      verify(configRepository, never()).updateOrAddQueueMembers(
        AgentQueueMember(32, 2L, -1)
      )
      verify(configRepository).getAgent(32)
    }

    "refresh when agent was associated to another queue" in new Helper {
      val (ref, _) = actor
      val agentId  = 32
      val agent    = Agent(agentId, "Isabel", "Smith", "1010", "default", 1)
      val agentMember = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val agCfg = AgentConfigUpdate(
        agentId,
        "Isabel",
        "Smith",
        "1010",
        "default",
        List(agentMember),
        1L,
        Some(0L)
      )

      val previousMember = List(AgentQueueMember(32, 1L, 1))
      val currentMember  = List(AgentQueueMember(32, 2L, 1))

      stub(configRepository.getAgentQueueMembersToRemove(currentMember))
        .toReturn(previousMember)

      ref.underlyingActor.receive(
        RefreshAgentQueueMember(agentId, currentMember)
      )

      verify(configRepository).getAgentQueueMembersToRemove(currentMember)
      verify(configRepository).removeQueueMember(previousMember(0))
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(32, 1L, -1)
        )
      )
    }

    "refresh when agent was removed from all queues" in new Helper {
      val (ref, _) = actor
      val agentId  = 32
      val agent    = Agent(agentId, "Isabel", "Smith", "1010", "default", 1)
      val agentMember = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val agCfg = AgentConfigUpdate(
        agentId,
        "Isabel",
        "Smith",
        "1010",
        "default",
        List(agentMember),
        1L,
        Some(0L)
      )

      val previousMember = List(
        AgentQueueMember(32, 1L, 1),
        AgentQueueMember(32, 3L, 1),
        AgentQueueMember(32, 4L, 1)
      )
      val currentMember = List(AgentQueueMember(32, 2L, 1))

      stub(configRepository.filterOutQueueMember(32)).toReturn(previousMember)

      ref.underlyingActor.receive(RefreshAgentQueueMember(agentId, List()))

      verify(configRepository).filterOutQueueMember(32)
      verify(configRepository, never()).getAgentQueueMembersToRemove(
        currentMember
      )
      verify(configRepository).removeQueueMember(previousMember(0))
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(32, 1L, -1)
        )
      )
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(32, 3L, -1)
        )
      )
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(32, 4L, -1)
        )
      )
    }

    "reload the specified agent and publish it on the bus" in new Helper {
      val (ref, _) = actor
      val agentId  = 74
      val agent    = Agent(agentId, "John", "Doe", "1000", "default", 1)
      stub(configRepository.getAgent(agentId)).toReturn(Some(agent))

      ref ! RefreshAgent(agentId)

      verify(configRepository).getAgent(agentId)
      verify(configRepository).loadAgent(agentId)
      verify(eventBus).publish(agent)
    }

    "retrieve the agent on a given phone number" in new Helper {
      val (ref, _)  = actor
      val agentId   = 12
      val number    = "1000"
      val requester = TestProbe()
      stub(configRepository.getAgentLoggedOnPhoneNumber(number))
        .toReturn(Some(agentId))

      ref ! RequestConfig(requester.ref, GetAgentOnPhone(number))

      requester.expectMsg(AgentOnPhone(agentId, number))
    }

    "retrieve the queues for a given agent" in new Helper {
      val (ref, _) = actor
      val agentQueues =
        List(AgentQueueMember(12, 3, 1), AgentQueueMember(12, 4, 5))

      stub(configRepository.getQueuesForAgent(12)).toReturn(agentQueues)
      val requester = TestProbe()

      ref ! RequestConfig(requester.ref, GetQueuesForAgent(12))

      requester.expectMsg(QueuesForAgent(agentQueues, 12))
    }

    "remove an agent" in new Helper {
      val (ref, _) = actor
      val agentQueues = List(
        AgentQueueMember(1, 3, 1),
        AgentQueueMember(2, 3, 1),
        AgentQueueMember(2, 4, 5)
      )

      stub(configRepository.filterOutQueueMember(2)).toReturn(agentQueues)

      ref ! RemoveAgentQueueMember(2)

      verify(configRepository).filterOutQueueMember(2)
      verify(configRepository).removeQueueMember(agentQueues(1))
      verify(configRepository).removeQueueMember(agentQueues(2))
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(2, 3L, -1)
        )
      )
      verify(eventBus).publish(
        XucEvent(
          XucEventBus.configTopic(ObjectType.TypeQueueMember),
          AgentQueueMember(2, 4L, -1)
        )
      )
    }

    "retrieve phone state when user line is received" in new Helper {
      val (ref, _) = actor

      ref ! UserLineNumber(13, 130, "1000")

      extensionsManager.expectMsg(GetExtensionStatus("1000"))
    }

    "get user display name for user" in new Helper {
      val (ref, _) = actor
      val probe    = TestProbe()

      stub(configRepository.getCtiUserDisplayName("jbond"))
        .toReturn(Some("James Bond"))

      ref ! RequestConfig(probe.ref, DisplayNameLookup("jbond"))

      probe.expectMsg(UserDisplayName("jbond", "James Bond"))
    }

    "not get user display name for user" in new Helper {
      val (ref, _) = actor
      val probe    = TestProbe()

      stub(configRepository.getCtiUserDisplayName("jbond")).toReturn(None)

      ref ! RequestConfig(probe.ref, DisplayNameLookup("jbond"))

      probe.expectMsg(UserDisplayName("jbond", "jbond"))
    }

    "update config repository on ICE server received" in new Helper {
      val (ref, _) = actor

      val addr: Option[String] = Some("host:3478")
      val iceSrv: IceServer    = IceServer(addr)

      ref ! iceSrv
      verify(configRepository).updateIceConfig(iceSrv)
    }

    "Send ICE config when requested" in new Helper {
      val (ref, _)             = actor
      val requester: TestProbe = TestProbe()
      val iceCfg = IceConfig(
        Some(StunConfig(List("stun:host:3478"))),
        Some(TurnConfig(List("turn:host:3478"), "2002:84600", "pwd", 3600))
      )

      stub(configRepository.getIceConfig).toReturn(iceCfg)

      ref ! RequestConfig(requester.ref, GetIceConfig)

      verify(configRepository).getIceConfig
      requester.expectMsg(iceCfg)
    }

    "update mobile app config status in repository" in new Helper {
      val (ref, _)                   = actor
      val mobileCfg: MobileAppConfig = MobileAppConfig(true)

      ref ! mobileCfg
      verify(configRepository).updateMobileConfigStatus(true)
    }
  }

  "A config dispatcher on extension status event received" should {
    trait exsEvent {
      val (phoneNumber, status) = ("1200", 0)

      var exs = new ExtensionStatusEvent("test")
      exs.setStatus(status)
      exs.setExten(phoneNumber)

    }
    "Update config repository with the status received" in new Helper
      with exsEvent {
      val (ref, _) = actor

      ref ! AmiExtensionStatusEvent(exs)

      verify(configRepository).updatePhoneStatus(exs.getExten, exs.getStatus)

    }

    "publish user phone status to the status publisher " in new Helper
      with exsEvent {
      val (ref, _) = actor

      val userPhoneStatus =
        UserPhoneStatus("user", PhoneHintStatus.getHintStatus(0))

      when(configRepository.getUserPhoneStatus(phoneNumber, status))
        .thenReturn(Some(userPhoneStatus))

      ref ! AmiExtensionStatusEvent(exs)

      statusPublish.expectMsg(userPhoneStatus)

    }
    "publish phone status to the event bus" in new Helper with exsEvent {
      val (ref, _) = actor
      stub(
        configRepository.getUserPhoneStatus(phoneNumber, status)
      ) toReturn None

      val evt =
        PhoneHintStatusEvent(phoneNumber, PhoneHintStatus.getHintStatus(status))

      when(configRepository.getPhoneHintStatusEventByNumber(phoneNumber))
        .thenReturn(Some(evt))

      ref ! AmiExtensionStatusEvent(exs)

      verify(eventBus).publish(evt)

    }

    "publish phoneEvent to the event bus" in new Helper {
      val (ref, _) = actor
      when(configRepository.userNameFromPhoneNb("1001"))
        .thenReturn(Some("jbond"))
      val phoneEvent = PhoneEvent(
        PhoneEventType.EventDialing,
        "1000",
        "1001",
        "James Bond",
        "123456789",
        "123456789.123"
      )
      ref ! phoneEvent

      verify(eventBus).publish(phoneEvent.copy(username = Some("jbond")))
    }

    "publish CurrentCallsPhoneEvents to the event bus" in new Helper {
      val (ref, _) = actor
      when(configRepository.userNameFromPhoneNb("1001"))
        .thenReturn(Some("jbond"))

      val phoneEvent = PhoneEvent(
        PhoneEventType.EventDialing,
        "1000",
        "1001",
        "James Bond",
        "123456789",
        "123456789.123"
      )
      val currentCallsPhoneEvents =
        CurrentCallsPhoneEvents("1000", List(phoneEvent))
      val expected = currentCallsPhoneEvents.copy(
        phoneEvents = List(
          currentCallsPhoneEvents.phoneEvents.head
            .copy(username = Some("jbond"))
        )
      )
      ref ! currentCallsPhoneEvents

      verify(eventBus).publish(expected)
    }

    "publish WsConferenceParticipantEvent to the event bus" in new Helper {
      val (ref, _) = actor
      when(configRepository.userNameFromPhoneNb("1001"))
        .thenReturn(Some("jbond"))

      val wcpe = WsConferenceParticipantEvent(
        WsConferenceParticipantEventJoin,
        "1234",
        "1000",
        "4000",
        42,
        "James Bond",
        "1001",
        15
      )

      ref ! wcpe

      verify(eventBus).publish(wcpe.copy(username = Some("jbond")))
    }

    "publish ConferenceEvent to the event bus" in new Helper {
      val (ref, _) = actor
      when(configRepository.userNameFromPhoneNb("1001"))
        .thenReturn(Some("jbond"))

      val wce = WsConferenceEvent(
        WsConferenceEventJoin,
        "1234",
        "1000",
        "4000",
        "RnD",
        List(
          WsConferenceParticipant(1, "James Bond", "1001", 0)
        ),
        0
      )

      val expected = wce.copy(
        participants =
          List(wce.participants.head.copy(username = Some("jbond")))
      )

      ref ! wce

      verify(eventBus).publish(expected)
    }

    "request line config update on phone device change" in new Helper {
      val (ref, _) = actor
      val line     = mock[Line]
      stub(configRepository.getLineForUser("username")).toReturn(Some(line))
      stub(line.id).toReturn(3)
      val requester = TestProbe()
      val lcr       = LineConfigQueryById(line.id)

      ref ! RequestConfigForUsername(
        requester.ref,
        ToggleUniqueAccountDevice(None, TypePhoneDevice),
        Some("username")
      )

      verify(configRepository).getLineConfig(lcr)
    }

    "request line config update on webrtc device change" in new Helper {
      val (ref, _) = actor
      val line     = mock[Line]
      stub(configRepository.getLineForUser("username")).toReturn(Some(line))
      stub(line.id).toReturn(3)
      val requester = TestProbe()
      val lcr       = LineConfigQueryById(line.id)

      ref ! RequestConfigForUsername(
        requester.ref,
        ToggleUniqueAccountDevice(None, TypeDefaultDevice),
        Some("username")
      )

      verify(configRepository).getLineConfig(lcr)
    }

    "request line config update on device change returns empty line config" in new Helper {
      val (ref, _) = actor
      val line     = mock[Line]
      stub(configRepository.getLineForUser("username")).toReturn(None)
      val requester = TestProbe()

      ref ! RequestConfigForUsername(
        requester.ref,
        ToggleUniqueAccountDevice(None, TypeDefaultDevice),
        Some("username")
      )

      verify(configRepository).getLineForUser("username")
      verifyNoMoreInteractions(configRepository)

      requester.expectMsg(LineConfig("-", "-", None))
    }

    "send line config update on phone device change" in new Helper {
      val (ref, _) = actor
      val line     = mock[Line]
      val lineCfg  = LineConfig("1", "1000", Some(line))

      val requester = TestProbe()

      stub(line.id).toReturn(3)
      val lcr = LineConfigQueryById(line.id)
      stub(configRepository.getLineConfig(lcr)).toReturn(Some(lineCfg))

      ref ! ConfigChangeRequest(
        requester.ref,
        ChangeDeviceForUser(lcr, Some(line), TypePhoneDevice)
      )

      verify(configRepository).updateLineDevice(line.id, TypePhoneDevice)
      requester.expectMsg(lineCfg)
      devicesTracker.expectMsg(DevicesTracker.SwitchTrackerFor(line, line))
    }

    "send line config update on webrtc device change" in new Helper {
      val (ref, _) = actor
      val line     = mock[Line]
      val lineCfg  = LineConfig("1", "1000", Some(line))

      val requester = TestProbe()

      stub(line.id).toReturn(3)
      val lcr = LineConfigQueryById(line.id)
      stub(configRepository.getLineConfig(lcr)).toReturn(Some(lineCfg))

      ref ! ConfigChangeRequest(
        requester.ref,
        ChangeDeviceForUser(lcr, Some(line), TypeDefaultDevice)
      )

      verify(configRepository).updateLineDevice(line.id, TypeDefaultDevice)
      requester.expectMsg(lineCfg)
      devicesTracker.expectMsg(DevicesTracker.SwitchTrackerFor(line, line))
    }

    "refresh line and update config repository with line" in new Helper {
      val (ref, _) = actor
      val line     = mock[Line]
      val xivoUser = mock[XivoUser]
      val endpoint = SipEndpoint(9)

      stub(line.id).toReturn(3)
      stub(xivoUser.id).toReturn(12)
      stub(configRepository.getLineByEndpoint(endpoint)).toReturn(Some(line))
      stub(configRepository.getLineUser(line.id)).toReturn(Some(xivoUser))

      ref ! RefreshLine(endpoint)

      verify(configRepository).getLineByEndpoint(endpoint)
      verify(configRepository).getLineUser(3)
      verify(configRepository).loadUserLine(12, line.id)
    }

    "load extension patterns at startup" in new Helper {
      val patterns = List(
        ExtensionPattern(ExtensionName.ProgrammableKey, "_*735."),
        ExtensionPattern(ExtensionName.DND, "*25"),
        ExtensionPattern(ExtensionName.UnconditionalForward, "_*21."),
        ExtensionPattern(ExtensionName.NoAnswerForward, "_*22."),
        ExtensionPattern(ExtensionName.BusyForward, "_*23.")
      )
      reset(extensionPattern)
      when(extensionPattern.getAll()).thenReturn(Future.successful(patterns))
      val (ref, _) = actor
      verify(extensionPattern).getAll()
      patterns.foreach(exten => {
        verify(configRepository, timeout(500)).updateExtensionPattern(exten)
      })
    }

    "on videoEvent received, update user video status and publish in the bus" in new Helper {
      val (ref, _)   = actor
      val videoEvent = UserVideoEvent("ahonnet", "videoEnd")
      ref ! videoEvent

      verify(configRepository).updateVideoStatus(videoEvent)
      verify(eventBus).publish(
        VideoStatusEvent("ahonnet", VideoEvents.Available)
      )
    }

    "forward userPreference update to userPreference service" in new Helper {
      val (ref, _)                                  = actor
      val userPreferenceEvent: UserPreferenceEdited = UserPreferenceEdited(42)
      ref ! userPreferenceEvent

      userPreferenceService.expectMsg(userPreferenceEvent)
    }

    "forward userPreference create to userPreference service" in new Helper {
      val (ref, _)                                   = actor
      val userPreferenceEvent: UserPreferenceCreated = UserPreferenceCreated(42)
      ref ! userPreferenceEvent

      userPreferenceService.expectMsg(userPreferenceEvent)
    }

    "forward userPreference delete to userPreference service" in new Helper {
      val (ref, _)                                   = actor
      val userPreferenceEvent: UserPreferenceDeleted = UserPreferenceDeleted(42)
      ref ! userPreferenceEvent

      userPreferenceService.expectMsg(userPreferenceEvent)
    }
  }
}
