package services.config

import akkatest.TestKitSpec
import models._
import org.joda.time.{DateTime, LocalDate, LocalTime}
import org.mockito.Matchers.{any, eq => eqFromMockito}
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.libs.ws.{BodyWritable, WSRequest, WSResponse}
import play.api.mvc.Cookie
import play.api.test.Helpers._
import xivo.models._
import xivo.network.{WebServiceException, XiVOWS}
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}

import java.util.UUID
import scala.concurrent.Future

class ConfigServerRequesterSpec
    extends TestKitSpec("ConfigServerRequesterSpec")
    with MockitoSugar
    with ScalaFutures {

  val appliConfig: Map[String, Any] = {
    Map(
      "config.host"  -> "configIP",
      "config.port"  -> 9000,
      "config.token" -> "abcdef12456"
    )
  }

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))

  class Helper(conf: Map[String, Any] = appliConfig) {
    val xivoWS: XiVOWS                   = mock[XiVOWS]
    val configWS: ConfigWS               = mock[ConfigWS]
    val requester: ConfigServerRequester = new ConfigServerRequester(configWS)
    val cookie: Cookie                   = Cookie("_eid", "g4jg34u9khf8vcu9e49keut")

    val wsReq: WSRequest   = mock[WSRequest]
    val wsResp: WSResponse = mock[WSResponse]

    when(configWS.ApiRelease2_0).thenReturn("2.0")

    def stubReqOk(json: JsValue) = {
      stub(wsReq.withBody(eqFromMockito(json))(any[BodyWritable[JsValue]]))
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
    }

  }

  "A ReponseWS" should {
    "return an error with unparsable JSON" in new Helper with ResponseWS {

      stub(wsResp.json).toReturn(Json.parse("{}"))

      a[WebServiceException] should be thrownBy {
        processResponse[String](wsResp)
      }
    }
    "return an error with invalid response from config server" in new Helper
      with ResponseWS {

      stub(wsResp.status).toReturn(INTERNAL_SERVER_ERROR)

      a[WebServiceException] should be thrownBy {
        checkError(wsResp)
      }
    }
  }

  "A ConfigWS" should {
    class ConfigHelper(httpContext: String) {
      val xucConfig: XucBaseConfig   = mock[XucBaseConfig]
      val config: ConfigServerConfig = mock[ConfigServerConfig]
      val xivoWS: XiVOWS             = mock[XiVOWS]

      stub(config.configHost).toReturn("configIP")
      stub(config.configPort).toReturn(9000)
      stub(config.configHttpContext).toReturn(httpContext)

      val configWs = new ConfigWS(xivoWS, config, xucConfig)

    }
    "generate URL when httpContext is empty" in new ConfigHelper("") {

      configWs.getConfigWsUrl(
        "test"
      ) shouldEqual "http://configIP:9000/api/1.0/test"
    }
    "generate URL when httpContext contains a path without /" in new ConfigHelper(
      "cfg"
    ) {

      configWs.getConfigWsUrl(
        "test"
      ) shouldEqual "http://configIP:9000/cfg/api/1.0/test"
    }
    "generate URL when httpContext contains a path with /" in new ConfigHelper(
      "cfgWith/"
    ) {

      configWs.getConfigWsUrl(
        "test"
      ) shouldEqual "http://configIP:9000/cfgWith/api/1.0/test"
    }
  }

  "A ConfigServerRequester" should {
    "list CallbackLists" in new Helper {
      val listUuid: UUID = UUID.randomUUID()
      val cbUuid         = UUID.randomUUID()
      val periodUuid     = UUID.randomUUID()
      val jsonResult     = Json.parse(s"""[{
             |"uuid":"$listUuid","name":"The List","queueId":3,
             |"callbacks":[{
             | "uuid": "$cbUuid","listUuid":"$listUuid", "phoneNumber": "1000", "company": "The company",
             | "preferredPeriod": {
             |   "uuid": "$periodUuid", "name": "morning", "periodStart": "08:00:00", "periodEnd": "11:00:00", "default": true
             | },
             | "dueDate": "2015-01-10"
             |}]
             |}]""".stripMargin)
      val period = PreferredCallbackPeriod(
        Some(periodUuid),
        "morning",
        new LocalTime(8, 0, 0),
        new LocalTime(11, 0, 0),
        true
      )
      val result = List(
        CallbackList(
          Some(listUuid),
          "The List",
          3,
          List(
            CallbackRequest(
              Some(cbUuid),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None,
              preferredPeriod = Some(period),
              dueDate = new LocalDate(2015, 1, 10)
            )
          )
        )
      )

      stub(configWS.request("callback_lists", "GET", null)).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.json).toReturn(jsonResult)

      val fValue = requester.getCallbackLists

      whenReady(fValue) { _ =>
        fValue.futureValue shouldEqual result

      }
    }

    "post a csv to the config server and return Unit" in new Helper {
      val listUuid: String = UUID.randomUUID().toString
      val csv              = "213333|John"

      stub(
        wsReq.withBody(eqFromMockito("213333|John"))(any[BodyWritable[String]])
      ).toReturn(wsReq)
      stub(
        configWS.request(
          s"callback_lists/$listUuid/callback_requests/csv",
          "POST",
          null
        )
      ).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))

      stub(wsResp.status).toReturn(CREATED)

      whenReady(requester.importCsvCallback(listUuid, csv)) { res =>
        res shouldEqual ((): Unit)
      }
    }

    "send a take callback request" in new Helper {
      val uuid: String = UUID.randomUUID().toString
      val agent        = Agent(12, "John", "Doe", "1000", "default")

      stub(configWS.request(s"callback_requests/$uuid/take", "POST", null))
        .toReturn(wsReq)

      stub(
        wsReq.withBody(eqFromMockito(Json.obj("agentId" -> agent.id)))(
          any[BodyWritable[JsObject]]
        )
      ).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      whenReady(requester.takeCallback(uuid, agent)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/take", "POST")
      }
    }

    "send a release callback request" in new Helper {
      val uuid = UUID.randomUUID().toString

      stub(configWS.request(s"callback_requests/$uuid/release", "POST", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      whenReady(requester.releaseCallback(uuid)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/release", "POST")
      }
    }

    "send a get all pending callback request for an agent" in new Helper {
      val uuid    = UUID.randomUUID()
      val agentId = 12

      stub(
        configWS.request(
          s"callback_requests/agent/${agentId}/taken",
          "GET",
          null
        )
      ).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(Json.parse(s"""{"ids": ["$uuid"]}"""))

      whenReady(requester.getTakenCallbacks(agentId)) { res =>
        res shouldEqual List(uuid)
        verify(configWS).request(
          s"callback_requests/agent/${agentId}/taken",
          "GET"
        )
      }
      requester.getTakenCallbacks(agentId).futureValue shouldEqual List(uuid)
    }

    "retrieve a CallbackRequest by uuid" in new Helper {
      val uuid           = UUID.randomUUID()
      val listUuid: UUID = UUID.randomUUID()

      stub(configWS.request(s"callback_requests/$uuid", "GET", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(Json.parse(s"""{
             | "uuid": "$uuid",
             | "listUuid":"$listUuid",
             | "phoneNumber": "1000",
             | "company": "The company",
             | "queueId": 23
             |}""".stripMargin))

      whenReady(requester.getCallbackRequest(uuid.toString)) { res =>
        res shouldEqual CallbackRequest(
          Some(uuid),
          listUuid,
          Some("1000"),
          None,
          None,
          None,
          Some("The company"),
          None,
          queueId = Some(23)
        )
      }
    }

    "find list of CallbackRequest matching criteria" in new Helper {

      val uuid1          = UUID.randomUUID()
      val uuid2          = UUID.randomUUID()
      val listUuid: UUID = UUID.randomUUID()

      val criteria = FindCallbackRequest(
        List(
          DynamicFilter("listUuid", Some(OperatorEq), Some(listUuid.toString))
        )
      )
      val payload = Json.toJson(criteria)

      val expected = FindCallbackResponse(
        2,
        List(
          CallbackRequest(
            Some(uuid1),
            listUuid,
            Some("1000"),
            None,
            None,
            None,
            Some("The company"),
            None,
            queueId = Some(23)
          ),
          CallbackRequest(
            Some(uuid2),
            listUuid,
            Some("1000"),
            None,
            None,
            None,
            Some("The company"),
            None,
            queueId = Some(23)
          )
        )
      )

      val jsonResponse = Json.toJson(expected)

      stub(configWS.request(s"callback_requests/find", "POST", null))
        .toReturn(wsReq)
      stub(wsReq.withBody(eqFromMockito(payload))(any[BodyWritable[JsValue]]))
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))

      stub(wsResp.json).toReturn(jsonResponse)
      stub(wsResp.status).toReturn(OK)

      val f = requester.findCallbackRequest(criteria)

      whenReady(f)(res => {
        res.list should contain only (expected.list: _*)
        verify(configWS).request(s"callback_requests/find", "POST")
      })
    }

    "cloture a CallbackRequest" in new Helper {
      val uuid = UUID.randomUUID()

      stub(configWS.request(s"callback_requests/$uuid/cloture", "POST", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      whenReady(requester.clotureRequest(uuid)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/cloture", "POST")
      }
    }

    "uncloture a CallbackRequest" in new Helper {
      val uuid = UUID.randomUUID()

      stub(configWS.request(s"callback_requests/$uuid/uncloture", "POST", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      whenReady(requester.unclotureRequest(uuid)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/uncloture", "POST")
      }
    }

    "reschedule a CallbackRequest" in new Helper {
      import RescheduleCallback._

      val uuid = UUID.randomUUID()
      val reschedule = RescheduleCallback(
        DateTime.parse("2016-08-09"),
        "268ce207-7619-4d79-bcf7-27215e8636eb"
      )
      val json = Json.toJson(reschedule)

      stub(
        configWS.request(s"callback_requests/$uuid/reschedule", "POST", null)
      ).toReturn(wsReq)
      stub(wsReq.withBody(eqFromMockito(json))(any[BodyWritable[JsValue]]))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      whenReady(requester.rescheduleCallback(uuid, reschedule)) { res =>
        res shouldEqual ((): Unit)
        verify(configWS).request(s"callback_requests/$uuid/reschedule", "POST")
      }
    }

    "Get preferred callback period" in new Helper {
      val uuid           = UUID.randomUUID()
      val listUuid: UUID = UUID.randomUUID()
      val pid1           = UUID.randomUUID()
      val pid2           = UUID.randomUUID()

      val morning = PreferredCallbackPeriod(
        Some(pid1),
        "morning",
        new LocalTime(8, 0, 0),
        new LocalTime(11, 0, 0),
        default = true
      )
      val afternoon = PreferredCallbackPeriod(
        Some(pid2),
        "afternoon",
        new LocalTime(14, 0, 0),
        new LocalTime(17, 0, 0),
        default = false
      )

      val jsonResult = Json.parse(
        s"""[{"uuid": "$pid1", "name": "morning", "periodStart": "08:00:00.000", "periodEnd": "11:00:00.000", "default": true },
             |{"uuid": "$pid2", "name": "afternoon", "periodStart": "14:00:00.000", "periodEnd": "17:00:00.000", "default": false }
             |]""".stripMargin
      )

      val periods = List(morning, afternoon)

      stub(configWS.request(s"preferred_callback_periods", "GET", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(jsonResult)

      whenReady(requester.getPreferredCallbackPeriods()) { res =>
        res shouldEqual periods
      }

    }

    "retrieve all default membership" in new Helper {
      import UserQueueDefaultMembership._

      val expected = List(
        UserQueueDefaultMembership(
          10,
          List(QueueMembership(1, 7), QueueMembership(3, 4))
        ),
        UserQueueDefaultMembership(
          12,
          List(QueueMembership(2, 5), QueueMembership(3, 8))
        )
      )

      val jsonResponse = Json.toJson(expected)

      stub(configWS.request(s"user_membership/", "GET", null)).toReturn(wsReq)

      stub(wsResp.json).toReturn(jsonResponse)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      val f = requester.getAllDefaultMembership
      whenReady(f)(res => {
        res should contain only (expected: _*)
        verify(configWS).request(s"user_membership/", "GET")
      })
    }

    "set a User default membership" in new Helper {
      import QueueMembership._

      val userId     = 1234
      val membership = List(QueueMembership(1, 5), QueueMembership(3, 0))
      val json       = Json.toJson(membership)

      stub(configWS.request(s"user_membership/$userId", "POST", null))
        .toReturn(wsReq)

      stubReqOk(json)

      val f = requester.setUserDefaultMembership(userId, membership)
      whenReady(f)(res => {
        res shouldEqual ((): Unit)
        verify(configWS).request(s"user_membership/$userId", "POST")
      })
    }

    "set default membership for a list of users" in new Helper {
      import UsersQueueMembership._

      val userIds = List(1L, 2L, 3L, 4L)
      val membership = UsersQueueMembership(
        userIds,
        List(QueueMembership(1, 5), QueueMembership(3, 0))
      )
      val json = Json.toJson(membership)

      stub(configWS.request(s"user_membership/bulk", "POST", null))
        .toReturn(wsReq)

      stub(wsReq.withBody(eqFromMockito(json))(any[BodyWritable[JsValue]]))
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      val f = requester.setUsersDefaultMembership(membership)
      whenReady(f)(res => {
        res shouldEqual ((): Unit)
        verify(configWS).request(s"user_membership/bulk", "POST")
      })
    }

    "retrieve a CallQualification by queue" in new Helper {
      val queueId = 1L

      val sq = List(SubQualification(Some(1), "subqualif1"))
      val expected: List[CallQualification] = List(
        CallQualification(Some(1), "qualif1", sq),
        CallQualification(Some(2), "qualif2", sq)
      )
      val json = Json.toJson(expected)

      stub(configWS.request(s"call_qualification/queue/$queueId", "GET", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[List[CallQualification]] =
        requester.getCallQualifications(queueId)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"call_qualification/queue/$queueId", "GET")
      })
    }

    "request a csv with CallQualificationAnswer" in new Helper {
      val queueId             = 1L
      val fromRefTime: String = "2016-01-01 00:00:00"
      val toRefTime: String   = "2018-12-12 00:00:00"

      stub(
        configWS.request(
          s"call_qualification_answer/$queueId/$fromRefTime/$toRefTime",
          "GET",
          null
        )
      ).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.body).toReturn("csv")

      val f: Future[String] =
        requester.exportQualificationsCsv(queueId, fromRefTime, toRefTime)
      whenReady(f)(res => {
        res shouldEqual "csv"
        verify(configWS).request(
          s"call_qualification_answer/$queueId/$fromRefTime/$toRefTime",
          "GET"
        )
      })
    }

    "create a CallQualificationAnswer" in new Helper {
      val qualificationAnswer = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "some custom data"
      )
      val json     = Json.toJson(qualificationAnswer)
      val expected = 1L

      stub(configWS.request(s"call_qualification_answer", "POST", null))
        .toReturn(wsReq)

      stub(wsReq.withBody(eqFromMockito(json))(any[BodyWritable[JsValue]]))
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(Json.toJson(expected))

      val f = requester.createCallQualificationAnswer(qualificationAnswer)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"call_qualification_answer", "POST")
      })
    }

    "retrieve a single QueueConfigUpdate by queue id" in new Helper {
      val queueId = 1L

      val expected = QueueConfigUpdate(
        1,
        "queue",
        "queue",
        "1010",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val json = Json.toJson(expected)

      stub(configWS.request(s"queue_config/$queueId", "GET", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[QueueConfigUpdate] = requester.getQueueConfig(queueId)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"queue_config/$queueId", "GET")
      })
    }

    "retrieve all QueueConfigUpdate" in new Helper {
      val qf1 = QueueConfigUpdate(
        1,
        "q1",
        "Queue One",
        "1000",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      val qf2 = QueueConfigUpdate(
        2,
        "q2",
        "Queue Two",
        "2000",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      val expected = List(qf1, qf2)

      val json = Json.toJson(expected)

      stub(configWS.request(s"queue_config", "GET", null)).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[List[QueueConfigUpdate]] = requester.getQueueConfigAll
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"queue_config", "GET")
      })
    }

    "retrieve a single AgentConfig" in new Helper {
      val agentId = 1L

      val member = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        8
      )
      val expected = AgentConfigUpdate(
        1L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(member),
        1L,
        Some(1L)
      )

      val json = Json.toJson(expected)

      stub(configWS.request(s"agent_config/$agentId", "GET", null))
        .toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[AgentConfigUpdate] = requester.getAgentConfig(agentId)
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"agent_config/$agentId", "GET")
      })
    }

    "retrieve all AgentConfig" in new Helper {

      val m1 = QueueMember(
        "queue1",
        1L,
        "Agent/1001",
        1,
        0,
        "agent",
        1,
        "Agent",
        "queue",
        1
      )
      val m2 = QueueMember(
        "queue2",
        2L,
        "Agent/1002",
        2,
        0,
        "agent",
        2,
        "Agent",
        "queue",
        2
      )

      val a1 = AgentConfigUpdate(
        1L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(m1),
        1L,
        Some(1L)
      )
      val a2 = AgentConfigUpdate(
        2L,
        "firstname",
        "lastname",
        "1001",
        "default",
        List(m2),
        2L,
        Some(2L)
      )
      val expected = List(a1, a2)

      val json = Json.toJson(expected)

      stub(configWS.request(s"agent_config", "GET", null)).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[List[AgentConfigUpdate]] = requester.getAgentConfigAll
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"agent_config", "GET")
      })
    }

    "retrieve all MediaServerConfig" in new Helper {
      private val mds1 = MediaServerConfig(
        1,
        "mds1",
        "MDS 1",
        "123.123.123.123",
        read_only = false
      )
      private val mds2 = MediaServerConfig(
        2,
        "mds2",
        "MDS 2",
        "123.123.123.123",
        read_only = false
      )
      private val mdsList = List(mds1, mds2)

      private val json = Json.toJson(mdsList)

      stub(configWS.request(s"mediaserver", "GET", null)).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[List[MediaServerConfig]] = requester.getMediaServerConfigAll
      whenReady(f)(res => {
        res shouldEqual mdsList
        verify(configWS).request(s"mediaserver", "GET")
      })

    }

    "retrieve a MediaServerConfig" in new Helper {
      private val mds: MediaServerConfig = MediaServerConfig(
        1,
        "mds1",
        "MDS 1",
        "123.123.123.123",
        read_only = false
      )
      private val json: JsValue = Json.toJson(mds)

      stub(configWS.request(s"mediaserver/1", "GET", null)).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[MediaServerConfig] = requester.getMediaServerConfig(1)
      whenReady(f)(res => {
        res shouldEqual mds
        verify(configWS).request(s"mediaserver/1", "GET")
      })

    }

    "forward a request with body to configmgt" in new Helper {
      private val json: JsValue = Json.parse("""
          |{
          | "fileName": "queue1_fileName"
          |}
        """.stripMargin)

      private val returnedJson: JsValue = Json.parse("""
          |{
          | "id": 3,
          | "fileName" : "queue1_fileName"
          |}
        """.stripMargin)

      stub(
        configWS.request(
          s"queue/3/dissuasion/sound_file",
          "PUT",
          Some(json),
          "1.0"
        )
      ).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(returnedJson)

      val f: Future[WSResponse] =
        requester.forward("queue/3/dissuasion/sound_file", "PUT", Some(json))
      whenReady(f)(res => {
        verify(configWS)
          .request(s"queue/3/dissuasion/sound_file", "PUT", Some(json), "1.0")
      })
    }

    "get a user preference" in new Helper {
      val userId  = 1234
      val key     = "SOME_KEY"
      val value   = "some value"
      val pref    = UserPreference(userId, key, value, "String")
      val payload = UserPreferencePayload(None, pref.value, pref.valueType)
      val json    = Json.toJson(payload)

      stub(
        configWS.request(s"users/$userId/preferences/$key", "GET", None, "2.0")
      ).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[UserPreference] =
        requester.getUserPreference(userId, pref.key)
      whenReady(f)(res => {
        res shouldEqual pref
        verify(configWS)
          .request(s"users/$userId/preferences/$key", "GET", None, "2.0")
      })

    }

    "get all user preferences" in new Helper {
      val userId = 1234
      val userPreferences = List(
        UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"),
        UserPreference(userId, "Mobile_info", "true", "String")
      )
      val payload = userPreferences.map(up =>
        UserPreferencePayload(Some(up.key), up.value, up.valueType)
      )
      val json = Json.toJson(payload)

      stub(
        configWS.request(s"users/$userId/preferences", "GET", None, "2.0")
      ).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[List[UserPreference]] =
        requester.getUserPreferences(userId)
      whenReady(f)(res => {
        res shouldEqual userPreferences
        verify(configWS)
          .request(s"users/$userId/preferences", "GET", None, "2.0")
      })
    }

    "update a user preference" in new Helper {
      val userId  = 1234
      val key     = "SOME_KEY"
      val value   = "some value"
      val pref    = UserPreference(userId, key, value, "String")
      val payload = UserPreferencePayload(None, pref.value, pref.valueType)
      val json    = Json.toJson(payload)

      stub(
        configWS.request(
          s"users/$userId/preferences/$key",
          "PUT",
          Some(json),
          "2.0"
        )
      ).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)

      val f = requester.setUserPreference(pref)
      whenReady(f)(_ => {
        verify(configWS)
          .request(s"users/$userId/preferences/$key", "PUT", Some(json), "2.0")
      })

    }

    "create a user preference when update returns 404" in new Helper {
      val userId  = 1234
      val key     = "SOME_KEY"
      val value   = "some value"
      val pref    = UserPreference(userId, key, value, "String")
      val payload = UserPreferencePayload(None, pref.value, pref.valueType)
      val json    = Json.toJson(payload)

      stub(
        configWS.request(
          s"users/$userId/preferences/$key",
          "PUT",
          Some(json),
          "2.0"
        )
      ).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(NOT_FOUND)

      val wsReq2: WSRequest   = mock[WSRequest]
      val wsResp2: WSResponse = mock[WSResponse]
      stub(
        configWS.request(
          s"users/$userId/preferences/$key",
          "POST",
          Some(json),
          "2.0"
        )
      ).toReturn(wsReq2)
      stub(wsReq2.execute()).toReturn(Future.successful(wsResp2))
      stub(wsResp2.status).toReturn(NO_CONTENT)

      val f = requester.setUserPreference(pref)
      whenReady(f)(_ => {
        verify(configWS)
          .request(s"users/$userId/preferences/$key", "PUT", Some(json), "2.0")
        verify(configWS)
          .request(s"users/$userId/preferences/$key", "POST", Some(json), "2.0")
      })

    }

    "get a user services" in new Helper {
      val userId = 1234
      val payload = UserServices(
        false,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      val json = Json.toJson(payload)

      stub(configWS.request(s"users/$userId/services", "GET", None, "2.0"))
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[UserServices] = requester.getUserServices(userId)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS).request(s"users/$userId/services", "GET", None, "2.0")
      })

    }

    "update user services" in new Helper {
      val userId   = 1234
      val services = PartialUserServices(Some(true), None, None, None)
      val json     = Json.toJson(services)
      val response = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )
      val jsonResp = Json.toJson(response)

      stub(
        configWS.request(s"users/$userId/services", "PUT", Some(json), "2.0")
      ).toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(jsonResp)

      val f = requester.setUserServices(userId, services)
      whenReady(f)(_ => {
        verify(configWS)
          .request(s"users/$userId/services", "PUT", Some(json), "2.0")
      })

    }

    "retrieve Ice configuration" in new Helper {
      val expected = IceServer(Some("host:3478"))
      val json     = Json.toJson(expected)

      stub(configWS.request(s"sip/ice_servers", "GET", null)).toReturn(wsReq)

      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[IceServer] = requester.getIceServer
      whenReady(f)(res => {
        res shouldEqual expected
        verify(configWS).request(s"sip/ice_servers", "GET")
      })
    }

    "get token information for a static meeting room" in new Helper {
      val roomId = "42"
      val payload = MeetingRoomToken(
        "f1def7a5-546c-43d4-a8a1-6e2c9a9f7a1f",
        """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
          |eyJuYW1lIjoiaGVsbG9yIiwidXVpZCI6ImYxZGVmN2E1LTU0NmMtNDNkNC1hOGExLTZlMmM5YTlmN2ExZiJ9.
          |L6SJEhXVfokLd2X1GBp_2-66QD7H0ajr6XfC88JshLY""".stripMargin
      )

      val json = Json.toJson(payload)

      stub(
        configWS.request(s"meetingrooms/token/$roomId", "GET", None, "2.0")
      )
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[MeetingRoomToken] =
        requester.getMeetingRoomToken(roomId, Some(1L), StaticMeetingRoom)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(s"meetingrooms/token/$roomId", "GET", None, "2.0")
      })
    }

    "get token information for a personal meeting room" in new Helper {
      val roomId = "42"
      val userId = 1L
      val payload = MeetingRoomToken(
        "f1def7a5-546c-43d4-a8a1-6e2c9a9f7a1f",
        """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
          |eyJuYW1lIjoiaGVsbG9yIiwidXVpZCI6ImYxZGVmN2E1LTU0NmMtNDNkNC1hOGExLTZlMmM5YTlmN2ExZiJ9.
          |L6SJEhXVfokLd2X1GBp_2-66QD7H0ajr6XfC88JshLY""".stripMargin
      )

      val json = Json.toJson(payload)

      stub(
        configWS.request(
          s"meetingrooms/token/$roomId?userId=$userId",
          "GET",
          None,
          "2.0"
        )
      )
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[MeetingRoomToken] =
        requester.getMeetingRoomToken(roomId, Some(1), PersonalMeetingRoom)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(
            s"meetingrooms/token/$roomId?userId=$userId",
            "GET",
            None,
            "2.0"
          )
      })
    }

    "get token information for a temporary meeting room" in new Helper {
      val room = "User One"
      val payload = MeetingRoomToken(
        "f1def7a5-546c-43d4-a8a1-6e2c9a9f7a1f",
        """eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
          |eyJuYW1lIjoiaGVsbG9yIiwidXVpZCI6ImYxZGVmN2E1LTU0NmMtNDNkNC1hOGExLTZlMmM5YTlmN2ExZiJ9.
          |L6SJEhXVfokLd2X1GBp_2-66QD7H0ajr6XfC88JshLY""".stripMargin
      )

      val json = Json.toJson(payload)

      stub(
        configWS.request(
          s"meetingrooms/temporary/token/$room",
          "GET",
          None,
          "2.0"
        )
      )
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[MeetingRoomToken] =
        requester.getMeetingRoomToken(room, Some(1), TemporaryMeetingRoom)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(
            s"meetingrooms/temporary/token/$room",
            "GET",
            None,
            "2.0"
          )
      })
    }

    "get alias information for a meeting room" in new Helper {
      val roomId = "42"
      val payload = MeetingRoomAlias(
        Some("f1de-546c")
      )

      val json = Json.toJson(payload)

      stub(
        configWS.request(s"meetingrooms/alias/$roomId", "GET", None, "2.0")
      )
        .toReturn(wsReq)
      stub(wsReq.execute()).toReturn(Future.successful(wsResp))
      stub(wsResp.status).toReturn(OK)
      stub(wsResp.json).toReturn(json)

      val f: Future[MeetingRoomAlias] =
        requester.getMeetingRoomAlias(roomId)
      whenReady(f)(res => {
        res shouldEqual payload
        verify(configWS)
          .request(s"meetingrooms/alias/$roomId", "GET", None, "2.0")
      })
    }
  }

  "set mobile push notification token " in new Helper {
    val username  = "jdoe"
    val pushToken = MobileAppPushToken(Some("myToken"))
    val json      = Json.toJson(pushToken)

    stub(
      configWS.request(
        s"mobile/push/register/$username",
        "POST",
        Some(json),
        "2.0"
      )
    ).toReturn(wsReq)
    stub(wsReq.execute()).toReturn(Future.successful(wsResp))
    stub(wsResp.status).toReturn(OK)

    val f = requester.setMobileAppPushToken(username, pushToken)
    whenReady(f)(_ => {
      verify(configWS)
        .request(s"mobile/push/register/$username", "POST", Some(json), "2.0")
    })

  }

  "delete mobile push notification token " in new Helper {
    val username  = "jdoe"
    val pushToken = MobileAppPushToken(Some("myToken"))

    stub(
      configWS.request(s"mobile/push/register/$username", "DELETE", None, "2.0")
    )
      .toReturn(wsReq)
    stub(wsReq.execute()).toReturn(Future.successful(wsResp))
    stub(wsResp.status).toReturn(NO_CONTENT)

    val f = requester.deleteMobileAppPushToken(username)
    whenReady(f)(_ => {
      verify(configWS)
        .request(s"mobile/push/register/$username", "DELETE", None, "2.0")
    })

  }

  "checks that mobile application setup is valid for 204 HTTP response" in new Helper {

    stub(
      configWS.request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0"
      )
    ).toReturn(wsReq)

    stub(wsReq.execute()).toReturn(Future.successful(wsResp))
    stub(wsResp.status).toReturn(NO_CONTENT)

    whenReady(requester.checkValidMobileAppSetup) { res =>
      res shouldEqual MobileAppConfig(true)
      verify(configWS).request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0"
      )
    }
    requester.checkValidMobileAppSetup.futureValue shouldEqual MobileAppConfig(
      true
    )
  }

  "checks that mobile application setup is invalid for other HTTP response" in new Helper {

    stub(
      configWS.request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0"
      )
    ).toReturn(wsReq)

    stub(wsReq.execute()).toReturn(Future.successful(wsResp))
    stub(wsResp.status).toReturn(INTERNAL_SERVER_ERROR)

    whenReady(requester.checkValidMobileAppSetup) { res =>
      res shouldEqual MobileAppConfig(false)
      verify(configWS).request(
        s"mobile/push/check",
        "GET",
        None,
        "2.0"
      )
    }
    requester.checkValidMobileAppSetup.futureValue shouldEqual MobileAppConfig(
      false
    )
  }
}
