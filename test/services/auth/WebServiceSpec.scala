package services.auth

import akkatest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import akka.testkit.TestProbe
import xivo.xuc.XucBaseConfig
import xivo.services.XivoAuthentication
import models.ws.auth.AuthenticationInformation
import models.Token
import org.joda.time.DateTime
import org.mockito.Mockito.stub

class WebServiceSpec extends TestKitSpec("WebServiceSpec") with MockitoSugar {

  "WebService" should {

    trait Helper {

      val xivoAuth      = TestProbe()
      val xucBaseConfig = mock[XucBaseConfig]

      val auth = mock[xucBaseConfig.Authentication.type]
      stub(xucBaseConfig.Authentication).toReturn(auth)
      stub(xucBaseConfig.Authentication.secret).toReturn("secret")
      val webService =
        new WebService(xivoAuth.ref, xucBaseConfig)
    }

    "retrieve a xivo-auth token " in new Helper() {
      webService.authenticate("webservice", "rightpassword", 3600)
      xivoAuth.expectMsg(
        XivoAuthentication.GetWebServiceToken(
          "webservice",
          "rightpassword",
          3600
        )
      )
    }

    "transform a web service token into a web service authentication information" in new Helper() {
      val authToken = Token(
        "token",
        new DateTime(1680011247),
        new DateTime(1679924847),
        "webservice",
        None,
        List("xivo.user.read", "xivo.user.write")
      )
      val authenticationInformation = AuthenticationInformation(
        "wsuser",
        authToken.expiresAt.getMillis / 1000,
        authToken.issuedAt.getMillis / 1000,
        "webservice",
        List("xivo.user.read", "xivo.user.write"),
        None
      )
      webService.getAuthenticationInformation(
        "wsuser",
        authToken
      ) shouldBe authenticationInformation
    }

    "encode xivo-auth token to a JWT token" in new Helper() {
      val authenticationInformation = AuthenticationInformation(
        "wsuser",
        1680011247,
        1679924847,
        "webservice",
        List("xivo.user.read", "xivo.user.write"),
        None
      )
      val jwtToken =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6IndzdXNlciIsImV4cGlyZXNBdCI6MTY4MDAxMTI0NywiaXNzdWVkQXQiOjE2Nzk5MjQ4NDcsInVzZXJUeXBlIjoid2Vic2VydmljZSIsImFjbHMiOlsieGl2by51c2VyLnJlYWQiLCJ4aXZvLnVzZXIud3JpdGUiXX0.vLr8KsxKcUuxzBOXQHcsPgiTViJa27NHUFthJcJE9FA"
      webService.encodeToJWT(authenticationInformation) shouldBe jwtToken
    }

  }
}
