package services.chat

import java.time.OffsetDateTime

import org.scalatest.BeforeAndAfterEach
import play.api.libs.json.Json
import services.chat.model.{
  BrowserEventEnvelope,
  BrowserGetDirectMessageHistory,
  BrowserRequestEnvelope,
  BrowserSendDirectMessage,
  ChatUser,
  Message,
  RequestAck,
  RequestNack
}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class FlashTextBrowserMessageSpec
    extends AnyWordSpec
    with Matchers
    with BeforeAndAfterEach {

  "FlashTextBrowserMessage" should {
    "deserialize BrowserSendDirectMessage" in {
      val browserSendDirectMessage = Json.obj(
        "to"       -> Json.obj("username" -> "esevellec"),
        "message"  -> "Hello world",
        "sequence" -> 42
      )

      val decoded = browserSendDirectMessage.as[BrowserSendDirectMessage]
      decoded shouldBe BrowserSendDirectMessage("esevellec", "Hello world", 42)
    }

    "deserialize BrowserGetDirectMessageHistory" in {
      val browserGetDirectMessageHistory =
        Json.obj("to" -> Json.obj("username" -> "esevellec"), "sequence" -> 42)

      val decoded =
        browserGetDirectMessageHistory.as[BrowserGetDirectMessageHistory]
      decoded shouldBe BrowserGetDirectMessageHistory("esevellec", 42)
    }

    "deserialize BrowserRequestEnvelope" in {
      val browserSendDirectMessage = Json.obj(
        "to"       -> Json.obj("username" -> "esevellec"),
        "message"  -> "Hello world",
        "sequence" -> 42
      )

      val browserEnvelope = Json.obj(
        "request" -> "FlashTextDirectMessage"
      ) ++ browserSendDirectMessage

      val decoded = browserEnvelope.as[BrowserRequestEnvelope]
      decoded shouldBe BrowserRequestEnvelope(
        BrowserSendDirectMessage("esevellec", "Hello world", 42)
      )
    }

    "serialize BrowserEventEnvelope with RequestAck" in {
      val browserEventEnvelopeAck = BrowserEventEnvelope(
        RequestAck(
          20,
          date = OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
        )
      )

      val expected = Json.obj(
        "event"    -> "FlashTextSendMessageAck",
        "sequence" -> 20,
        "offline"  -> false,
        "date"     -> "2020-04-01T14:55:00+02:00"
      )
      Json.toJson(browserEventEnvelopeAck) shouldBe expected
    }

    "serialize BrowserEventEnvelope with RequestAck offline" in {
      val browserEventEnvelopeAck = BrowserEventEnvelope(
        RequestAck(
          20,
          true,
          OffsetDateTime.parse("2020-04-01T14:55:00.00+02:00")
        )
      )

      val expected = Json.obj(
        "event"    -> "FlashTextSendMessageAckOffline",
        "sequence" -> 20,
        "offline"  -> true,
        "date"     -> "2020-04-01T14:55:00+02:00"
      )
      Json.toJson(browserEventEnvelopeAck) shouldBe expected
    }

    "serialize BrowserEventEnvelope with RequestNAck" in {
      val browserEventEnvelopeAck = BrowserEventEnvelope(RequestNack(22))

      val expected =
        Json.obj("event" -> "FlashTextSendMessageNack", "sequence" -> 22)
      Json.toJson(browserEventEnvelopeAck) shouldBe expected
    }

    "serialize BrowserEventEnvelope with Message" in {
      val browserEventEnvelopeAck = BrowserEventEnvelope(
        Message(
          ChatUser("bruce", Some("1000"), Some("Bruce Willis"), None),
          ChatUser("alice", Some("1001"), Some("Alice Sample"), None),
          "How are you Alice ?",
          OffsetDateTime.parse("2019-05-28T14:55:08.628+02:00"),
          425
        )
      )

      val expected = Json.obj(
        "from" -> Json.obj(
          "username"    -> "bruce",
          "phoneNumber" -> "1000",
          "displayName" -> "Bruce Willis"
        ),
        "to" -> Json.obj(
          "username"    -> "alice",
          "phoneNumber" -> "1001",
          "displayName" -> "Alice Sample"
        ),
        "sequence" -> 425,
        "message"  -> "How are you Alice ?",
        "date"     -> "2019-05-28T14:55:08.628+02:00",
        "event"    -> "FlashTextUserMessage"
      )
      Json.toJson(browserEventEnvelopeAck) shouldBe expected
    }
  }
}
