package services.user

import akka.actor.{ActorRef, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.json.JSONObject
import org.mockito.Mockito.{stub, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import services.agent.AgentAction
import services.config.{ConfigRepository, ConfigServerRequester}
import services.line.PhoneController
import services.request._
import services.{CtiFilter, CtiRouter, XucEventBus, XucStatsEventBus}
import xivo.directory.PersonalContactRepository
import xivo.models.{PartialUserServices, UserForward}
import xivo.websocket.WsBus
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import scala.concurrent.ExecutionContext

class UserActionSpec
    extends TestKitSpec("CtiUserAction")
    with MockitoSugar
    with GuiceOneAppPerSuite
    with XucUserHelper {

  implicit lazy val executionContext = app.injector.instanceOf[ExecutionContext]

  val testCtiLink        = TestProbe()
  val messageFactory     = mock[MessageFactory]
  val eventBus           = mock[XucEventBus]
  val statEventBus       = mock[XucStatsEventBus]
  val wsBus              = mock[WsBus]
  val agentActionService = TestProbe()

  val phoneControllerFactory     = mock[PhoneController.Factory]
  val ctiFilterFactory           = mock[CtiFilter.Factory]
  val personalContactRepoFactory = mock[PersonalContactRepository.Factory]
  val configRepository           = mock[ConfigRepository]
  val agentActionFactory         = mock[AgentAction.Factory]
  val callbackManager            = TestProbe()
  val configDispatcher           = TestProbe()
  val callHistory                = TestProbe()
  val xivoDirectory              = TestProbe()
  val amiBusConnector            = TestProbe()
  val queueDispatcher            = TestProbe()
  val agentConfig                = TestProbe()
  val voiceMailManager           = TestProbe()
  val flashTextService           = TestProbe()
  val xucConfig                  = mock[XucBaseConfig]
  val clientLogger               = TestProbe()
  val videoEventManager          = TestProbe()
  val configServerRequester      = mock[ConfigServerRequester]
  val videoService               = TestProbe()
  val userPreferenceService      = TestProbe()

  class Helper {
    when(xucConfig.metricsRegistryName).thenReturn("TestRegistry")
    def actor() = {
      val a = TestActorRef[CtiRouter](
        Props(
          new CtiRouter(
            wsBus,
            eventBus,
            statEventBus,
            messageFactory,
            phoneControllerFactory,
            ctiFilterFactory,
            personalContactRepoFactory,
            configRepository,
            agentActionFactory,
            configServerRequester,
            callbackManager.ref,
            configDispatcher.ref,
            callHistory.ref,
            xivoDirectory.ref,
            amiBusConnector.ref,
            queueDispatcher.ref,
            voiceMailManager.ref,
            flashTextService.ref,
            videoService.ref,
            userPreferenceService.ref,
            agentConfig.ref,
            clientLogger.ref,
            videoEventManager.ref,
            xucConfig,
            getXucUser("test", "pwd")
          ) {
            override def createCtiLink(username: String): ActorRef =
              testCtiLink.ref
          }
        )
      )
      a.underlyingActor.agentActionService = agentActionService.ref
      (a, a.underlyingActor)
    }
  }

  "a Cti user action" should {
    "send unconditionnal forward to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, UncForward("3125", true))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(
          None,
          None,
          None,
          Some(UserForward(enabled = true, "3125"))
        )
      )
    }
    "Send na forward to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, NaForward("2356", false))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(
          None,
          None,
          Some(UserForward(enabled = false, "2356")),
          None
        )
      )

    }

    "Send busy forward to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, BusyForward("5699", true))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(
          None,
          Some(UserForward(enabled = true, "5699")),
          None,
          None
        )
      )
    }

    "send user status update request to ctilink" in new Helper {
      val userId           = 456
      val status           = "away"
      val (ref, ctiRouter) = actor()

      val json = new JSONObject()
      json.append("test", "userstatusupdate")

      stub(messageFactory.createUserAvailState(userId.toString, status))
        .toReturn((json))

      ref ! BaseRequest(
        TestProbe().ref,
        UserStatusUpdateReq(Some(userId), status)
      )

      verify(messageFactory).createUserAvailState(userId.toString, status)
      testCtiLink.expectMsg(json)

    }

    "send dnd request to ConfigServerRequester" in new Helper {
      val (ref, _) = actor()

      ref ! BaseRequest(TestProbe().ref, DndReq(true))

      verify(configServerRequester).setUserServices(
        1,
        PartialUserServices(Some(true), None, None, None)
      )
    }
  }

}
