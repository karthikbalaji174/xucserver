package services.agent

import akka.actor.Props
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import services.agent.AgentInGroupAction.RemoveAgents
import services.config.ConfigDispatcher._
import services.request.RemoveAgentFromQueue
import xivo.models.Agent

class AgentGroupRemoverSpec extends TestKitSpec("agentingroupremover") {

  class Helper {
    val configDispatcher = TestProbe()

    def actor(groupId: Long, queueId: Long, penalty: Int) = {
      system.actorOf(
        Props(
          new AgentGroupRemover(groupId, queueId, penalty, configDispatcher.ref)
        )
      )
    }
  }

  "an agent group remover" should {

    "remove agent group from queue penalty on RemoveAgents command" in new Helper {

      val (groupId, queueId, penalty) = (9, 52, 7)
      val ref                         = actor(groupId, queueId, penalty)

      val agents = List(Agent(1L, "Kim", "Notch", "4589", "browser", groupId))

      ref ! RemoveAgents

      ref ! AgentList(agents)

      configDispatcher.expectMsgAllOf(
        RequestConfig(ref, GetAgents(groupId, queueId, penalty)),
        ConfigChangeRequest(ref, RemoveAgentFromQueue(1L, queueId))
      )

    }
  }
}
