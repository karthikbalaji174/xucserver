package services.agent

import akka.actor.{Props, ReceiveTimeout}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import controllers.helpers.{RequestError, RequestSuccess, RequestTimeout}
import org.joda.time.DateTime
import org.scalatestplus.mockito.MockitoSugar
import services.XucEventBus
import services.request.{AgentLoginRequest, BaseRequest}
import xivo.events.AgentLoginError
import xivo.events.AgentState.AgentReady

class LoginProcessorSpec
    extends TestKitSpec("LoginProcessorSpec")
    with MockitoSugar {

  val eventBus      = mock[XucEventBus]
  val configManager = TestProbe()

  class Helper {
    def actor = {
      val a = TestActorRef[LoginProcessor](
        Props(new LoginProcessor(eventBus, configManager.ref))
      )
      (a, a.underlyingActor)
    }
  }

  "A LoginProcessor" should {
    "process Login requests" in new Helper {
      val (ref, _) = actor
      ref ! AgentLoginRequest(None, Some("1010"))
      configManager.expectMsg(
        BaseRequest(ref, AgentLoginRequest(None, Some("1010")))
      )
    }

    "process AgentLogin event" in new Helper {
      val (ref, processor) = actor
      val requester        = TestProbe()
      val request          = AgentLoginRequest(None, Some("1010"))
      processor.expectingResult(requester.ref, request)(
        AgentReady(11L, new DateTime(), "1010", List(1), agentNb = "2000")
      )
      requester.expectMsgClass(classOf[RequestSuccess])
    }

    "process AgentLoginError event" in new Helper {
      val (ref, processor) = actor
      val requester        = TestProbe()
      val request          = AgentLoginRequest(None, Some("1010"))
      processor.expectingResult(requester.ref, request)(
        AgentLoginError("testError")
      )
      requester.expectMsgClass(classOf[RequestError])
    }

    "forward RequestResult events" in new Helper {
      val (ref, processor) = actor
      val requester        = TestProbe()
      val request          = AgentLoginRequest(None, Some("1010"))
      val result           = RequestSuccess("testOK")
      processor.expectingResult(requester.ref, request)(result)
      requester.expectMsg(result)
    }

    "process ReceiveTimeout event in receive state" in new Helper {
      val (ref, processor) = actor
      val requester        = TestProbe()
      watch(ref)
      ref ! ReceiveTimeout
      expectTerminated(ref)
    }

    "process ReceiveTimeout event in expectingResult state" in new Helper {
      val (ref, processor) = actor
      val requester        = TestProbe()
      val request          = AgentLoginRequest(None, Some("1010"))
      processor.expectingResult(requester.ref, request)(ReceiveTimeout)
      requester.expectMsgClass(classOf[RequestTimeout])
    }
  }
}
