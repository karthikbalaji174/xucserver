package services.agent

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.mockito.Mockito.verify
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigDispatcher.RefreshAgent
import services.request.SetAgentGroup
import xivo.models.AgentFactory

class AgentGroupSetterSpec
    extends TestKitSpec("AgentGroupSetter")
    with MockitoSugar {

  class Helper {
    val configDispatcher = TestProbe()
    val agentGroup       = mock[AgentFactory]

    def actor = {
      val a = TestActorRef[AgentGroupSetter](
        Props(new AgentGroupSetter(agentGroup, configDispatcher.ref))
      )
      (a, a.underlyingActor)
    }
  }

  "An AgentGroupSetter" should {
    "use the agentGroup object to update agent group association" in new Helper {
      val (agentId, groupId) = (54, 3)
      val (ref, setter)      = actor
      val request            = SetAgentGroup(agentId, groupId)

      ref ! request

      verify(agentGroup).moveAgentToGroup(agentId, groupId)
      configDispatcher.expectMsg(RefreshAgent(agentId))
    }
  }

}
