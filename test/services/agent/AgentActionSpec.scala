package services.agent

import java.util.Date

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import controllers.helpers.{RequestError, RequestSuccess}
import org.joda.time.DateTime
import org.mockito.Mockito.{stub, verify}
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.IpbxCommandResponse
import services.XucAmiBus._
import services.XucEventBus
import services.config.ConfigRepository
import services.request._
import xivo.events.AgentState._
import xivo.events.CallDirection
import xivo.models.Agent
import xivo.websocket.WebSocketEvent

class AgentActionSpec extends TestKitSpec("AgentAction") with MockitoSugar {

  import xivo.models.LineHelper.makeLine

  val ctiLink            = TestProbe()
  val configDispatcher   = TestProbe()
  val mockMessageFactory = mock[MessageFactory]
  val messageFactory     = new MessageFactory()
  val configRepo         = mock[ConfigRepository]
  val amiBusConnector    = TestProbe()
  val eventBus           = mock[XucEventBus]

  class Helper {
    def actor = {
      val a = TestActorRef[AgentAction](
        Props(
          new AgentAction(
            ctiLink.ref,
            mockMessageFactory,
            configRepo,
            eventBus,
            amiBusConnector.ref
          )
        )
      )
      (a, a.underlyingActor)
    }
  }

  "agent action service " should {
    "send agent listen to cti link" in new Helper {

      val agentId = 56
      val userId  = 72

      stub(configRepo.getLineForUser(userId)).toReturn(
        Some(makeLine(34, "default", "sip", "oljjo", None, None, "ip"))
      )
      stub(configRepo.getLineForAgent(agentId)).toReturn(
        Some(makeLine(56, "default", "sip", "kvdsd", None, None, "ip"))
      )

      val (ref, _) = actor

      ref ! AgentListen(agentId, Some(userId))

      amiBusConnector.expectMsg(
        ListenRequest(
          ListenActionRequest(listener = "SIP/oljjo", listened = "SIP/kvdsd")
        )
      )

    }
    "send request agent login agentid phone number to cti link" in new Helper {
      val loginMsg = messageFactory.createAgentLogin("89", "756")
      stub(mockMessageFactory.createAgentLogin("89", "7560")).toReturn(loginMsg)
      stub(configRepo.getAgentLoggedOnPhoneNumber("7560")).toReturn(None)
      val (ref, _) = actor

      ref ! AgentLoginRequest(Some(89), Some("7560"))

      ctiLink.expectMsg(loginMsg)

    }
    "send agent logout request if a different agent is logged on the phone number before sending login request" in
      new Helper {
        val phoneNumber     = "2000"
        val alreadyLoggedId = 56
        val agentId         = 345
        val loginMsg =
          messageFactory.createAgentLogin(agentId.toString, phoneNumber)
        val logoutMsg =
          messageFactory.createAgentLogout(alreadyLoggedId.toString)
        stub(configRepo.getAgentLoggedOnPhoneNumber(phoneNumber))
          .toReturn(Some(alreadyLoggedId))
        stub(mockMessageFactory.createAgentLogout(alreadyLoggedId.toString))
          .toReturn(logoutMsg)
        stub(mockMessageFactory.createAgentLogin(agentId.toString, phoneNumber))
          .toReturn(loginMsg)

        val (ref, _) = actor

        ref ! AgentLoginRequest(Some(agentId), Some(phoneNumber))

        ctiLink.expectMsg(logoutMsg)
        ctiLink.expectMsg(loginMsg)
      }
    "send request agent login with only agentid to cti link" in new Helper {
      val agentId  = 78
      val loginMsg = messageFactory.createAgentLogin(agentId.toString, "")
      stub(mockMessageFactory.createAgentLogin(agentId.toString, ""))
        .toReturn(loginMsg)
      val (ref, _) = actor

      ref ! AgentLoginRequest(Some(agentId), None)

      ctiLink.expectMsg(loginMsg)
    }
    "send request agent login with only phone number to cti link" in new Helper {
      val phoneNumber = "44200"
      val loginMsg    = messageFactory.createAgentLogin(phoneNumber)
      stub(mockMessageFactory.createAgentLogin(phoneNumber)).toReturn(loginMsg)
      val (ref, _) = actor

      ref ! AgentLoginRequest(None, Some(phoneNumber))

      ctiLink.expectMsg(loginMsg)
    }
    "send back encoded agent request error" in new Helper {
      val (ref, _) = actor

      val agentRequestError =
        new IpbxCommandResponse("agent_login_invalid_exten", new Date)

      ref ! agentRequestError

      expectMsg(WebSocketEvent.createEvent(agentRequestError))
    }

    "send agent logout request on agent logout received" in new Helper {
      val (ref, _)  = actor
      val phoneNb   = "1011"
      val agentId   = 11
      val logoutMsg = messageFactory.createAgentLogout(agentId.toString)
      stub(configRepo.getAgentLoggedOnPhoneNumber(phoneNb))
        .toReturn(Some(agentId))
      stub(mockMessageFactory.createAgentLogout(agentId.toString))
        .toReturn(logoutMsg)

      ref ! BaseRequest(self, AgentLogout(phoneNb))

      ctiLink.expectMsg(logoutMsg)
    }

    "reject a base request with agent login request if the agent doesn't exist" in new Helper {
      val (ref, _) = actor
      val agentId  = 11L

      stub(configRepo.getAgent(agentId)).toReturn(None)

      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      expectMsgClass(classOf[RequestError])
    }

    "forward a base request with agent login request with phoneNb only to itself" in new Helper {
      val (ref, _) = actor
      val phoneNb  = "1011"
      val loginMsg = messageFactory.createAgentLogin(phoneNb)
      stub(mockMessageFactory.createAgentLogin(phoneNb)).toReturn(loginMsg)
      ref ! BaseRequest(self, AgentLoginRequest(None, Some(phoneNb)))
      verify(eventBus).subscribe(self, XucEventBus.allAgentsEventsTopic)
      ctiLink.expectMsg(loginMsg)
    }

    "reply to a base request with agent login request when the agent is already logged on" in new Helper {
      val (ref, _) = actor
      val agentId  = 14L
      val agent    = Agent(agentId, "first", "last", "number", "context")
      stub(configRepo.getAgent(agentId)).toReturn(Some(agent))
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(
          AgentReady(
            agentId,
            new DateTime,
            "phoneNb",
            List[Int](),
            None,
            "2000"
          )
        )
      )
      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      expectMsgClass(classOf[RequestSuccess])
    }

    "forward a base request with agent login request to itself when agentState is logged out" in new Helper {
      val (ref, _) = actor
      val agentId  = 14L
      val agent    = Agent(agentId, "first", "last", "number", "context")
      val loginMsg = messageFactory.createAgentLogin(agentId.toString, "")
      stub(configRepo.getAgent(agentId)).toReturn(Some(agent))
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(
          AgentLoggedOut(
            agentId,
            new DateTime,
            "phoneNb",
            List[Int](),
            "",
            None
          )
        )
      )
      stub(mockMessageFactory.createAgentLogin(agentId.toString, ""))
        .toReturn(loginMsg)
      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      ctiLink.expectMsg(loginMsg)
    }

    "forward a base request with agent login request to itself when agentState is unknown" in new Helper {
      val (ref, _) = actor
      val agentId  = 14L
      val agent    = Agent(agentId, "first", "last", "number", "context")
      val loginMsg = messageFactory.createAgentLogin(agentId.toString, "")
      stub(configRepo.getAgent(agentId)).toReturn(Some(agent))
      stub(configRepo.getAgentState(agentId)).toReturn(None)
      stub(mockMessageFactory.createAgentLogin(agentId.toString, ""))
        .toReturn(loginMsg)
      ref ! BaseRequest(self, AgentLoginRequest(Some(agentId), None))

      ctiLink.expectMsg(loginMsg)
    }

    "reject a base request with agent login request when agentNumber is unknown" in new Helper {
      val (ref, _) = actor
      val agentNb  = "2001"
      val phoneNb  = "1001"
      stub(configRepo.getAgent(agentNb)).toReturn(None)
      ref ! BaseRequest(
        self,
        AgentLoginRequest(None, Some(phoneNb), Some(agentNb))
      )

      expectMsgClass(classOf[RequestError])
    }

    "forward a base request with agent login request to itself when agentNumber is known" in new Helper {
      val (ref, _) = actor
      val agentNb  = "2001"
      val phoneNb  = "1001"
      val agentId  = 14L
      val agent    = Agent(agentId, "first", "last", agentNb, "context")
      val loginMsg = messageFactory.createAgentLogin(agentId.toString, phoneNb)
      stub(configRepo.getAgent(agentNb)).toReturn(Some(agent))
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(
          AgentLoggedOut(agentId, new DateTime, phoneNb, List[Int](), "", None)
        )
      )
      stub(configRepo.getAgentLoggedOnPhoneNumber(phoneNb)).toReturn(None)
      stub(mockMessageFactory.createAgentLogin(agentId.toString, phoneNb))
        .toReturn(loginMsg)

      ref ! BaseRequest(
        self,
        AgentLoginRequest(None, Some(phoneNb), Some(agentNb))
      )

      ctiLink.expectMsg(loginMsg)
    }

    "send agent logout request to cti link" in new Helper {
      val (ref, _) = actor
      val agentId  = 67L

      val logoutMsg = messageFactory.createAgentLogout(agentId.toString)
      stub(mockMessageFactory.createAgentLogout(agentId.toString))
        .toReturn(logoutMsg)

      ref ! AgentLogoutRequest(Some(agentId))

      ctiLink.expectMsg(logoutMsg)

    }

    "send agent pause request to ami bus connector" in new Helper {
      val (ref, _) = actor
      val agentId  = 67L
      val reason   = "testReason"

      ref ! AgentPauseRequest(Some(agentId), Some(reason))

      val request = amiBusConnector.expectMsgType[QueuePauseRequest]
      request.message match {
        case ar: QueuePauseActionRequest =>
          ar.iface shouldBe s"Local/id-$agentId@agentcallback"
          ar.reason shouldBe Some("testReason")
        case u => fail("expected QueuePauseAction, got " + u.getClass)
      }
    }
    "send agent unpause request to ami bus connector" in new Helper {
      val (ref, _) = actor
      val agentId  = 987L

      ref ! AgentUnPauseRequest(Some(agentId))

      val request = amiBusConnector.expectMsgType[QueueUnpauseRequest]
      request.message match {
        case ar: QueueUnpauseActionRequest =>
          ar.iface shouldBe s"Local/id-$agentId@agentcallback"
        case u => fail("expected QueueUnpauseAction, got " + u.getClass)
      }

    }
  }
  "agent action service on toggle pause received " should {
    class togglePauseHelper extends Helper {
      val (ref, _)   = actor
      val phoneNb    = "3400"
      val agentId    = 35
      val pauseMsg   = messageFactory.createPauseAgent(agentId.toString)
      val unpauseMsg = messageFactory.createUnpauseAgent(agentId.toString)
      stub(configRepo.getAgentLoggedOnPhoneNumber(phoneNb))
        .toReturn(Some(agentId))
      stub(mockMessageFactory.createPauseAgent(agentId.toString))
        .toReturn(pauseMsg)
      stub(mockMessageFactory.createUnpauseAgent(agentId.toString))
        .toReturn(unpauseMsg)

    }
    "pause agent if agent ready" in new togglePauseHelper {
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(AgentReady(agentId, null, phoneNb, List(), Some(""), "2000"))
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(pauseMsg)
    }
    "pause agent if agent on call ready" in new togglePauseHelper {
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(
          AgentOnCall(
            agentId,
            null,
            true,
            CallDirection.Incoming,
            phoneNb,
            List(),
            onPause = false,
            agentNb = "2000"
          )
        )
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(pauseMsg)
    }
    "un pause agent if agent on call on pause" in new togglePauseHelper {
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(
          AgentOnCall(
            agentId,
            null,
            true,
            CallDirection.Incoming,
            phoneNb,
            List(),
            onPause = true,
            agentNb = "2000"
          )
        )
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(unpauseMsg)
    }
    "set agent ready if agent on pause" in new togglePauseHelper {
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(AgentOnPause(agentId, null, phoneNb, List(), agentNb = "2000"))
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(unpauseMsg)
    }
    "set agent ready if agent on wrapup" in new togglePauseHelper {
      stub(configRepo.getAgentState(agentId)).toReturn(
        Some(AgentOnWrapup(agentId, null, phoneNb, List(), agentNb = "2000"))
      )

      ref ! BaseRequest(self, AgentTogglePause(phoneNb))

      ctiLink.expectMsg(unpauseMsg)
    }

  }
}
