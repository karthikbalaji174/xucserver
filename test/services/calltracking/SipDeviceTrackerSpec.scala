package services.calltracking

import org.joda.time.DateTime
import org.scalatest._
import akka.actor._
import akka.testkit._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar

import services.calltracking.AsteriskGraphTracker.{
  AsteriskPath,
  PathsFromChannel
}
import services.calltracking.ConferenceTracker._
import services.calltracking.SingleDeviceTracker._
import services.calltracking.graph._
import xivo.websocket._
import services.{XucAmiBus, XucEventBus}
import services.XucAmiBus._
import services.calltracking.DeviceConferenceAction._
import services.calltracking.graph.NodeBridge.BridgeCreator
import xivo.events.{CurrentCallsPhoneEvents, PhoneEvent}
import xivo.models.{Line, XivoFeature}
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models._

import scala.concurrent.duration._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class SipDeviceTrackerSpec
    extends TestKit(ActorSystem("SipDeviceTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {
  import xivo.models.LineHelper.makeLine

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val defaultConfig = new DeviceTrackerConfig {
    def stopRecordingUponExternalXfer: Boolean = true
    def enableRecordingRules: Boolean          = true
  }

  val defaultLine = makeLine(
    1,
    "default",
    "sip",
    "abcd",
    None,
    None,
    "123.123.123.1",
    number = Some("1007")
  )

  class DeviceActorWrapper(val feature: XivoFeature, stopRecording: Boolean) {
    val channelTracker   = TestProbe()
    val graphTracker     = TestProbe()
    val bus              = mock[XucEventBus]
    val configDispatcher = TestProbe()
    val amiBus           = mock[XucAmiBus]
    val parent           = TestProbe()

    val deviceTrackerConfig = new DeviceTrackerConfig {
      def stopRecordingUponExternalXfer: Boolean = stopRecording
      def enableRecordingRules: Boolean          = true
    }

    val factory = new DeviceActorFactoryImpl(
      channelTracker.ref,
      graphTracker.ref,
      bus,
      amiBus,
      deviceTrackerConfig,
      configDispatcher.ref
    )

    val device = TestActorRef[SingleDeviceTracker](
      factory.props(feature),
      parent.ref,
      "MySDTActor"
    )
  }

  def deviceWrapper(feature: XivoFeature, stopRecording: Boolean = true) =
    new DeviceActorWrapper(feature, stopRecording)

  "SipDeviceTracker" should {
    import BaseTracker._

    "have the tracker type SipDeviceTracker" in {
      val line    = mock[Line]
      val wrapper = deviceWrapper(line)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        SipDeviceTrackerType
      )
    }

    "watch graph for its interface for lines" in {
      val wrapper = deviceWrapper(defaultLine)
      wrapper.graphTracker.expectMsg(
        WatchChannelStartingWith(
          wrapper.device.underlyingActor.channelStartsWith
        )
      )
    }

    "watch channels for its interface for lines" in {
      val wrapper = deviceWrapper(defaultLine)
      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith(
          wrapper.device.underlyingActor.channelStartsWith
        )
      )
    }

    "notify with PhoneEvent when graph change" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

    }

    "prevent notification if nothing changed" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      wrapper.device ! channel

      wrapper.configDispatcher.expectNoMessage(100.millis)

    }

    "prevent phone event notification if info changed without impacting phone event result" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "SIP/efgh-0000001"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val remoteChannel = Channel(
        "123456789.124",
        remoteCname,
        CallerId("", ""),
        "",
        ChannelState.RINGING
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        sourceTrackerType
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      wrapper.device ! party

      wrapper.configDispatcher.expectNoMessage(100.millisecond)

    }

    "notify current calls when asked for" in {
      val wrapper = deviceWrapper(defaultLine)

      val c1name = "SIP/abcd-00000001"
      val channel1 = Channel(
        "123456789.123",
        c1name,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val call1       = DeviceCall(c1name, Some(channel1), Set.empty, Map.empty)
      val phoneEvent1 = DeviceCallToPhoneEvent(call1, defaultLine, identity).get

      val c2name = "SIP/abcd-00000002"
      val channel2 = Channel(
        "123456798.234",
        c2name,
        CallerId("Lucky Luke", "1313"),
        "",
        ChannelState.ORIGINATING
      )
      val call2       = DeviceCall(c2name, Some(channel2), Set.empty, Map.empty)
      val phoneEvent2 = DeviceCallToPhoneEvent(call2, defaultLine, identity).get

      wrapper.device ! channel1
      wrapper.configDispatcher.expectMsg(phoneEvent1)
      wrapper.device ! channel2
      wrapper.configDispatcher.expectMsg(phoneEvent2)

      wrapper.device ! SingleDeviceTracker.SendCurrentCallsPhoneEvents(
        defaultLine.interface
      )

      wrapper.configDispatcher.expectMsg(
        CurrentCallsPhoneEvents(
          defaultLine.number.get,
          List(phoneEvent1, phoneEvent2)
        )
      )
    }

    "send an empty CurrentCallsPhoneEvents when there's no call" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )
      val call       = DeviceCall(cname, Some(channel), Set.empty, Map.empty)
      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel
      wrapper.configDispatcher.expectMsg(phoneEvent)

      wrapper.device ! SingleDeviceTracker.SendCurrentCallsPhoneEvents(
        defaultLine.interface
      )
      wrapper.configDispatcher.expectMsg(
        CurrentCallsPhoneEvents(defaultLine.number.get, List())
      )
    }

    "subscribe to Conference when joining a conference" in {
      val wrapper = deviceWrapper(defaultLine)

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, "SIP/efgh-00001")

      expectMsg(SubscribeToConference("4000"))

    }

    "unsubscribe from Conference when leaving a conference" in {
      val wrapper = deviceWrapper(defaultLine)

      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceAvailable,
        None,
        List.empty,
        "default"
      )

      wrapper.device ! DeviceLeaveConference(conf, "SIP/efgh-00001")

      expectMsg(UnsubscribeConference("4000"))
    }

    "not unsubscribe from Conference when another participant leaves" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname1 = "SIP/abcd-00000001"
      val cname2 = "SIP/efgh-00000001"
      val channel = Channel(
        "123456789.123",
        cname1,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", defaultLine.number.get),
        DateTime.now
      )
      val p2 = ConferenceParticipant(
        "4000",
        2,
        "SIP/efgh-00000001",
        CallerId("John Doe", "1008"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(p1, p2),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname1)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      wrapper.device ! ParticipantLeaveConference("4000", p2)
      expectNoMessage((100.millisecond))
    }

    "transform DeviceConferenceMessage and send to XucAmiBus" in {
      val wrapper = deviceWrapper(defaultLine)
      val cname   = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val call       = DeviceCall(cname, Some(channel), Set.empty, Map.empty)
      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get
      wrapper.device ! channel
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )
      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val msg = DeviceConferenceMessage("SIP/abcd-00001", MuteMe("4000"))

      wrapper.device ! msg

      verify(wrapper.amiBus, timeout(500)).publish(
        AmiRequest(MeetMeMuteRequest("4000", 1), Some("default"))
      )

    }

    "send conference ami action to the MDS hosting the conference" in {
      val wrapper = deviceWrapper(defaultLine)
      val cname   = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val call       = DeviceCall(cname, Some(channel), Set.empty, Map.empty)
      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get
      wrapper.device ! channel
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "mds1"
      )
      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val msg = DeviceConferenceMessage("SIP/abcd-00001", MuteMe("4000"))

      wrapper.device ! msg

      verify(wrapper.amiBus, timeout(500)).publish(
        AmiRequest(MeetMeMuteRequest("4000", 1), Some("mds1"))
      )

    }

    "respond with error when DeviceConferenceMessage trigger an error" in {
      val wrapper = deviceWrapper(defaultLine)
      val cname   = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val call       = DeviceCall(cname, Some(channel), Set.empty, Map.empty)
      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get
      wrapper.device ! channel
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )
      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val msg = DeviceConferenceMessage("SIP/abcd-00001", Mute("4000", 2))

      wrapper.device ! msg

      expectMsg(NotOrganizer)

    }

    "send WsConferenceEvent to XucEventBus when joining a conference" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent);

    }

    "detect which participant is me in conference" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      wrapper.device ! channel
      wrapper.configDispatcher.expectMsgType[PhoneEvent]

      val participant = ConferenceParticipant(
        "4000",
        1,
        channel.name,
        channel.callerId,
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0,
        isMe = true
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent);

    }

    "detect which participant is me in conference when connected through mds" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "SIP/from-mds1-000002"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname, "mds1"),
        Set(
          AsteriskPath(
            NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
            NodeChannel("SIP/to-main-00000009", "mds1"),
            NodeMdsTrunkBridge("SIP_CALLID-123456"),
            NodeChannel(remoteCname, "main")
          )
        )
      )

      wrapper.device ! channel
      wrapper.device ! pfc
      wrapper.configDispatcher.expectMsgType[PhoneEvent]

      val participant = ConferenceParticipant(
        "4000",
        1,
        remoteCname,
        channel.callerId,
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0,
        isMe = true
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent);

    }

    "ensure WsConferenceEvent when joining is updated with latest conference information" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))

      val newParticipant = ConferenceParticipant(
        "4000",
        2,
        "SIP/ijkl-00002",
        CallerId("Jason Bourne", "1002"),
        DateTime.now
      )
      val updatedConf = conf.addParticipant(newParticipant)

      wrapper.device ! ConferenceSubscriptionAck(updatedConf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsNewParticipant = WsConferenceParticipant(
        newParticipant.index,
        newParticipant.callerId.name,
        newParticipant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant, wsNewParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)
    }

    "send WsConferenceEvent to XucEventBus when leaving a conference" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

      wrapper.device ! DeviceLeaveConference(conf, cname)
      expectMsg(UnsubscribeConference("4000"))
      wrapper.configDispatcher.expectMsg(
        wsConfEvent.copy(eventType = WsConferenceEventLeave)
      )
    }

    "reset conference when leaving a conference and administrator" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-0002"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      val p1 = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-0001",
        CallerId("Some  one", "1001"),
        DateTime.now,
        isMuted = true
      )
      val me = ConferenceParticipant(
        "4000",
        2,
        cname,
        CallerId("James Bond", "1007"),
        DateTime.now,
        role = OrganizerRole
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(p1, me),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      wrapper.device ! ParticipantLeaveConference(conf.number, me)
      verify(wrapper.amiBus, timeout(500)).publish(
        AmiRequest(MeetMeUnmuteRequest(conf.number, 1), Some("default"))
      )
      expectMsg(UnsubscribeConference("4000"))

    }

    "notify current conferences when asked for" in {
      val wrapper = deviceWrapper(defaultLine)

      val c1name = "SIP/abcd-00000001"
      val channel1 = Channel(
        "123456789.123",
        c1name,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val call1       = DeviceCall(c1name, Some(channel1), Set.empty, Map.empty)
      val phoneEvent1 = DeviceCallToPhoneEvent(call1, defaultLine, identity).get

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )
      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel1.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.device ! channel1
      wrapper.configDispatcher.expectMsg(phoneEvent1)

      wrapper.device ! DeviceJoinConference(conf, c1name)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)
      wrapper.configDispatcher.expectMsgType[WsConferenceEvent]

      reset(wrapper.bus)

      wrapper.device ! SingleDeviceTracker.SendCurrentCallsPhoneEvents(
        defaultLine.interface
      )
      wrapper.configDispatcher.expectMsg(
        CurrentCallsPhoneEvents(defaultLine.number.get, List(phoneEvent1))
      )
    }

    "send WsConferenceParticipantEvent to XucEventBus when a participant join a conference we are in" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      val participant = ConferenceParticipant(
        "4000",
        1,
        cname,
        CallerId("James Bond", "1007"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0,
        false,
        WsConferenceParticipantUserRole,
        false,
        true
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

      val newPart = ConferenceParticipant(
        "4000",
        2,
        "SIP/ijkl-000002",
        CallerId("Jason Bourne", "1001"),
        DateTime.now
      )
      wrapper.device ! ParticipantJoinConference("4000", newPart)

      val wsParticipantJoinEvent = WsConferenceParticipantEvent(
        WsConferenceParticipantEventJoin,
        channel.id,
        defaultLine.number.get,
        "4000",
        2,
        "Jason Bourne",
        "1001",
        0
      )

      wrapper.configDispatcher.expectMsg(wsParticipantJoinEvent)
    }

    "send WsConferenceParticipantEvent to XucEventBus when a participant leave a conference we are in" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )

      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      val phoneEvent = DeviceCallToPhoneEvent(call, defaultLine, identity).get

      wrapper.device ! channel

      wrapper.configDispatcher.expectMsg(phoneEvent)

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00000001",
        CallerId("James Bond", defaultLine.number.get),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, cname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0,
        false,
        WsConferenceParticipantUserRole,
        false,
        true
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

      wrapper.device ! ParticipantLeaveConference("4000", participant)
      expectMsg(UnsubscribeConference("4000"))

      val wsParticipantJoinEvent = WsConferenceParticipantEvent(
        WsConferenceParticipantEventLeave,
        channel.id,
        defaultLine.number.get,
        "4000",
        1,
        "James Bond",
        defaultLine.number.get,
        0
      )

      wrapper.configDispatcher.expectMsg(wsParticipantJoinEvent)
    }

    "register to trackers when asked to monitor a specific outbound number" in {
      val wrapper    = deviceWrapper(defaultLine)
      val mobile     = "0612345678"
      val localIface = s"Local/$mobile@"

      // Ignore first messages
      wrapper.channelTracker.expectMsgType[WatchChannelStartingWith]
      wrapper.graphTracker.expectMsgType[WatchChannelStartingWith]
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! SipDeviceTracker.WatchOutboundCallTo(
        defaultLine.interface,
        mobile
      )

      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(localIface))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(localIface))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(localIface, wrapper.device)
      )
    }

    "unregister to trackers when asked to unmonitor a specific outbound number" in {
      val wrapper    = deviceWrapper(defaultLine)
      val mobile     = "0612345678"
      val localIface = s"Local/$mobile@"

      // Ignore first messages
      wrapper.channelTracker.expectMsgType[WatchChannelStartingWith]
      wrapper.graphTracker.expectMsgType[WatchChannelStartingWith]
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]
      wrapper.device ! SipDeviceTracker.WatchOutboundCallTo(
        defaultLine.interface,
        mobile
      )
      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(localIface))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(localIface))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(localIface, wrapper.device)
      )

      wrapper.device ! SipDeviceTracker.UnWatchOutboundCallTo(
        defaultLine.interface,
        mobile
      )

      wrapper.channelTracker.expectMsg(UnWatchChannelStartingWith(localIface))
      wrapper.graphTracker.expectMsg(UnWatchChannelStartingWith(localIface))
      wrapper.parent.expectMsg(
        DevicesTracker.UnRegisterActor(localIface, wrapper.device)
      )
    }

    "unregister trackers when asked to monitor a new specific outbound number" in {
      val wrapper     = deviceWrapper(defaultLine)
      val mobile1     = "0612345678"
      val localIface1 = s"Local/$mobile1@"
      val mobile2     = "0612345679"
      val localIface2 = s"Local/$mobile2@"

      // Ignore first messages
      wrapper.channelTracker.expectMsgType[WatchChannelStartingWith]
      wrapper.graphTracker.expectMsgType[WatchChannelStartingWith]
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]
      wrapper.device ! SipDeviceTracker.WatchOutboundCallTo(
        defaultLine.interface,
        mobile1
      )
      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(localIface1))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(localIface1))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(localIface1, wrapper.device)
      )

      wrapper.device ! SipDeviceTracker.WatchOutboundCallTo(
        defaultLine.interface,
        mobile2
      )

      wrapper.channelTracker.expectMsg(UnWatchChannelStartingWith(localIface1))
      wrapper.graphTracker.expectMsg(UnWatchChannelStartingWith(localIface1))
      wrapper.parent.expectMsg(
        DevicesTracker.UnRegisterActor(localIface1, wrapper.device)
      )

      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(localIface2))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(localIface2))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(localIface2, wrapper.device)
      )
    }

    "filter out localchannel second leg" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = "Local/0612345678@default-00000001;2"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "not filter out localchannel first leg" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = "Local/0612345678@default-00000001;1"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(cname, Some(c), Set.empty, Map.empty))
        )
      )
    }

    "filter out localchannel second leg in path information" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname       = "Local/0612345678@default-00000001;2"
      val remoteCname = "SIP/abcd-00000001"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname),
        Set(AsteriskPath(NodeLocalChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "not filter out localchannel first leg in path information" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname       = "Local/0612345678@default-00000001;1"
      val remoteCname = "SIP/abcd-00000001"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname),
        Set(AsteriskPath(NodeLocalChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(cname, None, pfc.paths, Map.empty))
        )
      )
    }

    "clear calls from mobile number when unwatching mobile number" in {
      val mobile = "0612345678"
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = s"Local/$mobile@default-00000001;1"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! SipDeviceTracker.WatchOutboundCallTo(
        defaultLine.interface,
        mobile
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls
      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(cname, Some(c), Set.empty, Map.empty))
        )
      )

      wrapper.device ! SipDeviceTracker.UnWatchOutboundCallTo(
        defaultLine.interface,
        mobile
      )

      wrapper.device ! SingleDeviceTracker.GetCalls
      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }
  }
}
