package services.calltracking

import org.joda.time.DateTime

import services.calltracking.AsteriskGraphTracker.PathsFromChannel
import services.calltracking.ConferenceTracker._
import services.calltracking.SingleDeviceTracker._
import services.calltracking.graph.NodeLocalBridge
import xivo.websocket._
import services.calltracking.graph.NodeBridge.BridgeCreator
import xivo.events.PhoneEvent
import xivo.xucami.models._

class CustomDeviceTrackerSpec extends SipDeviceTrackerSpec {
  import xivo.models.LineHelper.makeLine

  override val defaultLine = makeLine(
    1,
    "default",
    "custom",
    "Local/01230041302@default/n",
    None,
    None,
    "123.123.123.1",
    number = Some("1010")
  )

  "CustomDeviceTracker" should {
    import BaseTracker._

    def makePath(lcname: String, realChannel: String): PathsFromChannel =
      PathsFromChannel(
        NodeLocalChannel(lcname + ";2"),
        Set(
          List(
            NodeLocalBridge(
              NodeLocalChannel(lcname + ";1"),
              NodeLocalChannel(lcname + ";2")
            ),
            NodeLocalChannel(lcname + ";1"),
            NodeBridge("b2", Some(new BridgeCreator("bc2"))),
            NodeChannel("SIP/abcd-0001")
          ),
          List(
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(realChannel)
          )
        )
      )

    "ignore local channel" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname = "Local/01230041302@default-00001;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      wrapper.device ! channel

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "watch real channel based on path received" in {
      val wrapper = deviceWrapper(defaultLine)

      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.graphTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(defaultLine.interface, wrapper.device)
      )

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      wrapper.device ! path
      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(realChannel, wrapper.device)
      )
    }

    "user real channel info to monitor calls" in {
      val wrapper = deviceWrapper(defaultLine)

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      val channel = Channel(
        "123456789.123",
        realChannel,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! channel
      wrapper.device ! GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(realChannel, Some(channel), Set.empty, Map.empty))
        )
      )
    }

    "unwatch real channel when channel is hangup" in {
      val wrapper = deviceWrapper(defaultLine)

      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.graphTracker.expectMsg(
        WatchChannelStartingWith(defaultLine.interface)
      )
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(defaultLine.interface, wrapper.device)
      )

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      wrapper.device ! path
      wrapper.channelTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.graphTracker.expectMsg(WatchChannelStartingWith(realChannel))
      wrapper.parent.expectMsg(
        DevicesTracker.RegisterActor(realChannel, wrapper.device)
      )

      val channel = Channel(
        "123456789.123",
        realChannel,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      wrapper.device ! channel

      wrapper.channelTracker.expectMsg(UnWatchChannelStartingWith(realChannel))
      wrapper.graphTracker.expectMsg(UnWatchChannelStartingWith(realChannel))
      wrapper.parent.expectMsg(
        DevicesTracker.UnRegisterActor(realChannel, wrapper.device)
      )

    }

    "remove call from list when channel is hangup (ensure SipDeviceTracker behavior is maintained)" in {
      val wrapper = deviceWrapper(defaultLine)

      val lcname      = "Local/01230041302@default-00001"
      val realChannel = "SIP/trunk-something-0001"
      val path        = makePath(lcname, realChannel)

      val channel = Channel(
        "123456789.123",
        realChannel,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! channel
      wrapper.device ! channel.copy(state = ChannelState.HUNGUP)
      wrapper.device ! GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "rewrite DeviceJoinConference to be attached to the correct channel" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname  = "SIP/abcd-00000001"
      val lcname = "Local/1000@default-0001;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val remoteChannel = Channel(
        "123454321.001",
        lcname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        CustomDeviceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! party
      wrapper.configDispatcher.expectMsgType[PhoneEvent]

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, lcname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

    }

    "rewrite DeviceLeaveConference to be attached to the correct channel" in {
      val wrapper = deviceWrapper(defaultLine)

      val cname  = "SIP/abcd-00000001"
      val lcname = "Local/1000@default-0001;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val remoteChannel = Channel(
        "123454321.001",
        lcname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        remoteChannel,
        CustomDeviceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! party
      wrapper.configDispatcher.expectMsgType[PhoneEvent]

      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/efgh-00001",
        CallerId("James Bond", "1001"),
        DateTime.now
      )
      val conf = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default"
      )

      wrapper.device ! DeviceJoinConference(conf, lcname)
      expectMsg(SubscribeToConference("4000"))
      wrapper.device ! ConferenceSubscriptionAck(conf)

      val wsParticipant = WsConferenceParticipant(
        participant.index,
        participant.callerId.name,
        participant.callerId.number,
        0
      )
      val wsConfEvent = WsConferenceEvent(
        WsConferenceEventJoin,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )

      wrapper.configDispatcher.expectMsg(wsConfEvent)

      wrapper.device ! DeviceLeaveConference(conf, lcname)
      val wsConfLeaveEvent = WsConferenceEvent(
        WsConferenceEventLeave,
        channel.id,
        defaultLine.number.get,
        conf.number,
        conf.name,
        List(wsParticipant),
        0
      )
      wrapper.configDispatcher.expectMsg(wsConfLeaveEvent)
    }
  }
}
