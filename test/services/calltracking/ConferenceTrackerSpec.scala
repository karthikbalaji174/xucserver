package services.calltracking

import akka.actor._
import akka.testkit._
import org.asteriskjava.manager.action.MuteAudioAction
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.ManagerResponse
import org.joda.time.{DateTime, Seconds}
import org.mockito.Mockito._
import org.scalatest._
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.{AmiAction, AmiResponse, AmiType}
import services.calltracking.BaseTracker._
import services.calltracking.ConferenceTracker._
import services.{AmiEventHelper, XucAmiBus}
import xivo.models.{ConferenceFactory, MeetMeConference}
import xivo.xuc.TransferConfig
import xivo.xucami.models.{CallerId, Channel, ChannelState}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class ConferenceTrackerSpec
    extends TestKit(ActorSystem("CallTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AmiEventHelper {

  type LeaveOrJoin = (ActorRef, String, Int, String, String, String) => Unit

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  def requestConf(
      testRef: ActorRef,
      confNo: String,
      maxIteration: Int = 5
  ): ConferenceRoom = {
    require(maxIteration > 1)

    val conf = LazyList
      .range(1, maxIteration)
      .map(i => {
        testRef ! GetConference(confNo)
        receiveOne(100.millis) match {
          case null => None
          case x    => Some(x)
        }
      })
      .dropWhile(_.isEmpty)
      .head

    conf match {
      case Some(conf: ConferenceRoom) => conf
      case _ =>
        throw new java.lang.AssertionError(
          "assertion failed: Timeout: No ConferenceRoom received"
        )
    }
  }

  def leaveOrJoin(amiEvent: AbstractMeetMeEvent, mds: String): LeaveOrJoin = {
    (ref, channel, index, callerName, callerNum, confNo) =>
      amiEvent.setChannel(channel)
      amiEvent.setUser(index)
      amiEvent.setCallerIdName(callerName)
      amiEvent.setCallerIdNum(callerNum)
      amiEvent.setMeetMe(confNo)

      ref ! AmiEvent(amiEvent, mds)
  }

  def join: LeaveOrJoin  = leaveOrJoin(new MeetMeJoinEvent(this), "default")
  def leave: LeaveOrJoin = leaveOrJoin(new MeetMeLeaveEvent(this), "default")
  def joinMds(mds: String): LeaveOrJoin =
    leaveOrJoin(new MeetMeJoinEvent(this), mds)

  abstract class CreateTracker(defaultConf: Option[MeetMeConference] = None) {
    val xucAmiBus   = mock[XucAmiBus]
    val confFactory = mock[ConferenceFactory]
    val confList    = defaultConf.map(List(_)).getOrElse(List.empty)
    val xucConfig = new TransferConfig {
      def enableAmiTransfer             = true
      def amiTransferTimeoutSeconds     = 180
      def conferenceRefreshTimerSeconds = 150
    }
    val schedulerMock  = mock[Scheduler]
    val deviceTracker  = TestProbe()
    val channelTracker = TestProbe()
    when(confFactory.all()).thenReturn(Future.successful(confList))
    val ref = TestActorRef[ConferenceTracker](
      new ConferenceTracker(
        xucAmiBus,
        confFactory,
        deviceTracker.ref,
        channelTracker.ref,
        xucConfig
      ) {
        override def scheduler = schedulerMock
      }
    )
  }

  "ConferenceRoomTracker" should {

    val defaultConf = Some(MeetMeConference(1, 4, "My Conf", "4000", "default"))
    val defaultParticipant = ConferenceParticipant(
      "4000",
      4,
      "sip/abcd-001",
      CallerId("Erwan Sevellec", "1001"),
      DateTime.now
    )

    "subscribe to AmiEvent through the XucAmiBus" in new CreateTracker {
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiEvent)
    }

    "subscribe to AmiResponse through the XucAmiBus" in new CreateTracker {
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiResponse)
    }

    "load conference room from database" in new CreateTracker {
      verify(confFactory, timeout(500)).all()
    }

    "get conference by number" in new CreateTracker(defaultConf) {
      val expected = ConferenceRoom(
        "4000",
        "My Conf",
        ConferenceAvailable,
        None,
        List.empty,
        "default"
      )

      verify(confFactory, timeout(500)).all()
      requestConf(ref, "4000") shouldBe expected
    }

    "send an error message if conference is not found when requested" in new CreateTracker {
      verify(confFactory, timeout(500)).all()

      ref ! GetConference("4000")
      expectMsg(NoConferenceRoom("4000"))
    }

    "track participant in a dynamic conference" in new CreateTracker(None) {
      joinMds("mds1")(
        ref,
        "sip/abcd-001",
        4,
        "Erwan Sevellec",
        "1001",
        "dynamic-conf-no1"
      )

      val conf = requestConf(ref, "dynamic-conf-no1")
      conf.mds shouldBe ("mds1")
    }

    "track participant in a dynamic conference on a mds" in new CreateTracker(
      None
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "dynamic-conf-no1")

      val conf = requestConf(ref, "dynamic-conf-no1")
      conf.participants.size shouldBe (1)
      val p = conf.participants.head
      p.channelName shouldBe "sip/abcd-001"
      p.index shouldBe 4
      p.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }

    "track new participant in a conference and request associated channel" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      channelTracker.expectMsg(WatchChannelStartingWith("sip/abcd-001"))

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe (1)
      val p = conf.participants.head
      p.channelName shouldBe "sip/abcd-001"
      p.index shouldBe 4
      p.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      Seconds.secondsBetween(p.joinTime, DateTime.now).getSeconds should be < 5
    }

    "track a removed participant in a conference and unsubscribe from ChannelTracker" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      channelTracker.expectMsg(WatchChannelStartingWith("sip/abcd-001"))
      deviceTracker.expectMsgType[DeviceJoinConference]

      join(ref, "sip/abcd-002", 5, "Jp Thomasset", "1002", "4000")
      channelTracker.expectMsg(WatchChannelStartingWith("sip/abcd-002"))
      deviceTracker.expectMsgType[DeviceJoinConference]

      leave(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      channelTracker.expectMsg(UnWatchChannelStartingWith("sip/abcd-001"))
      deviceTracker.expectMsgType[DeviceLeaveConference]

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe (1)
      val p = conf.participants.head

      p.channelName shouldBe "sip/abcd-002"
      p.index shouldBe 5
      p.callerId shouldBe CallerId("Jp Thomasset", "1002")
    }

    "acknowledge subscription to conference" in new CreateTracker(defaultConf) {
      ref ! SubscribeToConference("4000")

      val evt = expectMsgType[ConferenceSubscriptionAck]
      evt.conference.number shouldBe "4000"
    }

    "notify subscribers of new participant in a conference" in new CreateTracker(
      defaultConf
    ) {
      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]

      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val evt = expectMsgType[ParticipantJoinConference]
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe ("sip/abcd-001")
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      Seconds
        .secondsBetween(evt.participant.joinTime, DateTime.now)
        .getSeconds should be < 5
    }

    "notify subscribers of removed participant in a conference" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      deviceTracker.expectMsgType[DeviceJoinConference]

      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]

      leave(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      deviceTracker.expectMsgType[DeviceLeaveConference]

      val evt = expectMsgType[ParticipantLeaveConference]
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe ("sip/abcd-001")
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
    }

    "notify subscribers when conference room become busy " in new CreateTracker(
      defaultConf
    ) {
      ref ! SubscribeToConferenceStatus("4000")

      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      expectMsg(ConferenceStatusChange("4000", ConferenceBusy))
    }

    "notify subscribers when conference room become available " in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")
      deviceTracker.expectMsgType[DeviceJoinConference]

      ref ! SubscribeToConferenceStatus("4000")

      leave(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      expectMsg(ConferenceStatusChange("4000", ConferenceAvailable))
    }

    "unsubscribe to conference" in new CreateTracker(defaultConf) {

      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]
      ref ! UnsubscribeConference("4000")

      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      expectNoMessage(500.millis)
    }

    "unsubscribe to conference status" in new CreateTracker(defaultConf) {

      ref ! SubscribeToConferenceStatus("4000")
      ref ! UnsubscribeConferenceStatus("4000")

      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      expectNoMessage(500.millis)
    }

    "notify DeviceTracker when a device join a conference" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val msg = deviceTracker.expectMsgType[DeviceJoinConference]
      msg.channel shouldBe "sip/abcd-001"
      msg.conference.number shouldBe "4000"
    }

    "notify DeviceTracker when a device leave a conference" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val joinMsg = deviceTracker.expectMsgType[DeviceJoinConference]
      joinMsg.channel shouldBe "sip/abcd-001"
      joinMsg.conference.number shouldBe "4000"

      leave(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val leaveMsg = deviceTracker.expectMsgType[DeviceLeaveConference]
      leaveMsg.channel shouldBe "sip/abcd-001"
      leaveMsg.conference.number shouldBe "4000"
    }

    "use the scheduler" in new CreateTracker(defaultConf) {
      verify(schedulerMock, timeout(500)).scheduleWithFixedDelay(
        xucConfig.conferenceRefreshTimerSeconds second,
        xucConfig.conferenceRefreshTimerSeconds second,
        ref,
        RefreshConference
      )(ref.underlyingActor.context.dispatcher, ref)
    }

    "reload conference room from database" in new CreateTracker(defaultConf) {
      verify(confFactory, timeout(5000)).all()
    }

    "update confList if a conf is removed" in new CreateTracker(defaultConf) {
      when(confFactory.all()).thenReturn(Future.successful(List.empty))
      ref ! RefreshConference
      ref ! GetConference("4000")
      expectMsg(NoConferenceRoom("4000"))
    }

    "update confList if a conf is add" in new CreateTracker(defaultConf) {
      val firstConf = MeetMeConference(1, 4, "My Conf", "4000", "default")
      val newConf   = MeetMeConference(2, 5, "My new Conf", "4001", "default")
      val expected  = List(firstConf, newConf)
      when(confFactory.all()).thenReturn(Future.successful(expected))
      ref ! RefreshConference
      requestConf(ref, "4001") shouldBe ConferenceRoom(newConf)
    }

    "udpate confList if a participant begin to speak" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val talkingEvent = new MeetMeTalkingEvent(this)
      talkingEvent.setChannel("sip/abcd-001")
      talkingEvent.setUser(4)
      talkingEvent.setCallerIdName("Erwan Sevellec")
      talkingEvent.setCallerIdNum("1001")
      talkingEvent.setMeetMe("4000")
      talkingEvent.setStatus(true)

      ref ! AmiEvent(talkingEvent)

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isTalking shouldBe true
    }

    "udpate confList if a participant stop to speak" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val talkingEventTrue = new MeetMeTalkingEvent(this)
      talkingEventTrue.setChannel("sip/abcd-001")
      talkingEventTrue.setUser(4)
      talkingEventTrue.setCallerIdName("Erwan Sevellec")
      talkingEventTrue.setCallerIdNum("1001")
      talkingEventTrue.setMeetMe("4000")
      talkingEventTrue.setStatus(true)

      ref ! AmiEvent(talkingEventTrue)

      val talkingEventFalse = new MeetMeTalkingEvent(this)
      talkingEventFalse.setChannel("sip/abcd-001")
      talkingEventFalse.setUser(4)
      talkingEventFalse.setCallerIdName("Erwan Sevellec")
      talkingEventFalse.setCallerIdNum("1001")
      talkingEventFalse.setMeetMe("4000")
      talkingEventFalse.setStatus(false)

      ref ! AmiEvent(talkingEventFalse)

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isTalking shouldBe false
    }

    "notify if a participant begin to speak" in new CreateTracker(defaultConf) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val talkingEvent = new MeetMeTalkingEvent(this)
      talkingEvent.setChannel("sip/abcd-001")
      talkingEvent.setUser(4)
      talkingEvent.setCallerIdName("Erwan Sevellec")
      talkingEvent.setCallerIdNum("1001")
      talkingEvent.setMeetMe("4000")
      talkingEvent.setStatus(true)

      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]

      ref ! AmiEvent(talkingEvent)

      val evt = expectMsgType[ParticipantUpdated]
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe "sip/abcd-001"
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      evt.participant.isTalking shouldBe true
    }

    "notify if a participant stop speaking" in new CreateTracker(defaultConf) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val talkingEventTrue = new MeetMeTalkingEvent(this)
      talkingEventTrue.setChannel("sip/abcd-001")
      talkingEventTrue.setUser(4)
      talkingEventTrue.setCallerIdName("Erwan Sevellec")
      talkingEventTrue.setCallerIdNum("1001")
      talkingEventTrue.setMeetMe("4000")
      talkingEventTrue.setStatus(true)

      ref ! AmiEvent(talkingEventTrue)

      val talkingEventFalse = new MeetMeTalkingEvent(this)
      talkingEventFalse.setChannel("sip/abcd-001")
      talkingEventFalse.setUser(4)
      talkingEventFalse.setCallerIdName("Erwan Sevellec")
      talkingEventFalse.setCallerIdNum("1001")
      talkingEventFalse.setMeetMe("4000")
      talkingEventFalse.setStatus(false)

      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]

      ref ! AmiEvent(talkingEventFalse)

      val evt = expectMsgType[ParticipantUpdated]
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe "sip/abcd-001"
      evt.participant.callerId shouldBe CallerId("Erwan Sevellec", "1001")
      evt.participant.isTalking shouldBe false
    }

    "do not notify of participant role change when user as it's the default" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val channel = Channel(
        "123456789.123",
        "sip/abcd-001",
        CallerId("Erwan Sevellec", "1001"),
        "",
        ChannelState.UP
      )
        .updateVariable("XIVO_MEETMEROLE", "USER")

      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]

      ref ! channel
      expectNoMessage(250.millis)
    }

    "notify of participant role change when user is detected as organizer" in new CreateTracker(
      defaultConf
    ) {
      val participant = defaultParticipant.copy(
        channelName = "sip/efgh-002",
        callerId = CallerId("James Bond", "1007")
      )
      join(ref, "sip/efgh-002", 4, "James Bond", "1007", "4000")

      val channel = Channel(
        "123456702.456",
        "sip/efgh-002",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
        .updateVariable("XIVO_MEETMEROLE", "ADMIN")

      ref ! SubscribeToConference("4000")
      expectMsgType[ConferenceSubscriptionAck]

      ref ! channel
      val evt = expectMsgType[ParticipantUpdated]
      evt.numConf shouldBe "4000"
      evt.participant.channelName shouldBe "sip/efgh-002"
      evt.participant.role shouldBe OrganizerRole
    }

    "udpate confList if a participant is muted" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val evt = new MeetMeMuteEvent(this)
      evt.setChannel("sip/abcd-001")
      evt.setUser(4)
      evt.setCallerIdName("Erwan Sevellec")
      evt.setCallerIdNum("1001")
      evt.setMeetMe("4000")
      evt.setStatus(true)

      ref ! AmiEvent(evt)

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isMuted shouldBe true
    }

    "udpate confList if a participant is UN-muted" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "Erwan Sevellec", "1001", "4000")

      val evt = new MeetMeMuteEvent(this)
      evt.setChannel("sip/abcd-001")
      evt.setUser(4)
      evt.setCallerIdName("Erwan Sevellec")
      evt.setCallerIdNum("1001")
      evt.setMeetMe("4000")
      evt.setStatus(true)
      ref ! AmiEvent(evt)

      evt.setStatus(false)
      ref ! AmiEvent(evt)

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isMuted shouldBe false
    }

    "udpate confList if a participant is deafened" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "James Bond", "1001", "4000")

      val action = new MuteAudioAction()
      action.setChannel("sip/abcd-001")
      action.setDirection(MuteAudioAction.Direction.OUT)
      action.setState(MuteAudioAction.State.MUTE)

      val evt = new ManagerResponse()
      evt.setResponse("Success")

      ref ! AmiResponse((evt, Some(AmiAction(action))))

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isDeaf shouldBe true
    }

    "udpate confList if a participant is UN-deafened" in new CreateTracker(
      defaultConf
    ) {
      join(ref, "sip/abcd-001", 4, "James Bond", "1001", "4000")

      val action = new MuteAudioAction()
      action.setChannel("sip/abcd-001")
      action.setDirection(MuteAudioAction.Direction.OUT)
      action.setState(MuteAudioAction.State.UNMUTE)

      val evt = new ManagerResponse()
      evt.setResponse("Success")

      ref ! AmiResponse((evt, Some(AmiAction(action))))

      val conf = requestConf(ref, "4000")
      conf.participants.size shouldBe 1
      conf.participants.head.isDeaf shouldBe false
    }
  }
}
