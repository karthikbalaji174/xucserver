package services.calltracking

import org.joda.time.DateTime
import org.scalatest._
import akka.actor._
import akka.testkit._
import org.scalatestplus.mockito.MockitoSugar

import services.calltracking.AsteriskGraphTracker.{
  AsteriskPath,
  PathsFromChannel
}
import services.calltracking.ConferenceTracker._
import services.calltracking.SingleDeviceTracker._
import services.calltracking.graph._
import services.{XucAmiBus, XucEventBus}
import services.calltracking.graph.NodeBridge.BridgeCreator
import xivo.models.{LocalChannelFeature, XivoFeature}
import xivo.xuc.DeviceTrackerConfig
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class UnknownDeviceTrackerSpec
    extends TestKit(ActorSystem("UnknownDeviceTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {

  private val xivoFeature = LocalChannelFeature

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val defaultConfig = new DeviceTrackerConfig {
    def stopRecordingUponExternalXfer: Boolean = true
    def enableRecordingRules: Boolean          = true
  }

  class DeviceActorWrapper(
      val feature: XivoFeature,
      stopRecording: Boolean,
      enableRules: Boolean
  ) {
    val channelTracker   = TestProbe()
    val graphTracker     = TestProbe()
    val bus              = mock[XucEventBus]
    val amiBus           = mock[XucAmiBus]
    val parent           = TestProbe()
    val configDispatcher = TestProbe()

    val deviceTrackerConfig = new DeviceTrackerConfig {
      def stopRecordingUponExternalXfer: Boolean = stopRecording
      def enableRecordingRules: Boolean          = enableRules
    }

    val factory = new DeviceActorFactoryImpl(
      channelTracker.ref,
      graphTracker.ref,
      bus,
      amiBus,
      deviceTrackerConfig,
      configDispatcher.ref
    )

    val device = TestActorRef[SingleDeviceTracker](
      factory.props(feature),
      parent.ref,
      "MySDTActor"
    )
  }

  def deviceWrapper(
      feature: XivoFeature,
      stopRecording: Boolean = true,
      enableRules: Boolean = true
  ) = new DeviceActorWrapper(feature, stopRecording, enableRules)

  "UnknownDeviceTrackerSpec" should {

    val conference = ConferenceRoom(
      "4000",
      "Superconf",
      ConferenceBusy,
      Some(DateTime.now),
      List.empty,
      "default"
    )

    "be of correct type" in {
      val wrapper = deviceWrapper(xivoFeature)
      wrapper.device.underlyingActor.deviceTrackerType should be(
        UnknownDeviceTrackerType
      )
    }

    "forward join conference event to correct channel" in {
      val cname1      = "Local/4000@default-00001;1"
      val cname2      = "Local/4000@default-00001;2"
      val remoteCname = "SIP/abcd-0000001"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2),
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname)
          )
        )
      )

      val wrapper = deviceWrapper(xivoFeature)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc

      wrapper.device ! DeviceJoinConference(conference, cname1)
      wrapper.parent.expectMsg(DeviceJoinConference(conference, remoteCname))
    }

    "forward leave conference event to correct channel" in {
      val cname1      = "Local/4000@default-00001;1"
      val cname2      = "Local/4000@default-00001;2"
      val remoteCname = "SIP/abcd-0000001"
      val pfc = PathsFromChannel(
        NodeLocalChannel(cname1),
        Set(
          AsteriskPath(
            NodeLocalBridge(NodeLocalChannel(cname1), NodeLocalChannel(cname2)),
            NodeLocalChannel(cname2),
            NodeBridge("b1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname)
          )
        )
      )

      val wrapper = deviceWrapper(xivoFeature)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc

      wrapper.device ! DeviceLeaveConference(conference, cname1)
      wrapper.parent.expectMsg(DeviceLeaveConference(conference, remoteCname))
    }
  }
}
