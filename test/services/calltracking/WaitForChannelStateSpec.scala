package services.calltracking

import org.mockito.Mockito
import org.scalatest._
import org.scalatest.concurrent._
import akka.actor._
import akka.testkit._
import akka.util.Timeout
import org.mockito.Matchers._
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.graph._
import services.calltracking.BaseTracker._
import scala.concurrent.{Future, Promise}
import scala.concurrent.duration._
import xivo.xucami.models.{CallerId, Channel, ChannelState}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class WaitForChannelStateSpec
    extends TestKit(ActorSystem("WaitForChannelState"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with ScalaFutures
    with AsteriskObjectHelper {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "WaitForChannelState" should {
    "subscribe to channel state of given channel" in {
      val channelTracker = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelStateResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelState(
            channelTracker.ref,
            channel,
            ChannelState.UP,
            promise
          )
        )
      )

      channelTracker.expectMsg(WatchChannelStartingWith(channel.name))
    }

    "unsubscribe to channel state of given channel when finishing" in {
      val channelTracker = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelStateResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelState(
            channelTracker.ref,
            channel,
            ChannelState.UP,
            promise
          )
        )
      )

      channelTracker.expectMsg(WatchChannelStartingWith(channel.name))

      actor ! PoisonPill

      channelTracker.expectMsg(UnWatchChannelStartingWith(channel.name))
    }

    "complete the promise with success of ChannelStateOk when given channel is in expected state" in {
      val channelTracker = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelStateResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelState(
            channelTracker.ref,
            channel,
            ChannelState.UP,
            promise
          )
        )
      )

      actor ! Channel(
        "123456789.123",
        channel.name,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      whenReady(promise.future) { r =>
        r should be(ChannelStateOk)
      }

    }

    "complete the promise with a failure of ChannelStateKo when given channel is hung up" in {

      val channelTracker = TestProbe()

      val channel = NodeChannel("SIP/abcd-000001")
      val promise = Promise[ChannelStateResult]()
      val actor = system.actorOf(
        Props(
          new WaitForChannelState(
            channelTracker.ref,
            channel,
            ChannelState.UP,
            promise
          )
        )
      )

      actor ! Channel(
        "123456789.123",
        channel.name,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      whenReady(promise.future.failed) { e =>
        e should be(ChannelStateKo)
      }
    }

    "be wrapped in a future" in {
      val channelTracker   = TestProbe()
      val channel          = NodeChannel("SIP/abcd-000001")
      val dummyActor       = TestProbe()
      val promise          = Promise[ChannelStateResult]()
      implicit val context = mock[ActorContext]

      Mockito.when(context.actorOf(any[Props])).thenReturn(dummyActor.ref)
      Mockito.when(context.system).thenReturn(system)
      Mockito.when(context.dispatcher).thenReturn(system.dispatcher)

      val f = WaitForChannelState(channelTracker.ref, channel, ChannelState.UP)

      f shouldBe a[Future[_]]
    }

    "handle timeout in the wrapping future" in {
      val channelTracker = TestProbe()
      val testProbe      = TestProbe()
      val channel        = NodeChannel("SIP/abcd-000001")
      val promise        = Promise[ChannelStateResult]()

      class MyExecutor(ref: ActorRef) extends Actor {
        import akka.pattern.pipe
        implicit val ec = context.dispatcher

        pipe(
          WaitForChannelState(
            channelTracker.ref,
            channel,
            ChannelState.UP,
            Timeout(10.millis)
          )
        ) to self

        override def receive = { case akka.actor.Status.Failure(t) =>
          ref ! t
        }
      }

      val a = system.actorOf(Props(new MyExecutor(testProbe.ref)))

      testProbe.expectMsg(ChannelStateTimeout(channel.name))

    }

  }

}
