package services.calltracking

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import org.joda.time.DateTime
import org.mockito.Mockito
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import services.calltracking.BaseTracker.{
  UnWatchChannelStartingWith,
  WatchChannelStartingWith
}
import services.calltracking.ChannelTracker.{GetChannel, NoSuchChannel}
import services.calltracking.RetrieveUtil.RetrieveQueueCallContext
import xivo.phonedevices.DeviceAdapter
import xivo.xucami.models._
import xuctest.ChannelGen
import org.scalatest.wordspec.AnyWordSpecLike

class RetrieveUtilSpec
    extends TestKit(ActorSystem("RetrieveUtilSpec"))
    with AnyWordSpecLike
    with MockitoSugar
    with BeforeAndAfterAll
    with ScalaFutures
    with ChannelGen {

  import xivo.models.LineHelper.makeLine

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper() {
    val deviceAdapter  = mock[DeviceAdapter]
    val channelTracker = TestProbe()
    val sender         = TestProbe()
    val lineNumber     = "1000"
    val lineUser       = "User One"
    val agentNumber    = "8000"
    val line = makeLine(
      1,
      "default",
      "sip",
      "abcd",
      None,
      None,
      "123.123.123.1",
      number = Some(lineNumber)
    )
    val queueCall = QueueCall(
      1,
      Some("Graham Bell"),
      "12345",
      DateTime.now,
      "SIP/defg",
      "main"
    )

    val retrieveUtilImpl = new RetrieveUtil(channelTracker.ref)

    class ExecuteIt(ref: ActorRef, deviceCalls: List[DeviceCall])
        extends Actor {

      import akka.pattern.pipe

      implicit val ec = context.dispatcher

      val retrieveContext = RetrieveQueueCallContext(
        line,
        queueCall,
        Map.empty,
        Some(agentNumber),
        lineUser
      )

      retrieveUtilImpl
        .processRetrieve(deviceCalls, retrieveContext, deviceAdapter, ref)
        .pipeTo(self)

      override def receive = { case _ =>
      }
    }

    def processRetrieve(
        ref: ActorRef,
        deviceCalls: List[DeviceCall]
    ): ActorRef = {
      system.actorOf(Props(new ExecuteIt(ref, deviceCalls)))
    }
  }

  "RetrieveUtil" should {
    "retrieve queue call" in new Helper {
      val channel          = bchan(queueCall.channel)
      val emptyDeviceCalls = List()

      processRetrieve(sender.ref, emptyDeviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      verify(deviceAdapter, Mockito.timeout(2000)).retrieveQueueCall(
        line.interface,
        line.number,
        lineUser,
        queueCall,
        Map.empty,
        sender.ref,
        channel.id,
        channel.lastQueueName,
        Some(agentNumber),
        autoAnswer = true,
        line.driver
      )
    }

    "retrieve queue call if another call is RINGING" in new Helper {
      val channel = bchan(queueCall.channel)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      val deviceCalls = List(
        DeviceCall(call1Channel.name, Some(call1Channel), Set(), Map.empty)
      )

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      channelTracker.expectMsg(WatchChannelStartingWith(call1Channel.name))
      channelTracker.reply(call1Channel.copy(state = ChannelState.HUNGUP))
      channelTracker.expectMsg(UnWatchChannelStartingWith(call1Channel.name))

      verify(deviceAdapter, Mockito.timeout(2000)).hangup(sender.ref)
      verify(deviceAdapter, Mockito.timeout(2000)).retrieveQueueCall(
        line.interface,
        line.number,
        lineUser,
        queueCall,
        Map.empty,
        sender.ref,
        channel.id,
        channel.lastQueueName,
        Some(agentNumber),
        autoAnswer = true,
        line.driver
      )
    }

    "retrieve queue call if another call is UP" in new Helper {
      val channel = bchan(queueCall.channel)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val deviceCalls = List(
        DeviceCall(call1Channel.name, Some(call1Channel), Set(), Map.empty)
      )

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      channelTracker.expectMsg(WatchChannelStartingWith(call1Channel.name))
      channelTracker.reply(call1Channel.copy(state = ChannelState.HOLD))
      channelTracker.expectMsg(UnWatchChannelStartingWith(call1Channel.name))

      verify(deviceAdapter, Mockito.timeout(2000)).hold(None, sender.ref)
      verify(deviceAdapter, Mockito.timeout(2000)).retrieveQueueCall(
        line.interface,
        line.number,
        lineUser,
        queueCall,
        Map.empty,
        sender.ref,
        channel.id,
        channel.lastQueueName,
        Some(agentNumber),
        autoAnswer = true,
        line.driver
      )
    }

    "not retrieve queue call if another call is UNITIALIZED" in new Helper {
      reset(deviceAdapter)
      val channel = bchan(queueCall.channel)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UNITIALIZED
      )

      val deviceCalls = List(
        DeviceCall(call1Channel.name, Some(call1Channel), Set(), Map.empty)
      )

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      channelTracker.expectNoMessage()

      verifyZeroInteractions(deviceAdapter)
    }

    "not retrieve queue call if another call is ORIGINATING" in new Helper {
      reset(deviceAdapter)
      val channel = bchan(queueCall.channel)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )

      val deviceCalls = List(
        DeviceCall(call1Channel.name, Some(call1Channel), Set(), Map.empty)
      )

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      channelTracker.expectNoMessage()

      verifyZeroInteractions(deviceAdapter)
    }

    "not retrieve queue call if another call is not in expected state" in new Helper {
      reset(deviceAdapter)
      val channel = bchan(queueCall.channel)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val deviceCalls = List(
        DeviceCall(call1Channel.name, Some(call1Channel), Set(), Map.empty)
      )

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      channelTracker.expectMsg(WatchChannelStartingWith(call1Channel.name))
      channelTracker.reply(call1Channel.copy(state = ChannelState.UP))
      channelTracker.expectMsg(UnWatchChannelStartingWith(call1Channel.name))

      verify(deviceAdapter, Mockito.timeout(2000)).hold(None, sender.ref)
      verifyNoMoreInteractions(deviceAdapter)
    }

    "retrieve queue call if no channel is associated" in new Helper {
      reset(deviceAdapter)
      val channel = bchan(queueCall.channel)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val deviceCalls =
        List(DeviceCall(call1Channel.name, None, Set(), Map.empty))

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(channel)

      verify(deviceAdapter, Mockito.timeout(2000)).retrieveQueueCall(
        line.interface,
        line.number,
        lineUser,
        queueCall,
        Map.empty,
        sender.ref,
        channel.id,
        channel.lastQueueName,
        Some(agentNumber),
        autoAnswer = true,
        line.driver
      )
      verifyZeroInteractions(deviceAdapter)
    }

    "not retrieve queue call if no channel is found" in new Helper {
      reset(deviceAdapter)
      val call1Channel = Channel(
        "123456789.123",
        "SIP/abcd-00001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val deviceCalls = List(
        DeviceCall(call1Channel.name, Some(call1Channel), Set(), Map.empty)
      )

      processRetrieve(sender.ref, deviceCalls)

      channelTracker.expectMsg(GetChannel(queueCall.channel))
      channelTracker.reply(NoSuchChannel(queueCall.channel))

      verifyZeroInteractions(deviceAdapter)
    }
  }
}
