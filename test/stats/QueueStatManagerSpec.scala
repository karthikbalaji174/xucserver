package stats

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.guice.GuiceInjectorBuilder
import us.theatr.akka.quartz.AddCronSchedule
import xivo.xuc.XucConfig
import xivo.xucstats.XucBaseStatsConfig

class QueueStatManagerSpec
    extends TestKitSpec("QueueStatManagerSpec")
    with MockitoSugar {
  import stats.Statistic._
  val cronScheduler = TestProbe()

  val injector = new GuiceInjectorBuilder()
    .injector()

  class Helper {
    val queueId                        = 13
    val cronScheduler                  = TestProbe()
    val config: XucConfig              = mock[XucConfig]
    val statConfig: XucBaseStatsConfig = mock[XucBaseStatsConfig]

    when(statConfig.resetSchedule).thenReturn("0 32 0 * * ?")
    when(statConfig.queuesStatTresholdsInSec).thenReturn(Set[Int]())
    when(config.metricsRegistryName).thenReturn("TestMetrics")

    def actor() = {
      val a = TestActorRef(
        new QueueStatManager(
          queueId,
          injector,
          cronScheduler.ref,
          config,
          statConfig
        )
      )
      (a, a.underlyingActor)
    }
  }

  "Queue Stat Manager" should {
    "self register to quartz scheduler" in new Helper {

      val (ref, _) = actor()

      val cronSchedule = AddCronSchedule(ref, "0 32 0 * * ?", ResetStat)

      cronScheduler.expectMsg(cronSchedule)

    }
  }
}
