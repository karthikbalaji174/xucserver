package stats

import akka.actor.Actor
import akkatest.TestKitSpec
import akka.testkit.TestProbe

class QueueStatRepositorySpec extends TestKitSpec("QueueStatRepositorySpec") {

  class Helper {
    val queueStatManagerFactory = new QueueStatManager.Factory {
      def apply(queueId: Int): Actor =
        new Actor { def receive = Actor.emptyBehavior }
    }
    val queueStatRepository = new QueueStatRepository(queueStatManagerFactory)
  }

  "Queue stat repository" should {
    "should retrieve created actor" in new Helper {
      val queueId   = 4
      val statActor = TestProbe()

      queueStatRepository.repo += (4 -> statActor.ref)

      queueStatRepository.getQueueStat(queueId) should be(Some(statActor.ref))
    }
    "shoud pass a list of created stats managers" in new Helper {
      val statActor = TestProbe()

      queueStatRepository.repo += (4 -> statActor.ref)

      queueStatRepository.getAllStats(0) should be(statActor.ref)
    }
  }
}
