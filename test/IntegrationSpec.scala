import akka.actor.{Actor, Props}
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.asteriskjava.manager.action.CoreStatusAction
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers.running
import services.XucAmiBus
import services.XucAmiBus.{AmiAction, AmiResponse, AmiType}
import xivo.xucami.XucAmiConfig
import xivo.xucami.ami.ManagerConnector

import scala.concurrent.duration._

class IntegrationSpec
    extends TestKitSpec("Integration")
    with GuiceOneAppPerSuite {

  val testConfig: Map[String, Any] = {
    Map(
      "logger.root"                      -> "INFO",
      "logger.application"               -> "INFO",
      "logger.services"                  -> "INFO",
      "akka.loggers"                     -> List("akka.event.slf4j.Slf4jLogger"),
      "log-dead-letters-during-shutdown" -> "on",
      "loglevel"                         -> "ERROR",
      "xucami.amiIpAddress"              -> "xivo-integration",
      "xucami.amiPort"                   -> "5038",
      "xucami.username"                  -> "xuc",
      "xucami.secret"                    -> "xucpass",
      "api.eventUrl"                     -> ""
    )
  }

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration = Configuration.from(testConfig))
      .build()

  "Application" should (
    "be able to connect to AMI and get CoreStatus" in {
      running(app) {
        val probe  = TestProbe()
        val amiBus = new XucAmiBus()
        val manager = system.actorOf(
          ManagerConnector.props(
            amiBus,
            new XucAmiConfig(app.configuration),
            "default"
          )
        )
        system.actorOf(Props(new Actor() {
          amiBus.subscribe(self, AmiType.separator)

          def receive = { case response: AmiResponse =>
            probe.ref ! response
          }
        }))
        manager ! AmiAction(new CoreStatusAction())
        probe.expectMsgClass(5.seconds, classOf[AmiResponse])
      }
    }
  )
}
