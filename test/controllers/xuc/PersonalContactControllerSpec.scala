package controllers.xuc

import akka.actor.{ActorRef, ActorSystem}
import models.ws.auth.{ApiUser, AuthenticationInformation}
import models.{RichDirectoryResult, XivoUser}
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSResponse
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.config.{ConfigRepository, DeleteAllEntries}
import xivo.directory.DirdRequester
import xivo.models.{
  PersonalContactImportResult,
  PersonalContactRequest,
  PersonalContactResult
}
import xuctest.BaseTest
import akka.testkit.TestProbe
import org.mockito.Matchers
import services.ActorIdsFactory
import xivo.directory.PersonalContactRepository.{
  DeletePersonalContact,
  SetPersonalContact
}
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class PersonalContactControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {
  implicit lazy val mat            = app.materializer
  implicit val system: ActorSystem = ActorSystem()

  val repo: ConfigRepository       = mock[ConfigRepository]
  val dirdMock: DirdRequester      = mock[DirdRequester]
  val actorIdMock: ActorIdsFactory = mock[ActorIdsFactory]

  class Helper {

    reset(repo)
    reset(dirdMock)

    val user =
      XivoUser(1, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)

    val apiUser = ApiUser(user.username, user.id)

    val testProbe = TestProbe()
    when(
      actorIdMock.personalContactRepositoryPath(
        Matchers.any[ActorRef],
        Matchers.eq(user.username.get)
      )
    )
      .thenReturn(testProbe.ref.path)

    def getCtrl = app.injector.instanceOf[PersonalContactController]

    val contactId = "d0e4286b-b3e3-4d25"
    val expires   = 54000
    stub(repo.getCtiUser("jbond")).toReturn(Some(user))
    val now = new DateTime().getMillis / 1000
    val token =
      AuthenticationInformation(
        user.username.get,
        now + expires,
        now,
        "cti",
        List(
          "alias.ctiuser"
        ),
        None
      )
    val wsResponse: WSResponse = mock[WSResponse]
    val fakeAuth = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )

    val contactRaw: String =
      """|{
         |  "firstname": "Mike",
         |  "lastname": "Swarm",
         |  "number": "1234",
         |  "mobile": "4321",
         |  "fax": "4444",
         |  "email": "m.swarm@mail.com",
         |  "company" : "myCompany"
         |}""".stripMargin
    val contact: JsValue = Json.parse(contactRaw)
    val contactCreated =
      PersonalContactResult(contactId, None, None, None, None, None, None, None)

    val csvContacts: String =
      """company,email,fax,firstname,lastname,mobile,number"
        |"corp,j.doe@my.corp,3333,doe,john,2222,1111""".stripMargin

    val contactsImported =
      PersonalContactImportResult(Some(List(contactCreated)), None)
  }

  "PersonalContact Controller" should {

    "allow user to list its personal contact" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/contact/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      val result = List()
      when(dirdMock.list(apiUser)).thenReturn(Future(result))
      val ctrl: Future[Result] = call(getCtrl.list(), rq)

      status(ctrl) shouldBe OK
      verify(dirdMock, timeout(500)).list(apiUser)
    }

    "allow user to list its personal contact as RichDirectoryResult" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/contact/display/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      val result = new RichDirectoryResult(List("userPersonalContacts"))
      when(dirdMock.richList(apiUser)).thenReturn(Future(result))
      val ctrl: Future[Result] = call(getCtrl.richList(), rq)

      status(ctrl) shouldBe OK
      verify(dirdMock, timeout(500)).richList(apiUser)
    }

    "refuse to add personal contact if request is invalid" in new Helper {
      val rq = FakeRequest(
        "POST",
        "/xuc/api/2.0/contact/personal",
        fakeAuth,
        AnyContentAsEmpty
      )
      val ctrl: Future[Result] = call(getCtrl.add(), rq)

      testProbe.expectNoMessage()

      status(ctrl) should not be CREATED
    }

    "allow user to add personal contact if request is valid" in new Helper {
      val rq = FakeRequest(
        "POST",
        "/xuc/api/2.0/contact/personal",
        fakeAuth,
        AnyContentAsJson(contact)
      )

      val pc: PersonalContactRequest =
        contact.validate[PersonalContactRequest].get
      when(dirdMock.add(apiUser, pc)).thenReturn(Future(contactCreated))
      val ctrl: Future[Result] = call(getCtrl.add(), rq)

      testProbe.expectMsg(SetPersonalContact(contactCreated))

      status(ctrl) shouldBe CREATED
      verify(dirdMock, timeout(500)).add(apiUser, pc)
    }

    "edit personal contact" in new Helper {
      val rq = FakeRequest(
        "PUT",
        "/xuc/api/2.0/contact/personal/1",
        fakeAuth,
        AnyContentAsJson(contact)
      )

      val pc: PersonalContactRequest =
        contact.validate[PersonalContactRequest].get
      when(dirdMock.edit(apiUser, pc)(contactId))
        .thenReturn(Future(contactCreated))
      val ctrl: Future[Result] = call(getCtrl.edit(contactId), rq)

      testProbe.expectMsg(SetPersonalContact(contactCreated))

      status(ctrl) shouldBe OK
      verify(dirdMock, timeout(500)).edit(apiUser, pc)(contactId)
    }

    "get personal contact" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/contact/personal/1",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(dirdMock.get(apiUser)(contactId)).thenReturn(Future(contactCreated))
      val ctrl: Future[Result] = call(getCtrl.get(contactId), rq)

      status(ctrl) shouldBe OK
      verify(dirdMock, timeout(500)).get(apiUser)(contactId)
    }

    "delete one personal contact" in new Helper {
      val rq = FakeRequest(
        "DELETE",
        "/xuc/api/2.0/contact/personal/1",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(dirdMock.delete(apiUser)(contactId)).thenReturn(Future(wsResponse))
      val ctrl: Future[Result] = call(getCtrl.delete(contactId), rq)

      testProbe.expectMsg(DeletePersonalContact(contactId))

      status(ctrl) shouldBe NO_CONTENT
      verify(dirdMock, timeout(500)).delete(apiUser)(contactId)
    }

    "delete all personal contacts" in new Helper {
      val rq = FakeRequest(
        "DELETE",
        "/xuc/api/2.0/contact/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(dirdMock.deleteAll(apiUser)).thenReturn(Future(wsResponse))
      val ctrl: Future[Result] = call(getCtrl.deleteAll(), rq)

      testProbe.expectMsg(DeleteAllEntries)

      status(ctrl) shouldBe NO_CONTENT
      verify(dirdMock, timeout(500)).deleteAll(apiUser)
    }

    "export personal contacts" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/contact/export/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(dirdMock.export(apiUser)).thenReturn(Future("csv"))
      val ctrl: Future[Result] = call(getCtrl.export(), rq)

      status(ctrl) shouldBe OK
      contentAsBytes(ctrl) shouldBe Array(
        99,
        115,
        118
      ) //"csv" string as Bytes to avoid test helper bug
      verify(dirdMock, timeout(500)).export(apiUser)
    }

    "import personal contacts" in new Helper {
      val rq = FakeRequest(
        "POST",
        "/xuc/api/2.0/contact/import/personal",
        fakeAuth,
        AnyContentAsText(csvContacts)
      )

      when(dirdMock.importCsv(apiUser)(Some(rq.body.txt)))
        .thenReturn(Future(contactsImported))
      val ctrl: Future[Result] = call(getCtrl.importCsv(), rq)

      testProbe.expectMsg(SetPersonalContact(contactCreated))

      status(ctrl) shouldBe CREATED
      verify(dirdMock, timeout(500)).importCsv(apiUser)(Some(csvContacts))
    }
  }

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ActorSystem].to(system))
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[DirdRequester].to(dirdMock))
      .overrides(bind[ActorIdsFactory].to(actorIdMock))
      .build()

}
