package controllers.xuc

import models.authentication.AuthenticationProvider
import models.ws.auth._
import models.{AuthenticatedUser, Token, XivoUser}
import org.joda.time.DateTime
import org.mockito.Mockito.{reset, stub}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.auth.WebService
import services.calltracking.SipDriver
import services.config.ConfigRepository
import xivo.models.{Line, LineHelper}
import xivo.xuc.WebServiceUserConfig
import xivo.xucami.models.CallerId
import xuctest.BaseTest

import scala.concurrent.Future

class AuthenticationControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  implicit lazy val mat = app.materializer
  val repo              = mock[ConfigRepository]
  val authProvider      = mock[AuthenticationProvider]
  val config            = mock[WebServiceUserConfig]
  val webService        = mock[WebService]

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[AuthenticationProvider].to(authProvider))
      .overrides(bind[WebService].to(webService))
      .build()

  def getCtrl() =
    app.injector.instanceOf[AuthenticationController]

  class Helper {

    val authToken = Token(
      "adeblouse",
      new DateTime(1680011247),
      new DateTime(1679924847),
      "webservice",
      None,
      List("..read")
    )

    val jwtToken =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6ImFkZWJsb3VzZSIsImV4cGlyZXNBdCI6MTY4MDAxMTI0NywiaXN1ZWRBdCI6MTY3OTkyNDg0NywidXNlclR5cGUiOiJ3ZWJzZXJ2aWNlIiwiYWNscyI6WyJhbGlhcy5jdGl1c2VyIl19.YuRBDpy_Amtwz_E8CPbdXT3GNg3BlOzjRALHe3E5R7s"

    val authenticationInformation = AuthenticationInformation(
      "adeblouse",
      1680011247,
      1679924847,
      "webservice",
      List("alias.ctiuser"),
      None
    )

    stub(config.defaultWebServiceExpires).toReturn(86400)
    stub(config.maxExpires).toReturn(86400)
    stub(webService.getAuthenticationInformation("adeblouse", authToken))
      .toReturn(authenticationInformation)
    stub(webService.encodeToJWT(authenticationInformation))
      .toReturn(jwtToken)

  }

  "AuthenticationController" should {
    "allow user to login" in {
      val authUser = mock[AuthenticatedUser]
      stub(authProvider.authenticate("jbond", "mypass"))
        .toReturn(Some(authUser))

      reset(repo)
      stub(repo.getCtiUser("jbond")).toReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      import LineHelper.makeLine
      stub(repo.getLineForUser("jbond")).toReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass"}"""
        )
      )
      val ctrl = call(getCtrl().login(), rq)

      status(ctrl) shouldBe OK
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "login").as[String] shouldBe ("jbond")
    }

    "disallow user to login" in {
      stub(authProvider.authenticate("jbond", "mywrongpassword")).toReturn(None)
      reset(repo)
      stub(repo.getCtiUser("jbond")).toReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "jbond", "password":"mywrongpassword"}""")
      )
      val ctrl = call(getCtrl().login(), rq)

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe ("InvalidCredentials")
    }

    "disallow xuc user to login" in {
      stub(authProvider.authenticate("jbond", "mywrongpassword")).toReturn(None)
      reset(repo)
      stub(repo.getCtiUser("xuc")).toReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "Xuc",
            Some("Technical"),
            Some("xuc"),
            Some("xucpass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "xuc", "password":"xucpass"}""")
      )
      val ctrl = call(getCtrl().login(), rq)

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe ("InvalidCredentials")
    }

    "disallow mobile software type to login if mobile config is not ok" in {
      val authUser = mock[AuthenticatedUser]
      stub(authProvider.authenticate("jbond", "mypass"))
        .toReturn(Some(authUser))
      stub(repo.getCtiUser("jbond")).toReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "softwareType":"mobile"}"""
        )
      )

      import LineHelper.makeLine
      stub(repo.getLineForUser("jbond")).toReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val ctrl = call(getCtrl().login(), rq)

      status(ctrl) shouldBe FORBIDDEN
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe ("WrongMobileSetup")
    }

    "allow mobile software type to login if mobile config is ok" in {
      val authUser = mock[AuthenticatedUser]
      stub(authProvider.authenticate("jbond", "mypass"))
        .toReturn(Some(authUser))
      stub(repo.mobileConfigIsValid).toReturn(true)
      stub(repo.getCtiUser("jbond")).toReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "softwareType":"mobile"}"""
        )
      )

      import LineHelper.makeLine
      stub(repo.getLineForUser("jbond")).toReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val ctrl = call(getCtrl().login(), rq)
      status(ctrl) shouldBe OK
    }

    "check username upon login" in {
      reset(repo)
      stub(repo.getCtiUser("otheruser")).toReturn(None)

      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "otheruser", "password":"mypass"}""")
      )
      val ctrl = call(getCtrl().login(), rq)

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe ("InvalidCredentials")
    }

    "check username is not empty" in {
      reset(repo)
      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "", "password":"mypass"}""")
      )
      val ctrl = call(getCtrl().login(), rq)

      status(ctrl) shouldBe BAD_REQUEST
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe ("EmptyLogin")
    }

    "validate credentials and check api can be called with ACL cti aliases" in {
      reset(repo)
      val authUser = mock[AuthenticatedUser]
      stub(authProvider.authenticate("jbond", "mypass"))
        .toReturn(Some(authUser))
      stub(repo.getCtiUser("jbond")).toReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "softwareType":"webBrowser"}"""
        )
      )

      import LineHelper.makeLine
      stub(repo.getLineForUser("jbond")).toReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val loginCtrl = call(getCtrl().login(), rq)

      val jsonResult = contentAsJson(loginCtrl)
      val token      = (jsonResult \ "token").as[String]
      val rq1=
        FakeRequest("GET", "/xuc/api/2.0/contact/personal").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl1 = call(getCtrl().check(), rq1)
      status(checkCtrl1) shouldBe OK

      val rq2 =
        FakeRequest("GET", "/xuc/api/2.0/contact/export/personal").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl2 = call(getCtrl().check(), rq2)
      status(checkCtrl2) shouldBe OK

      val rq3 =
        FakeRequest("GET", "/xuc/api/2.0/config/meetingrooms/personal/1").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl3 = call(getCtrl().check(), rq3)
      status(checkCtrl3) shouldBe OK

      val rq4 =
        FakeRequest("GET", "/xuc/api/2.0/dial/xivo/username/").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl4 = call(getCtrl().check(), rq4)
      status(checkCtrl4) shouldBe FORBIDDEN
    }

    "log LoginType" in {
      val line = Line(
        0,
        "default",
        "",
        "ato",
        None,
        None,
        "",
        webRTC = true,
        None,
        None,
        ua = false,
        CallerId("", ""),
        SipDriver.SIP
      )
      LineType.lineToLineType(line).shouldBe(LineType.webRTC)

      val line2 = Line(
        0,
        "default",
        "",
        "ato",
        None,
        None,
        "",
        webRTC = true,
        None,
        None,
        ua = true,
        CallerId("", ""),
        SipDriver.SIP
      )
      LineType.lineToLineType(line2).shouldBe(LineType.ua)
    }

    "allow web service user to get JWT token with expiration time set to 3s" in new Helper {
      stub(webService.authenticate("adeblouse", "rightpassword", 3))
        .toReturn(Future.successful(authToken))
      stub(webService.limitExpiration(3, config.maxExpires)).toReturn(3)

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "adeblouse", "password":"rightpassword", "expiration": 3}"""
        )
      )

      val ctrl = call(getCtrl().webService(), rq)
      status(ctrl) shouldBe OK
      contentAsString(
        ctrl
      ) shouldBe s"""{"login":"adeblouse","token":"$jwtToken","TTL":3}"""

    }
  }

  "throw a error message if no login is provided" in new Helper {
    stub(webService.authenticate("", "rightpassword", 3600))
      .toReturn(
        Future.failed(
          new AuthenticationException(
            AuthenticationError.InvalidCredentials,
            "Invalid credentials"
          )
        )
      )
    stub(webService.limitExpiration(3600, config.maxExpires)).toReturn(3600)

    val rq = FakeRequest().withJsonBody(
      Json.parse(
        """{"login": "", "password":"rightpassword", "expiration": 3600}"""
      )
    )

    val ctrl = call(getCtrl().webService(), rq)
    status(ctrl) shouldBe UNAUTHORIZED
    contentAsString(
      ctrl
    ) shouldBe """{"error":"InvalidCredentials","message":"Invalid credentials"}"""
  }

  "throw a error message if credentials are invalid" in new Helper {
    stub(webService.authenticate("webservice", "wrongpassword", 3))
      .toReturn(
        Future.failed(
          new AuthenticationException(
            AuthenticationError.InvalidCredentials,
            "Invalid credentials"
          )
        )
      )
    stub(webService.limitExpiration(3, config.maxExpires)).toReturn(3)

    val rq = FakeRequest().withJsonBody(
      Json.parse(
        """{"login": "webservice", "password":"wrongpassword", "expiration": 3}"""
      )
    )

    val ctrl = call(getCtrl().webService(), rq)
    status(ctrl) shouldBe UNAUTHORIZED
    contentAsString(
      ctrl
    ) shouldBe """{"error":"InvalidCredentials","message":"Invalid credentials"}"""
  }

  "allow web service user to get JWT token with default expiration time" in new Helper {
    stub(webService.authenticate("webservice", "rightpassword", 86400))
      .toReturn(Future.successful(authToken))
    stub(webService.limitExpiration(86400, config.maxExpires))
      .toReturn(86400)
    stub(webService.getAuthenticationInformation("webservice", authToken))
      .toReturn(authenticationInformation)
    stub(webService.encodeToJWT(authenticationInformation))
      .toReturn(jwtToken)

    val rq = FakeRequest().withJsonBody(
      Json.parse(
        """{"login": "webservice", "password":"rightpassword"}"""
      )
    )

    val ctrl = call(getCtrl().webService(), rq)
    status(ctrl) shouldBe OK
    contentAsString(
      ctrl
    ) shouldBe s"""{"login":"webservice","token":"$jwtToken","TTL":86400}"""
  }

}
