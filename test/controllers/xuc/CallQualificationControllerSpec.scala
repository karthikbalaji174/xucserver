package controllers.xuc

import models.XivoUser
import models.ws.auth.AuthenticationInformation
import org.mockito.Mockito.{reset, stub, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSResponse
import play.api.mvc._
import play.api.test.Helpers.{call, status, _}
import play.api.test.{FakeHeaders, FakeRequest}
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{
  CallQualification,
  CallQualificationAnswer,
  SubQualification
}
import xivo.network.WebServiceException
import xuctest.BaseTest
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CallQualificationControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {
  val repo                                 = mock[ConfigRepository]
  val configMgtMock: ConfigServerRequester = mock[ConfigServerRequester]
  implicit lazy val mat                    = app.materializer

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ConfigServerRequester].to(configMgtMock))
      .build()

  class Helper {
    reset(repo)
    reset(configMgtMock)

    def getCtrl = app.injector.instanceOf[CallQualificationController]

    val wsResponse: WSResponse = mock[WSResponse]
    val expires                = 56700

    val user =
      XivoUser(1, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)
    stub(repo.getCtiUser("jbond")).toReturn(Some(user))
    val now = new DateTime().getMillis / 1000
    val token =
      AuthenticationInformation(
        user.username.get,
        now + expires,
        now,
        "cti",
        List(
          "alias.ctiuser"
        ),
        None
      )
    val fakeAuth = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )

    val queueId: Long = 1L
    val sq            = List(SubQualification(Some(1), "subqualif1"))
    val q             = CallQualification(Some(1), "qualif1", sq)

    val answerRaw: String =
      """
        | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
        | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
        | """.stripMargin

    val answer: JsValue = Json.parse(answerRaw)
    val qualificationAnswer = CallQualificationAnswer(
      sub_qualification_id = 1,
      time = "2018-03-21 17:00:00",
      callid = "callid1",
      agent = 1,
      queue = 1,
      firstName = "first",
      lastName = "last",
      comment = "some comment",
      customData = "some custom data"
    )

    val fromRefTime: String = "2016-01-01 00:00:00"
    val toRefTime: String   = "2018-12-12 00:00:00"
  }

  "CallQualification Controller" should {
    "get all qualifications by queue id" in new Helper {
      val rq = FakeRequest(
        "GET",
        s"/xuc/api/2.0/call_qualification/queue/$queueId",
        fakeAuth,
        AnyContentAsEmpty
      )

      val result: List[CallQualification] = List(q)
      when(configMgtMock.getCallQualifications(queueId))
        .thenReturn(Future(result))

      val ctrl: Future[Result] = call(getCtrl.get(queueId), rq)

      status(ctrl) shouldBe OK
      verify(configMgtMock).getCallQualifications(queueId)
    }

    "create a call qualification answer" in new Helper {
      val rq = FakeRequest(
        "POST",
        s"/xuc/api/2.0/call_qualification",
        fakeAuth,
        AnyContentAsJson(answer)
      )
      val expected = 1L

      when(configMgtMock.createCallQualificationAnswer(qualificationAnswer))
        .thenReturn(Future.successful(expected))

      val ctrl: Future[Result] = call(getCtrl.create(), rq)

      status(ctrl) shouldBe CREATED
      verify(configMgtMock).createCallQualificationAnswer(qualificationAnswer)
    }

    "respond 400 if json body is not found in create" in new Helper {
      val rq = FakeRequest(
        "POST",
        s"/xuc/api/2.0/call_qualification",
        fakeAuth,
        AnyContentAsEmpty
      )
      val ctrl: Future[Result] = call(getCtrl.create(), rq)

      status(ctrl) shouldBe BAD_REQUEST
    }

    "respond 400 if json body is incorrectly formatted" in new Helper {
      val errorJson: String =
        """
          | {"sub_qualification_id": 1, "custom_data": "some custom data"}
          | """.stripMargin

      val errorAnswer: JsValue = Json.parse(errorJson)

      val rq = FakeRequest(
        "POST",
        s"/xuc/api/2.0/call_qualification",
        fakeAuth,
        AnyContentAsJson(errorAnswer)
      )
      val ctrl: Future[Result] = call(getCtrl.create(), rq)

      status(ctrl) shouldBe BAD_REQUEST
    }

    "respond 500 if exception in web service in get" in new Helper {
      val rq = FakeRequest(
        "GET",
        s"/xuc/api/2.0/call_qualification/queue/$queueId",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(configMgtMock.getCallQualifications(queueId))
        .thenReturn(
          Future.failed(
            new WebServiceException("Call to web service call failed.")
          )
        )

      val ctrl: Future[Result] = call(getCtrl.get(queueId), rq)

      status(ctrl) shouldBe INTERNAL_SERVER_ERROR
      verify(configMgtMock).getCallQualifications(queueId)
    }

    "respond 500 if exception in web service in create" in new Helper {
      val rq = FakeRequest(
        "POST",
        s"/xuc/api/2.0/call_qualification",
        fakeAuth,
        AnyContentAsJson(answer)
      )

      when(configMgtMock.createCallQualificationAnswer(qualificationAnswer))
        .thenReturn(
          Future.failed(
            new WebServiceException("Call  to web service call failed.")
          )
        )

      val ctrl: Future[Result] = call(getCtrl.create(), rq)

      status(ctrl) shouldBe INTERNAL_SERVER_ERROR
      verify(configMgtMock).createCallQualificationAnswer(qualificationAnswer)
    }

    "respond 500 in case of unhandled exception" in new Helper {
      val rq = FakeRequest(
        "GET",
        s"/xuc/api/2.0/call_qualification/queue/$queueId",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(configMgtMock.getCallQualifications(queueId))
        .thenReturn(Future.failed(new Exception("Generic error.")))

      val ctrl: Future[Result] = call(getCtrl.get(queueId), rq)

      status(ctrl) shouldBe INTERNAL_SERVER_ERROR
      verify(configMgtMock).getCallQualifications(queueId)
    }
  }
}
