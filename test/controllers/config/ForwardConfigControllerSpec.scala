package controllers.config

import akka.util.ByteString
import models.XivoUser
import models.ws.auth.AuthenticationInformation
import org.mockito.Mockito.{reset, stub, timeout, verify}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSResponse
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.config.{ConfigRepository, ConfigServerRequester}
import xuctest.BaseTest
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ForwardConfigControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {
  val repo                                       = mock[ConfigRepository]
  val configRequesterMock: ConfigServerRequester = mock[ConfigServerRequester]

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ConfigServerRequester].to(configRequesterMock))
      .build()

  "ForwardConfig Controller" should {
    "allow forward any request to /config towards configmgt" in new Helper {
      val rawText       = "{\"data\":{}}"
      val json: JsValue = Json.parse(rawText)
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/handler",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(wsResponse.status).toReturn(OK)
      stub(wsResponse.bodyAsBytes).toReturn(ByteString(rawText))
      stub(wsResponse.headers).toReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      stub(configRequesterMock.forward("handler", "GET", None))
        .toReturn(Future(wsResponse))
      val ctrl         = app.injector.instanceOf[ForwardConfigController]
      implicit val mat = app.materializer

      val res: Future[Result] = call(ctrl.forward("handler"), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldBe json
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        "handler",
        "GET",
        None,
        null
      )
    }

    "allow forward any request to /config towards configmgt and forward the body" in new Helper {
      val rawText       = "{\"data\":{}}"
      val json: JsValue = Json.parse(rawText)
      val body          = Json.parse("""{"fileName": "queue1_fileName"}""")
      val rq            = FakeRequest("GET", "/xuc/api/2.0/config/handler", fakeAuth, body)

      stub(wsResponse.status).toReturn(OK)
      stub(wsResponse.bodyAsBytes).toReturn(ByteString(rawText))
      stub(wsResponse.headers).toReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      stub(configRequesterMock.forward("handler", "GET", Some(body)))
        .toReturn(Future(wsResponse))
      val ctrl         = app.injector.instanceOf[ForwardConfigController]
      implicit val mat = app.materializer

      val res: Future[Result] = call(ctrl.forward("handler"), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldBe json
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        "handler",
        "GET",
        Some(body),
        null
      )
    }
  }

  class Helper {
    reset(repo)
    reset(configRequesterMock)
    val expires = 54000

    val user =
      XivoUser(1, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)
    stub(repo.getCtiUser("jbond")).toReturn(Some(user))

    val now = new DateTime().getMillis / 1000
    val token =
      AuthenticationInformation(
        user.username.get,
        now + expires,
        now,
        "cti",
        List("alias.ctiuser"),
        None
      )
    val wsResponse: WSResponse = mock[WSResponse]
    val fakeAuth = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )
  }
}
