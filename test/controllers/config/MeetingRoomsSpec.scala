package controllers.config

import models.XivoUser
import models.ws.auth.AuthenticationInformation
import org.mockito.Mockito.{reset, stub, timeout, verify}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.WSResponse
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{
  MeetingRoomToken,
  PersonalMeetingRoom,
  StaticMeetingRoom,
  TemporaryMeetingRoom
}
import xuctest.BaseTest
import org.joda.time.DateTime

import scala.concurrent.Future

class MeetingRoomsSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  val repo                                       = mock[ConfigRepository]
  val configRequesterMock: ConfigServerRequester = mock[ConfigServerRequester]

  override def fakeApplication() =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ConfigServerRequester].to(configRequesterMock))
      .build()

  class Helper {
    reset(repo)
    reset(configRequesterMock)
    val expires = 54000

    val user =
      XivoUser(1, None, None, "James", Some("Bond"), Some("jbond"), None, None, None)
    stub(repo.getCtiUser("jbond")).toReturn(Some(user))

    val now = new DateTime().getMillis / 1000
    val token = AuthenticationInformation(
      user.username.get,
      now + expires,
      now,
      "cti",
      List(
        "alias.ctiuser"
      ),
      None
    )
    val wsResponse: WSResponse = mock[WSResponse]
    val fakeAuth = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )
  }

  "MeetingRoomsSpec" should {
    "Forward token request to configmgt for personal meetingRoom" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/personal/42",
        fakeAuth,
        AnyContentAsEmpty
      )
      stub(
        configRequesterMock.getMeetingRoomToken(
          "42",
          Some(1),
          PersonalMeetingRoom
        )
      )
        .toReturn(
          Future.successful(MeetingRoomToken("some-uuid", "some-token"))
        )

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] =
        call(ctrl.getTokenById("42", PersonalMeetingRoom), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).getMeetingRoomToken(
        "42",
        Some(1),
        PersonalMeetingRoom
      )
    }

    "Forward token request to configmgt for static meetingRoom" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/personal/42",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(
        configRequesterMock.getMeetingRoomToken(
          "42",
          Some(1),
          StaticMeetingRoom
        )
      )
        .toReturn(
          Future.successful(MeetingRoomToken("some-uuid", "some-token"))
        )

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] =
        call(ctrl.getTokenById("42", StaticMeetingRoom), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).getMeetingRoomToken(
        "42",
        Some(1),
        StaticMeetingRoom
      )
    }

    "Forward token request to configmgt for temporary meetingRoom" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/temporary/token/User1",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(
        configRequesterMock.getMeetingRoomToken(
          "User1",
          Some(1),
          TemporaryMeetingRoom
        )
      )
        .toReturn(
          Future.successful(MeetingRoomToken("some-uuid", "some-token"))
        )

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] =
        call(ctrl.getTokenById("User1", TemporaryMeetingRoom), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).getMeetingRoomToken(
        "User1",
        Some(1),
        TemporaryMeetingRoom
      )
    }

    "Forward getPersonalRoomById request to configmgt" in new Helper {
      val rq = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/personal/5",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(wsResponse.status).toReturn(OK)
      stub(wsResponse.headers).toReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      stub(
        configRequesterMock.forward(
          s"meetingrooms/personal/5?userId=${user.id}",
          "GET",
          None,
          "2.0"
        )
      )
        .toReturn(Future.successful(wsResponse))

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] = call(ctrl.getPersonalRoomById(5), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal/5?userId=${user.id}",
        "GET",
        None,
        "2.0"
      )
    }

    "Forward addPersonalRoom request to configmgt" in new Helper {
      val rq = FakeRequest(
        "POST",
        "/xuc/api/2.0/config/meetingrooms/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(wsResponse.status).toReturn(OK)
      stub(wsResponse.headers).toReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      stub(
        configRequesterMock.forward(
          s"meetingrooms/personal?userId=${user.id}",
          "POST",
          None,
          "2.0"
        )
      )
        .toReturn(Future.successful(wsResponse))

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] = call(ctrl.addPersonalRoom(), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal?userId=${user.id}",
        "POST",
        None,
        "2.0"
      )
    }

    "Forward editPersonalRoom request to configmgt" in new Helper {
      val rq = FakeRequest(
        "PUT",
        "/xuc/api/2.0/config/meetingrooms/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(wsResponse.status).toReturn(OK)
      stub(wsResponse.headers).toReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      stub(
        configRequesterMock.forward(
          s"meetingrooms/personal?userId=${user.id}",
          "PUT",
          None,
          "2.0"
        )
      )
        .toReturn(Future.successful(wsResponse))

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] = call(ctrl.editPersonalRoom(), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal?userId=${user.id}",
        "PUT",
        None,
        "2.0"
      )
    }

    "Forward deletePersonalRoom request to configmgt" in new Helper {
      val rq = FakeRequest(
        "DELETE",
        "/xuc/api/2.0/config/meetingrooms/personal/5",
        fakeAuth,
        AnyContentAsEmpty
      )

      stub(wsResponse.status).toReturn(OK)
      stub(wsResponse.headers).toReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      stub(
        configRequesterMock.forward(
          s"meetingrooms/personal/5?userId=${user.id}",
          "DELETE",
          None,
          "2.0"
        )
      )
        .toReturn(Future.successful(wsResponse))

      val ctrl         = app.injector.instanceOf[MeetingRooms]
      implicit val mat = app.materializer

      val res: Future[Result] = call(ctrl.deletePersonalRoom(5), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal/5?userId=${user.id}",
        "DELETE",
        None,
        "2.0"
      )
    }
  }
}
