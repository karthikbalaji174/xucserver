package controllers.api

import controllers.helpers.RequestSuccess
import org.mockito.Mockito.{timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test.FakeRequest
import play.api.test.Helpers._
import services.request.AgentLoginRequest
import xivo.xuc.IPFilterConfig
import xuctest.BaseTest

import scala.concurrent.{ExecutionContext, Future}

class RestAgentSpec
    extends BaseTest
    with MockitoSugar
    with Results
    with GuiceOneAppPerSuite {

  implicit lazy val mat              = app.materializer
  implicit lazy val executionContext = app.injector.instanceOf[ExecutionContext]
  val bodyParsers                    = app.injector.instanceOf[PlayBodyParsers]

  class Helper {

    val defaultParser: BodyParser[AnyContent] = new BodyParsers.Default()
    val agentRequester                        = mock[AgentRequester]

    val iPFilterConfig = new IPFilterConfig {
      def ipAccepted = List[String]("127.0.0.1")
    }
    val iPFilter = new IPFilter(new BodyParsers.Default(), iPFilterConfig)
    val controller = new RestAgent(agentRequester, bodyParsers, iPFilter)(
      executionContext
    )
    controller.setControllerComponents(
      app.injector.instanceOf[ControllerComponents]
    )

    def buildRequest(api: String) = {
      val jsonHeader = ("Content-Type", "application/json")
      FakeRequest("POST", s"/api/1.0/$api/").withHeaders(jsonHeader)
    }
  }

  "Agent Rest API on agent logout request" should {
    "Report an error if no phone number is passed" in new Helper {
      val req                    = buildRequest("agentLogout")
      val result: Future[Result] = call(controller.agentLogout(), req)

      status(result) shouldBe BAD_REQUEST
    }

    "Execute" in new Helper {
      val nb      = "3432"
      val phoneNb = s"""{"phoneNumber": "$nb"}""".stripMargin
      val req     = buildRequest("agentLogout").withJsonBody(Json.parse(phoneNb))
      when(agentRequester.logout(nb)).thenReturn(Future(RequestSuccess("test")))

      val result: Future[Result] = call(controller.agentLogout(), req)

      verify(agentRequester, timeout(500)).logout(nb)
      status(result) shouldBe OK

    }
  }
  "Agent Rest API on agent login request" should {

    "Report an error if no data passed" in new Helper {
      val req                    = buildRequest("agentLogin")
      val result: Future[Result] = call(controller.agentLogin(), req)

      status(result) shouldBe BAD_REQUEST
    }

    "Execute agent login" in new Helper {
      val nb      = "3432"
      val agentNb = "2400"

      val request =
        s"""{"agentphonenumber": "$nb", "agentnumber": "$agentNb"}""".stripMargin
      val req = buildRequest("agentLogin").withJsonBody(Json.parse(request))

      val loginRequest = AgentLoginRequest(None, Some(nb), Some(agentNb))

      when(agentRequester.login(loginRequest))
        .thenReturn(Future(RequestSuccess("true")))

      val result: Future[Result] = call(controller.agentLogin(), req)

      verify(agentRequester, timeout(500)).login(loginRequest)

      status(result) shouldBe OK

    }
  }

  "Agent Rest API Toggle pause" should {
    "Report an error if no data passed" in new Helper {
      val req                    = buildRequest("togglePause")
      val result: Future[Result] = call(controller.togglePause(), req)

      status(result) shouldBe BAD_REQUEST
    }

    "execute on phone number passed" in new Helper {
      val nb      = "3432"
      val phoneNb = s"""{"phoneNumber": "$nb"}"""
      val req     = buildRequest("togglePause").withJsonBody(Json.parse(phoneNb))

      when(agentRequester.togglePause(nb))
        .thenReturn(Future(RequestSuccess("test")))

      val result: Future[Result] = call(controller.togglePause(), req)

      verify(agentRequester, timeout(500)).togglePause(nb)
      status(result) shouldBe OK

    }

  }

}
