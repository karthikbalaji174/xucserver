package controllers.api

import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc._
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.xuc.IPFilterConfig
import xuctest.BaseTest
import scala.concurrent.{ExecutionContext, Future}

class IPFilterSpec
    extends BaseTest
    with MockitoSugar
    with Results
    with GuiceOneAppPerSuite {

  implicit lazy val mat              = app.materializer
  implicit lazy val executionContext = app.injector.instanceOf[ExecutionContext]

  class Helper {

    val defaultParser: BodyParser[AnyContent] = new BodyParsers.Default()

    def buildRequest() = {
      FakeRequest("POST", s"").withHeaders(("Content-Type", "application/json"))
    }

    def block[A](request: Request[A]) = {
      Future(Ok("block"))
    }

    def getIPFilter(address: String) = {
      new IPFilter(
        new BodyParsers.Default(),
        new IPFilterConfig { def ipAccepted = List[String](address) }
      )
    }
  }

  "IPFilter" should {
    "Return OK" in new Helper {
      val req    = buildRequest()
      val result = getIPFilter("127.0.0.1").invokeBlock(req, block)
      status(result) shouldBe OK
    }
    "Return Service Unavailable" in new Helper {
      val req    = buildRequest()
      val result = getIPFilter("13.13.13.13").invokeBlock(req, block)

      status(result) shouldBe SERVICE_UNAVAILABLE
    }
  }

}
