DROP TABLE if exists user_line;

CREATE TABLE user_line (
    id integer NOT NULL,
    user_id integer,
    line_id integer NOT NULL,
    extension_id integer,
    main_user boolean NOT NULL,
    main_line boolean NOT NULL
);

