drop table if exists queuemember;
DROP TYPE if exists queue_category;
DROP TYPE if exists queuemember_usertype;

CREATE TYPE queue_category AS ENUM
   ('group',
    'queue');


CREATE TYPE queuemember_usertype AS ENUM
   ('agent',
    'user');


CREATE TABLE queuemember
(
  queue_name character varying(128) NOT NULL,
  interface character varying(128) NOT NULL,
  penalty integer NOT NULL DEFAULT 0,
  commented integer NOT NULL DEFAULT 0,
  usertype queuemember_usertype NOT NULL,
  userid integer NOT NULL,
  channel character varying(25) NOT NULL,
  category queue_category NOT NULL,
  "position" integer NOT NULL DEFAULT 0,
  CONSTRAINT queuemember_pkey PRIMARY KEY (queue_name, interface),
  CONSTRAINT queuemember_queue_name_channel_usertype_userid_category_key UNIQUE (queue_name, channel, usertype, userid, category)
)