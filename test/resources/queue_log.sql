drop table if exists queue_log;

CREATE TABLE queue_log
(
  time character varying(26) NOT NULL DEFAULT ''::character varying,
  callid character varying(32) NOT NULL DEFAULT ''::character varying,
  queuename character varying(50) NOT NULL DEFAULT ''::character varying,
  agent character varying(50) NOT NULL DEFAULT ''::character varying,
  event character varying(20) NOT NULL DEFAULT ''::character varying,
  data1 character varying(30) DEFAULT ''::character varying,
  data2 character varying(30) DEFAULT ''::character varying,
  data3 character varying(30) DEFAULT ''::character varying,
  data4 character varying(30) DEFAULT ''::character varying,
  data5 character varying(30) DEFAULT ''::character varying
);