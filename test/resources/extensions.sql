DROP TABLE if exists extensions;
DROP TYPE if exists extenumbers_type;

CREATE TYPE extenumbers_type AS ENUM (
    'extenfeatures',
    'featuremap',
    'generalfeatures',
    'group',
    'incall',
    'meetme',
    'outcall',
    'queue',
    'user',
    'voicemenu'
);

CREATE TABLE extensions (
    id integer NOT NULL,
    commented integer DEFAULT 0 NOT NULL,
    context character varying(39) DEFAULT ''::character varying NOT NULL,
    exten character varying(40) DEFAULT ''::character varying NOT NULL,
    type extenumbers_type NOT NULL,
    typeval character varying(255) DEFAULT ''::character varying NOT NULL
);

