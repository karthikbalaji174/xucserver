package xivo.models

import play.api.libs.json._
import xivo.data.dbunit.DBUtil
import xuctest.BaseTest

class AgentSpec extends BaseTest {

  "agent" should {
    "be able to be transformed to json" in {
      val a    = Agent(34, "john", "doe", "5000", "default")
      val json = Json.toJson(a)
      json should not be (null)
    }

    "be able to be retreived from base by id" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("agentfeatures.xml")

        val aline        = Agent(2, "Aline", "Belle", "1702", "sales", 7, 7)
        val agentFactory = new AgentFactoryImpl(database)
        val ag           = agentFactory.getById(2)

        ag should be(Some(aline))

    }
    "return none if agent does not exists" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("agentfeatures.xml")
        val agentFactory = new AgentFactoryImpl(database)
        val ag           = agentFactory.getById(289)

        ag should be(None)

    }
    "move an agent to another group" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("agentfeatures.xml")
        val agentFactory = new AgentFactoryImpl(database)
        agentFactory.moveAgentToGroup(1, 7)

        agentFactory.getById(1) shouldEqual Some(
          Agent(1, "Jack", "Wright", "1502", "market", 7, 5)
        )

    }
  }
}
