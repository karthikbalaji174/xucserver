package xivo.rabbitmq

import akka.actor.SupervisorStrategy.stop
import akka.actor.{Actor, ActorSystem, OneForOneStrategy, PoisonPill, Props, SupervisorStrategy}
import akka.testkit.{TestActorRef, TestProbe}
import com.rabbitmq.client._
import org.mockito.Mockito._
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.XucEventBus
import services.config.ConfigDispatcher.RefreshLine
import services.config.ConfigInitializer.LoadAgentQueueMembers
import services.config.ConfigServerRequester
import services.request._
import xivo.models._
import xivo.network.WebServiceException
import xivo.xucami.AmiSupervisor

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.util.{Failure, Success}

class XivoRabbitEventsManagerSpec
    extends MockitoSugar
    with AnyWordSpecLike
    with Matchers
    with Eventually {

  class Helper {
    class DummySupervisor extends Actor {
      override def supervisorStrategy: SupervisorStrategy =
        OneForOneStrategy() { case _: Exception => stop }
      override def receive: Receive = { case msg => }
    }

    implicit val system: ActorSystem = ActorSystem()

    val rabbitFactory: XivoRabbitEventsFactory = mock[XivoRabbitEventsFactory]
    val requester: ConfigServerRequester       = mock[ConfigServerRequester]
    val bus                                    = mock[XucEventBus]
    val channel: Channel                       = mock[Channel]

    val eventManager     = TestProbe()
    val configDispatcher = TestProbe()
    val amiSupervisor    = TestProbe()
    val supervisorRef    = TestActorRef(new DummySupervisor)

    val consumerTag = "the-consumer-tag"

    when(rabbitFactory.channel).thenReturn(channel)
    when(rabbitFactory.queueName).thenReturn("test-queue")
    when(rabbitFactory.consumerTag).thenReturn(consumerTag)
    when(rabbitFactory.exchangeName).thenReturn("xivo")
    when(rabbitFactory.routingKeys).thenReturn(List("key1"))

    val eventConsumer =
      new XivoRabbitEventsConsumer(eventManager.ref, rabbitFactory)

    val queueConfig = QueueConfigUpdate(
      1,
      "queue",
      "queue",
      "1010",
      Some("context"),
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      "url",
      "announce",
      Some(1),
      Some("fs"),
      1,
      Some(1),
      Some(1),
      1,
      "recorded",
      1
    )

    val agentMember = QueueMember(
      "queue1",
      1L,
      "Agent/1001",
      1,
      0,
      "agent",
      1,
      "Agent",
      "queue",
      1
    )
    val agentConfig = AgentConfigUpdate(
      1L,
      "firstname",
      "lastname",
      "1001",
      "default",
      List(agentMember),
      1L,
      Some(1L)
    )

    def actor() = {
      def factory: XivoRabbitEventsManager =
        new XivoRabbitEventsManager(
          requester,
          rabbitFactory,
          bus,
          configDispatcher.ref,
          amiSupervisor.ref
        ) {
          override val initialLoginTimeoutDuration: FiniteDuration =
            150.milliseconds
          loginTimeoutDuration = initialLoginTimeoutDuration
        }
      val a = TestActorRef.apply[XivoRabbitEventsManager](
        Props(factory),
        supervisor = supervisorRef
      )
      (a, a.underlyingActor)
    }
  }

  "A XivoRabbitEventsManager" should {
    "connect to rabbitmq on init" in new Helper {
      when(rabbitFactory.createConnection()).thenReturn(Success(()))
      val (_, a) = actor()

      eventually(timeout(1.second)) {
        verify(rabbitFactory).createConnection()
        a.loginTimeout.isCancelled shouldBe true
      }

    }

    "retry connection on timeout" in new Helper {
      when(rabbitFactory.createConnection())
        .thenReturn(Failure(new Exception("failed to connect")))

      val probe: TestProbe = TestProbe()
      val (testActor, a)   = actor()
      probe watch testActor

      verify(rabbitFactory).createConnection()

      reset(rabbitFactory)
      eventually(
        timeout(a.loginTimeoutDuration * 2),
        interval(a.loginTimeoutDuration / 10)
      ) { verify(rabbitFactory).createConnection() }

      reset(rabbitFactory)
      eventually(
        timeout(a.loginTimeoutDuration * 3),
        interval(a.loginTimeoutDuration / 10)
      ) { verify(rabbitFactory).createConnection() }
    }

    "increment retry delay for each retry" in new Helper {
      when(rabbitFactory.createConnection())
        .thenReturn(Failure(new Exception("failed to connect")))

      val probe: TestProbe                             = TestProbe()
      val (testActor, a)                               = actor()
      val originalLoginTimeoutDuration: FiniteDuration = a.loginTimeoutDuration
      probe watch testActor

      eventually(
        timeout(a.loginTimeoutDuration),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 2),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 2)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 3),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 3)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 4),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 4)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 5),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 5)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 6),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 6)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 6),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 6)
      }
    }

    "reset retry delay on connect" in new Helper {
      when(rabbitFactory.createConnection())
        .thenReturn(Failure(new Exception("failed to connect")))

      val probe: TestProbe                             = TestProbe()
      val (testActor, a)                               = actor()
      val originalLoginTimeoutDuration: FiniteDuration = a.loginTimeoutDuration
      probe watch testActor

      eventually(
        timeout(a.loginTimeoutDuration),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration)
      }
      eventually(
        timeout(a.loginTimeoutDuration * 2),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration * 2)
      }

      when(rabbitFactory.createConnection()).thenReturn(Success(()))
      eventually(
        timeout(a.loginTimeoutDuration * 3),
        interval(a.loginTimeoutDuration / 10)
      ) {
        a.loginTimeoutDuration.shouldEqual(originalLoginTimeoutDuration)
      }

    }

    "retry connection on unexpected closing" in new Helper {
      when(rabbitFactory.createConnection()).thenReturn(Success(()))
      val probe: TestProbe = TestProbe()
      val (testActor, a)   = actor()
      probe watch testActor
      reset(rabbitFactory)

      when(rabbitFactory.createConnection())
        .thenReturn(Failure(new Exception("failed to connect")))

      testActor ! RabbitEventConnectionClosed
      eventually(
        timeout(a.loginTimeoutDuration),
        interval(a.loginTimeoutDuration / 10)
      ) { verify(rabbitFactory).createConnection() }
      reset(rabbitFactory)

      when(rabbitFactory.createConnection()).thenReturn(Success(()))
      eventually(
        timeout(a.loginTimeoutDuration * 2),
        interval(a.loginTimeoutDuration / 10)
      ) { verify(rabbitFactory).createConnection() }
    }

      "start consuming events on actor init" in new Helper {
      actor()

      verify(rabbitFactory.channel).basicConsume(
        "test-queue",
        false,
        consumerTag,
        eventConsumer.consumer
      )
    }

    "handle queue created" in new Helper {
      val event =
        RabbitEventQueueCreated(RabbitEventData(1L, None), "queue_created")

      when(requester.getQueueConfig(1L)).thenReturn(Future(queueConfig))

      val (ref, a) = actor()

      ref ! event

      verify(requester).getQueueConfig(1L)
      configDispatcher.expectMsg(queueConfig)
      configDispatcher.expectMsg(LoadAgentQueueMembers)
    }

    "handle queue edited" in new Helper {
      val event =
        RabbitEventQueueEdited(RabbitEventData(1L, None), "queue_edited")

      when(requester.getQueueConfig(1L)).thenReturn(Future(queueConfig))

      val (ref, a) = actor()

      ref ! event

      verify(requester).getQueueConfig(1L)
      configDispatcher.expectMsg(queueConfig)
      configDispatcher.expectMsg(LoadAgentQueueMembers)
    }

    "handle agent edited" in new Helper {
      val event =
        RabbitEventAgentEdited(RabbitEventAgentEditedData(1), "agent_edited")

      when(requester.getAgentConfig(1)).thenReturn(Future(agentConfig))

      val (ref, a) = actor()

      ref ! event

      verify(requester).getAgentConfig(1)
      configDispatcher.expectMsg(agentConfig)
    }

    "handle agent created" in new Helper {
      val event =
        RabbitEventAgentCreated(RabbitEventAgentCreatedData(1), "agent_created")

      when(requester.getAgentConfig(1)).thenReturn(Future(agentConfig))

      val (ref, a) = actor()

      ref ! event

      verify(requester).getAgentConfig(1)
      configDispatcher.expectMsg(agentConfig)
    }

    "handle agent deleted" in new Helper {
      val event = RabbitEventAgentDeleted(
        RabbitEventAgentDeletedData(1L),
        "agent_deleted"
      )

      when(requester.getQueueConfig(1L)).thenReturn(Future(queueConfig))

      val (ref, a) = actor()

      val res = ref ! event

      configDispatcher.expectMsg(RemoveAgentQueueMember(1))
    }

    "handle webservice user created" in new Helper {
      val event = RabbitEventWebserviceUserCreated(
        RabbitEventWebserviceUserData("jbond"),
        "webservice_user_created"
      )

      val (ref, a) = actor()

      val res = ref ! event

      configDispatcher.expectMsg(WebserviceUserActionCreated("jbond"))
    }

    "handle webservice user edited" in new Helper {
      val event = RabbitEventWebserviceUserEdited(
        RabbitEventWebserviceUserData("jbond"),
        "webservice_user_edited"
      )

      val (ref, a) = actor()

      val res = ref ! event

      configDispatcher.expectMsg(WebserviceUserActionEdited("jbond"))
    }

    "handle webservice users reload" in new Helper {
      val event = RabbitEventWebserviceUsersReload(
        "webservice_users_reload"
      )

      val (ref, a) = actor()

      val res = ref ! event

      configDispatcher.expectMsg(WebserviceUsersActionReload)
    }

    "handle sip endpoint updated" in new Helper {
      val event = RabbitEventSipEndpointEdited(
        RabbitEventData(1L, None),
        "sip_endpoint_edited"
      )

      val (ref, _) = actor()

      ref ! event

      configDispatcher.expectMsg(RefreshLine(SipEndpoint(1L)))
    }

    "handle user services updated" in new Helper {
      val event = RabbitEventUserServicesEdited(
        RabbitEventData(123L, None),
        "user_services_edited"
      )
      val response = UserServices(
        true,
        UserForward(false, "1234"),
        UserForward(false, "4567"),
        UserForward(false, "7890")
      )

      when(requester.getUserServices(123L)).thenReturn(Future(response))

      val (ref, a) = actor()

      val res = ref ! event

      configDispatcher.expectMsg(UserServicesUpdated(123, response))
    }

    "handle user preference updated" in new Helper {
      val event = RabbitEventUserPreferenceEdited(
        RabbitEventData(42L, None),
        "user_preference_edited"
      )

      val (ref, _) = actor()

      ref ! event

      configDispatcher.expectMsg(UserPreferenceEdited(42L))
    }

    "handle user preference created" in new Helper {
      val event = RabbitEventUserPreferenceCreated(
        RabbitEventData(42L, None),
        "user_preference_created"
      )

      val (ref, _) = actor()

      ref ! event

      configDispatcher.expectMsg(UserPreferenceCreated(42L))
    }

    "handle user preference delete" in new Helper {
      val event = RabbitEventUserPreferenceDeleted(
        RabbitEventData(42L, None),
        "user_preference_deleted"
      )

      val (ref, _) = actor()

      ref ! event

      configDispatcher.expectMsg(UserPreferenceDeleted(42L))
    }

    "handle sip config updated" in new Helper {
      val event    = RabbitEventSipConfigEdited("sip_config_edited")
      val response = IceServer(Some("host:3478"))
      when(requester.getIceServer).thenReturn(Future(response))

      val (ref, _) = actor()
      ref ! event

      configDispatcher.expectMsg(response)
    }

    "handle mobile app token delete" in new Helper {
      val event = RabbitEventMobilePushTokenDeleted(
        RabbitEventData(42L, None),
        "mobile_push_token_deleted"
      )

      val (ref, _) = actor()

      ref ! event

      configDispatcher.expectMsg(MobilePushTokenDeleted(42L))
    }

    "handle mobile app token edited" in new Helper {
      val event = RabbitEventMobilePushTokenAdded(
        RabbitEventData(42L, None),
        "mobile_push_token_edited"
      )

      val (ref, _) = actor()

      ref ! event

      configDispatcher.expectMsg(MobilePushTokenAdded(42L))
    }

    "forward RabbitEventMediaServerCreated to AmiSupervisor" in new Helper {
      val m = RabbitEventMediaServerCreated(
        RabbitEventData(1, None),
        "mediaserver_created"
      )
      val (ref, a) = actor()
      ref ! m
      amiSupervisor.expectMsg(AmiSupervisor.LoadOrReloadMds(1))
    }

    "forward RabbitEventMediaServerEdited to AmiSupervisor" in new Helper {
      val m = RabbitEventMediaServerEdited(
        RabbitEventData(1, None),
        "mediaserver_edited"
      )
      val (ref, a) = actor()
      ref ! m
      amiSupervisor.expectMsg(AmiSupervisor.LoadOrReloadMds(1))
    }

    "forward RabbitEventMediaServerDeleted to AmiSupervisor" in new Helper {
      val m = RabbitEventMediaServerDeleted(
        RabbitEventData(1, None),
        "mediaserver_deleted"
      )
      val (ref, a) = actor()
      ref ! m
      amiSupervisor.expectMsg(AmiSupervisor.DeleteMds(1))
    }

    "handle unknown message" in new Helper {
      val (ref, a) = actor()

      ref ! "unknown"
      verifyZeroInteractions(requester)
    }

    "handle failure on getQueueConfigUpdate" in new Helper {
      val event =
        RabbitEventQueueEdited(RabbitEventData(1L, None), "queue_edited")

      when(requester.getQueueConfig(1L)).thenReturn(
        Future.failed(
          new WebServiceException("Non understandable JSON returned")
        )
      )

      val (ref, a) = actor()

      ref ! event
      verify(requester).getQueueConfig(1L)
    }

    "handle failure on getQueueConfigCreated" in new Helper {
      val event =
        RabbitEventQueueCreated(RabbitEventData(1L, None), "queue_created")

      when(requester.getQueueConfig(1L)).thenReturn(
        Future.failed(
          new WebServiceException("Non understandable JSON returned")
        )
      )

      val (ref, a) = actor()

      ref ! event
      verify(requester).getQueueConfig(1L)
    }

    "close connection to rabbitmq" in new Helper {
      val (ref, a) = actor()

      ref ! PoisonPill
      verify(rabbitFactory).shutdownConnection()
    }
  }
}
