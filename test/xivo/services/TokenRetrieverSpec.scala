package xivo.services

import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import controllers.helpers.{RequestError, RequestSuccess, RequestUnauthorized}
import models.{Token, XivoUser}
import org.joda.time.DateTime
import org.mockito.Mockito.stub
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigRepository
import xivo.services.TokenRetriever.TokenByLogin
import xivo.services.XivoAuthentication.{
  AuthTimeSynchronizationError,
  AuthTimeout,
  AuthUnknownUser
}

class TokenRetrieverSpec
    extends TestKitSpec("TokenRetrieverSpec")
    with MockitoSugar {

  class Helper {
    val xivoAuthentication = TestProbe()
    val repo               = mock[ConfigRepository]

    def actor() = {
      val a = TestActorRef(new TokenRetriever(xivoAuthentication.ref, repo))
      (a, a.underlyingActor)
    }
  }

  "A TokenRetriever" should {
    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestSuccess on success""" in new Helper {
      val login = "j.doe"
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some(login), None, None, None)
      val token =
        Token(
          "thetoken",
          new DateTime(),
          new DateTime(),
          "cti",
          Some("userId"),
          List.empty
        )
      stub(repo.getCtiUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(token)
      expectMsg(RequestSuccess(token.token))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestError on AuthTimeout""" in new Helper {
      val login = "j.doe"
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some(login), None, None, None)
      stub(repo.getCtiUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(AuthTimeout)
      expectMsg(RequestError("Token retrieval timeout"))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestError on AuthTimeSynchronizationError""" in new Helper {
      val login = "j.doe"
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some(login), None, None, None)
      stub(repo.getCtiUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(AuthTimeSynchronizationError)
      expectMsg(RequestError("Token time synchronization error"))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestUnauthorized on AuthUnknownUser""" in new Helper {
      val login = "j.doe"
      val user =
        XivoUser(12, None, None, "John", Some("Doe"), Some(login), None, None, None)
      stub(repo.getCtiUser(login)).toReturn(Some(user))
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(AuthUnknownUser)
      expectMsg(RequestUnauthorized(s"User $login unknown by the token server"))
    }

    "answer with RequestUnauthorized if the user is not found in ConfigRepository" in new Helper {
      val login = "j.doe"
      stub(repo.getCtiUser(login)).toReturn(None)
      val (ref, a) = actor()

      ref ! TokenByLogin(login)

      expectMsg(RequestUnauthorized(s"User $login unknown by the Xuc"))
    }
  }
}
