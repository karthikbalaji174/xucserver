package xivo.network

import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props, Scheduler}
import akka.io.Tcp
import akka.testkit.{TestActorRef, TestProbe}
import akka.util.ByteString
import akkatest.TestKitSpec
import org.mockito.Matchers._
import org.mockito.Mockito.{stub, verify}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{LoginCapasAck, PhoneStatusUpdate}
import org.xivo.cti.model.Endpoint
import org.xivo.cti.model.Endpoint.EndpointType
import services.Start
import xivo.websocket.LinkState._
import xivo.websocket.LinkStatusUpdate
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{DurationInt, FiniteDuration}

class CtiLinkSpec
    extends TestKitSpec("CtiLinkSpec")
    with MockitoSugar
    with GuiceOneAppPerSuite
    with XucUserHelper {

  import xivo.network.CtiLink._

  class Helper {

    val messageProcessor: MessageProcessor = mock[MessageProcessor]
    val messageFactory                     = new MessageFactory()
    val xucConfig                          = mock[XucBaseConfig]
    val parent                             = TestProbe()
    val ctiLoginStep                       = TestProbe()
    val endpoint                           = new InetSocketAddress("0.0.0.0", 0)

    stub(xucConfig.XIVOCTI_HOST).toReturn("0.0.0.0")
    stub(xucConfig.XIVOCTI_PORT).toReturn("0")
    stub(xucConfig.XivoCtiVersion).toReturn("2.1")
    stub(xucConfig.metricsRegistryName).toReturn("testMetrics")

    def actor(username: String) = {
      val a = TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedKill {
            override def onConnectFailed: Unit = {}
          }
        ),
        parent.ref,
        "testCtiLink"
      )
      a.underlyingActor.ctiLoginStep = ctiLoginStep.ref
      (a, a.underlyingActor)
    }

    def fullActor(username: String): (TestActorRef[CtiLink], CtiLink) = {
      val a = TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedKill
        ),
        parent.ref,
        "testCtiLink"
      )
      a.underlyingActor.ctiLoginStep = ctiLoginStep.ref
      (a, a.underlyingActor)
    }

    def errorActorKilled(username: String): TestActorRef[CtiLink] =
      TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedKill {
            override def receive: Receive = onFailed
          }
        ),
        parent.ref,
        "testCtiLinkOnFailedKill"
      )

    def errorActorRestart(
        username: String
    ): (TestActorRef[CtiLink], CtiLink) = {
      val a = TestActorRef[CtiLink](
        Props(
          new CtiLink(username, messageProcessor, xucConfig)
            with OnTcpFailedRestart {
            override val scheduler: Scheduler = mock[Scheduler]

            override def receive: Receive = onFailed
          }
        ),
        parent.ref,
        "testCtiLinkOnFailedKill"
      )
      (a, a.underlyingActor)
    }

  }

  "A CtiLink actor" should {

    "forward login messages to the ctiLoginStepActor" in new Helper {
      val (ref, ctiLink) = actor("loginmessage")

      val message = new LoginCapasAck()

      ctiLink.processDecodedMessage(message)
      ctiLoginStep.expectMsg(message)
    }

    "parse received messages and send them to its parent except first login messages" in new Helper {
      val (ref, _)    = actor("firstlogin")
      val message     = "{.... json phone status udpate ....}"
      val phoneStatus = new PhoneStatusUpdate()
      stub(messageProcessor.processBuffer(message)).toReturn(Array(phoneStatus))

      ref ! Tcp.Received(ByteString(message))

      parent.expectMsgClass(classOf[PhoneStatusUpdate])
    }

    "not send received cti objects before the network connection is initialized" in new Helper {
      val (ref, ctiLink) = actor("receive")
      val ctiMessage =
        messageFactory.createDial(new Endpoint(EndpointType.PHONE, "1234"))
      val connection = TestProbe()
      ctiLink.connection = connection.ref
      ref ! ctiMessage

      connection.expectNoMessage(100.millis)
    }

    "send received cti objects to the initialized network connection" in new Helper {
      val (ref, ctiLink) = actor("sendreceive")
      val ctiMessage =
        messageFactory.createDial(new Endpoint(EndpointType.PHONE, "1234"))
      val connection = TestProbe()
      ctiLink.connection = connection.ref
      ctiLink.receiveNoBufferize(ctiMessage)
      connection.expectMsg(
        Tcp.Write(ByteString("%s\n".format(ctiMessage.toString)), ctiLink.Ack)
      )
    }

    "send a linkstatus up to parent on connect" in new Helper {
      val (ref, ctiLink) = actor("linkstatusup")

      ref ! Tcp.Connected(null, null)

      parent.expectMsg(LinkStatusUpdate(up))

    }
    "updates user field with user info received in start message and send start login to login step" in new Helper {
      val (ref, ctiLink) = actor("start")

      val user = getXucUser("username", "1234")
      ref ! Start(user)

      ctiLoginStep.expectMsg(StartLogin(user))

    }
  }

  "A CtiLink stop on failing actor" should {
    "send a linkstatus down message on peer disconnect and stop itself" in new Helper {
      val ref = errorActorKilled("linkstatusdown")

      val probe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.PeerClosed

      parent.expectMsg(LinkStatusUpdate(down))

      probe.expectTerminated(ref)
    }
    "Stop itself on connection closed" in new Helper {
      val ref = errorActorKilled("connectionClosed")

      val probe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.Closed

      probe.expectTerminated(ref)
    }
    "Stop itself on connect failed" in new Helper {
      val ref = errorActorKilled("connectfailed")

      val probe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.CommandFailed(Tcp.Connect(new InetSocketAddress(45000)))

      probe.expectTerminated(ref)
    }
    "Stop itself on write failed" in new Helper {
      val ref = errorActorKilled("writefailed")

      val probe = TestProbe()
      probe.watch(ref)

      ref ! Tcp.CommandFailed(Tcp.Write(ByteString.empty, Tcp.NoAck(null)))

      probe.expectTerminated(ref)
    }

    "A CtiLink restart on failing actor" should {

      "send a linkstatus down message on peer disconnect and schedule restart" in new Helper {
        val (ref, a) = errorActorRestart("linkstatusdown")
        a.connection = TestProbe().ref

        ref ! Tcp.PeerClosed

        parent.expectMsg(LinkStatusUpdate(down))

        a.connection shouldBe null

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
      """Restart itself on connection closed and clear the connection
        |Avoids sending a Tcp.Close to a connection which is not always closed, in this case this reschedule a restart
      """.stripMargin in new Helper {
        val (ref, a) = errorActorRestart("TcpClosed")
        a.connection = TestProbe().ref

        ref ! Tcp.Closed

        a.connection shouldBe null

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
      "Restart itself on connect failed" in new Helper {
        val (ref, a) = errorActorRestart("linkstatusdown")

        ref ! Tcp.CommandFailed(Tcp.Connect(new InetSocketAddress(45000)))

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
      "Restart itself  on write failed" in new Helper {
        val (ref, a) = errorActorRestart("linkstatusdown")

        ref ! Tcp.CommandFailed(Tcp.Write(ByteString.empty, Tcp.NoAck(null)))

        verify(a.scheduler).scheduleOnce(
          any[FiniteDuration],
          same(ref),
          any[Restart]
        )(any[ExecutionContext], any[ActorRef])
      }
    }
  }
}
