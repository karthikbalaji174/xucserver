package xivo.network

import java.security.InvalidParameterException

import org.mockito.Mockito.stub
import org.mockito.{Matchers => MockitoMatchers}
import org.scalatest.compatible.Assertion
import org.scalatestplus.mockito.MockitoSugar
import play.api.http.Status
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import xivo.models._
import xivo.xuc.ChatConfig

import scala.concurrent.Future
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpec

class ChatBackendWSSpec extends AsyncWordSpec with Matchers with MockitoSugar {

  case class InjectedConfig(chatConfig: ChatConfig, ws: WSClient)

  trait Helper {
    val assertion: Future[Assertion]

    val fromGuid   = "x9bg8xjcwbfqfqnw1zoun494zc"
    val toGuid     = "ctmrsg87wbbfxb7cjegpi7ad9a"
    val userToken  = "x11sfpzoap86bfu78wtqjsjyky"
    val adminToken = "my-admin-token"
    val channelId  = "abcfd"

    val adminHeaders = ("Authorization", s"Bearer $adminToken")
    val userHeaders  = ("Authorization", s"Bearer $userToken")

    val request: WSRequest   = mock[WSRequest]
    val response: WSResponse = mock[WSResponse]

    val (backend, config) = createBackend()

    def createBackend(): (ChatBackendWS, InjectedConfig) = {
      val chatConfig: ChatConfig = new ChatConfig {
        val chatEnable: Boolean    = true
        val chatHost: String       = "mattermost"
        val chatPort: String       = "8765"
        val chatAdminToken: String = adminToken
        val secret: Option[String] = Some("my-secret-ushhhhh")
      }
      val wsClient: WSClient = mock[WSClient]

      val inject = InjectedConfig(chatConfig, wsClient)

      (new ChatBackendWS(inject.chatConfig, inject.ws), inject)
    }
  }

  "ChatBackend WS" should {
    "Handle unexpected HTTP status with a failure" in new Helper {
      stub(response.status).toReturn(Status.INTERNAL_SERVER_ERROR)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .checkResponseStatus(response)
        .failed
        .map(t => {
          t shouldBe a[WebServiceException]
        })

    }.assertion

    "Handle HTTP status OK with a success" in new Helper {
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .checkResponseStatus(response)
        .map(r => {
          r shouldBe response
        })

    }.assertion

    "Handle HTTP status CREATED with a success" in new Helper {
      stub(response.status).toReturn(Status.CREATED)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .checkResponseStatus(response)
        .map(r => {
          r shouldBe response
        })

    }.assertion

    "Handle JSON parsing error with a failure" in new Helper {
      stub(response.json).toReturn(Json.parse("""{"error":"invalid json"}"""))

      val assertion = backend
        .handleParsingError[MattermostUser](response)
        .failed
        .map(t => {
          t shouldBe a[WebServiceException]
        })
    }.assertion

    "get user" in new Helper {
      val payload = List("user_1")

      val expected: List[MattermostUser] = List(
        MattermostUser(
          "1",
          1,
          1,
          1,
          "user",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          MattermostTimeZone("", "", "")
        )
      )
      val json: JsValue = Json.toJson(expected)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.post(Json.toJson(payload)))
        .toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getCtiUser(1)
        .map(r => {
          r shouldBe expected
        })
    }.assertion

    "get user token" in new Helper {
      val json: JsObject = Json.obj(
        "login_id" -> "user_1",
        "password" -> "suAeyY122JmNhczxN1Acusx9eesfhnWW"
      )

      val expected = "some token"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.post(json)).toReturn(Future.successful(response))

      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")
      stub(response.header("Token")).toReturn(Some("some token"))

      val assertion = backend
        .getUserToken(1)
        .map(r => {
          r shouldBe expected
        })
    }.assertion

    "get user by guid" in new Helper {
      val mattermostUser = MattermostUser(
        "1",
        1,
        1,
        1,
        "user",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )

      val json: JsValue = Json.toJson(mattermostUser)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getCtiUser(toGuid)
        .map(r => {
          r shouldBe mattermostUser
        })
    }.assertion

    "create user" in new Helper {
      val payload: NewMattermostUser = NewMattermostUser(
        "user_1@xivopbx",
        "user_1",
        "suAeyY122JmNhczxN1Acusx9eesfhnWW",
        "user",
        "one"
      )

      val expected: MattermostUser = MattermostUser(
        "1",
        1,
        1,
        1,
        "user",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        MattermostTimeZone("", "", "")
      )
      val json: JsValue = Json.toJson(expected)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.post(Json.toJson(payload)))
        .toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.CREATED)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .createUser(1, "user", "one")
        .map(r => {
          r shouldBe expected
        })
    }.assertion

    "create direct channel" in new Helper {
      val payload = List(fromGuid, toGuid)

      val expected: MattermostDirectChannel =
        MattermostDirectChannel("123", "xivo", "from_to", "D")
      val json: JsValue = Json.toJson(expected)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.post(Json.toJson(payload)))
        .toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.CREATED)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .createDirectMessageChannel(fromGuid, toGuid, userToken)
        .map(r => {
          r shouldBe expected
        })
    }.assertion

    "create post in channel" in new Helper {
      val post: NewMattermostChannelPost =
        NewMattermostChannelPost("12345", "some message", 0, "jdoe", "asample")

      val expected = MattermostDirectMessageAck(
        "12345",
        channelId,
        "some message",
        1582203515356L,
        fromGuid,
        0,
        "jdoe",
        "asample"
      )
      val json: JsValue = Json.toJson(expected)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.post(Json.toJson(post)))
        .toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.CREATED)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .createPostInChannel(post, userToken)
        .map(r => {
          r shouldBe expected
        })
    }.assertion

    "get team id" in new Helper {
      val team: MattermostTeam = MattermostTeam("123", "xivo")
      val json: JsValue        = Json.toJson(team)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend.getTeamId.map(r => {
        r shouldBe "123"
      })
    }.assertion

    "get channels for user" in new Helper {
      val team: MattermostTeam = MattermostTeam("123", "xivo")
      val teamJson: JsValue    = Json.toJson(team)

      val channels              = List(MattermostDirectChannel("1", "", "name", "D"))
      val channelsJson: JsValue = Json.toJson(channels)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(teamJson).toReturn(channelsJson)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getChannelsForUser(fromGuid)
        .map(r => {
          r shouldBe channels
        })
    }.assertion

    "get channels for user (cached team id)" in new Helper {
      val channels      = List(MattermostDirectChannel("1", "", "name", "D"))
      val json: JsValue = Json.toJson(channels)
      backend.teamId = "123"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getChannelsForUser(fromGuid)
        .map(r => {
          r shouldBe channels
        })
    }.assertion

    "get no channels for user" in new Helper {
      val team: MattermostTeam = MattermostTeam("123", "xivo")
      val teamJson: JsValue    = Json.toJson(team)

      val errorResponse = NoChannelsFoundError(
        message = "No channels were found",
        status_code = 400
      )
      val json = Json.toJson(errorResponse)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(teamJson).toReturn(json)
      stub(response.status).toReturn(Status.BAD_REQUEST)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getChannelsForUser(fromGuid)
        .map(r => {
          r shouldBe List()
        })
    }.assertion

    "get channel members with filter" in new Helper {
      val members = List(
        MattermostChannelMember(channelId, fromGuid),
        MattermostChannelMember(channelId, toGuid)
      )
      val json: JsValue = Json.toJson(members)
      backend.teamId = "123"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getChannelMembers(
          channelId,
          userToken,
          Some(fromGuid),
          filtered = true
        )
        .map(r => {
          r shouldBe List(members(1))
        })
    }.assertion

    "get channel members without filter" in new Helper {
      val members = List(
        MattermostChannelMember(channelId, fromGuid),
        MattermostChannelMember(channelId, toGuid)
      )
      val json: JsValue = Json.toJson(members)
      backend.teamId = "123"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getChannelMembers(
          channelId,
          userToken,
          Some(fromGuid),
          filtered = false
        )
        .map(r => {
          r shouldBe members
        })
    }.assertion

    "get channel posts" in new Helper {
      val postId1        = "post-id-1"
      val postId2        = "post-id-2"
      val postId3        = "post-id-3"
      val created1: Long = 1582203515356L
      val created2: Long = 1582039531647L
      val created3: Long = 1582039531697L

      val posts =
        s"""{
           |    "$postId2": {
           |      "id": "$postId2",
           |      "create_at": $created2,
           |      "update_at": 1582039531647,
           |      "user_id": "$fromGuid",
           |      "channel_id": "$channelId",
           |      "message": "MESSAGE-ORDER-2",
           |      "metadata": {}
           |    },
           |    "$postId1": {
           |      "id": "$postId1",
           |      "create_at": $created1,
           |      "update_at": 1582203515356,
           |      "user_id": "$toGuid",
           |      "channel_id": "$channelId",
           |      "message": "MESSAGE-ORDER-1",
           |      "metadata": {}
           |    },
           |    "$postId3": {
           |      "id": "$postId3",
           |      "create_at": $created3,
           |      "update_at": 1582039531697,
           |      "user_id": "$toGuid",
           |      "channel_id": "$channelId",
           |      "message": "MESSAGE-ORDER-3",
           |      "metadata": {}
           |    }
           |}""".stripMargin

      val jsonResponse =
        s"""{
           |    "order": [
           |        "$postId3",
           |        "$postId2",
           |        "$postId1"
           |        ],
           |    "posts": $posts
           |}""".stripMargin

      val json: JsValue = Json.parse(jsonResponse)
      backend.teamId = "123"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val expected: List[MattermostDirectMessage] = List(
        MattermostDirectMessage(
          "post-id-1",
          channelId,
          "MESSAGE-ORDER-1",
          created1,
          toGuid
        ),
        MattermostDirectMessage(
          "post-id-2",
          channelId,
          "MESSAGE-ORDER-2",
          created2,
          fromGuid
        ),
        MattermostDirectMessage(
          "post-id-3",
          channelId,
          "MESSAGE-ORDER-3",
          created3,
          toGuid
        )
      )

      val assertion = backend
        .getPostsForChannel(channelId, userToken)
        .map(r => {
          r shouldBe expected
        })

    }.assertion

    "get unread messages for user and channel" in new Helper {
      val unread        = MattermostUnreadCounter("", channelId, 5)
      val json: JsValue = Json.toJson(unread)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getUnreadMessagessCounterByChannel(channelId, fromGuid, userToken)
        .map(r => {
          r shouldBe unread
        })
    }.assertion

    "get posts around last unread " in new Helper {
      val postId1        = "post-id-1"
      val postId2        = "post-id-2"
      val postId3        = "post-id-3"
      val created1: Long = 1582203515356L
      val created2: Long = 1582039531647L
      val created3: Long = 1582039531697L

      val posts =
        s"""{
           |    "$postId2": {
           |      "id": "$postId2",
           |      "create_at": $created2,
           |      "update_at": 1582039531647,
           |      "user_id": "$fromGuid",
           |      "channel_id": "$channelId",
           |      "message": "MESSAGE-ORDER-2",
           |      "metadata": {}
           |    },
           |    "$postId1": {
           |      "id": "$postId1",
           |      "create_at": $created1,
           |      "update_at": 1582203515356,
           |      "user_id": "$toGuid",
           |      "channel_id": "$channelId",
           |      "message": "MESSAGE-ORDER-1",
           |      "metadata": {}
           |    },
           |    "$postId3": {
           |      "id": "$postId3",
           |      "create_at": $created3,
           |      "update_at": 1582039531697,
           |      "user_id": "$toGuid",
           |      "channel_id": "$channelId",
           |      "message": "MESSAGE-ORDER-3",
           |      "metadata": {}
           |    }
           |}""".stripMargin

      val jsonResponse =
        s"""{
           |    "order": [
           |        "$postId3",
           |        "$postId2",
           |        "$postId1"
           |        ],
           |    "posts": $posts
           |}""".stripMargin

      val json: JsValue = Json.parse(jsonResponse)
      backend.teamId = "123"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val expected: List[MattermostDirectMessage] = List(
        MattermostDirectMessage(
          "post-id-1",
          channelId,
          "MESSAGE-ORDER-1",
          created1,
          toGuid
        ),
        MattermostDirectMessage(
          "post-id-2",
          channelId,
          "MESSAGE-ORDER-2",
          created2,
          fromGuid
        ),
        MattermostDirectMessage(
          "post-id-3",
          channelId,
          "MESSAGE-ORDER-3",
          created3,
          toGuid
        )
      )

      val assertion = backend
        .getPostsAroundLastUnread(channelId, fromGuid, userToken)
        .map(r => {
          r shouldBe expected
        })
    }.assertion

    "get zero posts around last unread " in new Helper {
      val jsonResponse =
        s"""{
           |    "order": [
           |        "",
           |        "",
           |        ""
           |        ],
           |    "posts": ""
           |}""".stripMargin

      val json: JsValue = Json.parse(jsonResponse)
      backend.teamId = "123"

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getPostsAroundLastUnread(channelId, fromGuid, userToken)
        .map(r => {
          r shouldBe List()
        })
    }.assertion

    "set active channel" in new Helper {
      val view = MattermostViewChannel(channelId)
      val jsonResponse: JsObject =
        Json.obj("eddbt7qpb7gexp4fqtdnesxxch" -> 1583422943750L)
      val viewResponse  = MattermostViewChannelResponse("OK", jsonResponse)
      val json: JsValue = Json.toJson(viewResponse)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(userHeaders)).toReturn(request)
      stub(request.post(Json.toJson(view)))
        .toReturn(Future.successful(response))

      stub(response.json).toReturn(json)
      stub(response.status).toReturn(Status.OK)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .setActiveChannel(channelId, fromGuid, userToken)
        .map(r => {
          r shouldBe viewResponse
        })
    }.assertion

    "parse id from user string" in {
      ChatBackendWS.getIdFromUser("user_1") shouldEqual "1"

      an[InvalidParameterException] should be thrownBy ChatBackendWS
        .getIdFromUser("1")
      an[InvalidParameterException] should be thrownBy ChatBackendWS
        .getIdFromUser("unknown")
    }

    "handle error response" in new Helper {
      val team: MattermostTeam = MattermostTeam("123", "xivo")
      val teamJson: JsValue    = Json.toJson(team)

      val errorResponse =
        NoChannelsFoundError(message = "an error", status_code = 400)
      val json = Json.toJson(errorResponse)

      stub(backend.ws.url(MockitoMatchers.any[String])).toReturn(request)
      stub(request.withHttpHeaders(adminHeaders)).toReturn(request)
      stub(request.get()).toReturn(Future.successful(response))

      stub(response.json).toReturn(teamJson).toReturn(json)
      stub(response.status).toReturn(Status.BAD_REQUEST)
      stub(response.body).toReturn("some body")

      val assertion = backend
        .getChannelsForUser(fromGuid)
        .failed
        .map(r => {
          r shouldBe a[WebServiceException]
        })
    }.assertion
  }
}
