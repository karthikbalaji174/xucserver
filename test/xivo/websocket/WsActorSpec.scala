package xivo.websocket

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.testkit.TestProbe
import akkatest.TestKitSpec
import controllers.helpers.RequestSuccess
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Json
import services._
import services.config.ConfigRepository
import services.request._
import services.user.{AdminRight, Right, UserRight}
import xivo.services.{AuthenticationToken, TokenRetriever}
import xivo.websocket.ActorFlowOut.ActorFlowOutAck
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.WsContent
import xivo.xuc.ConfigServerConfig
import xuctest.XucUserHelper

import scala.concurrent.duration._

class WsActorSpec
    extends TestKitSpec("WsActorSpec")
    with MockitoSugar
    with XucUserHelper {

  class DummyActor extends Actor {
    def receive: Receive = Actor.emptyBehavior
  }

  case class CustomRight(profile: String, filterFn: Any => Any = a => a)
      extends UserRight {
    def filter(o: Any, configRepository: ConfigRepository): Option[Any] =
      Some(filterFn(o))
    def profileName: String = profile
    def rights: List[Right] = List.empty
  }

  class Helper {

    val router              = TestProbe()
    val rabbitEventManager  = TestProbe()
    val out                 = TestProbe()
    val tokenRetrieverProbe = TestProbe()
    val login               = "testUser"
    val user                = getXucUser(login, "")
    val wsBus               = mock[WsBus]
    val requestDecoder      = mock[RequestDecoder]
    val wsEncoder           = mock[WsEncoder]
    val xivoAuthentication  = TestProbe()
    val token               = "thetoken"
    val tokenRetrieverFactory = new TokenRetriever.Factory {
      def apply(): Actor = new DummyActor()
    }
    val configRepository   = mock[ConfigRepository]
    val configServerConfig = mock[ConfigServerConfig]

    val repo = mock[ConfigRepository]

    class TestRouterFactory extends Actor {
      override def preStart(): Unit = {
        context.actorOf(
          Props(createActorProxy(router.ref)),
          "testUserCtiRouter"
        )
      }
      def receive = { case GetRouter(u) =>
        sender() ! Router(u, router.ref)
      }
    }

    val routerFactory = system.actorOf(Props(new TestRouterFactory()))

    def actor(right: UserRight = AdminRight()) = {
      val a = system.actorOf(
        Props(
          new WsActor(
            user,
            out.ref,
            right,
            wsBus,
            routerFactory,
            tokenRetrieverFactory,
            configRepository,
            requestDecoder,
            wsEncoder
          )
        )
      )
      a
    }

    def readyActor(right: UserRight = AdminRight()) = {
      val a = system.actorOf(
        Props(
          new WsActor(
            user,
            out.ref,
            right,
            wsBus,
            routerFactory,
            tokenRetrieverFactory,
            configRepository,
            requestDecoder,
            wsEncoder
          ) {}
        )
      )
      a ! Router(user, router.ref)
      a
    }

  }

  "WsActor" should {
    "subscribe to wsbus at startup and send wsconnected message to router on use router message" in new Helper {
      val ref = actor()

      verify(wsBus, timeout(3000)).subscribe(ref, WsBus.browserTopic(login))
      router.expectMsg(WsConnected(ref, user))
    }

    "unsubscribe from bus on stop" in new Helper {
      val ref = actor()

      verify(wsBus, timeout(3000)).subscribe(ref, WsBus.browserTopic(login))
      router.expectMsg(WsConnected(ref, user))
      ref ! PoisonPill

      verify(wsBus, timeout(3000)).unsubscribe(ref)
      router.expectMsg(Leave(ref))
    }

    "send back authentication message and right upon connection" in new Helper {
      val right = CustomRight("MyProfile")
      val ref   = readyActor(right)

      ref ! RequestSuccess(token)
      out.expectMsg(WebSocketEvent.createEvent(AuthenticationToken(token)))

      ref ! ActorFlowOutAck
      out.expectMsg(WebSocketEvent.createEvent(right))
    }

    "send back ping message to sender" in new Helper {
      val ref = readyActor()

      val pingMsg = Json.obj("claz" -> "ping")

      stub(requestDecoder.decode(pingMsg)).toReturn(Ping)

      ref ! pingMsg

      out.expectMsg(Json.parse(pingMsg.toString()))
    }

    "forward jsvalue messages to request decoder and send result to router" in new Helper {
      val message = Json.toJson("message")
      case object FakeRequest extends XucRequest
      stub(requestDecoder.decode(message)).toReturn(FakeRequest)

      val ref = readyActor()
      router.expectMsg(WsConnected(ref, user))

      ref ! message
      router.expectMsgAllOf(BaseRequest(ref, FakeRequest))
    }

    "send error to sender on invalid request " in new Helper {
      val ref     = readyActor()
      val message = Json.toJson("message")
      val invalidRequest =
        InvalidRequest(XucRequest.errors.invalid, message.toString())
      stub(requestDecoder.decode(message)).toReturn(invalidRequest)

      ref ! message

      out.expectMsg(
        WebSocketEvent.createError(WSMsgType.Error, invalidRequest.toString)
      )
    }

    "forward wscontent json message to browser when no message is pending" in new Helper {
      val ref         = readyActor()
      val jsonContent = Json.obj("dummy" -> "object")
      val message     = WsContent(jsonContent)

      ref ! message

      out.expectMsg(jsonContent)
    }

    "forward messages to browser when no message is pending" in new Helper {
      val message         = "dummy"
      val filteredMessage = "dummy2"
      val right           = CustomRight("Profile", _ => filteredMessage)
      val jsonContent     = Json.obj("dummy" -> "object")
      val wsMessage       = WsContent(jsonContent)

      stub(wsEncoder.encode(filteredMessage)).toReturn(Some(wsMessage))

      val ref = readyActor(right)

      ref ! message
      out.expectMsg(jsonContent)
    }

    "enqueue wscontent json message to browser when there's a message pending" in new Helper {
      val ref          = readyActor()
      val jsonContent  = Json.obj("dummy" -> "object")
      val message      = WsContent(jsonContent)
      val jsonContent2 = Json.obj("dummy" -> "object2")
      val message2     = WsContent(jsonContent2)

      ref ! message
      ref ! message2

      out.expectMsg(jsonContent)

      out.expectNoMessage(250.milliseconds)
    }

    "dequeue pending wscontent message on ActorFlowAck" in new Helper {
      val ref          = readyActor()
      val jsonContent  = Json.obj("dummy" -> "object")
      val message      = WsContent(jsonContent)
      val jsonContent2 = Json.obj("dummy" -> "object2")
      val message2     = WsContent(jsonContent2)

      ref ! message
      ref ! message2

      out.expectMsg(jsonContent)
      out.expectNoMessage(250.milliseconds)

      ref ! ActorFlowOutAck
      out.expectMsg(jsonContent2)
    }

    "dequeue pending wscontent message on ActorFlowAck with empty queue" in new Helper {
      val ref         = readyActor()
      val jsonContent = Json.obj("dummy" -> "object")
      val message     = WsContent(jsonContent)

      ref ! ActorFlowOutAck

      out.expectNoMessage(250.milliseconds)

      ref ! message
      out.expectMsg(jsonContent)
    }

    "not forward or enqueue messages to browser on ActorFlowAck" in new Helper {
      val ref         = readyActor()
      val jsonContent = Json.obj("dummy" -> "object")
      val message     = WsContent(jsonContent)

      ref ! ActorFlowOutAck

      out.expectNoMessage(250.milliseconds)
    }

    "filter and encode all other messages to browser" in new Helper {
      val message         = "dummy"
      val filteredMessage = "dummy2"
      val right           = CustomRight("Profile", _ => filteredMessage)

      val ref         = readyActor(right)
      val jsonContent = Json.obj("dummy" -> "object")
      val wsMessage   = WsContent(jsonContent)

      stub(wsEncoder.encode(filteredMessage)).toReturn(Some(wsMessage))

      ref ! message

      out.expectMsg(jsonContent)
    }

    "accept RequestSuccess even if Router is not instantiated yet" in new Helper {
      val ref: ActorRef = system.actorOf(
        Props(
          new WsActor(
            user,
            out.ref,
            AdminRight(),
            wsBus,
            routerFactory,
            tokenRetrieverFactory,
            configRepository,
            requestDecoder,
            wsEncoder
          ) {
            override val tokenRetriever = tokenRetrieverProbe.ref
          }
        )
      )

      ref ! RequestSuccess(token)
      ref ! Router(user, router.ref)
      out.expectMsg(WebSocketEvent.createEvent(AuthenticationToken(token)))

      ref ! PoisonPill
    }

    "accept RequestSuccess when Router is instantiated" in new Helper {
      val ref: ActorRef = system.actorOf(
        Props(
          new WsActor(
            user,
            out.ref,
            AdminRight(),
            wsBus,
            routerFactory,
            tokenRetrieverFactory,
            configRepository,
            requestDecoder,
            wsEncoder
          ) {
            override val tokenRetriever = tokenRetrieverProbe.ref
          }
        )
      )

      ref ! Router(user, router.ref)
      ref ! RequestSuccess(token)
      out.expectMsg(WebSocketEvent.createEvent(AuthenticationToken(token)))

      ref ! PoisonPill
    }
  }

}
