package xivo.websocket

import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import xivo.websocket.WsBus.WsMessageEvent

class WsBusSpec extends TestKitSpec("WsBusSpec") with MockitoSugar {

  class Helper {
    val wsBus      = new WsBus()
    val subscriber = TestProbe()
  }

  "WsBus" should {

    "send the event to the subscriber" in new Helper {
      wsBus.subscribe(subscriber.ref, "testtopic")

      wsBus.publish(WsMessageEvent("testtopic", "text"))

      subscriber.expectMsg("text")
    }
  }
}
