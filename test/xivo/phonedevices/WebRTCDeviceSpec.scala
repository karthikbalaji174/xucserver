package xivo.phonedevices

import akkatest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import services.calltracking.DeviceCall
import xivo.websocket.{
  WebRTCAnswerCmd,
  WebRTCHoldCmd,
  WebRTCMuteCmd,
  WebRTCSendDtmfCmd,
  WebSocketEvent
}
import xivo.xuc.XucConfig
import xivo.xucami.models.{CallerId, Channel}

class WebRTCDeviceSpec
    extends TestKitSpec("WebRTCDeviceSpec")
    with MockitoSugar {
  import xivo.models.LineHelper.makeLine

  val line =
    makeLine(1, "default", "SIP", "ihvbur", None, None, "ip", webRTC = true)
  val phoneNumber = "1000"

  class Helper {
    val device = new WebRTCDevice(line, phoneNumber, mock[XucConfig])
  }

  "WebRTCDevice actions without uniqueId" should {
    "send answer command to the sender parameter" in new Helper() {
      device.answer(None, self)
      expectMsg(WebSocketEvent.createWebRTCCommand(WebRTCAnswerCmd(None)))
    }

    "send hold command to the sender parameter" in new Helper() {
      device.hold(None, self)
      expectMsg(WebSocketEvent.createWebRTCCommand(WebRTCHoldCmd(None)))
    }

    "send dtmf command to the sender parameter" in new Helper() {
      device.sendDtmf('5', self)
      expectMsg(WebSocketEvent.createWebRTCCommand(WebRTCSendDtmfCmd('5')))
    }

    "send toggleMicrophone command to the sender parameter" in new Helper() {
      device.toggleMicrophone(None, self)
      expectMsg(WebSocketEvent.createWebRTCCommand(WebRTCMuteCmd(None)))
    }
  }

  "WebRTCDevice actions with uniqueId" should {
    val sipCallId = "123-456@192.168"
    def createDeviceCall: Option[DeviceCall] = {

      val channel = Channel(
        "12345679.012",
        "SIP/abcd-00001",
        CallerId("1000", "Someone"),
        "12345679.123",
        variables = Map("SIPCALLID" -> sipCallId)
      )
      Some(DeviceCall("chan", Some(channel), Set.empty, Map.empty))
    }

    "send answer command to the sender parameter" in new Helper() {
      device.answer(createDeviceCall, self)
      expectMsg(
        WebSocketEvent.createWebRTCCommand(WebRTCAnswerCmd(Some(sipCallId)))
      )
    }

    "send hold command to the sender parameter" in new Helper() {
      device.hold(createDeviceCall, self)
      expectMsg(
        WebSocketEvent.createWebRTCCommand(WebRTCHoldCmd(Some(sipCallId)))
      )
    }

    "send toggleMicrophone command to the sender parameter" in new Helper() {
      device.toggleMicrophone(createDeviceCall, self)
      expectMsg(
        WebSocketEvent.createWebRTCCommand(WebRTCMuteCmd(Some(sipCallId)))
      )
    }
  }
}
