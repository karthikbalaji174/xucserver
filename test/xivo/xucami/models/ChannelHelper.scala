package xivo.xucami.models

import org.asteriskjava.manager.event.{CoreShowChannelEvent, NewChannelEvent}
import xivo.xucami.models
import xivo.xucami.models.ChannelState.ChannelState
import xivo.xucami.models.MonitorState.MonitorState

trait ChannelHelper {
  object Channel {
    val VarNames             = models.Channel.VarNames
    val callTypeValOriginate = models.Channel.callTypeValOriginate
    val callTypeValOutboundOriginate =
      models.Channel.callTypeValOutboundOriginate

  }
  def Channel(e: NewChannelEvent)                 = models.Channel(e, "default")
  def Channel(e: NewChannelEvent, s: String)      = models.Channel(e, s)
  def Channel(e: CoreShowChannelEvent)            = models.Channel(e, "default")
  def Channel(e: CoreShowChannelEvent, s: String) = models.Channel(e, s)
  def Channel(
      id: String,
      name: String,
      callerId: CallerId,
      linkedChannelId: String,
      state: ChannelState = ChannelState.UNITIALIZED,
      monitored: MonitorState = MonitorState.DISABLED,
      exten: Option[String] = None,
      connectedLineNb: Option[String] = None,
      connectedLineName: Option[String] = None,
      agentNumber: Option[String] = None,
      variables: Map[String, String] = Map(),
      remoteState: Option[ChannelState] = None,
      queueHistory: List[QueueHistoryEvent] = List.empty,
      direction: Option[ChannelDirection.ChannelDirection] = None,
      mdsName: String = "default"
  ) =
    models.Channel(
      id,
      name,
      callerId,
      linkedChannelId,
      state,
      monitored,
      exten,
      connectedLineNb,
      connectedLineName,
      agentNumber,
      variables,
      remoteState,
      queueHistory,
      direction,
      mdsName
    )
}
