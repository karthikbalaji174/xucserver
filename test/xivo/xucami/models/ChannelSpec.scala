package xivo.xucami.models

import org.asteriskjava.manager.event.{
  CoreShowChannelEvent,
  NewChannelEvent,
  NewStateEvent
}
import org.joda.time.DateTime
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.mockito.MockitoSugar

class ChannelSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with ChannelHelper {

  "Channel" should {
    "be created from new channel event" in {
      val uniqueId     = "1432139825.43"
      val linkedId     = "1432139825.40"
      val chanName     = "Local/3099@default-0000000a;1"
      val callerId     = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setLinkedid(linkedId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setChannelState(0)

      val expected = Channel(
        uniqueId,
        chanName,
        callerId,
        linkedChannelId = linkedId,
        state = ChannelState.DOWN,
        direction = None
      )
      val channel = Channel(newChanEvent)

      channel should be(expected)
    }

    "be created from new channel event with mds" in {
      val uniqueId     = "1432139825.43"
      val linkedId     = "1432139825.40"
      val chanName     = "Local/3099@default-0000000a;1"
      val callerId     = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setLinkedid(linkedId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setChannelState(0)

      val expected = Channel(
        uniqueId,
        chanName,
        callerId,
        linkedChannelId = linkedId,
        state = ChannelState.DOWN,
        mdsName = "mds-0"
      )
      val channel = Channel(newChanEvent, "mds-0")

      channel should be(expected)
    }

    "be created from CoreShowChannelEvent" in {
      val uniqueId     = "1432139825.43"
      val linkedId     = "1432139825.99"
      val chanName     = "Local/3099@default-0000000a;1"
      val callerId     = CallerId("jack", "1200")
      val newChanEvent = new CoreShowChannelEvent(this)
      newChanEvent.setUniqueid(uniqueId)
      newChanEvent.setLinkedid(linkedId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setChannelState(0)

      val expected = Channel(
        uniqueId,
        chanName,
        callerId,
        linkedChannelId = linkedId,
        state = ChannelState.DOWN
      )
      val channel = Channel(newChanEvent)

      channel should be(expected)
    }

    "be created from CoreShowChannelEvent with mds" in {
      val uniqueId     = "1432139825.43"
      val linkedId     = "1432139825.99"
      val chanName     = "Local/3099@default-0000000a;1"
      val callerId     = CallerId("jack", "1200")
      val newChanEvent = new CoreShowChannelEvent(this)
      newChanEvent.setUniqueid(uniqueId)
      newChanEvent.setLinkedid(linkedId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setChannelState(0)

      val expected = Channel(
        uniqueId,
        chanName,
        callerId,
        linkedChannelId = linkedId,
        state = ChannelState.DOWN,
        mdsName = "mds-0"
      )
      val channel = Channel(newChanEvent, "mds-0")

      channel should be(expected)
    }

    "be created with default state UNITIALIZED" in {
      val uniqueId     = "1432139825.43"
      val chanName     = "Local/3099@default-0000000a;1"
      val callerId     = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setLinkedid(uniqueId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      val expected = Channel(
        uniqueId,
        chanName,
        callerId,
        linkedChannelId = uniqueId,
        state = ChannelState.UNITIALIZED
      )
      val channel = Channel(newChanEvent)

      channel should be(expected)

    }

    "set direction from new channel event if set in case new state event ringing is missing (i.e. pjsip)" in {
      val uniqueId     = "1432139825.43"
      val linkedId     = "1432139825.40"
      val chanName     = "Local/3099@default-0000000a;1"
      val callerId     = CallerId("jack", "1200")
      val newChanEvent = new NewChannelEvent("")
      newChanEvent.setUniqueId(uniqueId)
      newChanEvent.setLinkedid(linkedId)
      newChanEvent.setChannel(chanName)
      newChanEvent.setCallerIdName(callerId.name)
      newChanEvent.setCallerIdNum(callerId.number)
      newChanEvent.setChannelState(4)

      val expected = Channel(
        uniqueId,
        chanName,
        callerId,
        linkedChannelId = linkedId,
        state = ChannelState.ORIGINATING,
        direction = Some(ChannelDirection.OUTGOING)
      )
      val channel = Channel(newChanEvent)

      channel should be(expected)
    }

    "Replace state, connectedLineNb and connectedLineName (but not callerId) on new state" in {
      val chan = Channel(
        "1433776805.70",
        "SIP/01025555-0000003f",
        CallerId("Name", "1200"),
        "1433776805.70",
        state = ChannelState.DOWN
      )

      val newStateEvent = new NewStateEvent("")
      newStateEvent.setChannelState(4)
      newStateEvent.setCallerIdName("newName")
      newStateEvent.setCallerIdNum("1234")
      newStateEvent.setChannelState(ChannelState.DIALING.id)
      newStateEvent.setConnectedLineNum("44200")
      newStateEvent.setConnectedLineName("Serge Gainsbourg")

      val newChan = chan.withState(newStateEvent)
      newChan.state should be(ChannelState.DIALING)
      newChan.connectedLineNb should be(Some("44200"))
      newChan.connectedLineName should be(Some("Serge Gainsbourg"))
      newChan.callerId should be(CallerId("Name", "1200"))
    }
    "Replace callerId, state, connectedLineNb and connectedLineName on new state if agent callback channel" in {
      val chan = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        state = ChannelState.DOWN
      )
      val newStateEvent = new NewStateEvent("")
      newStateEvent.setChannelState(4)
      newStateEvent.setCallerIdName("newName")
      newStateEvent.setCallerIdNum("1234")
      newStateEvent.setChannelState(ChannelState.DIALING.id)
      newStateEvent.setConnectedLineNum("44200")
      newStateEvent.setConnectedLineName("Serge Gainsbourg")

      val newChan = chan.withState(newStateEvent)
      newChan.state should be(ChannelState.DIALING)
      newChan.connectedLineNb should be(Some("44200"))
      newChan.connectedLineName should be(Some("Serge Gainsbourg"))
      newChan.callerId should be(CallerId("newName", "1234"))
    }
    "add new variables while removing XUC_CALLTYPE if it's equal to 'Originate'" in {
      val chan = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        variables = Map("testVar" -> "testValue")
      )

      val newChan = chan.addVariables(
        Map(
          Channel.VarNames.xucCallType -> Channel.callTypeValOriginate,
          "newVar"                     -> "newValue"
        )
      )
      newChan.variables("newVar") should be("newValue")
      newChan.variables.get(Channel.VarNames.xucCallType) should be(None)
    }
    "add new variables and do not remove XUC_CALLTYPE if it's not equal to 'Originate'" in {
      val chan = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        variables = Map("testVar" -> "testValue")
      )

      val newChan = chan.addVariables(
        Map(Channel.VarNames.xucCallType -> "test", "newVar" -> "newValue")
      )
      newChan.variables("newVar") should be("newValue")
      newChan.variables.get(Channel.VarNames.xucCallType) should be(
        Some("test")
      )
    }

    "update queue history when event received" in {
      val chan = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
      val enterEvent = EnterQueue(
        "cars",
        "1434368938.106",
        QueueCall(
          1,
          Some("Someone"),
          "1000",
          new DateTime(),
          chan.name,
          "main"
        ),
        "Local/id-22@agentcallback-00000014;1f"
      )
      val leaveEvent = LeaveQueue(
        "cars",
        "1434368938.106",
        new DateTime(),
        "Local/id-22@agentcallback-00000014;1f"
      )

      val newChan = chan.withQueueHistory(enterEvent)
      newChan.queueHistory.size should be(1)
      newChan.queueHistory.head should be(enterEvent)

      val newChan2 = newChan.withQueueHistory(leaveEvent)
      newChan2.queueHistory.size should be(2)
      newChan2.queueHistory.head should be(leaveEvent)

    }

    "update the group pickup initiator bool when event received" in {
      val chan = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      )

      val newChan = chan.withInterception
      newChan.isGroupPickupInitiator should be(true)
    }

    "add remote party information from linked channel for originate" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      ).updateVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      val linkedC = Channel(
        "1434368938.107",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        ChannelState.RINGING
      )

      c.addOriginatePartyState(linkedC).remoteState should be(
        Some(ChannelState.RINGING)
      )
    }

    "not add remote party information from linked channel for other call types" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
      val linkedC = Channel(
        "1434368938.107",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        ChannelState.RINGING
      )

      c.addOriginatePartyState(linkedC).remoteState should be(None)
    }

    "report remote party state (not up) from linked channel for originate" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      ).updateVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      val linkedC = Channel(
        "1434368938.107",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        ChannelState.RINGING
      )

      c.addOriginatePartyState(linkedC).isOriginatePartyUp should be(false)
    }

    "report remote party state (up) from linked channel for originate" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      ).updateVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )
      val linkedC = Channel(
        "1434368938.107",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106",
        ChannelState.UP
      )

      c.addOriginatePartyState(linkedC).isOriginatePartyUp should be(true)
    }

    "report as originate if XUC_CALLTYPE is set to Originate" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      ).updateVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOriginate
      )

      c.isOriginate shouldBe true
    }

    "report as outboundOriginate if XUC_CALLTYPE is set to OutboundOriginate" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      ).updateVariable(
        Channel.VarNames.xucCallType,
        Channel.callTypeValOutboundOriginate
      )

      c.isOutboundOriginate shouldBe true
    }

    "report as outboundOriginate if __XUC_CALLTYPE (inherited) is set to OutboundOriginate" in {
      val c = Channel(
        "1434368938.106",
        "Local/id-22@agentcallback-00000014;1f",
        CallerId("Name", "1200"),
        "1434368938.106"
      ).updateVariable(
        "__" + Channel.VarNames.xucCallType,
        Channel.callTypeValOutboundOriginate
      )

      c.isOutboundOriginate shouldBe true
    }

    "store direction of incoming call" in {
      val ringing = new NewStateEvent(this)
      ringing.setChannelState(ChannelState.RINGING.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(ringing)

      channel.direction should be(Some(ChannelDirection.INCOMING))
    }

    "store direction of incoming call when established" in {
      val ringing = new NewStateEvent(this)
      ringing.setChannelState(ChannelState.RINGING.id)
      val up = new NewStateEvent(this)
      up.setChannelState(ChannelState.UP.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(ringing)
        .withState(up)

      channel.direction should be(Some(ChannelDirection.INCOMING))
    }

    "store direction of outgoing call" in {
      val dialing = new NewStateEvent(this)
      dialing.setChannelState(ChannelState.DIALING.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(dialing)

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }

    "store direction of outgoing call when established" in {
      val dialing = new NewStateEvent(this)
      dialing.setChannelState(ChannelState.DIALING.id)
      val up = new NewStateEvent(this)
      up.setChannelState(ChannelState.UP.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(dialing)
        .withState(up)

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }

    "store direction of outgoing call (originating)" in {
      val originating = new NewStateEvent(this)
      originating.setChannelState(ChannelState.ORIGINATING.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(originating)

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }

    "store direction of outgoing call (originating) when established" in {
      val originating = new NewStateEvent(this)
      originating.setChannelState(ChannelState.ORIGINATING.id)
      val up = new NewStateEvent(this)
      up.setChannelState(ChannelState.UP.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(originating)
        .withState(up)

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }

    "store direction of outgoing call (originating) when variable originating is set and then directly UP (for polycom devices)" in {
      val up = new NewStateEvent(this)
      up.setChannelState(ChannelState.UP.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .updateVariable(
          Channel.VarNames.xucCallType,
          Channel.callTypeValOriginate
        )
        .withState(up)

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }

    "store direction of outgoing call (originating) when directly UP and then originating variable is set (for polycom devices)" in {
      val up = new NewStateEvent(this)
      up.setChannelState(ChannelState.UP.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .withState(up)
        .updateVariable(
          Channel.VarNames.xucCallType,
          Channel.callTypeValOriginate
        )

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }

    "store direction of outgoing call (outbound-originating) when variable originating is set and then UP" in {
      val up = new NewStateEvent(this)
      up.setChannelState(ChannelState.UP.id)
      val channel = Channel(
        "1434368938.107",
        "SIP/abcd-00001",
        CallerId("Name", "1200"),
        "1434368938.106"
      )
        .updateVariable(
          "__" + Channel.VarNames.xucCallType,
          Channel.callTypeValOutboundOriginate
        )
        .withState(up)

      channel.direction should be(Some(ChannelDirection.OUTGOING))
    }
  }

  "CallerId" should {
    "return Some for name & number if set" in {
      val cid = CallerId("James Bond", "1007")

      cid.nameOption should be(Some("James Bond"))
      cid.numberOption should be(Some("1007"))
    }

    "return None for name & number if null or empty" in {
      val cid1 = CallerId(null, null)
      cid1.nameOption should be(None)
      cid1.numberOption should be(None)

      val cid2 = CallerId("", "")
      cid2.nameOption should be(None)
      cid2.numberOption should be(None)
    }
  }
}
