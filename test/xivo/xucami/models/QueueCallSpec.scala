package xivo.xucami.models

import org.asteriskjava.manager.event.{
  QueueCallerJoinEvent,
  QueueCallerLeaveEvent
}
import org.joda.time.DateTime
import org.mockito.Mockito.stub
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class QueueCallSpec extends AnyWordSpec with Matchers with MockitoSugar {

  "A QueueCall" should {
    "be created from a QueueCallerJoinEvent" in {
      val event      = mock[QueueCallerJoinEvent]
      val position   = 3
      val dateJoined = new DateTime()
      val name       = "foo"
      val number     = "3315987"
      val channel    = "SIP/abcd-000001"
      stub(event.getCallerIdName).toReturn(name)
      stub(event.getCallerIdNum).toReturn(number)
      stub(event.getPosition).toReturn(position)
      stub(event.getDateReceived).toReturn(dateJoined.toDate)
      stub(event.getChannel).toReturn(channel)

      QueueCall.from(event, "main") shouldEqual QueueCall(
        position,
        Some(name),
        number,
        dateJoined,
        channel,
        "main"
      )
    }
  }

  "An EnterQueue" should {
    "be created from a QueueCallerJoinEvent" in {
      val event      = mock[QueueCallerJoinEvent]
      val position   = 3
      val dateJoined = new DateTime()
      val name       = "foo"
      val number     = "3315987"
      val queue      = "bar"
      val uniqueid   = "123456.789"
      val channel    = "SIP/abcd-000001"
      stub(event.getCallerIdName).toReturn(name)
      stub(event.getCallerIdNum).toReturn(number)
      stub(event.getPosition).toReturn(position)
      stub(event.getDateReceived).toReturn(dateJoined.toDate)
      stub(event.getQueue).toReturn(queue)
      stub(event.getUniqueId).toReturn(uniqueid)
      stub(event.getChannel).toReturn(channel)

      EnterQueue(event, "main") shouldEqual EnterQueue(
        queue,
        uniqueid,
        QueueCall(position, Some(name), number, dateJoined, channel, "main"),
        channel
      )
    }
  }

  "A LeaveQueue" should {
    "be created from a QueueCallerLeaveEvent" in {
      val event    = mock[QueueCallerLeaveEvent]
      val queue    = "bar"
      val uniqueId = "12456.789"
      val dateLeft = new DateTime()
      val channel  = "SIP/abcd-000001"
      stub(event.getDateReceived).toReturn(dateLeft.toDate)
      stub(event.getQueue).toReturn(queue)
      stub(event.getUniqueId).toReturn(uniqueId)
      stub(event.getChannel).toReturn(channel)

      LeaveQueue(event) shouldEqual LeaveQueue(
        queue,
        uniqueId,
        dateLeft,
        channel
      )
    }

  }

}
