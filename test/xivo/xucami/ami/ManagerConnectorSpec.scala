package xivo.xucami.ami

import akka.actor.{Cancellable, PoisonPill}
import akka.testkit.TestActorRef
import akkatest.TestKitSpec
import org.asteriskjava.manager.action._
import org.asteriskjava.manager.event.CoreShowChannelsCompleteEvent
import org.mockito.Matchers.isA
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.CoreStatusResponse
import org.asteriskjava.manager.{ManagerConnection, ManagerConnectionFactory}
import org.mockito.Mockito.{timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.{AmiEventHelper, XucAmiBus}
import services.XucAmiBus._
import xivo.xucami.models.{EnterQueue, LeaveQueue}
import xivo.xucami.userevents.{QueueMemberWrapupStartEvent, UserEventAgent}

import scala.collection.immutable.HashMap

class ManagerConnectorSpec
    extends TestKitSpec("ManagerConnectorSpec")
    with MockitoSugar
    with AmiEventHelper {

  class Helper() {
    val amiBus     = mock[XucAmiBus]
    val factory    = mock[ManagerConnectionFactory]
    val connection = mock[ManagerConnection]

    def actor() = {
      when(factory.createManagerConnection()).thenReturn(connection)
      val sa = TestActorRef(new ManagerConnector(amiBus, factory, "default"))
      (sa, sa.underlyingActor)
    }
  }

  "ManagerConnector" should {

    "initiate ManagerConnection" in new Helper() {
      val (ref, a) = actor()
      verify(factory).createManagerConnection()
      a.managerConnection shouldBe connection
      verify(a.managerConnection, timeout(250)).login()
    }

    "stop LoginTimeout if ConnectEvent received" in new Helper() {
      val (ref, a) = actor()
      a.loginTimeout = mock[Cancellable]
      ref ! new ConnectEvent("test")
      verify(a.loginTimeout).cancel()
    }

    "logoff and defer manager connection if the actor is terminated" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("testLogout")
      ref ! PoisonPill

      a.managerConnection shouldBe null
    }

    "publish Extension status event to the bus" in new Helper() {
      val (ref, a) = actor()

      ref ! new ConnectEvent("test")
      val event = new ExtensionStatusEvent("test")

      ref ! event

      verify(amiBus).publish(XucAmiBus.AmiExtensionStatusEvent(event))
    }

    "publish AgentConnect event to the bus with channel id" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val event = new AgentConnectEvent("agentConnectTest")
      ref ! event

      verify(amiBus).publish(AmiEvent(event))
    }

    "publish unprocessed events to the bus without channel" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val event = new ChannelReloadEvent("unprocessed")
      ref ! event

      verify(amiBus).publish(AmiEvent(event))
    }

    "subscribe to the bus for AmiActions" in new Helper() {
      val (ref, a) = actor()
      verify(amiBus).subscribe(ref, AmiType.AmiAction)
    }

    "send Manager Actions with refence to the ami and save them in pendingRequests with actionId" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val action = mock[PauseMonitorAction]
      ref ! AmiAction(action, Some("uniqueId3"))

      verify(action).setActionId("1")
      verify(connection).sendAction(action, a.managerListener)
      a.pendingRequests.get(1) shouldBe Some(
        AmiAction(action, Some("uniqueId3"))
      )
    }

    "send Manager Actions only if targetMds match current mds if defined" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      val action  = mock[PauseMonitorAction]
      val action2 = mock[UnpauseMonitorAction]

      ref ! AmiAction(action, Some("uniqueId3"), targetMds = Some("hole"))
      ref ! AmiAction(action2, Some("uniqueId4"), targetMds = Some("default"))

      verify(action2).setActionId("1")
      verify(connection).sendAction(action2, a.managerListener)
      a.pendingRequests.size shouldBe 1
      a.pendingRequests.get(1) shouldBe Some(
        AmiAction(action2, Some("uniqueId4"), targetMds = Some("default"))
      )
    }

    "publish information when logged" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")
      verify(amiBus).publish(AmiConnected("default"))
    }

    "buffer Manager Actions when waiting for login and send them once logged" in new Helper() {
      val (ref, a) = actor()
      val action1  = new PauseMonitorAction()
      val action2  = new UnpauseMonitorAction()
      ref ! AmiAction(action1)
      ref ! AmiAction(action2)
      ref ! new ConnectEvent("test")

      verify(connection).sendAction(action1, a.managerListener)
      verify(connection).sendAction(action2, a.managerListener)
    }

    "publish Ami responses to the bus with corresponding action" in new Helper() {
      val (ref, a) = actor()
      val action   = new CoreStatusAction()
      a.pendingRequests = HashMap(2L -> AmiAction(action))
      val response = new CoreStatusResponse()
      response.setActionId("2")
      a.logged(response)

      a.pendingRequests.get(2) shouldBe None
      verify(amiBus).publish(AmiResponse((response, Some(AmiAction(action)))))
    }

    "Ami Manager responses with non integer value shouldn't throw an exception " in new Helper() {
      val (ref, a) = actor()
      val response = new CoreStatusResponse()
      response.setActionId("originate-fa38ee68d953e9b1e43")
      a.logged(response)
    }

    "Ami response events with non integer value shouldn't throw an exception " in new Helper() {
      val (ref, a) = actor()
      val response = new OriginateResponseEvent(this)
      response.setActionId("originate-fa38ee68d953e9b1e43")
      a.logged(response)
    }

    "publish Ami responses directly to requesting actor and not to the bus" in new Helper() {
      val (ref, a)  = actor()
      val action    = new CoreStatusAction()
      val amiAction = AmiAction(action, None, Some(testActor))
      a.pendingRequests = HashMap(2L -> amiAction)
      val response = new CoreStatusResponse()
      response.setActionId("2")
      val amiResponse = AmiResponse((response, Some(amiAction)))
      a.logged(response)

      a.pendingRequests.get(2) shouldBe None
      expectMsg(amiResponse)
      verify(amiBus, timeout(500).times(0)).publish(amiResponse)
    }

    "publish All Ami responses events when it's a multi response action request" in new Helper() {
      val (ref, a)  = actor()
      val action    = new CoreShowChannelsAction()
      val amiAction = AmiAction(action, None, Some(testActor))
      a.pendingRequests = HashMap(2L -> amiAction)
      val response1 = mock[CoreShowChannelEvent]
      response1.setActionId("2")
      val amiResponse1 = AmiEvent(response1)

      val response2 = mock[CoreShowChannelsCompleteEvent]
      response2.setActionId("2")
      val amiResponse2 = AmiEvent(response2)

      a.logged(response1)
      expectMsg(amiResponse1)
      a.pendingRequests.get(2) shouldNot be(empty)

      a.logged(response2)
      expectMsg(amiResponse2)

      a.pendingRequests.get(2) shouldBe None
    }

    "publish Ami user event agent to bus as AmiAgentEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val userEventAgent = mock[UserEventAgent]

      ref ! userEventAgent

      verify(amiBus).publish(AmiAgentEvent(userEventAgent))
    }

    "publish Ami QueueMemberPauseEvent to bus as AmiAgentEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt = mock[QueueMemberPauseEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish Ami QueueMemberWrapupStartEvent to bus as AmiAgentEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt = mock[QueueMemberWrapupStartEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish AMI QueueMemberEvent to bus as AmiAgentEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt = mock[QueueMemberEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish AMI QueueEntryEvent to bus as AmiAgentEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val evt = mock[QueueEntryEvent]

      ref ! evt

      verify(amiBus).publish(AmiAgentEvent(evt))
    }

    "publish an EnterQueue event on the bus when receiving a QueueCallerJoinEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val joinEvent = mock[QueueCallerJoinEvent]

      ref ! joinEvent

      verify(amiBus).publish(isA(classOf[EnterQueue]))
    }

    "publish a LeaveQueue event on the bus when receiving a QueueCallerLeaveEvent" in new Helper() {
      val (ref, a) = actor()
      ref ! new ConnectEvent("test")

      val leaveEvent = mock[QueueCallerLeaveEvent]

      ref ! leaveEvent

      verify(amiBus).publish(isA(classOf[LeaveQueue]))
    }

    "increase the key in pending requests" in new Helper() {
      val (ref, a) = actor()
      val action   = new ExtensionStateAction("1000", "")

      1 to 10 foreach { _ => a.logged(AmiAction(action)) }

      a.pendingRequests.keys.size shouldEqual 10
      a.pendingRequests.keys.toSet shouldEqual Set(1, 2, 3, 4, 5, 6, 7, 8, 9,
        10)

    }

  }
}
