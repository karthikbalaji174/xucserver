package models.ws.auth

import play.api.test.Helpers._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import models.ws.auth.{
  AuthenticationError,
  AuthenticationException,
  AuthenticationFailure,
  AuthenticationSuccess
}
class AuthenticationRequestSpec extends AnyWordSpec with Matchers {
  "AuthenticationSuccess" should {
    "convert to Results.Ok" in {
      AuthenticationSuccess(
        "jbond",
        "abcd-efgh",
        3600
      ).toResult.header.status should be(OK)
    }
  }

  "AuthenticationFailure" should {
    import AuthenticationError._
    "convert to BadRequest result if parameters are invalid" in {
      val errors = Seq(
        BearerNotFound,
        AuthorizationHeaderNotFound,
        InvalidJson,
        EmptyLogin
      )
      errors.foreach { e =>
        AuthenticationFailure(e, e.toString).toResult.header.status should be(
          BAD_REQUEST
        )
      }
    }

    "convert to InternalServerError result for unhandled errors" in {
      AuthenticationFailure(
        UnhandledError,
        "UnhandledError"
      ).toResult.header.status should be(INTERNAL_SERVER_ERROR)
    }

    "convert to Unauthorized result for InvalidToken error" in {
      AuthenticationFailure(
        InvalidToken,
        "Invalid issuer"
      ).toResult.header.status should be(UNAUTHORIZED)
    }

    "convert to Unauthorized result for sso errors" in {
      AuthenticationFailure(
        SsoAuthenticationFailed,
        "SsoAuthenticationFailed"
      ).toResult.header.status should be(UNAUTHORIZED)
    }

    "convert to Forbidden result for all other errors" in {
      val errors = Seq(UserNotFound, TokenExpired)
      errors.foreach { e =>
        AuthenticationFailure(e, e.toString).toResult.header.status should be(
          FORBIDDEN
        )
      }
    }

    "be created from an AuthenticationException" in {
      AuthenticationFailure.from(
        new AuthenticationException(InvalidCredentials, "Invalid credentials")
      ) should be(
        AuthenticationFailure(InvalidCredentials, "Invalid credentials")
      )
    }

    "be created from any exception" in {
      AuthenticationFailure.from(new Exception("test")) should be(
        AuthenticationFailure(UnhandledError, "test")
      )
    }
  }
}
