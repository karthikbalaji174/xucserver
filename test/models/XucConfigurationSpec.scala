package models

import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class XucConfigurationSpec extends AnyWordSpec with Matchers {
  "ConfigurationEntry" should {
    "convert to json" in {
      val c = ConfigurationEntry("titi", "toto")
      Json.toJson(c) should be(
        Json.parse("""{"name":"titi", "value":"toto"}""")
      )
    }
  }

  "ConfigurationIntSetEntry" should {
    "convert to json" in {
      val c = ConfigurationIntSetEntry("tutu", Set(1, 2, 3))
      Json.toJson(c) should be(
        Json.parse("""{"name":"tutu", "value":[1,2,3]}""")
      )
    }
  }
}
