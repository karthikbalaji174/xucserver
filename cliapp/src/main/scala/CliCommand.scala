package xuccli

sealed trait SubCommand

sealed trait InfoCommand                                extends SubCommand
case object GetVersion                                  extends InfoCommand
case object GetStartTime                                extends InfoCommand
case object GeneralInfo                                 extends InfoCommand
case class MdsState(name: String)                       extends InfoCommand
case class SipTrackerInfo(name: String, driver: String) extends InfoCommand

sealed trait OperationCommand extends SubCommand
sealed trait SipTrackerOperation
case object Stop       extends SipTrackerOperation
case object ClearCalls extends SipTrackerOperation
case class SipTrackerCommand(
    name: String,
    operation: SipTrackerOperation,
    driver: String = "PJSIP"
) extends OperationCommand

case class CliCommand(pid: Int, command: SubCommand)
