package xuccli

import cats.implicits._

import com.monovore.decline._

object CliParser {

  val pid = Opts.argument[Int]("pid")

  val versionCmd: Opts[SubCommand] =
    Opts.subcommand("getversion", "get application version")(Opts(GetVersion))

  val startTimeCmd: Opts[SubCommand] =
    Opts.subcommand("starttime", "get application start time")(
      Opts(GetStartTime)
    )

  val infoCmd: Opts[SubCommand] =
    Opts.subcommand("info", "get general information")(Opts(GeneralInfo))

  val mdsStateCmd: Opts[SubCommand] =
    Opts.subcommand("mds", "get mds state information") {
      Opts.argument[String]("mdsname").map(MdsState(_))
    }

  // Works like the function sipTrackerClearCalls but with different syntax
  def sipTrackerInfo: Opts[(String, String) => SubCommand] =
    Opts.subcommand("info", "get SipTracker state information")(
      Opts(SipTrackerInfo)
    )

  def sipTrackerClearCalls: Opts[(String, String) => SubCommand] =
    Opts.subcommand("clearcalls", "clear list of current calls in SipTracker")(
      Opts(SipTrackerCommand(_, ClearCalls, _))
    )

  def sipTrackerStop: Opts[(String, String) => SubCommand] =
    Opts.subcommand(
      "stop",
      "stop given SipTracker. It will be restarted when user reconnects."
    )(Opts(SipTrackerCommand(_, Stop, _)))

  val sipTrackerCmd: Opts[SubCommand] =
    Opts.subcommand("siptracker", "SipTracker information & operations") {
      (
        Opts.argument[String]("SIP Peer"),
        Opts
          .option[String](
            long = "driver",
            short = "d",
            metavar = "text",
            help = "Select the driver to use."
          )
          .withDefault("PJSIP"),
        sipTrackerInfo orElse sipTrackerClearCalls orElse sipTrackerStop
      ).mapN { (peer, option, cmd) =>
        cmd(peer, option)
      }
    }

  val subcommand =
    versionCmd orElse startTimeCmd orElse infoCmd orElse mdsStateCmd orElse sipTrackerCmd

  val parse: Opts[CliCommand] =
    (pid, subcommand).mapN { (p, cmd) =>
      CliCommand(p, cmd)
    }
}
