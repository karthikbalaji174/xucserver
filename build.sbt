import com.typesafe.sbt.packager.docker._
import com.typesafe.sbt.web.{CompileProblemsException, GeneralProblem}
import scala.sys.process._

val appName = "xuc"
// Do not change version directly but change TARGET_VERSION file and run `xivocc-build.sh apply-version`
val appVersion      = sys.env.getOrElse("TARGET_VERSION", "dev-version")
val appOrganisation = "xivo"

lazy val IntegrationTest = config("integrationTest") extend (Test)

inThisBuild(
  List(
    scalaVersion := Dependencies.scalaVersion,
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision
  )
)

lazy val root = (project in file("."))
  .enablePlugins(play.sbt.PlayScala, BuildInfoPlugin, DockerPlugin)
  .disablePlugins(JUnitXmlReportPlugin)
  .settings(
    name := appName,
    version := appVersion,
    scalaVersion := Dependencies.scalaVersion,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    Compile / packageDoc / publishArtifact := false
  )
  .settings(
    Test / testOptions += Tests.Argument(
      TestFrameworks.ScalaTest,
      "-o",
      "-u",
      "target/test-reports",
      "-l",
      "xuc.tags.WSApiSpecTest xuc.tags.IntegrationTest"
    ),
    Test / javaOptions ++= Seq(
      "-Dlogger.application=WARN",
      "-Dlogger.play=WARN",
      "-Dconfig.file=test/resources/application.conf"
    ),
    Test / testOptions += Tests.Setup(() => {
      System.setProperty("XUC_VERSION", appVersion)
    }),
    IntegrationTest / testOptions := Seq(
      Tests.Argument(
        TestFrameworks.ScalaTest,
        "-o",
        "-u",
        "target/integrationtest-reports",
        "-n",
        "xuc.tags.IntegrationTest"
      )
    ),
    WsApiTest / testOptions := Seq(
      Tests.Argument(
        TestFrameworks.ScalaTest,
        "-o",
        "-u",
        "target/test-reports",
        "-n",
        "xuc.tags.WSApiSpecTest"
      )
    ),
    scalacOptions ++= Seq(
      "-encoding",
      "UTF-8",
      "-unchecked",
      "-deprecation",
      "-feature",
      "-language:existentials",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-Xlint:adapted-args",
      "-Ywarn-dead-code",
      "-Wunused:imports",
      s"-Wconf:src=${target.value}/.*:s"
    )
  )
  .settings(
    npmTest := {
      val log = new CustomLogger
      if ("npm install".!(log) != 0)
        throw new CompileProblemsException(
          Array(new GeneralProblem(log.buf.toString, file("./package.json")))
        )
      if ("npm run test-headless".!(log) != 0)
        throw new RuntimeException("NPM Tests failed")
    },
    (Test / test) := ((Test / test) dependsOn (npmTest)).value
  )
  .settings(
    coverageExcludedPackages := "<empty>;views\\..*;Reverse.*;xuc\\.Routes"
  )
  .settings(dockerSettings: _*)
  .settings(editSourceSettings: _*)
  .settings(docSettings: _*)
  .settings(
    setVersionVarTask := { System.setProperty("XUC_VERSION", appVersion) },
    EditSource / edit := ((EditSource / edit) dependsOn (EditSource / EditSourcePlugin.autoImport.clean)).value,
    Compile / packageBin := ((Compile / packageBin) dependsOn (EditSource / edit)).value,
    Compile / run := ((Compile / run) dependsOn setVersionVarTask).evaluated
  )
  .settings(buildInfosettings: _*)
  .configs(WsApiTest)
  .settings(inConfig(WsApiTest)(Defaults.testTasks): _*)
  .configs(IntegrationTest)
  .settings(inConfig(IntegrationTest)(Defaults.testTasks): _*)
  .aggregate(cliApp)
  .dependsOn(cliApp)

lazy val cliApp = (project in file("cliapp"))
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := appName + "-cli",
    version := appVersion,
    scalaVersion := Dependencies.scalaVersion,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= CliDependencies.libraries,
    Compile / packageDoc / publishArtifact := false,
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "xuccli.info",
    scalacOptions ++= Seq(
      "-encoding",
      "UTF-8",
      "-unchecked",
      "-deprecation",
      "-feature",
      "-language:existentials",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-Xlint:adapted-args",
      "-Ywarn-dead-code",
      "-Wunused:imports"
    )
  )

lazy val WsApiTest = config("wsapi") extend (Test)

lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")
lazy val npmTest           = taskKey[Unit]("Run NPM headless test")

lazy val buildInfosettings = Seq(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoPackage := "xucserver.info"
)

lazy val dockerSettings = Seq(
  Docker / maintainer := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "openjdk:8u275-jdk-slim-buster",
  dockerExposedPorts := Seq(9000),
  Docker / daemonUserUid := None,
  Docker / daemonUser := "daemon",
  dockerExposedVolumes := Seq("/conf"),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="${appVersion}""""),
  dockerEntrypoint := Seq("bin/xuc_docker"),
  dockerChmodType := DockerChmodType.UserGroupWriteExecute
)

lazy val editSourceSettings = Seq(
  EditSource / flatten := true,
  Universal / mappings += file(
    "target/version/appli.version"
  ) -> "conf/appli.version",
  EditSource / targetDirectory := baseDirectory.value / "target/version",
  EditSource / variables += ("SBT_EDIT_APP_VERSION", appVersion),
  EditSource / sources ++= (baseDirectory.value / "src/res" * "appli.version").get
)

lazy val docSettings = Seq(
  Compile / packageDoc / publishArtifact := false,
  Compile / doc / sources := Seq.empty
)
