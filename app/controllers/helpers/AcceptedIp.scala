package controllers.helpers

import play.api.mvc.Request
import xivo.xuc.IPFilterConfig

trait AcceptedIp {

  def isAcceptedIp[A](config: IPFilterConfig, request: Request[A]): Boolean = {
    config.ipAccepted.contains(request.remoteAddress.split(":")(0).trim)
  }
}
