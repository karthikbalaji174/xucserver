package controllers

import com.google.inject.Inject
import configuration.AuthConfig
import play.api.mvc.InjectedController
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}
import xivo.xucami.XucBaseAmiConfig
import xivo.xucstats.XucBaseStatsConfig

import scala.concurrent.ExecutionContext

class XucDefault @Inject() (
    authConfig: AuthConfig,
    repo: ConfigRepository,
    xucConfig: XucBaseConfig,
    xucStatsConfig: XucBaseStatsConfig,
    xucAmiConfig: XucBaseAmiConfig,
    configServerConfig: ConfigServerConfig,
    configServer: ConfigServerRequester
)(implicit ec: ExecutionContext)
    extends InjectedController {

  def index =
    Action.async {
      configServer.getMediaServerConfigAll
        .map(mdsServers =>
          Ok(
            views.html.default.index(
              "Xuc",
              authConfig,
              repo,
              xucConfig,
              xucStatsConfig,
              xucAmiConfig,
              mdsServers,
              configServerConfig
            )
          )
        )
    }
}
