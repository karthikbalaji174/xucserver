package controllers.api

import com.google.inject.Inject
import controllers.helpers.RequestResultHelper
import play.api.libs.json.Reads._
import play.api.libs.json._
import play.api.mvc._
import services.request.AgentLoginRequest

import scala.concurrent.ExecutionContext

class RestAgent @Inject() (
    agentRequester: AgentRequester,
    parsers: PlayBodyParsers,
    iPFilter: IPFilter
)(implicit ec: ExecutionContext)
    extends InjectedController
    with RequestResultHelper {

  val phoneNumberReads: Reads[String] =
    (JsPath \ "phoneNumber").read[String](minLength[String](1))

  private def validatePhoneNumber =
    parsers.json.validate(
      _.validate[String](phoneNumberReads).asEither.left.map(e =>
        BadRequest(JsError.toJson(e))
      )
    )

  private def validateJson[A: Reads] =
    parsers.json.validate(
      _.validate[A].asEither.left.map(e => BadRequest(JsError.toJson(e)))
    )

  def agentLogout(): Action[String] =
    iPFilter.async(validatePhoneNumber) { request =>
      val phoneNumber = request.body
      log.info(s"[RestAPI] Req : <agentLogout - $phoneNumber>")
      processResult(agentRequester.logout(phoneNumber))
    }

  def agentLogin(): Action[AgentLoginRequest] =
    iPFilter.async(validateJson[AgentLoginRequest]) { request =>
      val agentLoginReq = request.body
      log.info(s"[RestAPI] Req : <agentLogin - $agentLoginReq>")
      processResult(agentRequester.login(agentLoginReq))
    }

  def togglePause(): Action[String] =
    iPFilter.async(validatePhoneNumber) { request =>
      val phoneNumber = request.body
      log.info(s"[RestAPI] Req : <togglePause - $phoneNumber>")
      processResult(agentRequester.togglePause(phoneNumber))
    }

}
