package controllers.api

import com.google.inject.Inject
import controllers.helpers.{AcceptedIp, RequestBlocked, RequestResultHelper}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import xivo.xuc.IPFilterConfig

class FilterIPAction[A] @Inject() (
    action: Action[A],
    config: IPFilterConfig
) extends Action[A]
    with RequestResultHelper
    with AcceptedIp {
  def apply(request: Request[A]): Future[Result] = {
    if (isAcceptedIp(config, request))
      action(request)
    else
      processResult(
        Future.successful(
          RequestBlocked(
            s"Apis are disabled for this IP ${request.remoteAddress}"
          )
        )
      )
  }

  override def parser           = action.parser
  override def executionContext = action.executionContext
}

class IPFilter @Inject() (
    parser: BodyParsers.Default,
    config: IPFilterConfig
)(implicit ec: ExecutionContext)
    extends ActionBuilderImpl(parser)
    with RequestResultHelper
    with AcceptedIp {
  override def invokeBlock[A](
      request: Request[A],
      block: Request[A] => Future[Result]
  ): Future[Result] = {
    if (isAcceptedIp(config, request))
      block(request)
    else
      processResult(
        Future.successful(
          RequestBlocked(
            s"Apis are disabled for this IP ${request.remoteAddress}"
          )
        )
      )
  }

  override def composeAction[A](action: Action[A]) =
    new FilterIPAction(action, config)
}
