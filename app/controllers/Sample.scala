package controllers

import com.google.inject.Inject
import play.api.i18n.Lang
import play.api.mvc.{AbstractController, ControllerComponents}

class Sample @Inject() (cc: ControllerComponents)
    extends AbstractController(cc) {

  val lang = Lang("fr")

  def index =
    Action {
      Ok(views.html.sample.sample("XiVOxc API Sample Page"))
    }
}
