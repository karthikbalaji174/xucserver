package controllers.config

import com.google.inject.Inject
import controllers.helpers.AuthenticatedAction
import controllers.helpers.AuthenticatedAction.getApiUser
import play.api.Logger
import play.api.http.HttpEntity
import play.api.mvc._
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.xuc.XucBaseConfig

import scala.concurrent.ExecutionContext

class ForwardConfigController @Inject() (implicit
    configRequester: ConfigServerRequester,
    repo: ConfigRepository,
    config: XucBaseConfig,
    ec: ExecutionContext,
    parsers: PlayBodyParsers
) extends InjectedController {
  val log                    = Logger(getClass.getName)
  implicit val contentParser = parsers.anyContent

  def forward(uri: String): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      log.debug(
        s"forwarding request $uri to configmgt for ${user.username} with body : ${request.body}"
      )

      configRequester
        .forward(uri, request.method, request.body.asJson)
        .map { response =>
          val headers = response.headers.map { h =>
            (h._1, h._2.head)
          }
          Result(
            ResponseHeader(response.status, headers),
            HttpEntity.Strict(response.bodyAsBytes, None)
          )
        }
    }

}
