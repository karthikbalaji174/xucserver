package controllers.xuc

import com.google.inject.Inject
import play.api.libs.json.Json
import play.api.mvc.InjectedController
import xivo.xuc.XucBaseConfig
import xivo.xucstats.XucBaseStatsConfig

import scala.concurrent.ExecutionContext

class Config @Inject() (
    xucConfig: XucBaseConfig,
    xucStatsConfig: XucBaseStatsConfig
)(implicit ec: ExecutionContext)
    extends InjectedController {

  def getStatsConfig(path: String) =
    Action {
      xucStatsConfig.getValue(path) match {
        case Some(c) => Ok(Json.toJson(c))
        case None    => NotFound
      }
    }
}
