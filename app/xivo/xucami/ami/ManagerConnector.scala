package xivo.xucami.ami

import akka.actor._
import org.asteriskjava.manager.action.EventGeneratingAction
import org.asteriskjava.manager.event._
import org.asteriskjava.manager.response.ManagerResponse
import org.asteriskjava.manager.{
  ManagerConnection,
  ManagerConnectionFactory,
  ManagerEventListener,
  SendActionCallback
}
import play.api.Logger
import services.XucAmiBus
import services.XucAmiBus._
import services.channel.AmiLogger
import xivo.xucami.XucBaseAmiConfig
import xivo.xucami.models.{AttendedTransferFinished, EnterQueue, LeaveQueue}
import xivo.xucami.userevents.{
  QueueMemberWrapupStartEvent,
  UserEventAgent,
  UserEventAgentLogin,
  UserEventAgentLogoff,
  HangupEvent => HangupUserEvent
}

import scala.collection.immutable.HashMap
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Try
import helpers.JmxActorMonitor

class AsteriskManagerListener(actor: ActorRef)
    extends ManagerEventListener
    with SendActionCallback {
  val log = Logger(getClass.getName)
  def onManagerEvent(event: ManagerEvent) = {
    log.debug(s"Received manager event: $event")
    actor ! event
  }
  def onResponse(response: ManagerResponse) = {
    log.debug(s"Received manager response: $response")
    actor ! response
  }
}

object ManagerConnector {
  case object Login
  case object LoginTimeout
  type PendingRequests = HashMap[Long, AmiAction]

  class LoginTimeoutException extends Exception

  def managerFactory(xucAmiConfig: XucBaseAmiConfig) =
    new ManagerConnectionFactory(
      xucAmiConfig.ipAddress,
      xucAmiConfig.port,
      xucAmiConfig.username,
      xucAmiConfig.secret
    )
  def props(amiBus: XucAmiBus, config: XucBaseAmiConfig, mdsName: String) =
    Props(new ManagerConnector(amiBus, managerFactory(config), mdsName))
}

class ManagerConnector(
    amiBus: XucAmiBus,
    managerFactory: ManagerConnectionFactory,
    mdsName: String
) extends Actor
    with ActorLogging
    with JmxActorMonitor {
  import ManagerConnector._
  private[ami] var managerConnection: ManagerConnection     = null
  private[ami] var loginTimeout: Cancellable                = null
  private[ami] var managerListener: AsteriskManagerListener = null
  private[ami] var pendingRequests: PendingRequests         = HashMap()
  val loggerAmiRequests                                     = Logger("amirequests")
  jmxBean.addString("MDS", mdsName)
  val jmxAMIHostName = jmxBean.addString("AMIHostname", "")
  val jmxAMIPort     = jmxBean.addLong("AMIPort", 0L)
  val jmxCnxState    = jmxBean.addString("State", "Initializing")
  val logger         = Logger(getClass.getName)

  var nextActionId: Long = 0

  def getNextActorId: Long = {
    nextActionId = nextActionId + 1
    nextActionId
  }

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"));
    amiBus.subscribe(self, AmiType.AmiAction)
    managerListener = new AsteriskManagerListener(self)
    managerConnection = managerFactory.createManagerConnection()
    managerConnection.addEventListener(managerListener)
    managerConnection.registerUserEventClass(classOf[UserEventAgentLogin])
    managerConnection.registerUserEventClass(classOf[UserEventAgentLogoff])
    managerConnection.registerUserEventClass(classOf[HangupUserEvent])
    managerConnection.registerUserEventClass(
      classOf[QueueMemberWrapupStartEvent]
    )
    logger.info(
      s"Starting AMI connection for ${managerConnection.getHostname}:${managerConnection.getPort}"
    )
    jmxAMIHostName.set(managerConnection.getHostname)
    jmxAMIPort.set(managerConnection.getPort)
    self ! Login
  }

  def receive = login(List())

  def login(actionsToProcess: List[AmiAction]): Receive = {
    case Login =>
      import scala.concurrent.ExecutionContext.Implicits.global
      jmxCnxState.set("Loging In")
      loginTimeout =
        context.system.scheduler.scheduleOnce(5.seconds, self, LoginTimeout)
      Future(managerConnection.login()).failed.foreach {
        logger.warn(
          s"AMI login failed for: ${managerConnection.getHostname}:${managerConnection.getPort}",
          _
        )
      }

    case LoginTimeout =>
      jmxCnxState.set("Login Timeout")
      log.error("Manager login timed out")
      throw new LoginTimeoutException

    case event: ManagerEvent =>
      event match {
        case connected: ConnectEvent =>
          log.info("AMI logged on")
          jmxCnxState.set("Connected")
          loginTimeout.cancel()
          amiBus.publish(AmiConnected(mdsName))
          actionsToProcess.foreach(action => processAction(action))
          context.become(logged)
        case any: Any =>
      }

    case action: AmiAction =>
      log.debug(s"Buffering action to process once logged: $action")
      context.become(login(actionsToProcess :+ action))

    case any =>
      log.warning(s"Receive/login: Unprocessed message received: $any")
  }

  def logged: Receive = {
    case event: ManagerEvent =>
      AmiLogger.logEvent(AmiEvent(event, mdsName))
      processEvent(event)

    case action: AmiAction =>
      processAction(action)

    case response: ManagerResponse =>
      processManagerResponse(response)

    case any =>
      log.warning(s"Receive/logged: Unprocessed message received: $any")
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
    try { managerConnection.logoff() }
    catch { case any: Throwable => log.error(s"Exception in postStop: $any") }
    managerListener = null
    managerConnection = null
  }

  private def processEvent(event: ManagerEvent): Unit = {
    event match {
      case responseEvent: ResponseEvent =>
        processResponseEvent(responseEvent)

      case agentConnect: AgentConnectEvent =>
        log.debug(
          s"Publishing AmiEvent: ${agentConnect.getBridgedChannel}, $agentConnect"
        )
        amiBus.publish(AmiEvent(agentConnect, mdsName))

      case agentEvent: UserEventAgent =>
        log.debug(s"Publishing UserEventAgent: $agentEvent")
        amiBus.publish(AmiAgentEvent(agentEvent))

      case e: QueueMemberPauseEvent =>
        log.debug(s"Publishing QueueMemberPauseEvent: $e")
        amiBus.publish(AmiAgentEvent(e))

      case e: QueueMemberWrapupStartEvent =>
        log.debug(s"Publishing QueueMemberWrapupStartEvent: $e")
        amiBus.publish(AmiAgentEvent(e))

      case joinEvent: QueueCallerJoinEvent =>
        log.debug(s"Transforming and publishing a JoinEvent: $joinEvent")
        amiBus.publish(EnterQueue(joinEvent, mdsName))

      case leaveEvent: QueueCallerLeaveEvent =>
        log.debug(s"Transforming and publishing a LeaveEvent: $leaveEvent")
        amiBus.publish(LeaveQueue(leaveEvent))

      case attendedTransferEvent: AttendedTransferEvent =>
        log.debug(
          s"Transforming and publishing an AttendedTransferEvent: $attendedTransferEvent"
        )
        amiBus.publish(AttendedTransferFinished(attendedTransferEvent))

      case exs: ExtensionStatusEvent =>
        amiBus.publish(AmiExtensionStatusEvent(exs))

      case _ => amiBus.publish(AmiEvent(event, mdsName))
    }
  }

  private def processManagerResponse(response: ManagerResponse): Unit = {
    Option(response.getActionId)
      .flatMap(id => Try(id.toInt).toOption)
      .foreach(actionId => {
        loggerAmiRequests.debug(
          s"Publishing AMI Response: $response with actionId: $actionId}"
        )
        val richResponse: XucManagerResponse =
          (response, pendingRequests.get(actionId))

        richResponse._2.flatMap(_.requester) match {
          case Some(ref) => ref ! AmiResponse(richResponse)
          case None      => amiBus.publish(AmiResponse(richResponse))
        }
        pendingRequests -= actionId
      })
  }

  private def processResponseEvent(response: ResponseEvent): Unit = {
    val actionId =
      Option(response.getActionId).flatMap(id => Try(id.toInt).toOption)
    loggerAmiRequests.debug(
      s"Publishing AMI Response: $response with actionId: $actionId}"
    )
    val amiEvent = AmiEvent(response, mdsName)

    val request = actionId.flatMap(pendingRequests.get(_))
    request.flatMap(_.requester) match {
      case Some(ref) => ref ! amiEvent
      case None =>
        publishResponseEvent(amiEvent)
    }

    val doDeleteRequest = request
      .map(_.message)
      .collect({ case x: EventGeneratingAction => x })
      .forall(_.getActionCompleteEventClass.isInstance(response))

    if (doDeleteRequest) {
      actionId.foreach(pendingRequests -= _)
    }
  }

  private def publishResponseEvent(event: XucAmiBus.AmiEvent): Unit = {
    event match {
      case AmiEvent(e: QueueMemberEvent, _) =>
        log.debug(s"Publishing QueueMemberStatusEvent: $e")
        amiBus.publish(AmiAgentEvent(e))
      case AmiEvent(e: QueueEntryEvent, _) =>
        log.debug(s"Publishing QueueEntryEvent: $e")
        amiBus.publish(AmiAgentEvent(e))
      case e: AmiEvent =>
        log.debug(s"Publishing answer event $e")
        amiBus.publish(e)
    }
  }

  private def processAction(action: AmiAction): Unit = {
    if (action.targetMds.isEmpty || action.targetMds.contains(mdsName)) {
      val actionId = getNextActorId
      pendingRequests += actionId -> action
      action.message.setActionId(actionId.toString)
      loggerAmiRequests.debug(
        s"Sending action to AMI: $action with actionId: $actionId"
      )
      managerConnection.sendAction(action.message, managerListener)
    }
  }

}
