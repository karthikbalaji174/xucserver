package xivo.xucami.userevents
import org.asteriskjava.manager.event.UserEvent

class HangupEvent(source: Object) extends UserEvent(source) {
  private var XIVO_USERUUID: String = ""

  def getXIVO_USERUUID()             = XIVO_USERUUID
  def setXIVO_USERUUID(uuid: String) = XIVO_USERUUID = uuid
}
