package xivo.xucami.models

import org.asteriskjava.manager.event._
import org.joda.time.DateTime
import services.XucAmiBus
import services.XucAmiBus.AmiMessage
import org.asteriskjava.manager.event.AttendedTransferEvent
import play.api.libs.json.{JodaReads, JodaWrites, Json, Reads, Writes}

sealed trait QueueHistoryEvent {
  def queue: String
  def queueTime: DateTime
  def channel: String
}

case class QueueCall(
    position: Int,
    name: Option[String],
    number: String,
    queueTime: DateTime,
    channel: String,
    mdsName: String
)

object QueueCall {
  val queueTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
  implicit val dateTimeWriter: Writes[DateTime] =
    JodaWrites.jodaDateWrites(queueTimeFormat)
  implicit val dateTimeReader: Reads[DateTime] =
    JodaReads.jodaDateReads(queueTimeFormat)
  implicit val QueueCallReads: Reads[QueueCall]   = Json.reads[QueueCall]
  implicit val QueueCallWrites: Writes[QueueCall] = Json.writes[QueueCall]

  def from(event: QueueCallerJoinEvent, mdsName: String): QueueCall =
    QueueCall(
      event.getPosition,
      Option(event.getCallerIdName),
      event.getCallerIdNum,
      new DateTime(event.getDateReceived),
      event.getChannel,
      mdsName
    )
}

case class EnterQueue(
    queue: String,
    uniqueId: String,
    queueCall: QueueCall,
    channel: String
) extends AmiMessage
    with QueueHistoryEvent {
  override val message: Any       = this
  override val classifier: String = XucAmiBus.AmiType.QueueEvent
  override def queueTime          = queueCall.queueTime
}

object EnterQueue {
  def apply(event: QueueCallerJoinEvent, mdsName: String): EnterQueue =
    EnterQueue(
      event.getQueue,
      event.getUniqueId,
      QueueCall.from(event, mdsName),
      event.getChannel
    )
}

case class LeaveQueue(
    queue: String,
    channelName: String,
    queueTime: DateTime,
    channel: String
) extends AmiMessage
    with QueueHistoryEvent {
  override val message: Any       = this
  override val classifier: String = XucAmiBus.AmiType.QueueEvent
}

object LeaveQueue {
  def apply(event: QueueCallerLeaveEvent): LeaveQueue =
    LeaveQueue(
      event.getQueue,
      event.getUniqueId,
      new DateTime(event.getDateReceived),
      event.getChannel
    )
}

case class AttendedTransferFinished(fromUniqueId: String, toUniqueId: String)
    extends AmiMessage {
  override val message: Any       = this
  override val classifier: String = XucAmiBus.AmiType.TransferEvent
}

object AttendedTransferFinished {
  def apply(event: AttendedTransferEvent): AttendedTransferFinished =
    AttendedTransferFinished(
      event.getSecondTransfererUniqueid,
      event.getTransfereeUniqueid
    )
}
