package xivo.xuc.api

import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.RequestResult
import org.xivo.cti.MessageFactory
import play.api.Logger
import services.ActorIds
import services.config.ConfigRepository
import services.request.DialFromQueue
import services.request.PhoneRequest.Dial

import scala.concurrent.{ExecutionContext, Future}

class CtiApi @Inject() (
    repo: ConfigRepository,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef
)(implicit system: ActorSystem, ec: ExecutionContext) {
  val log            = Logger(getClass.getName)
  val messageFactory = new MessageFactory

  def dnd(username: String, state: Boolean): Future[RequestResult] =
    Requester.execute(
      repo,
      ctiRouterFactory,
      username,
      messageFactory.createDND(state)
    )

  def dial(username: String, number: String): Future[RequestResult] =
    Requester.execute(repo, ctiRouterFactory, username, Dial(number))

  def dialFromQueue(
      username: String,
      dialFromQueue: DialFromQueue
  ): Future[RequestResult] =
    Requester.execute(repo, ctiRouterFactory, username, dialFromQueue)

  def uncForward(
      username: String,
      state: Boolean,
      destination: String
  ): Future[RequestResult] =
    Requester.execute(
      repo,
      ctiRouterFactory,
      username,
      messageFactory.createUnconditionnalForward(destination, state)
    )

  def naForward(
      username: String,
      state: Boolean,
      destination: String
  ): Future[RequestResult] =
    Requester.execute(
      repo,
      ctiRouterFactory,
      username,
      messageFactory.createNaForward(destination, state)
    )

  def busyForward(
      username: String,
      state: Boolean,
      destination: String
  ): Future[RequestResult] =
    Requester.execute(
      repo,
      ctiRouterFactory,
      username,
      messageFactory.createBusyForward(destination, state)
    )

}
