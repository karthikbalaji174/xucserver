package xivo.events

import xivo.models.Agent

case class EventAgentPause(agentId: Agent.Id, reason: Option[String] = None)
    extends EventAgent
