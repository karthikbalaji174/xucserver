package xivo.events

import xivo.models.Agent
import xivo.xucami.userevents.UserEventAgentLogoff

object EventAgentLogout {
  def apply(userEventAgentLogoff: UserEventAgentLogoff) =
    new EventAgentLogout(
      userEventAgentLogoff.getAgentId().toLong,
      userEventAgentLogoff.getAgentnumber
    )
}

case class EventAgentLogout(agentId: Agent.Id, agentNb: Agent.Number)
    extends EventAgent
