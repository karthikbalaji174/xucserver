package xivo.events

import play.api.libs.json.{JsValue, Json, Writes}

object CurrentCallsPhoneEvents {
  implicit val writes = new Writes[CurrentCallsPhoneEvents] {
    override def writes(ccpe: CurrentCallsPhoneEvents): JsValue =
      Json.obj("events" -> ccpe.phoneEvents)
  }
}

case class CurrentCallsPhoneEvents(DN: String, phoneEvents: List[PhoneEvent])
