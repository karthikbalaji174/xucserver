package xivo.events
package cti.models
import anorm.SqlParser.get
import anorm.{~, RowParser}
import play.api.libs.json.{Json, Reads, Writes}

/**  Object that replace the UserStatus object which is now Deprecated
  */
trait CtiStatusTraits {
  type CtiStatusMap = Map[String, List[CtiStatus]]
  type CtiStatusLegacyMap =
    Map[String, List[CtiStatusLegacy]] // Deprecated It will be removed

}

case class CtiStatusMapWrapper(
    map: Map[String, List[CtiStatus]]
)

// Deprecated It will be removed
case class CtiStatusLegacyMapWrapper(map: Map[String, List[CtiStatusLegacy]])

object CtiStatusMapWrapper {
  implicit val CtiStatusMapWrapperWrite: Writes[CtiStatusMapWrapper] =
    Json.writes[CtiStatusMapWrapper]
  implicit val CtiStatusMapWrapperRead: Reads[CtiStatusMapWrapper] =
    Json.reads[CtiStatusMapWrapper]
}

// Deprecated It will be removed
object CtiStatusLegacyMapWrapper {
  implicit val CtiStatusMapWrapperWrite: Writes[CtiStatusLegacyMapWrapper] =
    Json.writes[CtiStatusLegacyMapWrapper]
  implicit val CtiStatusMapWrapperRead: Reads[CtiStatusLegacyMapWrapper] =
    Json.reads[CtiStatusLegacyMapWrapper]
}

case class CtiStatus(
    name: Option[String],
    displayName: Option[String],
    status: Option[Int]
)

object CtiStatus {
  implicit val CtiStatusWrite: Writes[CtiStatus] =
    Json.writes[CtiStatus]
  implicit val CtiStatusRead: Reads[CtiStatus] =
    Json.reads[CtiStatus]

  val simple: RowParser[CtiStatus] = {
    get[Option[String]]("name") ~
      get[Option[String]]("displayName") ~
      get[Option[Int]]("status") map { case name ~ displayName ~ status =>
        CtiStatus(name, displayName, status)
      }
  }
}

// Deprecated It will be removed
case class CtiStatusActionLegacy(name: String, parameters: Option[String])

// Deprecated It will be removed
object CtiStatusActionLegacy {
  implicit val CtiStatusActionWrite: Writes[CtiStatusActionLegacy] =
    Json.writes[CtiStatusActionLegacy]
  implicit val CtiStatusActionRead: Reads[CtiStatusActionLegacy] =
    Json.reads[CtiStatusActionLegacy]

  implicit val CtiStatusActionLegacy: RowParser[CtiStatusActionLegacy] = {
    get[String]("name") ~
      get[Option[String]]("parameters") map { case name ~ parameters =>
        new CtiStatusActionLegacy(name, parameters)
      }
  }

}
// Deprecated It will be removed
case class CtiStatusLegacy(
    name: Option[String],
    longName: Option[String],
    color: Option[String],
    actions: List[CtiStatusActionLegacy]
)
// Deprecated It will be removed
object CtiStatusLegacy {
  implicit val CtiStatusWrite: Writes[CtiStatusLegacy] =
    Json.writes[CtiStatusLegacy]
  implicit val CtiStatusRead: Reads[CtiStatusLegacy] =
    Json.reads[CtiStatusLegacy]

  val simple: RowParser[CtiStatusLegacy] = {
    get[Option[String]]("name") ~
      get[Option[String]]("longName") ~
      get[Option[String]]("color") ~
      get[Option[String]]("actions") map {
        case name ~ longName ~ color ~ actions =>
          CtiStatusLegacy(
            name,
            longName,
            color,
            Json.parse(actions.getOrElse("[]")).as[List[CtiStatusActionLegacy]]
          )
      }
  }
}
