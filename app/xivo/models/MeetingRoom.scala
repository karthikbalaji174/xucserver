package xivo.models

import play.api.libs.json.{Json, Reads, Writes}

sealed trait MeetingRoom
case object PersonalMeetingRoom  extends MeetingRoom
case object StaticMeetingRoom    extends MeetingRoom
case object TemporaryMeetingRoom extends MeetingRoom

case class MeetingRoomToken(uuid: String, token: String)

object MeetingRoomToken {
  implicit val reads: Reads[MeetingRoomToken] =
    Json.reads[MeetingRoomToken]
  implicit val writes: Writes[MeetingRoomToken] =
    Json.writes[MeetingRoomToken]
}

case class MeetingRoomAlias(alias: Option[String])
object MeetingRoomAlias {
  implicit val reads: Reads[MeetingRoomAlias] =
    Json.reads[MeetingRoomAlias]
  implicit val writes: Writes[MeetingRoomAlias] =
    Json.writes[MeetingRoomAlias]
}
