package xivo.models

import play.api.libs.json._

case class UserPreference(
    userId: Long,
    key: String,
    value: String,
    valueType: String
)
case class UserPreferencePayload(
    key: Option[String],
    value: String,
    value_type: String
)

object UserPreferenceKey {
  val PreferredDevice = "PREFERRED_DEVICE"
  val MobileAppInfo   = "MOBILE_APP_INFO"
}

object UserPreferenceType {
  val StringType  = "String"
  val BooleanType = "Boolean"
}

object UserPreference {
  implicit val writes: Writes[UserPreference] = new Writes[UserPreference] {
    override def writes(up: UserPreference): JsValue =
      Json.obj("value" -> up.value, "value_type" -> up.valueType)
  }
  implicit val writesList: Writes[List[UserPreference]] =
    new Writes[List[UserPreference]] {
      override def writes(ups: List[UserPreference]): JsValue = {
        ups.foldLeft(Json.obj())((obj, up) => obj + (up.key -> Json.toJson(up)))
      }
    }
  implicit val reads: Reads[UserPreference] = Json.reads[UserPreference]
}

object UserPreferencePayload {
  implicit val writes: OWrites[UserPreferencePayload] =
    Json.writes[UserPreferencePayload]
  implicit val reads: Reads[UserPreferencePayload] =
    Json.reads[UserPreferencePayload]
}
