package xivo.models

import play.api.libs.json.{Json, Writes}

case class UserConfigUpdated(userId: Long, agentId: Option[Long] = None)
case class UserConfigUpdate(
    userId: Long,
    firstName: String,
    lastName: String,
    fullName: String,
    agentId: Long,
    dndEnabled: Boolean,
    naFwdEnabled: Boolean,
    naFwdDestination: String,
    uncFwdEnabled: Boolean,
    uncFwdDestination: String,
    busyFwdEnabled: Boolean,
    busyFwdDestination: String,
    mobileNumber: String,
    lineIds: List[Int],
    voiceMailId: Long,
    voiceMailEnabled: Boolean
)

object UserConfigUpdate {
  implicit val writes: Writes[UserConfigUpdate] = Json.writes[UserConfigUpdate]
}
