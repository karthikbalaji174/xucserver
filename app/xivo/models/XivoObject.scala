package xivo.models

import java.util.Date

import org.xivo.cti.message.{
  QueueStatistics,
  Sheet,
  UserConfigUpdate,
  UserStatusUpdate
}
import org.xivo.cti.model.sheet.{Profile, SheetInfo, User}
import org.xivo.cti.model._
import play.api.libs.json._

import scala.jdk.CollectionConverters._

object XivoObject {

  object ObjectType extends Enumeration {
    type ObjectType = Value
    val Queue = Value
  }

  import ObjectType._
  case class ObjectDefinition(
      objectType: ObjectType,
      objectRef: Option[Int] = None
  ) {
    def hasObjectRef: Boolean = objectRef.isDefined
  }

  implicit val actionWrites: Writes[Action] = new Writes[Action] {
    def writes(o: Action) =
      Json.obj(
        "name"       -> o.getName,
        "parameters" -> o.getParameters
      )
  }

  implicit val userStatusWrites: Writes[UserStatus] = new Writes[UserStatus] {
    def writes(o: UserStatus) =
      Json.obj(
        "name"     -> o.getName,
        "color"    -> Option(o.getColor),
        "longName" -> Option(o.getLongName),
        "actions"  -> o.getActions.asScala
      )
  }

  implicit val meetmeMemberWrites: Writes[MeetmeMember] =
    new Writes[MeetmeMember] {
      def writes(o: MeetmeMember) =
        Json.obj(
          "joinOrder" -> o.getJoinOrder,
          "joinTime"  -> o.getJoinTime,
          "muted"     -> o.isMuted,
          "name"      -> o.getName,
          "number"    -> o.getNumber
        )
    }

  implicit val meetmeWrites: Writes[Meetme] = new Writes[Meetme] {
    def writes(o: Meetme) =
      Json.obj(
        "number"      -> o.getNumber,
        "name"        -> o.getName,
        "pinRequired" -> o.isPinRequired,
        "startTime"   -> o.getStartTime,
        "members"     -> o.getMembers.asScala
      )
  }

  implicit val sheetInfoWrites = new Writes[SheetInfo] {
    def writes(o: SheetInfo) =
      Json.obj(
        "type"  -> o.getType,
        "name"  -> o.getName,
        "order" -> o.getOrder.longValue,
        "value" -> o.getValue
      )
  }

  implicit val userWrites = new Writes[User] {
    def writes(o: User) =
      Json.obj(
        "sheetInfo" -> o.getSheetInfo.asScala
      )
  }

  implicit val profileWrites = new Writes[Profile] {
    def writes(o: Profile) =
      Json.obj(
        "user" -> o.getUser
      )
  }

  implicit val payloadWrites = new Writes[Payload] {
    def writes(o: Payload) =
      Json.obj(
        "profile" -> o.getProfile
      )
  }

  implicit val sheetWrites: Writes[Sheet] = new Writes[Sheet] {

    implicit val timeWrites: Writes[Date] =
      Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

    def writes(o: Sheet) =
      Json.obj(
        "timenow"    -> o.timenow,
        "compressed" -> Option(o.compressed),
        "serial"     -> o.serial,
        "payload"    -> Option(o.payload),
        "channel"    -> o.channel
      )
  }

  implicit val counterWrites = new Writes[Counter] {
    def writes(o: Counter) =
      Json.obj(
        "statName" -> o.getStatName.toString(),
        "value"    -> o.getValue
      )
  }

  implicit val queueStatisticsWrites = new Writes[QueueStatistics] {
    def writes(o: QueueStatistics) =
      Json.obj(
        "queueId"  -> o.getQueueId,
        "counters" -> o.getCounters.asScala
      )
  }

  implicit val phoneHintStatusWrites = new Writes[PhoneHintStatus] {
    def writes(o: PhoneHintStatus) =
      Json.obj(
        "status" -> o.toString()
      )
  }

  implicit val userStatusUpdateWrites = new Writes[UserStatusUpdate] {
    def writes(o: UserStatusUpdate) =
      Json.obj(
        "status" -> o.getStatus
      )
  }

  implicit val userConfigUpdateWrites = new Writes[UserConfigUpdate] {
    def writes(o: UserConfigUpdate) =
      Json.obj(
        "userId"             -> o.getUserId,
        "dndEnabled"         -> o.isDndEnabled,
        "naFwdEnabled"       -> o.isNaFwdEnabled,
        "naFwdDestination"   -> Option(o.getNaFwdDestination),
        "uncFwdEnabled"      -> o.isUncFwdEnabled,
        "uncFwdDestination"  -> Option(o.getUncFwdDestination),
        "busyFwdEnabled"     -> o.isBusyFwdEnabled,
        "busyFwdDestination" -> Option(o.getBusyFwdDestination),
        "firstName"          -> Option(o.getFirstName),
        "lastName"           -> Option(o.getLastName),
        "fullName"           -> Option(o.getFullName),
        "mobileNumber"       -> Option(o.getMobileNumber),
        "agentId"            -> Option(o.getAgentId),
        "lineIds"            -> o.getLineIds.asScala.map(_.intValue),
        "voiceMailId"        -> Option(o.getVoiceMailId),
        "voiceMailEnabled"   -> Option(o.isVoiceMailEnabled())
      )
  }

}
