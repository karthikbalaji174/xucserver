package xivo.models

import java.time.{OffsetDateTime, ZoneOffset}

import play.api.libs.json.{__, JsObject, JsPath, JsValue, Json, Reads}
import xivo.network.ChatBackendWS
import play.api.libs.functional.syntax._

case class MattermostTimeZone(
    automaticTimezone: String,
    manualTimezone: String,
    useAutomaticTimezone: String
)
case class MattermostGuid(
    guid: String,
    username: String,
    displayName: Option[String]
)

case class UpdateUserWithGuid(guid: MattermostGuid)
case class MattermostParties(from: MattermostGuid, to: MattermostGuid)

case class NoChannelsFoundError(message: String, status_code: Int)

sealed trait MattermostRequest
trait Sequence {
  val sequence: Long
}
case class SendDirectMattermostMessage(
    userFrom: MattermostGuid,
    userTo: MattermostGuid,
    message: String,
    sequence: Long
) extends MattermostRequest
    with Sequence
case class GetDirectMattermostMessagesHistory(
    from: MattermostGuid,
    to: MattermostGuid,
    sequence: Long
) extends MattermostRequest
    with Sequence
case class MarkMattermostMessagesAsRead(
    from: MattermostGuid,
    to: MattermostGuid,
    sequence: Long
) extends MattermostRequest
    with Sequence

case class MattermostDirectChannel(
    id: String,
    team_id: String,
    name: String,
    `type`: String
)
object MattermostDirectChannel {
  implicit val reads  = Json.reads[MattermostDirectChannel]
  implicit val writes = Json.writes[MattermostDirectChannel]
}

case class NewMattermostChannelPost(
    channel_id: String,
    message: String,
    seq: Long,
    from: String,
    to: String
)
object NewMattermostChannelPost {
  implicit val reads  = Json.reads[NewMattermostChannelPost]
  implicit val writes = Json.writes[NewMattermostChannelPost]
}

case class MattermostDirectMessage(
    id: String,
    channel_id: String,
    message: String,
    create_at: Long,
    user_id: String
) {
  def offsetDateTime: OffsetDateTime =
    new java.util.Date(create_at).toInstant.atOffset(ZoneOffset.UTC)
}
object MattermostDirectMessage {
  implicit val reads  = Json.reads[MattermostDirectMessage]
  implicit val writes = Json.writes[MattermostDirectMessage]
}

case class MattermostDirectMessageAck(
    id: String,
    channel_id: String,
    message: String,
    create_at: Long,
    user_id: String,
    seq: Long,
    usernameFrom: String,
    usernameTo: String
) {
  def offsetDateTime: OffsetDateTime =
    new java.util.Date(create_at).toInstant.atOffset(ZoneOffset.UTC)
}
object MattermostDirectMessageAck {
  implicit val reads  = Json.reads[MattermostDirectMessageAck]
  implicit val writes = Json.writes[MattermostDirectMessageAck]
}

case class MattermostUser(
    id: String,
    create_at: Long,
    update_at: Long,
    delete_at: Long,
    username: String,
    auth_data: String,
    email: String,
    nickname: String,
    first_name: String,
    last_name: String,
    position: String,
    roles: String,
    locale: String,
    timezone: MattermostTimeZone
)
object MattermostUser {
  implicit val timezoneReads  = Json.reads[MattermostTimeZone]
  implicit val timezoneWrites = Json.writes[MattermostTimeZone]
  implicit val userReads      = Json.reads[MattermostUser]
  implicit val userWrites     = Json.writes[MattermostUser]
}

case class NewMattermostUser(
    email: String,
    username: String,
    password: String,
    first_name: String,
    last_name: String
)
object NewMattermostUser {
  implicit val tjs = Json.writes[NewMattermostUser]
  def apply(
      id: Long,
      secret: String,
      first_name: String,
      last_name: String
  ): NewMattermostUser =
    NewMattermostUser(
      ChatBackendWS.getEmailFromId(id),
      ChatBackendWS.getUsernameFromId(id),
      ChatBackendWS.getPasswordFromId(id, secret),
      first_name,
      last_name
    )
}

case class MattermostTeam(id: String, name: String)
object MattermostTeam {
  implicit val reads  = Json.reads[MattermostTeam]
  implicit val writes = Json.writes[MattermostTeam]
}

case class MattermostChannelMember(channel_id: String, user_id: String)
object MattermostChannelMember {
  implicit val reads  = Json.reads[MattermostChannelMember]
  implicit val writes = Json.writes[MattermostChannelMember]
}

object NoChannelsFoundError {
  implicit val reads: Reads[NoChannelsFoundError] = (
    (JsPath \ "message")
      .read[String]
      .filter(m => m == "No channels were found") and
      (JsPath \ "status_code").read[Int]
  )(NoChannelsFoundError.apply _)
  implicit val writes = Json.writes[NoChannelsFoundError]
}

case class MattermostChannelPosts(
    order: List[String],
    posts: List[MattermostDirectMessage]
)
object MattermostChannelPosts {
  implicit val reads: Reads[MattermostChannelPosts] = (
    (JsPath \ "order").read[List[String]] and
      (__ \ "posts").read[JsValue].map {
        case j: JsObject =>
          j.values.flatMap(r => r.asOpt[MattermostDirectMessage]).toList
        case _ => List()
      }
  )(MattermostChannelPosts.apply _)
}

case class MattermostUnreadCounter(
    team_id: String,
    channel_id: String,
    msg_count: Int
)
object MattermostUnreadCounter {
  implicit val reads  = Json.reads[MattermostUnreadCounter]
  implicit val writes = Json.writes[MattermostUnreadCounter]
}

case class MattermostViewChannelResponse(
    status: String,
    last_viewed_at_times: JsObject
)
object MattermostViewChannelResponse {
  implicit val reads  = Json.reads[MattermostViewChannelResponse]
  implicit val writes = Json.writes[MattermostViewChannelResponse]
}

case class MattermostViewChannel(channel_id: String)
object MattermostViewChannel {
  implicit val reads  = Json.reads[MattermostViewChannel]
  implicit val writes = Json.writes[MattermostViewChannel]
}

case class MattermostUnreadNotification(
    direction: MattermostMessageDirection,
    usernameFrom: String,
    userIdTo: Long,
    message: MattermostDirectMessage,
    offsetDateTime: OffsetDateTime,
    seq: Long
)

sealed trait MattermostMessageDirection
case object DirectionIsFromUser extends MattermostMessageDirection
case object DirectionIsToUser   extends MattermostMessageDirection

case class MattermostNoUnreadMessages(channel: MattermostDirectChannel)
    extends Exception
