package xivo.models

import models.DynamicFilter
import org.joda.time.format.{DateTimeFormat, PeriodFormatterBuilder}
import org.joda.time.{DateTime, Period}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class CustomerCallDetail(
    start: DateTime,
    duration: Option[Period],
    waitTime: Option[Period],
    agentName: Option[String],
    agentNum: Option[String],
    queueName: Option[String],
    queueNum: Option[String],
    status: CallStatus.CallStatus
)

object CustomerCallDetail {

  val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  def validate(json: JsValue) = json.validate[CustomerCallDetail]

  implicit val reads: Reads[CustomerCallDetail] =
    ((JsPath \ "start").read[String].map(format.parseDateTime) and
      (JsPath \ "duration")
        .readNullable[String]
        .map(sOpt => sOpt.map(periodFormat.parsePeriod)) and
      (JsPath \ "wait_time")
        .readNullable[String]
        .map(sOpt => sOpt.map(periodFormat.parsePeriod)) and
      (JsPath \ "agent_name").readNullable[String] and
      (JsPath \ "agent_num").readNullable[String] and
      (JsPath \ "queue_name").readNullable[String] and
      (JsPath \ "queue_num").readNullable[String] and
      (JsPath \ "status")
        .readNullable[String]
        .map(s => CallStatus.withName(s.getOrElse("answered"))))(
      CustomerCallDetail.apply _
    )

  implicit val writes = new Writes[CustomerCallDetail] {
    def writes(call: CustomerCallDetail) =
      Json.obj(
        "start"     -> call.start.toString(format),
        "duration"  -> call.duration.map(_.toString(periodFormat)),
        "waitTime"  -> call.waitTime.map(_.toString(periodFormat)),
        "agentName" -> call.agentName,
        "agentNum"  -> call.agentNum,
        "queueName" -> call.queueName,
        "queueNum"  -> call.queueNum,
        "status"    -> call.status.toString
      )
  }
}

case class FindCustomerCallHistoryRequest(
    filters: List[DynamicFilter],
    size: Int = 10
)
case class FindCustomerCallHistoryResponse(
    total: Long,
    list: List[CustomerCallDetail]
)

object FindCustomerCallHistoryRequest {
  implicit val writesFind: Writes[FindCustomerCallHistoryRequest] =
    Json.writes[FindCustomerCallHistoryRequest]
  implicit val readsFind: Reads[FindCustomerCallHistoryRequest] =
    Json.reads[FindCustomerCallHistoryRequest]

  def validate(json: JsValue) = json.validate[FindCustomerCallHistoryRequest]
}

object FindCustomerCallHistoryResponse {
  implicit val readsResp: Reads[FindCustomerCallHistoryResponse] =
    Json.reads[FindCustomerCallHistoryResponse]
  implicit val writesResp: Writes[FindCustomerCallHistoryResponse] =
    Json.writes[FindCustomerCallHistoryResponse]
}
