package xivo.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

object RabbitEventType extends Enumeration {
  type RabbitType = String

  val QueueCreated: RabbitType           = "queue_created"
  val QueueEdited: RabbitType            = "queue_edited"
  val AgentEdited: RabbitType            = "agent_edited"
  val AgentDeleted: RabbitType           = "agent_deleted"
  val AgentCreated: RabbitType           = "agent_created"
  val MediaServerCreated: RabbitType     = "mediaserver_created"
  val MediaServerEdited: RabbitType      = "mediaserver_edited"
  val MediaServerDeleted: RabbitType     = "mediaserver_deleted"
  val SipEndpointEdited: RabbitType      = "sip_endpoint_edited"
  val UserServicesEdited: RabbitType     = "user_services_edited"
  val SipConfigEdited: RabbitType        = "sip_config_edited"
  val UserConfigEdited: RabbitType       = "user_edited"
  val UserPreferenceCreated: RabbitType  = "user_preference_created"
  val UserPreferenceEdited: RabbitType   = "user_preference_edited"
  val UserPreferenceDeleted: RabbitType  = "user_preference_deleted"
  val MobilePushTokenAdded: RabbitType   = "mobile_push_token_added"
  val MobilePushTokenDeleted: RabbitType = "mobile_push_token_deleted"
  val WebserviceUserCreated: RabbitType  = "webservice_user_created"
  val WebserviceUserEdited: RabbitType   = "webservice_user_edited"
  val WebserviceUsersReload: RabbitType  = "webservice_users_reload"
}

abstract class RabbitEvent
case class RabbitEventData(id: Long, value: Option[String])
case class RabbitEventQueueEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventData {
  implicit val eventDataReads: Reads[RabbitEventData] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "value").readNullable[String]
  )(RabbitEventData.apply _)
}

object RabbitEventQueueEdited {
  implicit val eventQueueEditedReads: Reads[RabbitEventQueueEdited] = (
    (JsPath \ "data").read[RabbitEventData] and
      (JsPath \ "name").read[String]
  )(RabbitEventQueueEdited.apply _)
}

case class RabbitEventQueueCreated(data: RabbitEventData, name: String)
    extends RabbitEvent
object RabbitEventQueueCreated {
  implicit val rabbitEventRead = Json.reads[RabbitEventQueueCreated]
}

case class RabbitEventAgentDeletedData(id: Long)
case class RabbitEventAgentDeleted(
    data: RabbitEventAgentDeletedData,
    name: String
) extends RabbitEvent

object RabbitEventAgentDeletedData {
  implicit val eventAgentDeletedDataReads: Reads[RabbitEventAgentDeletedData] =
    Json.reads[RabbitEventAgentDeletedData]
}
object RabbitEventAgentDeleted {
  implicit val eventAgentDeletedReads: Reads[RabbitEventAgentDeleted] =
    Json.reads[RabbitEventAgentDeleted]
}

case class RabbitEventAgentCreatedData(id: Long)
case class RabbitEventAgentCreated(
    data: RabbitEventAgentCreatedData,
    name: String
) extends RabbitEvent

object RabbitEventAgentCreatedData {
  implicit val eventAgentAddedDataReads: Reads[RabbitEventAgentCreatedData] =
    Json.reads[RabbitEventAgentCreatedData]
}
object RabbitEventAgentCreated {
  implicit val eventAgentAddedReads: Reads[RabbitEventAgentCreated] =
    Json.reads[RabbitEventAgentCreated]
}

case class RabbitEventAgentEditedData(id: Long)
case class RabbitEventAgentEdited(
    data: RabbitEventAgentEditedData,
    name: String
) extends RabbitEvent

object RabbitEventAgentEditedData {
  implicit val eventAgentEditedDataReads: Reads[RabbitEventAgentEditedData] =
    Json.reads[RabbitEventAgentEditedData]
}

object RabbitEventAgentEdited {
  implicit val eventAgentEditedReads: Reads[RabbitEventAgentEdited] = (
    (JsPath \ "data").read[RabbitEventAgentEditedData] and
      (JsPath \ "name").read[String]
  )(RabbitEventAgentEdited.apply _)
}

case class RabbitEventMediaServerCreated(data: RabbitEventData, name: String)
    extends RabbitEvent
case class RabbitEventMediaServerEdited(data: RabbitEventData, name: String)
    extends RabbitEvent
case class RabbitEventMediaServerDeleted(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventMediaServerCreated {
  implicit val rabbitEventRead = Json.reads[RabbitEventMediaServerCreated]
}

object RabbitEventMediaServerEdited {
  implicit val rabbitEventRead = Json.reads[RabbitEventMediaServerEdited]
}

object RabbitEventMediaServerDeleted {
  implicit val rabbitEventRead = Json.reads[RabbitEventMediaServerDeleted]
}

case class RabbitEventSipEndpointEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventSipEndpointEdited {
  implicit val rabbitEventRead = Json.reads[RabbitEventSipEndpointEdited]
}

case class RabbitEventUserServicesEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserServicesEdited {
  implicit val rabbitEventRead = Json.reads[RabbitEventUserServicesEdited]
}

case class RabbitEventSipConfigEdited(name: String) extends RabbitEvent

object RabbitEventSipConfigEdited {
  implicit val rabbitEventRead = Json.reads[RabbitEventSipConfigEdited]
}

case class RabbitEventUserConfigEdited(data: RabbitEventData)
    extends RabbitEvent

object RabbitEventUserConfigEdited {
  implicit val rabbitEventRead = Json.reads[RabbitEventUserConfigEdited]
}

case class RabbitEventUserPreferenceCreated(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserPreferenceCreated {
  implicit val rabbitEventRead: Reads[RabbitEventUserPreferenceCreated] =
    Json.reads[RabbitEventUserPreferenceCreated]
}

case class RabbitEventUserPreferenceEdited(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserPreferenceEdited {
  implicit val rabbitEventRead: Reads[RabbitEventUserPreferenceEdited] =
    Json.reads[RabbitEventUserPreferenceEdited]
}
case class RabbitEventUserPreferenceDeleted(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventUserPreferenceDeleted {
  implicit val rabbitEventRead: Reads[RabbitEventUserPreferenceDeleted] =
    Json.reads[RabbitEventUserPreferenceDeleted]
}

case class RabbitEventMobilePushTokenDeleted(
    data: RabbitEventData,
    name: String
) extends RabbitEvent

object RabbitEventMobilePushTokenDeleted {
  implicit val rabbitEventRead: Reads[RabbitEventMobilePushTokenDeleted] =
    Json.reads[RabbitEventMobilePushTokenDeleted]
}

case class RabbitEventMobilePushTokenAdded(data: RabbitEventData, name: String)
    extends RabbitEvent

object RabbitEventMobilePushTokenAdded {
  implicit val rabbitEventRead: Reads[RabbitEventMobilePushTokenAdded] =
    Json.reads[RabbitEventMobilePushTokenAdded]
}

case class RabbitEventWebserviceUserData(login: String)
object RabbitEventWebserviceUserData {
  implicit val eventWebserviceUserDataReads
      : Reads[RabbitEventWebserviceUserData] =
    Json.reads[RabbitEventWebserviceUserData]
}

case class RabbitEventWebserviceUserCreated(
    data: RabbitEventWebserviceUserData,
    name: String
) extends RabbitEvent

object RabbitEventWebserviceUserCreated {
  implicit val rabbitEventRead: Reads[RabbitEventWebserviceUserCreated] =
    Json.reads[RabbitEventWebserviceUserCreated]
}

case class RabbitEventWebserviceUserEdited(
    data: RabbitEventWebserviceUserData,
    name: String
) extends RabbitEvent

object RabbitEventWebserviceUserEdited {
  implicit val rabbitEventRead: Reads[RabbitEventWebserviceUserEdited] =
    Json.reads[RabbitEventWebserviceUserEdited]
}

case class RabbitEventWebserviceUsersReload(name: String) extends RabbitEvent

object RabbitEventWebserviceUsersReload {
  implicit val rabbitEventRead: Reads[RabbitEventWebserviceUsersReload] =
    Json.reads[RabbitEventWebserviceUsersReload]
}

case class RabbitEventConnectionClosed() extends RabbitEvent
