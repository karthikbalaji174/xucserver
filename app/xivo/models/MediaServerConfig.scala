package xivo.models

import play.api.libs.json._

case class MediaServerConfig(
    id: Long,
    name: String,
    display_name: String,
    voip_ip: String,
    read_only: Boolean
)

object MediaServerConfig {
  implicit val MediaServerWrites: Writes[MediaServerConfig] =
    Json.writes[MediaServerConfig]
  implicit val mediaServerConfigReads: Reads[MediaServerConfig] =
    Json.reads[MediaServerConfig]
}
