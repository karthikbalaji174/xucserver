package xivo.models

import play.api.libs.json.{Json, Reads, Writes}

case class IceServer(
    stunAddress: Option[String]
)

object IceServer {
  implicit val reads: Reads[IceServer]   = Json.reads[IceServer]
  implicit val writes: Writes[IceServer] = Json.writes[IceServer]
}
