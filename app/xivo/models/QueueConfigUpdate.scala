package xivo.models

import play.api.libs.json._

case class QueueConfigUpdate(
    id: Long,
    name: String,
    displayName: String,
    number: String,
    context: Option[String],
    data_quality: Int,
    retries: Int,
    ring: Int,
    transfer_user: Int,
    transfer_call: Int,
    write_caller: Int,
    write_calling: Int,
    url: String,
    announceoverride: String,
    timeout: Option[Int],
    preprocess_subroutine: Option[String],
    announce_holdtime: Int,
    waittime: Option[Int],
    waitratio: Option[Double],
    ignore_forward: Int,
    recording_mode: String,
    recording_activated: Int
)

object QueueConfigUpdate {
  implicit val QueueFeatureReads: Reads[QueueConfigUpdate] =
    Json.reads[QueueConfigUpdate]
  implicit val QueueFeatureWrites: Writes[QueueConfigUpdate] =
    Json.writes[QueueConfigUpdate]
}
