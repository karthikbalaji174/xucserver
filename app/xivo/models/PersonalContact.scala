package xivo.models

import models.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Json, Reads, Writes}
import play.api.mvc.{Result, Results}

trait PersonalContact {}

case class PersonalContactRequest(
    firstname: Option[String],
    lastname: Option[String],
    number: Option[String],
    mobile: Option[String],
    fax: Option[String],
    email: Option[String],
    company: Option[String]
) extends PersonalContact {}

case class PersonalContactResult(
    id: String,
    firstname: Option[String],
    lastname: Option[String],
    number: Option[String],
    mobile: Option[String],
    fax: Option[String],
    email: Option[String],
    company: Option[String]
) extends PersonalContact

case class PersonalContactList(items: List[PersonalContactResult])

case class PersonalContactImportResult(
    created: Option[List[PersonalContactResult]],
    failed: Option[List[PersonalContactImportFailure]]
)
case class PersonalContactImportFailure(
    line: Option[Int],
    errors: Option[List[String]]
)

object PersonalContactImportResult {
  implicit val personalContactImportReads: Reads[PersonalContactImportResult] =
    Json.reads[PersonalContactImportResult]
  implicit val personalContactImportWrites
      : Writes[PersonalContactImportResult] =
    Json.writes[PersonalContactImportResult]
}

object PersonalContactImportFailure {
  implicit val personalContactImportFailureReads
      : Reads[PersonalContactImportFailure] =
    Json.reads[PersonalContactImportFailure]
  implicit val personalContactImportFailureWrites
      : Writes[PersonalContactImportFailure] =
    Json.writes[PersonalContactImportFailure]
}

object PersonalContactRequest {

  val nonEmptyStringValidation
      : PartialFunction[Option[String], Option[String]] = {
    case Some(s) if s.trim.nonEmpty => Some(s)
    case Some(_)                    => None
    case None                       => None
  }

  implicit val personalContactReads: Reads[PersonalContactRequest] = {
    (
      (JsPath \ "firstname")
        .readNullable[String]
        .map(nonEmptyStringValidation) and
        (JsPath \ "lastname")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "number")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "mobile")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "fax")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "email")
          .readNullable[String]
          .map(nonEmptyStringValidation) and
        (JsPath \ "company")
          .readNullable[String]
          .map(nonEmptyStringValidation)
    )(PersonalContactRequest.apply _)
      .filter(p =>
        (p.firstname.exists(_.trim.nonEmpty) || p.lastname
          .exists(_.trim.nonEmpty))
          && (p.mobile.exists(_.trim.nonEmpty) || p.fax
            .exists(_.trim.nonEmpty) || p.number.exists(_.trim.nonEmpty))
      )
  }

  implicit val personalContactWrites: Writes[PersonalContactRequest] = {
    Json.writes[PersonalContactRequest]
  }
}

object PersonalContactResult {
  implicit val personalContactReads: Reads[PersonalContactResult] =
    Json.reads[PersonalContactResult]
  implicit val personalContactWrites: Writes[PersonalContactResult] =
    Json.writes[PersonalContactResult]
}

object PersonalContactList {
  implicit val personalContactListReads: Reads[PersonalContactList] =
    Json.reads[PersonalContactList]
  implicit val personalContacListtWrites: Writes[PersonalContactList] =
    Json.writes[PersonalContactList]
}

case class PersonalContactError(error: ErrorType, message: String)
    extends ErrorResult {

  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case Unreachable => Results.ServiceUnavailable(body)
      case ContactNotFound | DuplicateContact | InvalidContact =>
        Results.BadRequest(body)
      case _ => Results.InternalServerError(body)
    }
  }
}

object PersonalContactError {
  implicit val PersonalContactErrorWrites: Writes[PersonalContactError] =
    Json.writes[PersonalContactError]
}

trait DirdErrorType     extends ErrorType
case object Unreachable extends DirdErrorType { val error = "Unreachable" }
case object ContactNotFound extends DirdErrorType {
  val error = "ContactNotFound"
}
case object DuplicateContact extends DirdErrorType {
  val error = "DuplicateContact"
}
case object InvalidContact extends DirdErrorType {
  val error = "InvalidContact"
}

class DirdRequesterException(val errorType: ErrorType, val message: String)
    extends Exception(message)

case class DirdError(
    timestamp: List[Double],
    reason: List[String],
    status_code: Int
)
object DirdError {
  implicit val dirdErrorReads: Reads[DirdError] = Json.reads[DirdError]
}
