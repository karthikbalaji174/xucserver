package xivo.models

import anorm.Macro
import anorm.SQL
import com.google.inject.{ImplementedBy, Inject}
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class MeetMeConference(
    id: Int,
    meetmeid: Int,
    name: String,
    confno: String,
    context: String,
    user_pin: Option[String] = None,
    admin_pin: Option[String] = None
)

@ImplementedBy(classOf[Conference])
trait ConferenceFactory {
  def all(): Future[List[MeetMeConference]]
}

class Conference @Inject() (db: Database) extends ConferenceFactory {

  val meetMeConference = Macro.namedParser[MeetMeConference]

  def all(): Future[List[MeetMeConference]] =
    Future(
      db.withConnection { implicit c =>
        SQL("""SELECT id, meetmeid, name, confno, context, user_pin, admin_pin
        FROM meetmefeatures""").as(meetMeConference.*)
      }
    )
}
