package xivo.models

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class QueueMember(
    queue_name: String,
    queue_id: Long,
    interface: String,
    penalty: Int,
    commented: Int,
    usertype: String,
    userid: Int,
    channel: String,
    category: String,
    position: Int
)

object QueueMember {
  implicit val QueueMemberReads: Reads[QueueMember]   = Json.reads[QueueMember]
  implicit val QueueMemberWrites: Writes[QueueMember] = Json.writes[QueueMember]
}

case class AgentConfigUpdate(
    id: Long,
    firstname: String,
    lastname: String,
    number: String,
    context: String,
    member: List[QueueMember],
    numgroup: Long,
    userid: Option[Long]
)

object AgentConfigUpdate {
  implicit val AgentConfigUpdateTmpReads: Reads[AgentConfigUpdate] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "firstname").read[String] and
      (JsPath \ "lastname").read[String] and
      (JsPath \ "number").read[String] and
      (JsPath \ "context").read[String] and
      (JsPath \ "member").read[List[QueueMember]] and
      (JsPath \ "numgroup").read[Long] and
      (JsPath \ "userid").readNullable[Long]
  )(AgentConfigUpdate.apply _)

  implicit val AgentConfigUpdateWrites: Writes[AgentConfigUpdate] =
    Json.writes[AgentConfigUpdate]
}
