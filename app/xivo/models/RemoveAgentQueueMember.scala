package xivo.models

case class RemoveAgentQueueMember(agentId: Long)
