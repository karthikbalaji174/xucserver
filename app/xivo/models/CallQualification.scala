package xivo.models

import models.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc.{Result, Results}

case class CallQualificationAnswer(
    sub_qualification_id: Int,
    time: String,
    callid: String,
    agent: Int,
    queue: Int,
    firstName: String,
    lastName: String,
    comment: String,
    customData: String
)
case class CallQualification(
    id: Option[Long],
    name: String,
    subQualification: List[SubQualification]
) {
  def toJson: JsValue =
    Json.obj(
      "id"                -> id,
      "qualificationName" -> name
    )
}
case class SubQualification(id: Option[Long], name: String) {
  def toJson: JsValue =
    Json.obj(
      "id"                   -> id,
      "subQualificationName" -> name
    )
}

object CallQualificationAnswer {

  implicit val writes: Writes[CallQualificationAnswer] {
    def writes(u: CallQualificationAnswer): JsObject
  } = new Writes[CallQualificationAnswer] {
    def writes(u: CallQualificationAnswer) =
      Json.obj(
        "sub_qualification_id" -> u.sub_qualification_id,
        "time"                 -> u.time,
        "callid"               -> u.callid,
        "agent"                -> u.agent,
        "queue"                -> u.queue,
        "first_name"           -> u.firstName,
        "last_name"            -> u.lastName,
        "comment"              -> u.comment,
        "custom_data"          -> u.customData
      )
  }

  implicit val reads: Reads[CallQualificationAnswer] = (
    (JsPath \ "sub_qualification_id").read[Int] and
      (JsPath \ "time").read[String] and
      (JsPath \ "callid").read[String] and
      (JsPath \ "agent").read[Int] and
      (JsPath \ "queue").read[Int] and
      (JsPath \ "first_name").read[String] and
      (JsPath \ "last_name").read[String] and
      (JsPath \ "comment").read[String] and
      (JsPath \ "custom_data").read[String]
  )(CallQualificationAnswer.apply _)
}

object SubQualification {

  implicit val subQualificationWrites: Writes[SubQualification] {
    def writes(subQualification: SubQualification): JsObject
  } = new Writes[SubQualification] {
    def writes(subQualification: SubQualification) =
      Json.obj(
        "id"   -> subQualification.id,
        "name" -> subQualification.name
      )
  }

  implicit val subQualificationReads: Reads[SubQualification] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "name").read[String]
  )(SubQualification.apply _)
}

object CallQualification {

  implicit val callQualificationWrites: Writes[CallQualification] {
    def writes(qualification: CallQualification): JsObject
  } = new Writes[CallQualification] {
    def writes(qualification: CallQualification) =
      Json.obj(
        "id"                -> qualification.id,
        "name"              -> qualification.name,
        "subQualifications" -> qualification.subQualification
      )
  }

  implicit val callQualificationReads: Reads[CallQualification] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "subQualifications").read[List[SubQualification]]
  )(CallQualification.apply _)
}

class CallQualificationException(val errorType: ErrorType, val message: String)
    extends Exception

trait CallQualificationErrorType extends ErrorType
case object JsonBodyNotFound extends CallQualificationErrorType {
  val error = "JsonBodyNotFound"
}
case object JsonErrorDecoding extends CallQualificationErrorType {
  val error = "JsonErrorDecoding"
}

case class CallQualificationError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case JsonBodyNotFound  => Results.BadRequest(body)
      case JsonErrorDecoding => Results.BadRequest(body)
      case _                 => Results.InternalServerError(body)
    }
  }
}
object CallQualificationError {
  implicit val CallQualificationErrorWrites: Writes[CallQualificationError] =
    Json.writes[CallQualificationError]
}
