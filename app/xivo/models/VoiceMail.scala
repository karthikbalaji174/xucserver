package xivo.models

import anorm.SqlParser.{get => aGet}
import anorm.{~, SQL}
import com.google.inject.{ImplementedBy, Inject}
import play.api.db.Database

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class VoiceMail(id: Long, name: String, number: String, context: String)

@ImplementedBy(classOf[VoiceMailFacroryImpl])
trait VoiceMailFactory {
  def all(): Future[List[VoiceMail]]
  def get(username: String): Future[Option[VoiceMail]]
}

class VoiceMailFacroryImpl @Inject() (db: Database) extends VoiceMailFactory {

  val simple = {
    aGet[Long]("id") ~
      aGet[String]("mailbox") ~
      aGet[String]("number") ~
      aGet[String]("context") map { case id ~ mailbox ~ number ~ context =>
        VoiceMail(id, mailbox, number, context)
      }
  }

  def get(username: String): Future[Option[VoiceMail]] =
    Future(
      db.withConnection { implicit c =>
        SQL(
          s"""SELECT uniqueid AS id, CONCAT(voicemail.mailbox,'@',voicemail.context) AS mailbox,
          voicemail.mailbox as number, voicemail.context as context
        FROM voicemail
        LEFT JOIN userfeatures uf ON uf.voicemailid = voicemail.uniqueid
        WHERE uf.loginclient = '$username';""".stripMargin
        ).as(simple.*).headOption
      }
    )

  def all(): Future[List[VoiceMail]] =
    Future(
      db.withConnection { implicit c =>
        SQL("""SELECT uniqueid AS id, CONCAT(voicemail.mailbox,'@',voicemail.context) AS mailbox,
            voicemail.mailbox as number, voicemail.context as context
        FROM voicemail;""".stripMargin).as(simple.*)
      }
    )
}
