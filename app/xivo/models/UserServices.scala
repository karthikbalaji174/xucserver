package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsValue, Json, Reads, Writes}

case class UserForward(
    enabled: Boolean,
    destination: String,
    domain: String = "default"
)

case class UserServices(
    dndEnabled: Boolean,
    busy: UserForward,
    noanswer: UserForward,
    unconditional: UserForward
) {
  def update(partial: PartialUserServices): UserServices = {
    this.copy(
      dndEnabled = partial.dndEnabled.getOrElse(dndEnabled),
      busy = partial.busy.getOrElse(busy),
      noanswer = partial.noanswer.getOrElse(noanswer),
      unconditional = partial.unconditional.getOrElse(unconditional)
    )
  }

}

case class PartialUserServices(
    dndEnabled: Option[Boolean],
    busy: Option[UserForward],
    noanswer: Option[UserForward],
    unconditional: Option[UserForward]
)

case class UserServicesUpdated(userId: Int, services: UserServices)

object UserForward {
  def validate(json: JsValue) = json.validate[UserForward]

  implicit val reads: Reads[UserForward] =
    ((JsPath \ "enabled").read[Boolean] and
      (JsPath \ "destination").read[String] and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default")))(UserForward.apply _)
  implicit val writes: Writes[UserForward] = Json.writes[UserForward]
}

object UserServices {
  implicit val reads: Reads[UserServices]   = Json.reads[UserServices]
  implicit val writes: Writes[UserServices] = Json.writes[UserServices]
}

object PartialUserServices {
  implicit val reads: Reads[PartialUserServices] =
    Json.reads[PartialUserServices]
  implicit val writes: Writes[PartialUserServices] =
    Json.writes[PartialUserServices]
}
