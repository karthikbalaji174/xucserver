package xivo.services

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.pattern.ask
import com.google.inject.Inject
import models.Token
import models.TokenRequest
import models.XivoAuthToken
import models.ws.auth.AuthenticationError
import models.ws.auth.AuthenticationException
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.json.Json
import services.config.ConfigRepository
import xivo.network.XiVOWS
import xivo.services.XivoAuthentication._
import xivo.xuc.XucConfig

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.MILLISECONDS
import controllers.helpers.AuthenticatedAction
import scala.util.Success
import scala.util.Failure

object XivoAuthentication {
  implicit val timeout: akka.util.Timeout = 5.seconds
  val log                                 = Logger(getClass.getName)

  def getCtiTokenHelper(xivoAuth: ActorRef, userId: Long): Future[Token] = {
    (xivoAuth ? GetCtiToken(userId))
      .collect { case t: Token =>
        t
      }
      .recoverWith { case _ =>
        Future.failed(
          new AuthenticationException(
            AuthenticationError.InvalidToken,
            s"Unable to retrieve xivo-auth token for userId $userId"
          )
        )
      }
  }

  def getWebServiceTokenHelper(
      xivoAuth: ActorRef,
      login: String,
      password: String,
      expiration: Int
  ): Future[Token] = {
    (xivoAuth ? GetWebServiceToken(login, password, expiration))
      .transform {
        case Success(t: Token) =>
          Success(
            t.copy(userType = AuthenticatedAction.webServiceUserType)
          )
        case Success(authError: AuthenticationException) =>
          Failure(authError)
        case _ =>
          log.error(
            s"An unknown error occurred while trying to authenticate $login"
          )
          Failure(
            new AuthenticationException(
              AuthenticationError.UnhandledError,
              s"An unknown error occurred while trying to authenticate $login"
            )
          )
      }

  }

  case object Init
  case class Init(
      ctiTokens: Map[Long, Token]
  )
  case class GetCtiToken(userId: Long)
  case class GetWebServiceToken(
      username: String,
      password: String,
      expiration: Int
  )
  case object AuthTimeout
  case object AuthTimeSynchronizationError
  case object AuthUnknownUser
  case class Expired(t: Token, userId: Long)
  case class UserByToken(token: String)
}

class XivoAuthentication @Inject() (
    repo: ConfigRepository,
    xivoWS: XiVOWS,
    config: XucConfig
) extends Actor
    with ActorLogging {

  val renewTimeOverlap = 200

  override def preStart(): Unit = {
    context.become(
      receiveWithCache(Map[Long, Token]())
    )
  }

  def receive: Receive = Actor.emptyBehavior

  def receiveWithCache(
      ctiTokens: Map[Long, Token]
  ): Receive = {
    case Init(ctiMap) =>
      context.become(receiveWithCache(ctiMap))

    case XivoAuthentication.GetCtiToken(userId) =>
      ctiTokens.get(userId) match {
        case Some(t) =>
          sender() ! t
        case None =>
          getCtiToken(ctiTokens, userId)
      }

    case XivoAuthentication.GetWebServiceToken(
          login,
          password,
          expiration
        ) =>
      repo.getWebServiceUser(login) match {
        case Some(user) if user.password == password =>
          log.info(s"Web service user $login retrieved from xivo-auth")
          getWebServiceToken(
            login,
            password,
            expiration
          )
        case Some(user) if user.password != password =>
          log.error(s"Invalid credentials for web service user $login")
          sender() ! new AuthenticationException(
            AuthenticationError.InvalidCredentials,
            "The username or the password is incorrect"
          )
        case _ =>
          log.error(
            s"Web service user $login not found"
          )
          sender() ! new AuthenticationException(
            AuthenticationError.UserNotFound,
            s"Could not find web service user $login"
          )
      }

    case Expired(t, userId) =>
      self ! XivoAuthentication.GetCtiToken(userId)
      log.debug(s"Removing old CTI token and asking for new for user: $userId")
      context.become(receiveWithCache(ctiTokens - userId))

    case UserByToken(token) =>
      sender() ! ctiTokens
        .find(_._2.token == token)
        .map(_._1)
        .flatMap(repo.getCtiUser)

    case u =>
      log.debug(s"Unprocessed message: $u")
  }

  def requestXivoAuthToken(
      backend: String,
      defaultExpires: Int,
      username: Option[String],
      password: Option[String]
  ): XivoAuthToken = {
    val authConfig = config.XivoAuth.getConfig(backend, defaultExpires)
    val request = xivoWS.post(
      config.xivoHost,
      authConfig.getTokenURI,
      Some(
        Json.toJson(
          TokenRequest(
            authConfig.backend,
            authConfig.defaultExpires
          )
        )
      ),
      username,
      password,
      port = Some(authConfig.port)
    )

    val result = request.execute().map(r => r.json.validate[XivoAuthToken].get)
    Await.result(result, config.defaultWSTimeout)
  }

  def getCtiToken(
      ctiTokens: Map[Long, Token],
      userId: Long
  ): Unit = {
    repo.getCtiUser(userId) match {
      case Some(user) =>
        val token = requestXivoAuthToken(
          "xivo_user",
          config.defaultCtiExpires,
          user.username,
          user.password
        )
        token match {
          case t: XivoAuthToken =>
            log.debug(s"Got xivo-auth token: $t")
            if (!t.expiresAt.isAfterNow) {
              sender() ! AuthTimeSynchronizationError
            } else {
              val token = Token(
                t.token,
                t.expiresAt,
                t.issuedAt,
                AuthenticatedAction.ctiUserType,
                t.xivoUserUuid,
                t.acls
              )
              sender() ! token
              context.become(
                receiveWithCache(
                  ctiTokens + (userId -> token)
                )
              )
              context.system.scheduler.scheduleOnce(
                new FiniteDuration(
                  token.expiresAt
                    .minus(new DateTime().getMillis + renewTimeOverlap)
                    .getMillis,
                  MILLISECONDS
                ),
                self,
                Expired(token, userId)
              )
            }
          case _ =>
            log.debug(s"Timeout when getting token for user $userId")
            sender() ! AuthTimeout
        }
      case None =>
        log.error("Requested token for unknown user")
        sender() ! AuthUnknownUser
    }
  }

  def getWebServiceToken(
      username: String,
      password: String,
      expiration: Int
  ): Unit = {
    val token: XivoAuthToken = requestXivoAuthToken(
      "xivo_service",
      expiration,
      Some(username),
      Some(password)
    )
    token match {
      case t: XivoAuthToken =>
        log.debug(s"Got xivo-auth token: $t")
        val token = Token(
          t.token,
          t.expiresAt,
          t.issuedAt,
          AuthenticatedAction.webServiceUserType,
          None,
          t.acls
        )
        sender() ! token

      case _ => {
        log.error(s"Timeout while getting xivo-auth token for user $username")
        sender() ! AuthTimeout
      }
    }
  }
}
