package xivo.phonedevices

import akka.actor.ActorRef
import services.calltracking.DeviceCall
import xivo.models.Line
import xivo.phonedevices.YealinkDevice.Keys
import xivo.xuc.XucConfig

object YealinkDevice {
  object Keys {
    val ok   = "OK"
    val hold = "HOLD"
  }
}

class YealinkDevice(
    ip: String,
    line: Line,
    lineNumber: String,
    config: XucConfig
) extends CtiDevice(line, lineNumber, config) {

  private val contentType = Map("Content-type" -> "application/xml")
  private val event       = Map("Event" -> "Yealink-xml")

  def content(key: String) =
    s"""<?xml version='1.0' encoding='UTF-8'?> <YealinkIPPhoneExecute Beep='no'> <ExecuteItem URI='Key:$key'/> </YealinkIPPhoneExecute>"""

  def xml(key: String) = Map("content" -> content(key)) ++ contentType ++ event

  override def answer(call: Option[DeviceCall], sender: ActorRef) =
    sender ! SipNotifyCommand(line.interface, xml(Keys.ok), line.driver)

  override def hold(call: Option[DeviceCall], sender: ActorRef) =
    sender ! SipNotifyCommand(line.interface, xml(Keys.hold), line.driver)

}
