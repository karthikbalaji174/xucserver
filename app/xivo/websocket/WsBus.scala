package xivo.websocket

import akka.event.{ActorEventBus, LookupClassification}
import com.google.inject.Singleton
import play.api.libs.json.JsValue
import xivo.websocket.WsBus.WsMessageEvent

object WsBus {
  def browserTopic(username: String) = s"/ws/$username/"

  case class WsContent(browserMsg: JsValue)
  case class WsMessageEvent(val channel: String, val message: Any)
}

@Singleton
class WsBus extends ActorEventBus with LookupClassification {

  override type Event      = WsMessageEvent
  override type Classifier = String

  override protected def mapSize(): Int = 300

  override protected def publish(event: Event, subscriber: Subscriber): Unit = {
    subscriber ! event.message
  }

  override protected def classify(event: Event): Classifier = {
    event.channel
  }

}
