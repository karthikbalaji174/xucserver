package xivo.websocket

import akka.actor._
import controllers.helpers.{RequestResult, RequestSuccess}
import models.XucUser
import play.api.Logger
import play.api.libs.concurrent.InjectedActorSupport
import play.api.libs.json.{JsValue, Json}
import services._
import services.config.{ConfigRepository, ConfigServerRequester}
import services.request._
import services.user.UserRight
import xivo.services.{AuthenticationToken, TokenRetriever}
import xivo.websocket.ActorFlowOut.ActorFlowOutAck
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.WsContent
import xivo.xuc.ConfigServerConfig

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

object WsActor {
  def props(
      user: XucUser,
      configServerRequester: ConfigServerRequester,
      bus: WsBus,
      ctiRouterFactory: ActorRef,
      tokenRetrieverFactory: TokenRetriever.Factory,
      configRepository: ConfigRepository,
      configServerConfig: ConfigServerConfig
  ): Future[ActorRef => Props] =
    UserRight
      .getRights(user.username, configServerRequester, configServerConfig)
      .map(right =>
        out =>
          Props(
            new WsActor(
              user,
              out,
              right,
              bus,
              ctiRouterFactory,
              tokenRetrieverFactory,
              configRepository
            )
          )
      )
  case class WsConnected(wsActor: ActorRef, user: XucUser)
}

class WsActor(
    user: XucUser,
    out: ActorRef,
    right: UserRight,
    bus: WsBus,
    ctiRouterFactory: ActorRef,
    tokenRetrieverFactory: TokenRetriever.Factory,
    configRepository: ConfigRepository,
    requestDecoder: RequestDecoder = new RequestDecoder(),
    wsEncoder: WsEncoder = new WsEncoder()
) extends Actor
    with ActorLogging
    with InjectedActorSupport {

  log.info(s"${user.username} new web socket actor created")

  val tokenRetriever = injectedChild(tokenRetrieverFactory(), "token")
  val logWsIn        = Logger("xucevents.ws.in." + user.username)
  val logWsOut       = Logger("xucevents.ws.out." + user.username)
  val logWsPretty = Try(
    context.system.settings.config.getBoolean("logger.xucevents.prettyPrint")
  ).getOrElse(false)

  def receive: Receive = Actor.emptyBehavior

  override def preStart(): Unit = {
    ctiRouterFactory ! GetRouter(user)
    bus.subscribe(self, WsBus.browserTopic(user.username))
    context.become(initializing(List.empty))
  }

  def initializing(pendingMessages: List[Any]): Receive = {
    case Router(user, ref) =>
      ref ! WsConnected(self, user)
      log.info(s"${user.username} received router")

      tokenRetriever ! TokenRetriever.TokenByLogin(user.username)
      pendingMessages.foreach(m => self ! m)
      context.become(processToken orElse ready(ref, Queue[JsValue]()))

    case RequestSuccess(token) =>
      log.debug(s"Received token $token for ${user.username}")
      val newMsg = List(
        WsContent(WebSocketEvent.createEvent(AuthenticationToken(token))),
        WsContent(WebSocketEvent.createEvent(right))
      )
      context.become(initializing(pendingMessages ++ newMsg))

    case r: RequestResult =>
      log.debug(s"Failed received token for ${user.username}")
  }

  def processToken: Receive = {
    case RequestSuccess(token) =>
      log.debug(s"Received token $token for ${user.username}")
      self ! WsContent(WebSocketEvent.createEvent(AuthenticationToken(token)))
      self ! WsContent(WebSocketEvent.createEvent(right))
    case r: RequestResult =>
      log.debug(s"Failed received token for ${user.username}")
  }

  def ready(userRouter: ActorRef, wsEventsQueue: Queue[JsValue]): Receive =
    fromBrowser(userRouter) orElse readyToBrowser(userRouter, wsEventsQueue)

  def sendPending(
      userRouter: ActorRef,
      wsEventsQueue: Queue[JsValue]
  ): Receive =
    processToken orElse fromBrowser(userRouter) orElse sendPendingToBrowser(
      userRouter,
      wsEventsQueue
    )

  def fromBrowser(userRouter: ActorRef): Receive = { case msg: JsValue =>
    log.debug(s"[${user.username} - Val From browser] >>>> $msg")

    requestDecoder.decode(msg) match {
      case Ping => self ! WsContent(msg)
      case invalid: InvalidRequest => {
        logrequest(s"$invalid", log.error)
        self ! WsContent(
          WebSocketEvent.createError(WSMsgType.Error, invalid.toString)
        )
      }
      case xucRequest: XucRequest => {
        logXucRequest(xucRequest, msg)
        userRouter ! BaseRequest(self, xucRequest)
      }
    }
  }

  def logXucRequest(xucRequest: XucRequest, msg: JsValue) = {
    xucRequest match {
      case _: PushLogToServer =>
      case _                  => logrequest(s"$xucRequest")
    }
    logWsIn.debug {
      val json =
        if (logWsPretty) Json.prettyPrint(msg) else Json.stringify(msg)
      json + " -> " + xucRequest
    }
  }

  def readyToBrowser(
      userRouter: ActorRef,
      wsEventsQueue: Queue[JsValue]
  ): Receive = {
    case ActorFlowOutAck =>
      log.error("Received ActorFlowAck when no pending message")

    case WsContent(json) =>
      log.debug(
        s"[${user.username} - To browser  ] <<<< $json received from ${sender()}"
      )
      logWsOut.debug {
        if (logWsPretty) Json.prettyPrint(json) else Json.stringify(json)
      }
      out ! json
      context.become(sendPending(userRouter, wsEventsQueue))

    case other =>
      right
        .filter(other, configRepository)
        .foreach(m =>
          wsEncoder.encode(m) match {
            case Some(WsContent(json)) =>
              log.debug(
                s"[${user.username} - To browser  ] <<<< $json received from ${sender()}"
              )
              out ! json
              context.become(sendPending(userRouter, wsEventsQueue))
            case _ =>
              log.error(
                s"${user.username} cannot encode message for browser $other"
              )
          }
        )
  }

  def sendPendingToBrowser(
      userRouter: ActorRef,
      wsEventsQueue: Queue[JsValue]
  ): Receive = {
    case ActorFlowOutAck =>
      if (wsEventsQueue.nonEmpty) {
        val (elem, tail): (JsValue, Queue[JsValue]) = wsEventsQueue.dequeue
        out ! elem
        context.become(sendPending(userRouter, tail))
      } else {
        context.become(ready(userRouter, wsEventsQueue))
      }

    case WsContent(json) =>
      log.debug(
        s"[${user.username} - To browser  ] <<<< $json received from ${sender()}"
      )
      logWsOut.debug {
        if (logWsPretty) Json.prettyPrint(json) else Json.stringify(json)
      }
      context.become(sendPending(userRouter, enqueue(json, wsEventsQueue)))

    case other =>
      right
        .filter(other, configRepository)
        .foreach(m =>
          wsEncoder.encode(m) match {
            case Some(WsContent(json)) =>
              log.debug(
                s"[${user.username} - To browser  ] <<<< $json received from ${sender()}"
              )
              context
                .become(sendPending(userRouter, enqueue(json, wsEventsQueue)))
            case _ =>
              log.error(
                s"${user.username} cannot encode message for browser $other"
              )
          }
        )
  }

  private def enqueue(
      json: JsValue,
      wsEventsQueue: Queue[JsValue]
  ): Queue[JsValue] = wsEventsQueue.enqueue(json)

  override def postStop() = {
    log.info(s"${user.username} web socket closed")
    context.actorSelection(
      ActorIds.ctiRouterPath(ctiRouterFactory, user.username)
    ) ! Leave(self)
    bus.unsubscribe(self)
  }
  private def logrequest(request: String, logf: String => Unit = log.info) =
    logf(s"[${user.username}] Req: <$request>")
}
