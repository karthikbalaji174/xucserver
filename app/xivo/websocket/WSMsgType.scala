package xivo.websocket

import org.xivo.cti.message.Sheet
import play.api.libs.json._

object WSMsgType extends Enumeration {

  type WSMsgType = Value
  val AgentLogin                        = Value
  val AgentConfig                       = Value
  val AgentDirectory                    = Value
  val AgentError                        = Value
  val AgentGroupList                    = Value
  val AgentList                         = Value
  val AgentListen                       = Value
  val AgentLogout                       = Value
  val AgentPaused                       = Value
  val AgentReady                        = Value
  val AgentStateEvent                   = Value
  val AgentStatistics                   = Value
  val AuthenticationToken               = Value
  val CallbackClotured                  = Value
  val CallbackLists                     = Value
  val CallbackReleased                  = Value
  val CallbackRequestUpdated            = Value
  val CallbackStarted                   = Value
  val CallbackTaken                     = Value
  val CallHistory                       = Value
  val RichCallHistory                   = Value
  val ConferenceCommandError            = Value
  val ConferenceEvent                   = Value
  val ConferenceList                    = Value
  val ConferenceParticipantEvent        = Value
  val CurrentCallsPhoneEvents           = Value
  val CustomerCallHistoryResponseWithId = Value
  val DirectoryResult                   = Value
  val Error                             = Value
  val Favorites                         = Value
  val FavoriteUpdated                   = Value
  val FindCallbackResponseWithId        = Value
  val FlashTextEvent                    = Value
  val IceConfig                         = Value
  val LineConfig                        = Value
  val LineStateEvent                    = Value
  val LinkStatusUpdate                  = Value
  val LoggedOn                          = Value
  val PhoneAvailable                    = Value
  val PhoneCalling                      = Value
  val PhoneEvent                        = Value
  var PhoneHintStatusEvent              = Value
  val PhoneRinging                      = Value
  val PhoneStatusUpdate                 = Value
  val PreferredCallbackPeriodList       = Value
  val QueueCalls                        = Value
  val QueueConfig                       = Value
  val QueueList                         = Value
  val QueueMember                       = Value
  val QueueMemberList                   = Value
  val QueueStatistics                   = Value
  val RightProfile                      = Value
  val Sheet                             = Value(classOf[Sheet].getSimpleName)
  val UserConfigUpdate                  = Value
  val UserDisplayName                   = Value
  val UserPreference                    = Value
  val UserQueueDefaultMembership        = Value
  val UsersQueueDefaultMembership       = Value
  val UsersStatuses                     = Value
  val CtiStatuses                       = Value
  val UserStatusUpdate                  = Value
  val VoiceMailStatusUpdate             = Value
  val WebRTCCmd                         = Value
  val VideoStatusEvent                  = Value
  val MeetingRoomInvite                 = Value
  val MeetingRoomInviteAck              = Value
  val MeetingRoomInviteResponse         = Value
}

object LinkState extends Enumeration {
  type LinkState = Value
  val down, up = Value
}
import xivo.websocket.LinkState._

case class LinkStatusUpdate(status: LinkState)

object LinkStatusUpdate {

  implicit val linkStatusWrites = new Writes[LinkStatusUpdate] {
    def writes(linkStatus: LinkStatusUpdate) =
      Json.obj(
        "status" -> linkStatus.status.toString
      )
  }
  def toJson(linkStatus: LinkStatusUpdate) = Json.toJson(linkStatus)
}
