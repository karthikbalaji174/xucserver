package xivo.websocket

import play.api.libs.json._
import services.calltracking.ConferenceParticipant
import services.calltracking.ConferenceTracker._
import services.calltracking.{OrganizerRole, UserRole}
import services.calltracking.DeviceConferenceAction._
import helpers.DateUtil

sealed trait WsConferenceParticipantRole
case object WsConferenceParticipantOrganizerRole
    extends WsConferenceParticipantRole
case object WsConferenceParticipantUserRole extends WsConferenceParticipantRole

case class WsConferenceParticipant(
    index: Int,
    callerIdName: String,
    callerIdNum: String,
    since: Int,
    isTalking: Boolean = false,
    role: WsConferenceParticipantRole = WsConferenceParticipantUserRole,
    isMuted: Boolean = false,
    isMe: Boolean = false,
    username: Option[String] = None,
    isDeaf: Boolean = false
)

object WsConferenceParticipant {
  def from(p: ConferenceParticipant, me: List[Int]): WsConferenceParticipant = {
    val role = p.role match {
      case OrganizerRole => WsConferenceParticipantOrganizerRole
      case UserRole      => WsConferenceParticipantUserRole
    }

    WsConferenceParticipant(
      p.index,
      p.callerId.name,
      p.callerId.number,
      DateUtil.secondsSince(p.joinTime).toInt,
      p.isTalking,
      role,
      p.isMuted,
      me.contains(p.index),
      isDeaf = p.isDeaf
    )
  }
}

sealed trait WsConferenceEventType
case object WsConferenceEventJoin  extends WsConferenceEventType
case object WsConferenceEventLeave extends WsConferenceEventType

sealed trait WsConferenceParticipantEventType
case object WsConferenceParticipantEventJoin
    extends WsConferenceParticipantEventType
case object WsConferenceParticipantEventLeave
    extends WsConferenceParticipantEventType
case object WsConferenceParticipantEventUpdate
    extends WsConferenceParticipantEventType

case class WsConferenceEvent(
    eventType: WsConferenceEventType,
    uniqueId: String,
    phoneNumber: String,
    conferenceNumber: String,
    conferenceName: String,
    participants: List[WsConferenceParticipant],
    since: Int
)

object WsConferenceEvent {

  def from(
      evt: DeviceConference,
      uniqueId: String,
      phoneNumber: String,
      me: List[Int]
  ): WsConferenceEvent = {
    val eventType = evt match {
      case _: DeviceJoinConference  => WsConferenceEventJoin
      case _: DeviceLeaveConference => WsConferenceEventLeave
    }
    val ps =
      evt.conference.participants.map(WsConferenceParticipant.from(_, me))
    val since =
      evt.conference.startTime.map(DateUtil.secondsSince(_).toInt).getOrElse(0)
    WsConferenceEvent(
      eventType,
      uniqueId,
      phoneNumber,
      evt.conference.number,
      evt.conference.name,
      ps,
      since
    )
  }

  implicit val wsConferenceParticipantRoleWrite =
    new Writes[WsConferenceParticipantRole] {
      def writes(r: WsConferenceParticipantRole) =
        r match {
          case WsConferenceParticipantOrganizerRole => JsString("Organizer")
          case WsConferenceParticipantUserRole      => JsString("User")
        }
    }

  implicit val wsConferenceParticipantRoleReads =
    new Reads[WsConferenceParticipantRole] {
      override def reads(json: JsValue): JsResult[WsConferenceParticipantRole] =
        json match {
          case JsString("Organizer") =>
            JsSuccess(WsConferenceParticipantOrganizerRole)
          case JsString("User") => JsSuccess(WsConferenceParticipantUserRole)
          case _                => JsError("Invalid Conference Role")
        }
    }

  implicit val wsParticipantWrites = Json.writes[WsConferenceParticipant]

  implicit val wsConferenceEventTypeWrite = new Writes[WsConferenceEventType] {
    def writes(eventType: WsConferenceEventType) =
      eventType match {
        case WsConferenceEventJoin  => JsString("Join")
        case WsConferenceEventLeave => JsString("Leave")
      }
  }

  implicit val wsConferenceEventWrites = Json.writes[WsConferenceEvent]

}

case class WsConferenceParticipantEvent(
    eventType: WsConferenceParticipantEventType,
    uniqueId: String,
    phoneNumber: String,
    conferenceNumber: String,
    index: Int,
    callerIdName: String,
    callerIdNum: String,
    since: Int,
    isTalking: Boolean = false,
    role: WsConferenceParticipantRole = WsConferenceParticipantUserRole,
    isMuted: Boolean = false,
    isMe: Boolean = false,
    username: Option[String] = None,
    isDeaf: Boolean = false
)

object WsConferenceParticipantEvent {

  def from(
      evt: ConferenceParticipantChange,
      uniqueId: String,
      phoneNumber: String,
      me: List[Int],
      username: Option[String] = None
  ): WsConferenceParticipantEvent = {
    val eventType = evt match {
      case _: ParticipantJoinConference  => WsConferenceParticipantEventJoin
      case _: ParticipantLeaveConference => WsConferenceParticipantEventLeave
      case _: ParticipantUpdated         => WsConferenceParticipantEventUpdate
    }

    val role = evt.participant.role match {
      case OrganizerRole => WsConferenceParticipantOrganizerRole
      case UserRole      => WsConferenceParticipantUserRole
    }

    WsConferenceParticipantEvent(
      eventType,
      uniqueId,
      phoneNumber,
      evt.numConf,
      evt.participant.index,
      evt.participant.callerId.name,
      evt.participant.callerId.number,
      DateUtil.secondsSince(evt.participant.joinTime).toInt,
      evt.participant.isTalking,
      role,
      evt.participant.isMuted,
      me.contains(evt.participant.index),
      username,
      isDeaf = evt.participant.isDeaf
    )
  }

  import WsConferenceEvent.wsConferenceParticipantRoleWrite

  implicit val wsConferenceParticipantEventTypeWrite =
    new Writes[WsConferenceParticipantEventType] {
      def writes(eventType: WsConferenceParticipantEventType) =
        eventType match {
          case WsConferenceParticipantEventJoin   => JsString("Join")
          case WsConferenceParticipantEventLeave  => JsString("Leave")
          case WsConferenceParticipantEventUpdate => JsString("Update")
        }
    }

  implicit val wsConferenceParticipantEvent =
    Json.writes[WsConferenceParticipantEvent]
}

case class WsConferenceCommandError(error: DeviceConferenceCommandErrorType)

object WsConferenceCommandError {
  implicit val wsDeviceConferenceCommandErrorType =
    new Writes[DeviceConferenceCommandErrorType] {
      def writes(error: DeviceConferenceCommandErrorType) =
        error match {
          case NotAMember          => JsString("NotAMember")
          case NotOrganizer        => JsString("NotOrganizer")
          case CannotKickOrganizer => JsString("CannotKickOrganizer")
          case ParticipantNotFound => JsString("ParticipantNotFound")
          case ConferenceNotFound  => JsString("ConferenceNotFound")
        }
    }

  implicit val writes = Json.writes[WsConferenceCommandError]
}
