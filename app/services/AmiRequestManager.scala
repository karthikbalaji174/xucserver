package services

import akka.actor.{Actor, ActorLogging, Props}
import org.asteriskjava.manager.action.SetVarAction
import services.XucAmiBus._

object AmiRequestManager {
  def props(amiBus: XucAmiBus) = Props(new AmiRequestManager(amiBus))
}

class AmiRequestManager(amiBus: XucAmiBus) extends Actor with ActorLogging {

  amiBus.subscribe(self, AmiType.SetVarActionRequest)
  amiBus.subscribe(self, AmiType.ListenActionRequest)
  amiBus.subscribe(self, AmiType.BeepActionRequest)
  amiBus.subscribe(self, AmiType.AmiRequest)

  override def receive: Receive = {

    case SetVarRequest(SetVarActionRequest(name, value)) =>
      val action = new SetVarAction(name, value)
      amiBus.publish(AmiAction(action))

    case ListenRequest(listenActionRequest) =>
      amiBus.publish(AmiAction(listenActionRequest.buildAction()))

    case BeepRequest(beepActionRequest) =>
      amiBus.publish(AmiAction(beepActionRequest.buildAction()))

    case amiRequest: AmiRequest =>
      log.debug(s"$amiRequest")
      amiBus.publish(
        AmiAction(
          amiRequest.message.buildAction(),
          targetMds = amiRequest.targetMds
        )
      )

    case unknown => log.debug(s"unknown $unknown request received")
  }
}
