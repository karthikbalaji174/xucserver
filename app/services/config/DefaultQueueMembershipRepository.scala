package services.config

import com.google.inject.Inject
import models.{
  QueueMembership,
  UserQueueDefaultMembership,
  UsersQueueDefaultMembership,
  UsersQueueMembership
}
import services.XucEventBus
import services.config.ConfigDispatcher.ObjectType
import services.request.{SetUserDefaultMembership, SetUsersDefaultMembership}

import scala.concurrent.{ExecutionContext, Future}

class DefaultQueueMembershipRepository @Inject() (
    configServerRequester: ConfigServerRequester,
    val eventBus: XucEventBus
) extends RepositoryMap[Long, List[QueueMembership]]
    with PublishToBus[Long, List[QueueMembership]] {

  import context.dispatcher

  val topic = XucEventBus.configTopic(ObjectType.TypeBaseQueueMember)

  override def load(implicit
      ec: ExecutionContext
  ): Future[Map[Long, List[QueueMembership]]] = {
    log.debug("loading DefaultQueueMembership")
    configServerRequester.getAllDefaultMembership.map(
      _.map(m => (m.userId -> m.membership)).toMap
    )
  }

  override def mapResponse(e: Entry[Long, List[QueueMembership]]): Any =
    UserQueueDefaultMembership(e.key, e.value.getOrElse(List.empty))

  override def mapResponse(m: Map[Long, List[QueueMembership]]): Any =
    UsersQueueDefaultMembership(
      m.map(e => UserQueueDefaultMembership(e._1, e._2)).toList
    )

  override def receiveCommand = {
    case SetUserDefaultMembership(userId, membership) =>
      log.debug(s"${sender()} ! SetUserDefaultMembership($userId, $membership)")
      configServerRequester
        .setUserDefaultMembership(userId, membership)
        .map(_ => Entry(userId, Some(membership)))
        .foreach(command)

    case SetUsersDefaultMembership(userIds, membership) =>
      log.debug(
        s"${sender()} ! SetUsersDefaultMembership($userIds, $membership)"
      )
      configServerRequester
        .setUsersDefaultMembership(UsersQueueMembership(userIds, membership))
        .foreach(_ =>
          userIds
            .map(Entry(_, Some(membership)))
            .foreach(command)
        )
  }

}
