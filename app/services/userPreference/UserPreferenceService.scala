package services.userPreference

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import helpers.JmxActorSingletonMonitor
import services.ClientConnected
import services.config.ConfigRepository
import services.request._
import xivo.models.UserPreference
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class UserPreferenceService @Inject() (
    configRepo: ConfigRepository
) extends Actor
    with ActorLogging
    with JmxActorSingletonMonitor {

  var usersPreferences: Map[Long, UserPreferencesWithRef] = Map.empty

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
  }

  override def receive: Receive = {
    case SetUserPreferenceRequest(maybeUserId, key, value, value_type) => {
      maybeUserId.foreach(userId => {
        val ctiRouterRef = sender()
        addOrUpdateUserPreference(
          userId,
          UserPreference(userId, key, value, value_type),
          ctiRouterRef
        )
      })
    }

    case UserPreferencesInit(maybeUserId, ctiRouterRef) =>
      maybeUserId.foreach(userId =>
        getOrFetchUserPreference(userId).foreach(ups => {
          usersPreferences =
            usersPreferences + (userId -> UserPreferencesWithRef(
              ctiRouterRef,
              ups
            ))
          ctiRouterRef !
            WebSocketEvent.createUserPreferenceEvent(ups)
        })
      )

    case ClientConnected(connectedActor, ln) =>
      val userId = ln.user.xivoUser.id
      getOrFetchUserPreference(userId)
        .foreach(ups =>
          connectedActor ! WsContent(
            WebSocketEvent.createUserPreferenceEvent(ups)
          )
        )

    case UserPreferenceDisconnect(maybeUserId) =>
      maybeUserId.foreach(userId =>
        usersPreferences
          .get(userId)
          .foreach(up => usersPreferences = usersPreferences - userId)
      )
    case UnregisterMobileApp(maybeUsername) =>
      maybeUsername.map(username =>
        configRepo.configServerRequester
          .deleteMobileAppPushToken(username)
      )

    case UserPreferenceCreated(userId) => onUserPreferenceChange(userId)
    case UserPreferenceDeleted(userId) => onUserPreferenceChange(userId)
    case UserPreferenceEdited(userId)  => onUserPreferenceChange(userId)
  }

  private def fetchUserPreferences(userId: Long): Future[List[UserPreference]] =
    configRepo.configServerRequester
      .getUserPreferences(userId)

  private def getOrFetchUserPreference(
      userId: Long
  ): Future[List[UserPreference]] =
    usersPreferences
      .get(userId)
      .map(_.preferences)
      .map(Future.successful)
      .getOrElse(fetchUserPreferences(userId))

  private def onUserPreferenceChange(userId: Long): Unit =
    usersPreferences
      .get(userId)
      .foreach(oldUserPrefs =>
        configRepo.configServerRequester
          .getUserPreferences(userId)
          .foreach(ups => {
            updateUserPreferences(userId, ups)
              .foreach(userPreferencesWithRef => {
                userPreferencesWithRef.ctiRouterRef ! WebSocketEvent
                  .createUserPreferenceEvent(ups)
              })
          })
      )

  private def updateUserPreferences(
      userId: Long,
      userPreferences: List[UserPreference]
  ): Option[UserPreferencesWithRef] = usersPreferences
    .get(userId)
    .map(upu => {
      usersPreferences =
        usersPreferences + (userId -> upu.copy(preferences = userPreferences))
      upu
    })

  private def addOrUpdateUserPreference(
      userId: Long,
      userPreference: UserPreference,
      ctiRouterRef: ActorRef
  ): Future[UserPreferencesWithRef] =
    getOrFetchUserPreference(userId).map(userPrefs => {
      val newPrefs = userPreference :: userPrefs.filter(
        _.key != userPreference.key
      )
      val userPrefWithRef = updateUserPreferences(userId, newPrefs)
        .getOrElse({
          val userPrefWithRef = UserPreferencesWithRef(ctiRouterRef, newPrefs)
          usersPreferences = usersPreferences + (userId -> userPrefWithRef)
          userPrefWithRef
        })
      configRepo.configServerRequester
        .setUserPreference(userPreference)
      userPrefWithRef
    })
}

object UserPreferenceService {
  final val serviceName = "UserPreferenceService"
}
