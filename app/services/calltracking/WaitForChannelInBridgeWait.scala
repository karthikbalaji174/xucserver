package services.calltracking

import akka.actor._
import scala.concurrent.{Future, Promise}
import services.calltracking.AsteriskGraphTracker.PathsFromChannel
import services.calltracking.BaseTracker._
import services.calltracking.graph.{NodeBridge, NodeChannelLike}
import xivo.xucami.models.{Channel, ChannelState}
import akka.util.Timeout
import scala.concurrent.duration._

sealed trait ChannelBridgeResult
case object ChannelBridged extends ChannelBridgeResult
case object ChannelDead
    extends Exception("Channel is dead")
    with ChannelBridgeResult
case class ChannelWaitTimeout(channel: String)
    extends Exception(
      s"Timeout while waiting for channel $channel to be bridged"
    )
    with ChannelBridgeResult

class WaitForChannelInBridgeWait(
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    channel: NodeChannelLike,
    result: Promise[ChannelBridgeResult]
) extends Actor
    with ActorLogging {

  override def preStart(): Unit = {
    channelTracker ! WatchChannelStartingWith(channel.name)
    graphTracker ! WatchChannelStartingWith(channel.name)
  }

  override def postStop(): Unit = {
    channelTracker ! UnWatchChannelStartingWith(channel.name)
    graphTracker ! UnWatchChannelStartingWith(channel.name)
  }

  override def receive = {
    case c: Channel if c.state == ChannelState.HUNGUP =>
      log.debug(s"$channel is hung up")
      result.failure(ChannelDead)
      context.stop(self)

    case PathsFromChannel(_, paths) =>
      if (
        !paths
          .flatMap(_.headOption)
          .collect({
            case b: NodeBridge if b.bridgeCreator.contains("BridgeWait") => b
          })
          .isEmpty
      ) {
        log.debug(s"$channel is entering bridge")
        result.success(ChannelBridged)
        context.stop(self)
      }
  }
}

object WaitForChannelInBridgeWait {
  private val defaultTimeout: Timeout = Timeout(3.second)

  def apply(
      channelTracker: ActorRef,
      graphTracker: ActorRef,
      channel: NodeChannelLike
  )(implicit context: ActorContext): Future[ChannelBridgeResult] =
    apply(channelTracker, graphTracker, channel, defaultTimeout)(context)

  def apply(
      channelTracker: ActorRef,
      graphTracker: ActorRef,
      channel: NodeChannelLike,
      timeout: Timeout
  )(implicit context: ActorContext): Future[ChannelBridgeResult] = {
    val result      = Promise[ChannelBridgeResult]()
    val scheduler   = context.system.scheduler
    implicit val ec = context.dispatcher
    val actor = context.actorOf(
      Props(
        new WaitForChannelInBridgeWait(
          channelTracker,
          graphTracker,
          channel,
          result
        )
      )
    )
    val f = scheduler.scheduleOnce(timeout.duration) {
      result tryComplete scala.util.Failure(ChannelWaitTimeout(channel.name))
    }

    result.future onComplete { _ =>
      actor ! PoisonPill
      f.cancel()
    }

    result.future
  }
}
