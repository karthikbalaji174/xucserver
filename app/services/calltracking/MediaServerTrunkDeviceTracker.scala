package services.calltracking

import akka.actor._
import services.XucAmiBus
import services.calltracking.DeviceMessage._
import xivo.xuc.DeviceTrackerConfig

class MediaServerTrunkDeviceTracker(
    trunkInterface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig
) extends UnknownDeviceTracker(
      trunkInterface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {

  override val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.MediaServerTrunkDeviceTrackerType

}
