package services.calltracking

import helpers.FastLogging
import akka.actor.Actor
import akka.actor._
import helpers.JmxActorSingletonMonitor

object BaseTracker {
  case class WatchChannelStartingWith(name: String)
  case class UnWatchChannelStartingWith(name: String)
}

trait BaseTracker[T] extends JmxActorSingletonMonitor {
  this: Actor with FastLogging =>
  import BaseTracker._

  val jmxNotificationCount = jmxBean.addLong(
    "Notifications",
    0L,
    Some("Total number of notification triggered")
  )
  val jmxWatchersCount =
    jmxBean.addLong("Watchers", 0L, Some("Number of notification subscriber"))

  var watchers = Map.empty[String, Set[ActorRef]]

  def onNewWatcher(name: String, actor: ActorRef): Unit = {}

  def addWatcher(name: String, actor: ActorRef): Unit = {
    log.debug(s"New watcher for $name : $actor")
    jmxWatchersCount.inc()
    val actors = watchers
      .get(name)
      .map(_ + actor)
      .getOrElse(Set(actor))

    watchers = watchers.updated(name, actors)
    onNewWatcher(name, sender())
  }

  def delWatcher(name: String, actor: ActorRef): Unit = {
    jmxWatchersCount.dec()
    watchers
      .get(name)
      .map(_ - actor)
      .foreach(s =>
        if (s.nonEmpty) {
          watchers = watchers.updated(name, s)
        } else {
          watchers -= name
        }
      )
  }

  def notify(key: String, o: T): Unit = {
    watchers.view
      .filterKeys(key.startsWith)
      .flatMap(_._2)
      .foreach(dest => {
        jmxNotificationCount.inc()
        dest ! o
      })
  }

  def handleWatchers: Receive = {
    case WatchChannelStartingWith(name) =>
      addWatcher(name, sender())

    case UnWatchChannelStartingWith(name) =>
      delWatcher(name, sender())
  }

}
