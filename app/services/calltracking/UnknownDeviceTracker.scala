package services.calltracking

import akka.actor._
import services.XucAmiBus
import services.calltracking.ConferenceTracker._
import services.calltracking.DeviceMessage._
import services.calltracking.SingleDeviceTracker.PartyInformation
import xivo.xuc.DeviceTrackerConfig

class UnknownDeviceTracker(
    interface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig
) extends SingleDeviceTracker(
      interface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {

  override def customBehavior: Receive =
    redirectConferenceEvent orElse super.customBehavior

  def redirectConferenceEvent: Receive = {
    case DeviceJoinConference(conf, channel) =>
      getRemoteChannelName(channel)
        .map(DeviceJoinConference(conf, _))
        .foreach(context.parent.forward(_))

    case DeviceLeaveConference(conf, channel) =>
      getRemoteChannelName(channel)
        .map(DeviceLeaveConference(conf, _))
        .foreach(context.parent.forward(_))
  }

  private def getRemoteChannelName(channel: String): Option[String] =
    tracker.calls
      .get(channel)
      .map(call => AsteriskGraphTracker.pathsEndChannels(call.paths))
      .flatMap(_.headOption)
      .map(_.name)

  val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.UnknownDeviceTrackerType
  def notify(call: DeviceCall, redoLast: Boolean = false): Unit = {}
  def onPartyInformation(evt: PartyInformation): Unit = {}
}
