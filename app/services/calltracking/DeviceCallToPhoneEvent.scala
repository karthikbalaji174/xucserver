package services.calltracking

import xivo.events.{CallDirection, PhoneEvent, PhoneEventType, UserData}
import xivo.models.Line
import xivo.xucami.models.ChannelState

object DeviceCallToPhoneEvent {

  def channelStateToPhoneEventType(
      call: DeviceCall
  ): Option[PhoneEventType.PhoneEventType] =
    call.callState.flatMap(_ match {
      case ChannelState.RINGING => Some(PhoneEventType.EventRinging)

      case ChannelState.UP => Some(PhoneEventType.EventEstablished)

      case ChannelState.HOLD => Some(PhoneEventType.EventOnHold)

      case ChannelState.HUNGUP => Some(PhoneEventType.EventReleased)

      case ChannelState.ORIGINATING => Some(PhoneEventType.EventDialing)

      case _ => None
    })

  def apply(
      call: DeviceCall,
      line: Line,
      attachedFilter: UserData.filter = UserData.filterData
  ): Option[PhoneEvent] = {
    for {
      channel   <- call.channel
      eventType <- channelStateToPhoneEventType(call)
      number    <- line.number
    } yield PhoneEvent(
      eventType,
      number,
      call.remoteLineNumber.getOrElse(""),
      call.remoteLineName.getOrElse(""),
      channel.linkedChannelId,
      channel.id,
      queueName = call.queueName,
      userData = attachedFilter(call.variables),
      CallDirection(channel.direction)
    )
  }
}
