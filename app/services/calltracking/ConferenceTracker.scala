package services.calltracking

import akka.actor._
import com.google.inject.Inject
import com.google.inject.name.Named
import helpers.{FastLogging, JmxActorSingletonMonitor}
import org.asteriskjava.manager.action.MuteAudioAction
import org.asteriskjava.manager.event.{
  MeetMeJoinEvent,
  MeetMeLeaveEvent,
  MeetMeMuteEvent,
  MeetMeTalkingEvent
}
import org.asteriskjava.manager.response.ManagerResponse
import services.XucAmiBus.{AmiAction, AmiEvent, AmiResponse, AmiType}
import services.calltracking.BaseTracker._
import services.calltracking.ConferenceTracker._
import services.calltracking.DeviceMessage.DeviceInterface
import services.{ActorIds, XucAmiBus}
import xivo.models.ConferenceFactory
import xivo.xuc.TransferConfig
import xivo.xucami.models.Channel

import scala.concurrent.duration._
import scala.language.postfixOps

object ConferenceTracker {

  def serviceName: String = "ConferenceTracker"

  final case class GetConference(numConf: String)
  final case object RefreshConference
  final case class SubscribeToConference(numConf: String)
  final case class SubscribeToConferenceStatus(numConf: String)
  final case class UnsubscribeConference(numConf: String)
  final case class UnsubscribeConferenceStatus(numConf: String)
  final case class ConferenceSubscriptionAck(conference: ConferenceRoom)

  sealed trait ConferenceParticipantChange {
    def numConf: String
    def participant: ConferenceParticipant
  }

  final case class ParticipantJoinConference(
      numConf: String,
      participant: ConferenceParticipant
  ) extends ConferenceParticipantChange

  final case class ParticipantLeaveConference(
      numConf: String,
      participant: ConferenceParticipant
  ) extends ConferenceParticipantChange

  final case class ParticipantUpdated(
      numConf: String,
      participant: ConferenceParticipant
  ) extends ConferenceParticipantChange

  final case class ConferenceStatusChange(
      numConf: String,
      status: ConferenceStatus
  )

  final case class NoConferenceRoom(numConf: String)

  final case class RemoveConferenceRoom(numConf: String)

  final case class StopConference(numConf: String)

  sealed trait DeviceConference {
    def channel: String
    def conference: ConferenceRoom
    def isFor(interface: DeviceInterface): Boolean =
      channel.startsWith(interface)
  }

  final case class DeviceJoinConference(
      conference: ConferenceRoom,
      channel: String
  ) extends DeviceMessage
      with DeviceConference
  final case class DeviceLeaveConference(
      conference: ConferenceRoom,
      channel: String
  ) extends DeviceMessage
      with DeviceConference
}

class ConferenceTracker @Inject() (
    amiBus: XucAmiBus,
    conferenceFactory: ConferenceFactory,
    @Named(ActorIds.DevicesTrackerId) deviceTracker: ActorRef,
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef,
    configuration: TransferConfig
) extends Actor
    with FastLogging
    with JmxActorSingletonMonitor {

  type ConferenceMap         = Map[String, ConferenceRoom]
  type ChannelParticipantMap = Map[String, ConferenceParticipant]
  type SubscriberMap         = Map[String, Set[ActorRef]]

  val jmxConferenceCount =
    jmxBean.addLong("Conferences", 0L, Some("Number of Conferences"))
  val jmxParticipantCount =
    jmxBean.addLong("Participants", 0L, Some("Number of Participants"))

  def scheduler: Scheduler = context.system.scheduler

  def receive: Receive =
    processMessage(ConferenceRoomRepository.empty, Map.empty, Map.empty)

  def processMessage(
      conferences: ConferenceRoomRepository,
      conferenceSubscribers: SubscriberMap,
      statusSubscribers: SubscriberMap
  ): Receive = {

    case conf: ConferenceRoom =>
      log.debug("Loading Conference {}", conf)
      val newRepo = conferences.updateConference(conf)
      jmxConferenceCount.set(newRepo.conferences.size)
      context.become(
        processMessage(newRepo, conferenceSubscribers, statusSubscribers)
      )

    case RemoveConferenceRoom(numConf: String) =>
      log.debug("Remove Conference {}", numConf)
      val newRepo = conferences.removeConference(numConf)
      jmxConferenceCount.set(newRepo.conferences.size)
      context.become(
        processMessage(newRepo, conferenceSubscribers, statusSubscribers)
      )

    case GetConference(numConf) =>
      sender() ! conferences
        .getConference(numConf)
        .getOrElse(NoConferenceRoom(numConf))

    case AmiEvent(evt: MeetMeJoinEvent, mds) =>
      log.debug("AmiEvent({})", evt)
      val p       = ConferenceParticipant(evt)
      val newRepo = conferences.addParticipant(p, mds)
      val numConf = evt.getMeetMe
      jmxParticipantCount.set(newRepo.channelMap.size)

      for {
        oldConf <-
          conferences
            .getConference(numConf)
            .orElse(Some(ConferenceRoom.empty(numConf, mds)))
        newConf <- newRepo.getConference(numConf)
        newPart <- newConf.getParticipant(p.index)
      } {
        notifyChange(
          conferenceSubscribers,
          ParticipantJoinConference(newConf.number, newPart)
        )
        notifyStatusChange(statusSubscribers, oldConf, newConf)
        channelTracker ! WatchChannelStartingWith(newPart.channelName)
        deviceTracker ! DeviceJoinConference(newConf, newPart.channelName)
      }

      context.become(
        processMessage(newRepo, conferenceSubscribers, statusSubscribers)
      )

    case AmiEvent(evt: MeetMeLeaveEvent, mds) =>
      log.debug("AmiEvent({})", evt)
      val p       = ConferenceParticipant(evt)
      val newRepo = conferences.removeParticipant(p, mds)
      val numConf = evt.getMeetMe
      jmxParticipantCount.set(newRepo.channelMap.size)

      for {
        oldConf <-
          conferences
            .getConference(numConf)
            .orElse(Some(ConferenceRoom.empty(numConf, mds)))
        newConf <- newRepo.getConference(numConf)
        oldPart <- oldConf.getParticipant(p.index)
      } {
        notifyChange(
          conferenceSubscribers,
          ParticipantLeaveConference(newConf.number, oldPart)
        )
        notifyStatusChange(statusSubscribers, oldConf, newConf)
        channelTracker ! UnWatchChannelStartingWith(oldPart.channelName)
        deviceTracker ! DeviceLeaveConference(newConf, oldPart.channelName)
      }

      context.become(
        processMessage(newRepo, conferenceSubscribers, statusSubscribers)
      )

    case AmiEvent(evt: MeetMeTalkingEvent, _) =>
      updateParticipant(
        conferences,
        conferenceSubscribers,
        statusSubscribers,
        evt.getMeetMe,
        evt.getUser
      )(_.copy(isTalking = evt.getStatus))

    case AmiEvent(evt: MeetMeMuteEvent, _) =>
      updateParticipant(
        conferences,
        conferenceSubscribers,
        statusSubscribers,
        evt.getMeetMe,
        evt.getUser
      )(_.copy(isMuted = evt.getStatus))

    case AmiResponse(
          (
            evt: ManagerResponse,
            Some(AmiAction(action: MuteAudioAction, _, _, _))
          )
        ) =>
      if (Option(evt.getResponse).contains("Success")) {
        def setDeafenState() = {
          action.getState match {
            case MuteAudioAction.State.MUTE   => true
            case MuteAudioAction.State.UNMUTE => false
          }
        }
        for {
          p <- conferences.getParticipantByChannel(action.getChannel)
        } updateParticipant(
          conferences,
          conferenceSubscribers,
          statusSubscribers,
          p.numConf,
          p.index
        )(_.copy(isDeaf = setDeafenState()))
      }

    case c: Channel =>
      val roleOpt = c.variables
        .get(Channel.VarNames.ConferenceRoleVariable)
        .flatMap(ConferenceParticipantRole.fromString)

      for {
        role <- roleOpt
        p    <- conferences.getParticipantByChannel(c.name)
      } updateParticipant(
        conferences,
        conferenceSubscribers,
        statusSubscribers,
        p.numConf,
        p.index
      )(_.copy(role = role))

    case SubscribeToConference(numConf) =>
      val list           = conferenceSubscribers.getOrElse(numConf, Set.empty) + sender()
      val newSubscribers = conferenceSubscribers.updated(numConf, list)
      conferences
        .getConference(numConf)
        .foreach(conf => sender() ! ConferenceSubscriptionAck(conf))
      context.become(
        processMessage(conferences, newSubscribers, statusSubscribers)
      )

    case SubscribeToConferenceStatus(numConf) =>
      val list                 = statusSubscribers.getOrElse(numConf, Set.empty) + sender()
      val newStatusSubscribers = statusSubscribers + (numConf -> list)
      context.become(
        processMessage(conferences, conferenceSubscribers, newStatusSubscribers)
      )

    case UnsubscribeConference(numConf) =>
      conferenceSubscribers
        .get(numConf)
        .map(_.filterNot(_.equals(sender())))
        .map(conferenceSubscribers.updated(numConf, _))
        .foreach(s =>
          context.become(processMessage(conferences, s, statusSubscribers))
        )

    case UnsubscribeConferenceStatus(numConf) =>
      statusSubscribers
        .get(numConf)
        .map(_.filterNot(_.equals(sender())))
        .map(statusSubscribers.updated(numConf, _))
        .foreach(s =>
          context.become(processMessage(conferences, conferenceSubscribers, s))
        )

    case RefreshConference =>
      import context.dispatcher
      val existing     = conferences.numbers
      val dbConf       = conferenceFactory.all()
      val newConf      = dbConf.map(_.filterNot(c => existing.contains(c.confno)))
      val dbConfString = dbConf.map(_.map(_.confno))
      val toRemove     = dbConfString.map(l => existing.filterNot(l.contains))

      for {
        list    <- toRemove
        numConf <- list
      } {
        self ! RemoveConferenceRoom(numConf)
      }

      for {
        updateConf <- dbConf
        conf       <- updateConf
        listNew    <- newConf
        if listNew.contains(conf)
      } {
        self ! ConferenceRoom(conf)
      }

  }

  override def preStart(): Unit = {
    import context.dispatcher
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    amiBus.subscribe(self, AmiType.AmiEvent)
    amiBus.subscribe(self, AmiType.AmiResponse)
    for {
      list       <- conferenceFactory.all()
      meetmeconf <- list
    } {
      self ! ConferenceRoom(meetmeconf)
    }

    scheduler.scheduleWithFixedDelay(
      configuration.conferenceRefreshTimerSeconds second,
      configuration.conferenceRefreshTimerSeconds second,
      self,
      RefreshConference
    )
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  def notifyStatusChange(
      subscribers: SubscriberMap,
      oldConf: ConferenceRoom,
      newConf: ConferenceRoom
  ): Unit = {
    if (newConf.status != oldConf.status) {
      subscribers
        .get(newConf.number)
        .foreach(
          _.foreach(_ ! ConferenceStatusChange(newConf.number, newConf.status))
        )
    }
  }

  def notifyChange(
      subscribers: SubscriberMap,
      conferenceParticipantChange: ConferenceParticipantChange
  ): Unit = {
    log.debug("ConferenceChange {}", conferenceParticipantChange)
    subscribers
      .get(conferenceParticipantChange.numConf)
      .foreach(
        _.foreach(_ ! conferenceParticipantChange)
      )
  }

  def updateParticipant(
      conferences: ConferenceRoomRepository,
      conferenceSubscribers: SubscriberMap,
      statusSubscribers: SubscriberMap,
      numConf: String,
      index: Int
  )(fn: ConferenceParticipant => ConferenceParticipant): Unit = {
    val newRepo = conferences.updateParticipant(numConf, index)(fn)

    for {
      oldP <- conferences.getParticipant(numConf, index)
      newP <- newRepo.getParticipant(numConf, index) if oldP != newP
    } notifyChange(conferenceSubscribers, ParticipantUpdated(numConf, newP))

    context.become(
      processMessage(newRepo, conferenceSubscribers, statusSubscribers)
    )
  }
}
