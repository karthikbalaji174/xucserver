package services

import akka.actor.{Actor, ActorLogging}
import services.request.{PushLogToServer, UserBaseRequest}

class ClientLogger extends Actor with ActorLogging {
  override def receive: Receive = {
    case UserBaseRequest(ref, req: PushLogToServer, user) =>
      req.level match {
        case "error" => log.error(s"[${user.username}] ${req.log}")
        case "info"  => log.info(s"[${user.username}] ${req.log}")
        case "warn"  => log.warning(s"[${user.username}] ${req.log}")
        case "debug" => log.debug(s"[${user.username}] ${req.log}")
        case _ =>
          log.error(
            s"[${user.username}][Unknown log level: ${req.level}] ${req.log}"
          )
      }
  }
}
