package services

import akka.actor.ActorSystem
import akka.event.{ActorEventBus, SubchannelClassification}
import akka.util.Subclassification
import com.google.inject.{Inject, Singleton}
import models.{CallbackList, QueueCallList}
import services.XucEventBus.TopicType.TopicType
import services.XucEventBus.{Topic, XucEvent}
import services.agent.{AgentStatistic, AgentTransition}
import services.config.ConfigDispatcher.ObjectType
import services.config.ConfigDispatcher.ObjectType._
import services.request._
import services.video.model.VideoStatusEvent
import xivo.ami.AmiBusConnector.AgentListenNotification
import xivo.events._
import xivo.models.{
  Agent,
  QueueConfigUpdate,
  UserConfigUpdate,
  UserServicesUpdated
}
import xivo.websocket._

object XucEventBus {

  def configTopic(objectType: ObjectType) = Topic(TopicType.Config, objectType)

  def allAgentsEventsTopic = Topic(TopicType.Event, ObjectType.TypeAgent)
  def agentEventTopic(agentId: Agent.Id) =
    Topic(TopicType.Event, ObjectType.TypeAgent, Some(agentId))
  def lineEventTopic(lineNumber: String) =
    Topic(TopicType.Event, ObjectType.TypeLine, Some(lineNumber))
  def allPhoneEventsTopic = Topic(TopicType.Event, ObjectType.TypePhone)
  def phoneEventTopic(phoneNumber: String) =
    Topic(TopicType.Event, ObjectType.TypePhone, Some(phoneNumber))
  def currentCallsPhoneEvents(phoneNumber: String) =
    Topic(TopicType.Event, ObjectType.TypePhone, Some(phoneNumber))
  def statEventTopic(agentId: Agent.Id) =
    Topic(TopicType.Stat, ObjectType.TypeAgent, Some(agentId))
  def allAgentsStatsTopic = Topic(TopicType.Stat, ObjectType.TypeAgent)
  def agentTransitionTopic(agentId: Agent.Id) =
    Topic(TopicType.Transition, ObjectType.TypeAgent, Some(agentId))
  def agentLogTransitionTopic(agentId: Agent.Id) =
    Topic(TopicType.AgentLogTransition, ObjectType.TypeAgent, Some(agentId))
  def queueCallsTopic(queueId: Long) =
    Topic(TopicType.Event, ObjectType.TypeQueue, Some(queueId))
  def queueConfigUpdateTopic = Topic(TopicType.Config, ObjectType.TypeQueue)
  def callbackListsTopic()   = Topic(TopicType.Config, ObjectType.TypeCallback)
  def phoneHintEventTopic(phoneNumber: String) =
    Topic(TopicType.Event, ObjectType.TypePhoneHint, Some(phoneNumber))
  def allPhoneHintEventTopic = Topic(TopicType.Event, ObjectType.TypePhoneHint)
  def userEventTopic(userId: Long) =
    Topic(TopicType.Event, ObjectType.TypeUser, Some(userId))
  def videoStatusTopic(fromUser: String) =
    Topic(TopicType.Event, ObjectType.TypeVideoStatusEvent, Some(fromUser))
  def allVideoStatusTopic =
    Topic(TopicType.Event, ObjectType.TypeVideoStatusEvent)

  case class XucEvent(val topic: Topic, val message: Any)

  object TopicType extends Enumeration {
    type TopicType = Value
    val Event              = Value
    val Transition         = Value
    val Config             = Value
    val Stat               = Value
    val AgentLogTransition = Value
  }
  case class Topic(
      topicType: TopicType,
      objectType: ObjectType,
      objectId: Option[Any] = None
  )
}

@Singleton
class XucEventBus @Inject() (implicit val system: ActorSystem)
    extends ActorEventBus
    with SubchannelClassification
    with EventBusSupervision {
  type Event      = XucEvent
  type Classifier = Topic

  protected def subclassification =
    new Subclassification[Classifier] {
      def isEqual(x: Classifier, y: Classifier) = x == y
      def isSubclass(x: Classifier, y: Classifier) = {
        x.topicType == y.topicType && x.objectType == y.objectType && (x.objectId != None && y.objectId == None) || x == y
      }
    }

  override def classify(event: Event) = event.topic

  protected def publish(event: Event, subscriber: Subscriber) =
    subscriber ! event.message

  def publish(agentState: AgentState): Unit =
    publish(
      XucEvent(XucEventBus.agentEventTopic(agentState.agentId), agentState)
    )

  def publish(agentError: AgentError): Unit =
    publish(XucEvent(XucEventBus.allAgentsEventsTopic, agentError))

  def publish(agentListenStarted: AgentListenNotification): Unit =
    publish(
      XucEvent(
        XucEventBus.lineEventTopic(agentListenStarted.phoneNumber),
        agentListenStarted
      )
    )

  def publish(agentStatistic: AgentStatistic): Unit =
    publish(
      XucEvent(
        XucEventBus.statEventTopic(agentStatistic.agentId),
        agentStatistic
      )
    )

  def publish(agentTransition: AgentTransition, agentId: Agent.Id): Unit =
    publish(
      XucEvent(XucEventBus.agentTransitionTopic(agentId), agentTransition)
    )

  def publish(queueCalls: QueueCallList): Unit =
    publish(
      XucEvent(XucEventBus.queueCallsTopic(queueCalls.queueId), queueCalls)
    )

  def publish(queueConfig: QueueConfigUpdate): Unit =
    publish(XucEvent(XucEventBus.queueConfigUpdateTopic, queueConfig))

  def publish(agent: Agent): Unit =
    publish(XucEvent(XucEventBus.agentEventTopic(agent.id), agent))

  def publish(phoneEvent: PhoneEvent): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(phoneEvent.DN), phoneEvent))

  def publish(ccpe: CurrentCallsPhoneEvents): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(ccpe.DN), ccpe))

  def publish(evt: WsConferenceEvent): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(evt.phoneNumber), evt))

  def publish(evt: WsConferenceParticipantEvent): Unit =
    publish(XucEvent(XucEventBus.phoneEventTopic(evt.phoneNumber), evt))

  def publish(callbacks: List[CallbackList]): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbacks))

  def publish(callbackTaken: CallbackTaken): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackTaken))

  def publish(callbackReleased: CallbackReleased): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackReleased))

  def publish(callbackClotured: CallbackClotured): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackClotured))

  def publish(callbackUpdated: CallbackRequestUpdated): Unit =
    publish(XucEvent(XucEventBus.callbackListsTopic(), callbackUpdated))

  def publish(e: PhoneHintStatusEvent): Unit =
    publish(XucEvent(XucEventBus.phoneHintEventTopic(e.number), e))

  def publish(e: AgentLogTransition): Unit =
    publish(XucEvent(XucEventBus.agentLogTransitionTopic(e.agentId), e))

  def publish(e: UserServicesUpdated): Unit =
    publish(XucEvent(XucEventBus.userEventTopic(e.userId), e))

  def publish(e: UserConfigUpdate): Unit =
    publish(XucEvent(XucEventBus.userEventTopic(e.userId), e))

  def publish(e: VideoStatusEvent): Unit =
    publish(XucEvent(XucEventBus.videoStatusTopic(e.fromUser), e))
}
