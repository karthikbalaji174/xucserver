package services.video.model

import play.api.libs.json._
import services.request.XucRequest

object VideoEvents extends Enumeration {
  type Event = Value
  val Available = Value
  val Busy      = Value
}

object VideoInviteAck extends Enumeration {
  type Type = Value
  val ACK  = Value
  val NACK = Value

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(VideoInviteAck.withName(json.as[String]))
  }
}

object VideoInviteResponse extends Enumeration {
  type Type = Value
  val ACCEPT = Value("Accept")
  val REJECT = Value("Reject")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(VideoInviteResponse.withName(json.as[String]))
  }
}

case class VideoEvent(status: String) extends XucRequest
case object VideoEvent {
  def validate(json: JsValue)             = json.validate[VideoEvent]
  implicit val reads: Reads[VideoEvent]   = Json.reads[VideoEvent]
  implicit val writes: Writes[VideoEvent] = Json.writes[VideoEvent]
}

case class UserVideoEvent(fromUser: String, status: String) extends XucRequest
case object UserVideoEvent {
  def matchStatusToEvent(e: String): VideoEvents.Event = {
    e match {
      case "videoStart" => VideoEvents.Busy
      case "videoEnd"   => VideoEvents.Available
    }
  }
}

case class VideoStatusEvent(fromUser: String, status: VideoEvents.Event)
case object VideoStatusEvent {
  implicit val writes: Writes[VideoStatusEvent] = Json.writes[VideoStatusEvent]
}

sealed trait VideoInvitationCommand extends XucRequest

case class InviteToMeetingRoom(requestId: Int, token: String, username: String)
    extends VideoInvitationCommand

case object InviteToMeetingRoom {
  implicit val reads: Reads[InviteToMeetingRoom] =
    Json.reads[InviteToMeetingRoom]
  def validate(json: JsValue) = json.validate[InviteToMeetingRoom]
}

case class MeetingRoomInviteAck(requestId: Int, username: String)
    extends VideoInvitationCommand

case object MeetingRoomInviteAck {
  implicit val reads: Reads[MeetingRoomInviteAck] =
    Json.reads[MeetingRoomInviteAck]
  def validate(json: JsValue) = json.validate[MeetingRoomInviteAck]
}

case class MeetingRoomInviteAccept(requestId: Int, username: String)
    extends VideoInvitationCommand

case object MeetingRoomInviteAccept {
  implicit val reads: Reads[MeetingRoomInviteAccept] =
    Json.reads[MeetingRoomInviteAccept]
  def validate(json: JsValue) = json.validate[MeetingRoomInviteAccept]
}

case class MeetingRoomInviteReject(requestId: Int, username: String)
    extends VideoInvitationCommand

case object MeetingRoomInviteReject {
  implicit val reads: Reads[MeetingRoomInviteReject] =
    Json.reads[MeetingRoomInviteReject]
  def validate(json: JsValue) = json.validate[MeetingRoomInviteReject]
}

sealed trait VideoInvitationEvent extends XucRequest

case class MeetingRoomInvite(
    requestId: Int,
    token: String,
    destUsername: String,
    username: String,
    displayName: String
) extends VideoInvitationEvent

case object MeetingRoomInvite {
  implicit val reads: Reads[MeetingRoomInvite] =
    Json.reads[MeetingRoomInvite]

  implicit val writes = new Writes[MeetingRoomInvite] {
    def writes(mri: MeetingRoomInvite) =
      Json.obj(
        "requestId"   -> mri.requestId,
        "token"       -> mri.token,
        "username"    -> mri.username,
        "displayName" -> mri.displayName
      )
  }
}

case class MeetingRoomInviteAckReply(
    requestId: Int,
    destUsername: String,
    displayName: String,
    ackType: VideoInviteAck.Type
) extends VideoInvitationEvent

case object MeetingRoomInviteAckReply {

  implicit val reads: Reads[MeetingRoomInviteAckReply] =
    Json.reads[MeetingRoomInviteAckReply]

  implicit val writes = new Writes[MeetingRoomInviteAckReply] {
    def writes(ack: MeetingRoomInviteAckReply) =
      Json.obj(
        "requestId"   -> ack.requestId,
        "displayName" -> ack.displayName,
        "ackType"     -> ack.ackType
      )
  }
}

case class MeetingRoomInviteResponse(
    requestId: Int,
    destUsername: String,
    displayName: String,
    responseType: VideoInviteResponse.Type
) extends VideoInvitationEvent

case object MeetingRoomInviteResponse {
  implicit val reads: Reads[MeetingRoomInviteResponse] =
    Json.reads[MeetingRoomInviteResponse]

  implicit val writes = new Writes[MeetingRoomInviteResponse] {
    def writes(r: MeetingRoomInviteResponse) =
      Json.obj(
        "requestId"    -> r.requestId,
        "displayName"  -> r.displayName,
        "responseType" -> r.responseType.toString
      )
  }
}

case object VideoInvitationEvent {
  implicit val writes = new Writes[VideoInvitationEvent] {
    def writes(vie: VideoInvitationEvent) =
      vie match {
        case mi: MeetingRoomInvite           => Json.toJson(mi)
        case ack: MeetingRoomInviteAckReply  => Json.toJson(ack)
        case resp: MeetingRoomInviteResponse => Json.toJson(resp)
      }
  }
}
