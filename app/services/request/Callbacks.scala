package services.request

import java.util.UUID

import models.{
  CallbackRequest,
  CallbackStatus,
  FindCallbackRequest,
  FindCallbackResponse
}
import org.joda.time.DateTime
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.libs.json.JodaReads

class CallbackMgrRequest extends XucRequest

case object GetCallbackLists extends XucRequest {
  def validate(json: JsValue) = JsSuccess(GetCallbackLists)
}

case object GetCallbackPreferredPeriods extends CallbackMgrRequest {
  def validate(json: JsValue) = JsSuccess(GetCallbackPreferredPeriods)
}

case class FindCallbackRequestWithId(id: Long, request: FindCallbackRequest)
    extends CallbackMgrRequest

object FindCallbackRequestWithId {
  implicit val writesFind = new Writes[FindCallbackRequestWithId] {
    def writes(f: FindCallbackRequestWithId) =
      Json.obj(
        "id"      -> f.id,
        "request" -> f.request
      )
  }

  implicit val readsFind: Reads[FindCallbackRequestWithId] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "request").read[FindCallbackRequest]
  )(FindCallbackRequestWithId.apply _)

  def validate(json: JsValue) = json.validate[FindCallbackRequestWithId]
}

case class FindCallbackResponseWithId(id: Long, response: FindCallbackResponse)

object FindCallbackResponseWithId {
  implicit val writesRequestList = new Writes[FindCallbackResponseWithId] {
    def writes(o: FindCallbackResponseWithId) =
      Json.obj(
        "id"       -> o.id,
        "response" -> o.response
      )
  }
}

case class TakeCallback(uuid: String) extends XucRequest

case class TakeCallbackWithAgent(uuid: String, userName: String)
    extends XucRequest

object TakeCallback {
  implicit val reads = Json.reads[TakeCallback]

  def validate(json: JsValue) = json.validate[TakeCallback]
}

case class CallbackTaken(uuid: UUID, agentId: Long)

object CallbackTaken {
  implicit val writes = new Writes[CallbackTaken] {
    def writes(cb: CallbackTaken) =
      Json.obj(
        "uuid"    -> cb.uuid,
        "agentId" -> cb.agentId
      )
  }
}

object ReleaseCallback {
  implicit val reads = Json.reads[ReleaseCallback]

  def validate(json: JsValue) = json.validate[ReleaseCallback]
}

case class ReleaseCallback(uuid: String) extends CallbackMgrRequest

case class CallbackReleased(uuid: UUID)

object CallbackReleased {
  implicit val writes = new Writes[CallbackReleased] {
    def writes(cb: CallbackReleased) =
      Json.obj(
        "uuid" -> cb.uuid
      )
  }
}

case class ReleaseAllCallbacks(agentId: Long) extends CallbackMgrRequest

object ReleaseAllCallbacks {
  implicit val reads = Json.reads[ReleaseAllCallbacks]

  def validate(json: JsValue) = json.validate[ReleaseAllCallbacks]
}

case class StartCallback(uuid: String, number: String) extends XucRequest

object StartCallback {
  implicit val reads = Json.reads[StartCallback]

  def validate(json: JsValue) = json.validate[StartCallback]
}

case class StartCallbackWithUser(uuid: String, number: String, username: String)
    extends XucRequest

case class CallbackStarted(requestUiid: UUID, ticketUuid: UUID)

object CallbackStarted {
  implicit val writes = new Writes[CallbackStarted] {
    def writes(cb: CallbackStarted) =
      Json.obj(
        "requestUuid" -> cb.requestUiid,
        "ticketUuid"  -> cb.ticketUuid
      )
  }
}

case class UpdateCallbackTicket(
    uuid: String,
    status: Option[CallbackStatus.CallbackStatus],
    comment: Option[String],
    dueDate: Option[DateTime] = None,
    periodUuid: Option[String] = None
) extends CallbackMgrRequest

object UpdateCallbackTicket {
  val dateFormat                    = "yyyy-MM-dd"
  val readJodaTime: Reads[DateTime] = JodaReads.jodaDateReads(dateFormat)

  implicit val reads: Reads[UpdateCallbackTicket] = (
    (JsPath \ "uuid").read[String] and
      (JsPath \ "status")
        .readNullable[String]
        .map(_.map(s => CallbackStatus.withName(s.toLowerCase))) and
      (JsPath \ "comment").readNullable[String] and
      (JsPath \ "dueDate").readNullable[DateTime](readJodaTime) and
      (JsPath \ "periodUuid").readNullable[String]
  )(UpdateCallbackTicket.apply _)

  def validate(json: JsValue) = json.validate[UpdateCallbackTicket]
}

case class CallbackClotured(uuid: UUID)

object CallbackClotured {
  implicit val writes = new Writes[CallbackClotured] {
    def writes(cb: CallbackClotured) = Json.obj("uuid" -> cb.uuid)
  }
}

case class CallbackRequestUpdated(request: CallbackRequest)

object CallbackRequestUpdated {
  implicit val writes = new Writes[CallbackRequestUpdated] {
    def writes(o: CallbackRequestUpdated) = Json.obj("request" -> o.request)
  }
}
