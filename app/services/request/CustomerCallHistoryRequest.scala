package services.request

import play.api.libs.json._
import xivo.models.{
  FindCustomerCallHistoryRequest,
  FindCustomerCallHistoryResponse
}

case class CustomerCallHistoryRequestWithId(
    id: Long,
    request: FindCustomerCallHistoryRequest
) extends XucRequest

object CustomerCallHistoryRequestWithId {
  implicit val writesFind: Writes[CustomerCallHistoryRequestWithId] =
    Json.writes[CustomerCallHistoryRequestWithId]
  implicit val readsFind: Reads[CustomerCallHistoryRequestWithId] =
    Json.reads[CustomerCallHistoryRequestWithId]

  def validate(json: JsValue) = json.validate[CustomerCallHistoryRequestWithId]
}

case class CustomerCallHistoryResponseWithId(
    id: Long,
    response: FindCustomerCallHistoryResponse
)

object CustomerCallHistoryResponseWithId {
  implicit val writesRequestList: Writes[CustomerCallHistoryResponseWithId] =
    Json.writes[CustomerCallHistoryResponseWithId]
}
