package services.request

import play.api.libs.json.{JsSuccess, JsValue, Json}

class DirectoryRequest extends XucRequest

case class DirectoryLookUp(term: String) extends DirectoryRequest
object DirectoryLookUp {
  implicit val DirectoryLookUpRead = Json.reads[DirectoryLookUp]
  def validate(json: JsValue)      = json.validate[DirectoryLookUp]
}

case object GetFavorites extends DirectoryRequest {
  def validate(json: JsValue) = JsSuccess(GetFavorites)
}

case class AddFavorite(contactId: String, source: String)
    extends DirectoryRequest
object AddFavorite {
  implicit val SetFavoriteRead = Json.reads[AddFavorite]
  def validate(json: JsValue)  = json.validate[AddFavorite]
}

case class RemoveFavorite(contactId: String, source: String)
    extends DirectoryRequest
object RemoveFavorite {
  implicit val RemoveFavoriteRead = Json.reads[RemoveFavorite]
  def validate(json: JsValue)     = json.validate[RemoveFavorite]
}
