package services.request

import play.api.libs.json.{JsValue, Json, Reads}

class StatisticsRequest extends XucRequest

case class QueueStatisticsReq(queueId: Long, window: Int, xqos: Int)
    extends StatisticsRequest

object QueueStatisticsReq {
  def validate(json: JsValue) = json.validate[QueueStatisticsReq]
  implicit val QueueStatisticsReqRead: Reads[QueueStatisticsReq] =
    Json.reads[QueueStatisticsReq]
}
