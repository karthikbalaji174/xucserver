package services.request

import play.api.libs.json._

class SubscribeRequest extends XucRequest

case object SubscribeToAgentEvents extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(SubscribeToAgentEvents)
}

case object SubscribeToQueueStats extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(SubscribeToQueueStats)
}

case object SubscribeToAgentStats extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(SubscribeToAgentStats)
}

case class SubscribeToPhoneHints(phoneNumbers: List[String])
    extends SubscribeRequest

object SubscribeToPhoneHints {
  def validate(json: JsValue) = json.validate[SubscribeToPhoneHints]
  implicit val read: Reads[SubscribeToPhoneHints] = (JsPath \ "phoneNumbers")
    .read[List[String]]
    .map(SubscribeToPhoneHints.apply)
}

case object UnsubscribeFromAllPhoneHints extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(UnsubscribeFromAllPhoneHints)
}

case class SubscribeToVideoStatus(usernames: List[String])
    extends SubscribeRequest

object SubscribeToVideoStatus {
  def validate(json: JsValue) = json.validate[SubscribeToVideoStatus]
  implicit val read: Reads[SubscribeToVideoStatus] = (JsPath \ "usernames")
    .read[List[String]]
    .map(SubscribeToVideoStatus.apply)
}

case object UnsubscribeFromAllVideoStatus extends SubscribeRequest {
  def validate(json: JsValue) = JsSuccess(UnsubscribeFromAllVideoStatus)
}

case class SubscribeToQueueCalls(queueId: Long) extends SubscribeRequest

object SubscribeToQueueCalls {
  def validate(json: JsValue) = json.validate[SubscribeToQueueCalls]
  implicit val read: Reads[SubscribeToQueueCalls] =
    (JsPath \ "queueId").read[Long].map(SubscribeToQueueCalls.apply)
}

case class UnSubscribeToQueueCalls(queueId: Long) extends SubscribeRequest

object UnSubscribeToQueueCalls {
  def validate(json: JsValue) = json.validate[UnSubscribeToQueueCalls]
  implicit val read: Reads[UnSubscribeToQueueCalls] =
    (JsPath \ "queueId").read[Long].map(UnSubscribeToQueueCalls.apply)
}
