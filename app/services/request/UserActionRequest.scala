package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsValue, Json, Reads}

abstract class UserActionRequest extends XucRequest
abstract class ForwardRequest(destination: String, state: Boolean)
    extends UserActionRequest

case class NaForward(destination: String, state: Boolean)
    extends ForwardRequest(destination, state)
case class UncForward(destination: String, state: Boolean)
    extends ForwardRequest(destination, state)
case class BusyForward(destination: String, state: Boolean)
    extends ForwardRequest(destination, state)

object NaForward {
  def validate(json: JsValue)                  = json.validate[NaForward]
  implicit val NaForwardRead: Reads[NaForward] = Json.reads[NaForward]
}

object UncForward {
  def validate(json: JsValue)                    = json.validate[UncForward]
  implicit val UncForwardRead: Reads[UncForward] = Json.reads[UncForward]
}

object BusyForward {
  def validate(json: JsValue)                      = json.validate[BusyForward]
  implicit val BusyForwardRead: Reads[BusyForward] = Json.reads[BusyForward]
}

case class UserStatusUpdateReq(userId: Option[Long], status: String)
    extends UserActionRequest

object UserStatusUpdateReq {
  def validate(json: JsValue) = json.validate[UserStatusUpdateReq]
  implicit val UserStatusUpdateReqRead: Reads[UserStatusUpdateReq] =
    Json.reads[UserStatusUpdateReq]
}

case class DndReq(state: Boolean, domain: String = "default")
    extends UserActionRequest
object DndReq {
  def validate(json: JsValue) = json.validate[DndReq]
  implicit val DndReqRead: Reads[DndReq] = ((JsPath \ "state").read[Boolean] and
    (JsPath \ "domain")
      .readNullable[String]
      .map(_.getOrElse("default")))(DndReq.apply _)
}
