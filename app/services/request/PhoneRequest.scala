package services.request

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import xivo.phonedevices.WebRTCDeviceBearer
import xivo.xucami.models.QueueCall
import xivo.websocket.{WsConferenceEvent, WsConferenceParticipantRole}

abstract class PhoneRequest extends XucRequest

object PhoneRequest {
  case class Dial(
      destination: String,
      variables: Map[String, String] = Map(),
      domain: String = "default"
  ) extends PhoneRequest
  object Dial {
    implicit val reads = ((JsPath \ "destination").read[String] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())) and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default")))(Dial.apply _)
    def validate(json: JsValue) = json.validate[Dial]
  }

  case class DialFromMobile(
      destination: String,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object DialFromMobile {
    implicit val reads = ((JsPath \ "destination").read[String] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())))(DialFromMobile.apply _)
    def validate(json: JsValue) = json.validate[DialFromMobile]
  }

  case class DialByUsername(
      username: String,
      variables: Map[String, String] = Map(),
      domain: String
  ) extends PhoneRequest
  object DialByUsername {
    implicit val reads = ((JsPath \ "username").read[String] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())) and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default")))(DialByUsername.apply _)
    def validate(json: JsValue) = json.validate[DialByUsername]
  }

  case class ListenCallbackMessage(
      voiceMsgRef: String,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object ListenCallbackMessage {
    implicit val reads = ((JsPath \ "voiceMessageRef").read[String] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())))(ListenCallbackMessage.apply _)
    def validate(json: JsValue) = json.validate[ListenCallbackMessage]
  }

  case class Answer(uniqueId: Option[String]) extends PhoneRequest
  object Answer {
    implicit val reads          = Json.reads[Answer]
    def validate(json: JsValue) = json.validate[Answer]
  }

  case class Hangup(uniqueId: Option[String]) extends PhoneRequest
  object Hangup {
    implicit val reads          = Json.reads[Hangup]
    def validate(json: JsValue) = json.validate[Hangup]
  }

  case class ToggleMicrophone(uniqueId: Option[String]) extends PhoneRequest
  object ToggleMicrophone {
    implicit val reads          = Json.reads[ToggleMicrophone]
    def validate(json: JsValue) = json.validate[ToggleMicrophone]
  }

  case class AttendedTransfer(
      destination: String,
      device: Option[WebRTCDeviceBearer]
  ) extends PhoneRequest
  object AttendedTransfer {
    implicit val reads          = Json.reads[AttendedTransfer]
    def validate(json: JsValue) = json.validate[AttendedTransfer]
  }

  case object CompleteTransfer extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(CompleteTransfer)
  }

  case object CancelTransfer extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(CancelTransfer)
  }

  case class DirectTransfer(destination: String) extends PhoneRequest
  object DirectTransfer {
    implicit val reads          = Json.reads[DirectTransfer]
    def validate(json: JsValue) = json.validate[DirectTransfer]
  }

  case object Conference extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(Conference)
  }

  case class ConferenceInvite(
      numConf: String,
      exten: String,
      role: WsConferenceParticipantRole,
      earlyJoin: Boolean,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object ConferenceInvite {
    import WsConferenceEvent.wsConferenceParticipantRoleReads;
    implicit val reads: Reads[ConferenceInvite] =
      ((JsPath \ "numConf").read[String] and
        (JsPath \ "exten").read[String] and
        (JsPath \ "role").read[WsConferenceParticipantRole] and
        (JsPath \ "earlyJoin").read[Boolean] and
        (JsPath \ "variables")
          .readNullable[Map[String, String]]
          .map(_.getOrElse(Map())))(ConferenceInvite.apply _)
    def validate(json: JsValue) = json.validate[ConferenceInvite]
  }

  case class ConferenceMute(numConf: String, index: Int) extends PhoneRequest
  object ConferenceMute {
    implicit val reads          = Json.reads[ConferenceMute]
    def validate(json: JsValue) = json.validate[ConferenceMute]
  }

  case class ConferenceUnmute(numConf: String, index: Int) extends PhoneRequest
  object ConferenceUnmute {
    implicit val reads          = Json.reads[ConferenceUnmute]
    def validate(json: JsValue) = json.validate[ConferenceUnmute]
  }

  case class ConferenceMuteAll(numConf: String) extends PhoneRequest
  object ConferenceMuteAll {
    implicit val reads          = Json.reads[ConferenceMuteAll]
    def validate(json: JsValue) = json.validate[ConferenceMuteAll]
  }

  case class ConferenceUnmuteAll(numConf: String) extends PhoneRequest
  object ConferenceUnmuteAll {
    implicit val reads          = Json.reads[ConferenceUnmuteAll]
    def validate(json: JsValue) = json.validate[ConferenceUnmuteAll]
  }

  case class ConferenceMuteMe(numConf: String) extends PhoneRequest
  object ConferenceMuteMe {
    implicit val reads          = Json.reads[ConferenceMuteMe]
    def validate(json: JsValue) = json.validate[ConferenceMuteMe]
  }

  case class ConferenceUnmuteMe(numConf: String) extends PhoneRequest
  object ConferenceUnmuteMe {
    implicit val reads          = Json.reads[ConferenceUnmuteMe]
    def validate(json: JsValue) = json.validate[ConferenceUnmuteMe]
  }

  case class ConferenceKick(numConf: String, index: Int) extends PhoneRequest
  object ConferenceKick {
    implicit val reads          = Json.reads[ConferenceKick]
    def validate(json: JsValue) = json.validate[ConferenceKick]
  }

  case class ConferenceClose(numConf: String) extends PhoneRequest
  object ConferenceClose {
    implicit val reads          = Json.reads[ConferenceClose]
    def validate(json: JsValue) = json.validate[ConferenceClose]
  }

  case class ConferenceDeafen(numConf: String, index: Int) extends PhoneRequest
  object ConferenceDeafen {
    implicit val reads          = Json.reads[ConferenceDeafen]
    def validate(json: JsValue) = json.validate[ConferenceDeafen]
  }

  case class ConferenceUndeafen(numConf: String, index: Int)
      extends PhoneRequest
  object ConferenceUndeafen {
    implicit val reads          = Json.reads[ConferenceUndeafen]
    def validate(json: JsValue) = json.validate[ConferenceUndeafen]
  }

  case class ConferenceReset(numConf: String) extends PhoneRequest
  object ConferenceReset {
    implicit val reads          = Json.reads[ConferenceReset]
    def validate(json: JsValue) = json.validate[ConferenceReset]
  }

  case class Hold(uniqueId: Option[String]) extends PhoneRequest
  object Hold {
    implicit val reads          = Json.reads[Hold]
    def validate(json: JsValue) = json.validate[Hold]
  }

  case class SetData(variables: Map[String, String]) extends PhoneRequest
  object SetData {
    implicit val reads          = Json.reads[SetData]
    def validate(json: JsValue) = json.validate[SetData]
  }

  case object GetCurrentCallsPhoneEvents extends PhoneRequest {
    def validate(json: JsValue) = JsSuccess(GetCurrentCallsPhoneEvents)
  }

  case class SendDtmfRequest(key: Char) extends PhoneRequest
  object SendDtmfRequest {
    implicit val sendDtmfRead = (JsPath \ "key")
      .read[String](minLength[String](1) keepAnd maxLength[String](1))
      .map(s => SendDtmfRequest(s.charAt(0)))
    def validate(json: JsValue) = json.validate[SendDtmfRequest]
  }

  case class RetrieveQueueCall(
      queueCall: QueueCall,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object RetrieveQueueCall {
    implicit val reads = ((JsPath \ "queueCall").read[QueueCall] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())))(RetrieveQueueCall.apply _)
    def validate(json: JsValue) = json.validate[RetrieveQueueCall]
  }
}
