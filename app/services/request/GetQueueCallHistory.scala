package services.request

import play.api.libs.json.{JsValue, Reads}
import play.api.libs.json._

case class GetQueueCallHistory(queue: String, size: Int) extends XucRequest

object GetQueueCallHistory {
  def validate(json: JsValue) = json.validate[GetQueueCallHistory]

  implicit val reads: Reads[GetQueueCallHistory] =
    Json.reads[GetQueueCallHistory]
}
