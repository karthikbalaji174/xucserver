package services.request

import play.api.libs.json.{JsPath, JsResult, JsValue, Reads}

case class GetUserCallHistory(param: HistorySize) extends XucRequest
case class UserCallHistoryRequest(param: HistorySize, ctiUserName: String)
    extends XucRequest

object GetUserCallHistory {
  def validate(json: JsValue): JsResult[GetUserCallHistory] = {
    json.validate[GetUserCallHistory]
  }

  implicit val reads: Reads[GetUserCallHistory] = {
    (JsPath \ "size")
      .read[Int]
      .map(size => GetUserCallHistory(HistorySize(size)))
  }
}
