package services.request

import play.api.libs.json.{JsValue, Json, Reads}

case class PushLogToServer(level: String, log: String) extends XucRequest

object PushLogToServer {
  def validate(json: JsValue) = json.validate[PushLogToServer]
  implicit val PushLogToServerRead: Reads[PushLogToServer] =
    Json.reads[PushLogToServer]
}
