package services.request

import play.api.libs.json.{JsResult, JsValue, Json, Reads}

case class UnregisterMobileApp(username: Option[String]) extends XucRequest

object UnregisterMobileApp {
  implicit val reads: Reads[UnregisterMobileApp] =
    Json.reads[UnregisterMobileApp]

  def validate(json: JsValue): JsResult[UnregisterMobileApp] =
    json.validate[UnregisterMobileApp]
}
