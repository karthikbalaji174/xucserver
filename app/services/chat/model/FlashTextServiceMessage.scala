package services.chat.model

import java.time.OffsetDateTime

import models.XivoUser

sealed trait FlashTextRequest

trait Sequence {
  val sequence: Long
}
case class SendDirectMessage(
    from: String,
    to: String,
    message: String,
    sequence: Long
) extends FlashTextRequest
    with Sequence
case class GetDirectMessageHistory(users: (String, String), sequence: Long)
    extends FlashTextRequest
    with Sequence
case class MarkDirectchannelAsRead(users: (String, String), sequence: Long)
    extends FlashTextRequest
    with Sequence
case class ConnectFlashTextUser(xivoUser: XivoUser)    extends FlashTextRequest
case class DisconnectFlashTextUser(xivoUser: XivoUser) extends FlashTextRequest

sealed trait FlashTextEvent
case class RequestAck(
    sequence: Long,
    offline: Boolean = false,
    date: OffsetDateTime
)                                      extends FlashTextEvent
case class RequestNack(sequence: Long) extends FlashTextEvent
case class Message(
    from: ChatUser,
    to: ChatUser,
    message: String,
    date: OffsetDateTime,
    sequence: Long
) extends FlashTextEvent
    with Sequence
case class MessageHistory(
    users: (ChatUser, ChatUser),
    messages: List[Message],
    sequence: Long
) extends FlashTextEvent
    with Sequence
case class MessageUnreadNotification(messages: List[Message], sequence: Long)
    extends FlashTextEvent
    with Sequence
case class MessageMarkAsRead(username: String, status: String, sequence: Long)
    extends FlashTextEvent
    with Sequence

case class MessageHistoryError(msg: String) extends Exception {
  override def getMessage: String = msg
}
case class MessagePersistError(msg: String) extends Exception {
  override def getMessage: String = msg
}
