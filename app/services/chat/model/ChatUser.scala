package services.chat.model

import play.api.libs.json.Json
import services.VideoChat.VideoChatUser

case class ChatUser(
    username: String,
    phoneNumber: Option[String],
    displayName: Option[String],
    guid: Option[String]
) extends VideoChatUser

object ChatUser {
  implicit val chatUserWriter = Json.writes[ChatUser]
}
