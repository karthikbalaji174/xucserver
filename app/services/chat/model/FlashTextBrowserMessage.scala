package services.chat.model

import play.api.libs.functional.syntax._
import play.api.libs.json._
import services.request.XucRequest

sealed trait BrowserRequest {
  def enrichWithSender(from: String): FlashTextRequest
}

case class BrowserSendDirectMessage(to: String, message: String, sequence: Long)
    extends BrowserRequest {
  def enrichWithSender(from: String) =
    SendDirectMessage(from, to, message, sequence)
}

case class BrowserGetDirectMessageHistory(to: String, sequence: Long)
    extends BrowserRequest {
  def enrichWithSender(from: String) =
    GetDirectMessageHistory((from, to), sequence)
}

case class BrowserMarkAsReadDirectChannel(to: String, sequence: Long)
    extends BrowserRequest {
  def enrichWithSender(from: String) =
    MarkDirectchannelAsRead((from, to), sequence)
}

case class BrowserRequestEnvelope(request: BrowserRequest) extends XucRequest
case class BrowserEventEnvelope(event: FlashTextEvent)

object BrowserRequestEnvelope {
  implicit val read: Reads[BrowserRequestEnvelope] = (
    (JsPath \ "request")
      .read[String]
      .flatMap {
        case "FlashTextDirectMessage" =>
          BrowserSendDirectMessage.read.map[BrowserRequest](identity)
        case "FlashTextDirectMessageHistory" =>
          BrowserGetDirectMessageHistory.read.map[BrowserRequest](identity)
        case "FlashTextMarkAsRead" =>
          BrowserMarkAsReadDirectChannel.read.map[BrowserRequest](identity)
        case request =>
          Reads[BrowserRequest] { js =>
            JsError(s"Unknown FlashTextBrowserRequest $request !")
          }
      }
      .map(BrowserRequestEnvelope(_))
  )

  def validate(json: JsValue) = json.validate[BrowserRequestEnvelope]
}

object BrowserSendDirectMessage {
  implicit val read: Reads[BrowserSendDirectMessage] = (
    (__ \ "to" \ "username").read[String] and
      (__ \ "message").read[String] and
      (__ \ "sequence").read[Long]
  )(BrowserSendDirectMessage.apply _)
}

object BrowserGetDirectMessageHistory {
  implicit val read: Reads[BrowserGetDirectMessageHistory] = (
    (__ \ "to" \ "username").read[String] and
      (__ \ "sequence").read[Long]
  )(BrowserGetDirectMessageHistory.apply _)
}

object BrowserMarkAsReadDirectChannel {
  implicit val read: Reads[BrowserMarkAsReadDirectChannel] = (
    (__ \ "to" \ "username").read[String] and
      (__ \ "sequence").read[Long]
  )(BrowserMarkAsReadDirectChannel.apply _)
}

object BrowserEventEnvelope {

  implicit val writeBEEnvelope = new Writes[BrowserEventEnvelope] {
    implicit val ackWriter = Json.writes[RequestAck]

    implicit val nackWriter = Json.writes[RequestNack]

    implicit val messageWriter = new OWrites[Message] {
      def writes(m: Message) =
        Json.obj(
          "from"     -> m.from,
          "to"       -> m.to,
          "sequence" -> m.sequence,
          "message"  -> m.message,
          "date"     -> m.date
        )
    }

    implicit val historyWriter             = Json.writes[MessageHistory]
    implicit val historyUnreadWriter       = Json.writes[MessageUnreadNotification]
    implicit val messageMarkedAsReadWriter = Json.writes[MessageMarkAsRead]

    def writes(o: BrowserEventEnvelope) = {
      o.event match {
        case r: RequestAck if r.offline =>
          Json.obj(
            "event" -> JsString("FlashTextSendMessageAckOffline")
          ) ++ Json.toJsObject(r)
        case r: RequestAck =>
          Json.obj("event" -> JsString("FlashTextSendMessageAck")) ++ Json
            .toJsObject(r)
        case r: RequestNack =>
          Json.obj("event" -> JsString("FlashTextSendMessageNack")) ++ Json
            .toJsObject(r)
        case m: Message =>
          Json.obj("event" -> JsString("FlashTextUserMessage")) ++ Json
            .toJsObject(m)
        case m: MessageHistory =>
          Json.obj("event" -> JsString("FlashTextUserMessageHistory")) ++ Json
            .toJsObject(m)
        case m: MessageUnreadNotification =>
          Json.obj("event" -> JsString("FlashTextUnreadMessages")) ++ Json
            .toJsObject(m)
        case m: MessageMarkAsRead =>
          Json.obj("event" -> JsString("FlashTextMarkedAsRead")) ++ Json
            .toJsObject(m)
      }
    }
  }
}
