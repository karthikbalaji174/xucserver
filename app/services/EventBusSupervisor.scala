package services

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Terminated}
import akka.event.ActorEventBus
import services.EventBusSupervisor.Monitor

trait EventBusSupervision extends ActorEventBus {

  implicit val system: ActorSystem

  lazy val supervisor = system.actorOf(EventBusSupervisor.props(this))

  abstract override def subscribe(
      subscriber: Subscriber,
      to: Classifier
  ): Boolean = {
    if (super.subscribe(subscriber, to)) {
      supervisor ! Monitor(subscriber)
      true
    } else false
  }

}

private[services] class EventBusSupervisor(bus: EventBusSupervision)
    extends Actor {

  def receive = {
    case Monitor(ref) =>
      context.watch(ref)

    case Terminated(ref) =>
      bus.unsubscribe(ref)
  }

}

private[services] object EventBusSupervisor {
  case class Monitor(ref: ActorRef)

  def props(bus: EventBusSupervision): Props =
    Props(new EventBusSupervisor(bus))
}
