package services.agent

import akka.actor.{ActorRef, Props}
import services.config.ConfigDispatcher._
import services.request.SetAgentQueue
import xivo.models.Agent

object AgentInGroupAdder {
  def props(
      groupId: Long,
      fromQueueId: Long,
      fromPenalty: Int,
      configDispatcher: ActorRef
  ) =
    Props(
      new AgentInGroupAdder(groupId, fromQueueId, fromPenalty, configDispatcher)
    )
}

class AgentInGroupAdder(
    groupId: Long,
    fromQueueId: Long,
    fromPenalty: Int,
    configDispatcher: ActorRef
) extends AgentInGroupAction(
      groupId,
      fromQueueId,
      fromPenalty,
      configDispatcher
    ) {
  override def actionOnAgent(
      agent: Agent,
      toQueueId: Option[Long],
      toPenalty: Option[Int]
  ) = {
    configDispatcher ! ConfigChangeRequest(
      self,
      SetAgentQueue(agent.id, toQueueId.get, toPenalty.get)
    )
  }
}
