package services

import akka.actor.ActorSystem
import akka.event.ActorEventBus
import akka.event.SubchannelClassification
import akka.util.Subclassification
import com.google.inject.{Inject, Singleton}
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectDefinition
import akka.actor.ActorRef
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._

object XucStatsEventBus {
  trait StatEvent {
    val xivoObject: ObjectDefinition
    val stat: List[Stat]
  }
  case class Stat(statRef: String, value: Double)
  object Stat {
    implicit val statWrites = ((__ \ "statName").write[String] and
      (__ \ "value").write[Double])(unlift(Stat.unapply))
  }
  case class AggregatedStatEvent(
      override val xivoObject: ObjectDefinition,
      override val stat: List[Stat]
  ) extends StatEvent
  case class StatUpdate(
      override val xivoObject: ObjectDefinition,
      override val stat: List[Stat]
  ) extends StatEvent

  object AggregatedStatEvent {
    implicit val AggregatedStatEventWrites = ((__ \ "queueId").write[Long] and
      (__ \ "counters").write[List[Stat]])(unlift(AggregatedStatEvent.extract))

    def extract(ags: AggregatedStatEvent): Option[(Long, List[Stat])] =
      Some((ags.xivoObject.objectRef.get.toLong, ags.stat))
  }
}

@Singleton
class XucStatsEventBus @Inject() (implicit val system: ActorSystem)
    extends ActorEventBus
    with SubchannelClassification
    with EventBusSupervision {
  type Event      = XucStatsEventBus.StatEvent
  type Classifier = ObjectDefinition
  val logger = Logger(getClass.getName)

  private var statCache: Option[ActorRef] = None

  def setStatCache(statCache: ActorRef): Unit = this.statCache = Some(statCache)

  protected def subclassification =
    new Subclassification[Classifier] {
      def isEqual(x: Classifier, y: Classifier) = x == y
      def isSubclass(x: Classifier, y: Classifier) =
        x.objectType == y.objectType && (!y.hasObjectRef)
    }

  override def classify(event: Event) = event.xivoObject

  protected def publish(event: Event, subscriber: Subscriber) =
    subscriber ! event

  override def subscribe(subscriber: Subscriber, to: Classifier) = {
    logger.debug(s"subscribe : $subscriber to : $to")
    statCache.foreach(_ ! RequestStat(subscriber, to))
    super.subscribe(subscriber, to)
  }
}
