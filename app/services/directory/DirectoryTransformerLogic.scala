package services.directory

import com.google.inject.Inject
import models._
import org.slf4j.{Logger, LoggerFactory}
import org.xivo.cti.model.PhoneHintStatus
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.xuc.XucConfig

import scala.collection.mutable
import scala.concurrent.Await
import scala.util.{Success, Try}

class DirectoryTransformerLogic @Inject() (
    repo: ConfigRepository,
    configRequester: ConfigServerRequester,
    config: XucConfig
) {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  private def enrich(
      entry: ResultItem,
      requestedUserId: Long
  ): RichEntry = {
    val lineNumber = entry.item.number
    val phoneStatus = lineNumber.fold(PhoneHintStatus.UNEXISTING)(lineNumber =>
      repo.richPhones.getOrElse(lineNumber, PhoneHintStatus.UNEXISTING)
    )
    val item = entry.item
    val username =
      entry.relations.userId.flatMap(repo.getCtiUser(_).flatMap(_.username))

    RichEntry(
      phoneStatus,
      mutable.Buffer(
        item.name,
        item.number.getOrElse(""),
        item.mobile.getOrElse(""),
        item.externalNumber.getOrElse(""),
        item.favorite,
        item.email.getOrElse("")
      ),
      username.flatMap(u => repo.getUserVideoStatus(u)),
      entry.relations.sourceEntryId,
      Some(entry.source),
      Some(item.favorite),
      username,
      getSharingLink(entry),
      item.personal
    )
  }

  def getPhoneNumbers(result: List[ResultItem]): List[String] = {
    result
      .flatMap(_.relations.endpointId)
      .flatMap(x => repo.linePhoneNbs.get(x))
  }

  def getUsernames(results: List[RichEntry]): List[String] = {
    results.flatMap(r => r.username)
  }

  def enrichDirResult(
      directoryResult: DirSearchResult,
      requesterUserId: Long
  ): RichDirectoryResult = {
    val result = new RichDirectoryResult(
      RichDirectoryEntries.defaultHeaders
    )
    val entries = directoryResult.results
    entries.foreach(entry => { result.add(enrich(entry, requesterUserId)) })
    result
  }

  def enrichFavorites(
      directoryResult: DirSearchResult,
      requesterUserId: Long
  ): RichFavorites = {
    val result = new RichFavorites(
      RichDirectoryEntries.defaultHeaders
    )
    val entries = directoryResult.results
    entries.foreach(entry => { result.add(enrich(entry, requesterUserId)) })
    result
  }

  def getMeetingRoomAlias(
      roomId: String
  ): Option[String] =
    Try(
      Await.result(
        configRequester.getMeetingRoomAlias(roomId),
        config.defaultWSTimeout
      )
    ) match {
      case Success(room) if room.alias.isDefined =>
        Some(s"/meet?id=${room.alias.get}")
      case _ => None
    }

  def getSharingLink(
      result: ResultItem
  ): Option[String] = {
    if (result.source == "xivo_meetingroom") {
      result.relations.sourceEntryId.flatMap(meetingRoomId =>
        getMeetingRoomAlias(
          meetingRoomId
        )
      )
    } else {
      None
    }
  }
}
