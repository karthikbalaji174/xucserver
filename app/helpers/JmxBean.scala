package helpers

import akka.actor.Actor
import java.lang.management.ManagementFactory
import java.util.concurrent.atomic.{AtomicLong, AtomicReference}
import javax.management._
import scala.util.{Failure, Try}
import scala.jdk.CollectionConverters._

case class RegisteredMBean(name: String, instance: ObjectInstance)
case class UnregisteredMBean(name: String)

sealed trait JmxMetric {
  def metricType: String
  def get(): AnyRef
}

class JmxLongMetric(initialValue: Long) extends JmxMetric {
  val metricType                = "java.lang.Long"
  val value                     = new AtomicLong(initialValue)
  def inc(): Unit               = value.incrementAndGet()
  def dec(): Unit               = value.decrementAndGet()
  def get(): java.lang.Long     = java.lang.Long.valueOf(value.get())
  def set(newValue: Long): Unit = value.set(newValue)
  def add(delta: Long): Long    = value.addAndGet(delta)
}

object JmxLongMetric {
  def apply(initialValue: Long): JmxLongMetric = new JmxLongMetric(initialValue)

  implicit class JmxLongMetricOps(val m: Try[JmxLongMetric]) extends AnyVal {
    def inc(): Unit               = m.foreach(_.inc())
    def dec(): Unit               = m.foreach(_.dec())
    def set(newValue: Long): Unit = m.foreach(_.set(newValue))
    def add(delta: Long): Unit    = m.foreach(_.add(delta))
  }
}

class JmxStringMetric(initialValue: String) extends JmxMetric {
  val metricType                  = "java.lang.String"
  val value                       = new AtomicReference[String](initialValue)
  def get(): String               = value.get()
  def set(newValue: String): Unit = value.set(newValue)
}

object JmxStringMetric {
  def apply(initialValue: String): JmxStringMetric =
    new JmxStringMetric(initialValue)

  implicit class JmxStringMetricOps(val m: Try[JmxStringMetric])
      extends AnyVal {
    def set(newValue: String): Unit = m.foreach(_.set(newValue))
  }
}

class JmxBooleanMetric(initialValue: Boolean) extends JmxMetric {
  val metricType               = "java.lang.Boolean"
  val value                    = new AtomicReference[Boolean](initialValue)
  def get(): java.lang.Boolean = java.lang.Boolean.valueOf(value.get)
  def set(newValue: Boolean)   = value.set(newValue)
}

object JmxBooleanMetric {
  def apply(initialValue: Boolean): JmxBooleanMetric =
    new JmxBooleanMetric(initialValue)

  implicit class JmxBooleanMetricOps(val m: Try[JmxBooleanMetric])
      extends AnyVal {
    def set(newValue: Boolean): Unit = m.foreach(_.set(newValue))
  }
}

case class JmxMetricInfo(metric: JmxMetric, info: MBeanAttributeInfo)
case class JmxOperationInfo(method: () => Unit, info: MBeanOperationInfo)

class JmxBean(jmxBeanName: String, jmxBeanDescription: Option[String] = None)
    extends DynamicMBean {
  private lazy val mbeanServer: MBeanServer =
    ManagementFactory.getPlatformMBeanServer();
  private var objectName: Try[ObjectName] = Failure(
    new Exception("Not Initialized")
  );
  private var metrics: Map[String, JmxMetricInfo]       = Map.empty
  private var operations: Map[String, JmxOperationInfo] = Map.empty
  private var registered: Boolean                       = false
  private var mbeanInfo: Option[MBeanInfo]              = None;

  def addLong(
      name: String,
      initialValue: Long,
      description: Option[String] = None
  ): Try[JmxLongMetric] = {
    val m = JmxLongMetric(initialValue)
    registerMetric(name, m, description)
  }

  def addString(
      name: String,
      initialValue: String,
      description: Option[String] = None
  ): Try[JmxStringMetric] = {
    val m = JmxStringMetric(initialValue)
    registerMetric(name, m, description)
  }

  def addBoolean(
      name: String,
      initialValue: Boolean,
      description: Option[String] = None
  ): Try[JmxBooleanMetric] = {
    val m = JmxBooleanMetric(initialValue)
    registerMetric(name, m, description)
  }

  private def registerMetric[M <: JmxMetric](
      name: String,
      metric: M,
      description: Option[String] = None
  ): Try[M] = {
    if (registered) {
      Failure(
        new Exception(s"Cannot add $name metric as bean is already registered")
      )
    } else {
      Try(
        new MBeanAttributeInfo(
          name,
          metric.metricType,
          description.getOrElse(""),
          true,
          false,
          false
        )
      )
        .map(attr => {
          val info = JmxMetricInfo(metric, attr)
          metrics = metrics.updated(name, info)
          metric
        })
    }
  }

  def registerOperation(
      name: String,
      method: () => Unit,
      description: Option[String] = None
  ): Try[JmxOperationInfo] = {
    if (registered) {
      Failure(
        new Exception(
          s"Cannot add $name operation as bean is already registered"
        )
      )
    } else {
      Try(
        new MBeanOperationInfo(
          name,
          description.getOrElse(""),
          null,
          "void",
          MBeanOperationInfo.ACTION
        )
      )
        .map(mbeanInfo => {
          val info = JmxOperationInfo(method, mbeanInfo)
          operations = operations.updated(name, info)
          info
        })
    }
  }

  def register(): Try[RegisteredMBean] = {
    if (registered) {
      Failure(new Exception(s"$jmxBeanName Already registered"))
    } else {
      objectName = Try(new ObjectName(jmxBeanName))
      objectName
        .flatMap(name => {
          val attrs = metrics.values.map(_.info).toArray
          val opes  = operations.values.map(_.info).toArray
          mbeanInfo = Some(
            new MBeanInfo(
              "helpers.JmxBean",
              jmxBeanDescription.getOrElse(""),
              attrs,
              null,
              opes,
              null
            )
          )
          Try(mbeanServer.registerMBean(this, name))
        })
        .map(instance => {
          registered = true
          RegisteredMBean(jmxBeanName, instance)
        })
    }
  }

  def unregister(): Try[UnregisteredMBean] = {
    if (!registered) {
      Failure(new Exception(s"$jmxBeanName is not registered"))
    } else {
      objectName
        .flatMap(name => Try(mbeanServer.unregisterMBean(name)))
        .map(_ => {
          registered = false
          UnregisteredMBean(jmxBeanName)
        })
    }
  }

  override def getAttribute(name: String): AnyRef =
    metrics.get(name).map(_.metric.get()).getOrElse(null)

  override def getAttributes(attributes: Array[String]): AttributeList = {
    val l = for {
      name <- attributes
      value = getAttribute(name)
    } yield new Attribute(name, value)

    new AttributeList(l.toList.asJava)
  }

  override def getMBeanInfo(): MBeanInfo = mbeanInfo.get

  override def invoke(
      actionName: String,
      params: Array[AnyRef],
      signature: Array[String]
  ): AnyRef = {
    operations.get(actionName).foreach(op => Try(op.method()))
    null
  }

  override def setAttribute(attribute: Attribute): Unit = {}

  override def setAttributes(attributes: AttributeList): AttributeList =
    new AttributeList()

}

object JmxBean {
  def formatBeanType(namespace: String, typeName: String): String =
    namespace + ":type=" + typeName
  def formatBeanName(
      namespace: String,
      typeName: String,
      name: String
  ): String = formatBeanType(namespace, typeName) + ",name=" + name
}
trait JmxActorMonitor { this: Actor =>
  lazy val jmxPrefix: String = "xuc."
  lazy val jmxActorNamespace: String =
    jmxPrefix + this.getClass.getPackage.getName
  lazy val jmxActorType: String = this.getClass.getSimpleName
  lazy val jmxActorName: String = self.path.name.replace(":", "_")

  lazy val jmxBean: JmxBean = new JmxBean(
    JmxBean.formatBeanName(jmxActorNamespace, jmxActorType, jmxActorName)
  )
}

trait JmxActorSingletonMonitor extends JmxActorMonitor { this: Actor =>
  override lazy val jmxBean: JmxBean = new JmxBean(
    JmxBean.formatBeanType(jmxActorNamespace, jmxActorType)
  )
}
