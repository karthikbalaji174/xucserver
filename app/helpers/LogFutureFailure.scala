package helpers

import scala.concurrent.{ExecutionContext, Future}
import play.api.Logger

object LogFutureFailure {
  def logFailure[A](
      message: => String
  )(implicit logger: Logger): PartialFunction[Throwable, Future[A]] = {
    case t =>
      logger.error(message, t)
      Future.failed(t)
  }
}

trait FutureFailureLogger {
  def logFailure[T](
      message: => String
  )(f: Future[T])(implicit logger: Logger, ec: ExecutionContext): Future[T] = {
    f.recoverWith(LogFutureFailure.logFailure(message))
  }
}
