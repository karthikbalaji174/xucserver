package models

import play.api.libs.json.{Json, Reads, Writes}

case class StunConfig(urls: List[String])

object StunConfig {
  implicit val reads: Reads[StunConfig]   = Json.reads[StunConfig]
  implicit val writes: Writes[StunConfig] = Json.writes[StunConfig]
}
