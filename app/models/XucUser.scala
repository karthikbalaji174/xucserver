package models

case class XucUser(
    username: String,
    xivoUser: XivoUser,
    phoneNumber: Option[String] = None
)
