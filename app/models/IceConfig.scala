package models

import play.api.libs.json.{Json, Reads, Writes}

case class IceConfig(
    stunConfig: Option[StunConfig] = None,
    turnConfig: Option[TurnConfig] = None
)

object IceConfig {
  implicit val reads: Reads[IceConfig]   = Json.reads[IceConfig]
  implicit val writes: Writes[IceConfig] = Json.writes[IceConfig]
}
