package models

import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.libs.json._

sealed trait XivoDird

/*
 * compatible with XiVO auth swagger specification v0.1
 */

case class DirSearchResult(
    columnHeaders: List[String],
    columnTypes: List[String],
    results: List[ResultItem]
)
object DirSearchResult {

  def parse(json: JsValue): DirSearchResult = {
    val columnHeaders = (json \ "column_headers").validate[List[String]].get
    val columnTypes   = (json \ "column_types").validate[List[String]].get
    val results       = ResultItem.read(columnTypes, (json \ "results").get)
    DirSearchResult(columnHeaders, columnTypes, results)
  }

}

case class ResultItem(
    item: DefaultDisplayViewItem,
    relations: Relations,
    source: String
)
object ResultItem {
  def read(columnTypes: List[String], json: JsValue): List[ResultItem] = {
    val list = json.validate[List[JsValue]].get
    val result = list.map(item => {
      val columnValues =
        DefaultDisplayViewItem.read(columnTypes, (item \ "column_values").get)
      val relations = (item \ "relations").validate[Relations].get
      val source    = (item \ "source").validate[String].get
      ResultItem(columnValues, relations, source)
    })
    result
  }
}

case class DefaultDisplayViewItem(
    name: String,
    number: Option[String],
    mobile: Option[String],
    externalNumber: Option[String],
    email: Option[String],
    favorite: Boolean,
    personal: Boolean
)
object DefaultDisplayViewItem {
  def read(columnTypes: List[String], json: JsValue): DefaultDisplayViewItem = {
    val index: Map[String, List[Int]] =
      columnTypes.zipWithIndex.groupBy(_._1).view.mapValues(_.map(_._2)).toMap
    def extractCols[T: Reads](id: String): List[T] = {
      index
        .get(id)
        .map(
          _.map(p => json(p).validateOpt[T].get)
            .filter(_.isDefined)
            .map(_.get)
        )
        .getOrElse(List.empty)
    }

    val name      = extractCols[String]("name").mkString(" ")
    val numbers   = extractCols[String]("number")
    val mobiles   = extractCols[String]("mobile")
    val callables = extractCols[String]("callable")
    val email = extractCols[String]("email").headOption match {
      case Some(e) => Some(e)
      case None    => extractCols[String]("other").headOption
    }
    val favorites = extractCols[Boolean]("favorite").headOption.getOrElse(false)

    val number = numbers.headOption
    val mobile = mobiles.headOption match {
      case o @ Some(_) => o
      case None        => callables.headOption
    }

    val externalNumber = numbers match {
      case _ :: number2 :: _ => Some(number2)
      case _ =>
        if (mobiles.nonEmpty)
          callables.headOption
        else
          callables match {
            case _ :: callable2 :: _ => Some(callable2)
            case _                   => None
          }
    }

    val personal = extractCols[Boolean]("personal").headOption.getOrElse(false)

    DefaultDisplayViewItem(
      name,
      number,
      mobile,
      externalNumber,
      email,
      favorites,
      personal
    )
  }

}

case class Relations(
    agentId: Option[Long],
    endpointId: Option[Long],
    userId: Option[Long],
    xivoId: Option[String],
    sourceEntryId: Option[String]
)
object Relations {
  implicit val relationReads: Reads[Relations] = (
    (JsPath \ "agent_id").readNullable[Long] and
      (JsPath \ "endpoint_id").readNullable[Long] and
      (JsPath \ "user_id").readNullable[Long] and
      (JsPath \ "xivo_id").readNullable[String] and
      (JsPath \ "source_entry_id").readNullable[String]
  )(Relations.apply _)

}
