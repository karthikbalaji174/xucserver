package models.ws.auth

import models.ws.auth.AuthType.AuthType
import models.ws.auth.LineType.LineType
import models.ws.auth.SoftwareType.SoftwareType
import pdi.jwt.{JwtAlgorithm, JwtJson, JwtJsonImplicits}
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.{Result, Results}
import play.mvc.Http
import xivo.models.Line

import scala.util.{Failure, Success, Try}
import controllers.helpers.AuthenticatedAction

case class ConnectionType(
    softwareType: SoftwareType,
    authType: AuthType,
    lineType: Option[LineType]
)

object AuthType extends Enumeration {
  type AuthType = Value

  val basic    = Value("basic")
  val kerberos = Value("kerberos")
  val cas      = Value("cas")
  val oidc     = Value("oidc")
}

object SoftwareType extends Enumeration {
  implicit val format: Format[SoftwareType] = Json.formatEnum(this)

  type SoftwareType = Value

  val electron   = Value("electron")
  val webBrowser = Value("webBrowser")
  val mobile     = Value("mobile")
  val unknown    = Value("unknown")

  def decode(s: Option[String]): Option[Value] = Try(
    withName(s.getOrElse(unknown.toString))
  ).toOption
}

object LineType extends Enumeration {

  implicit val format: Format[LineType] = Json.formatEnum(this)

  type LineType = Value

  val phone  = Value("phone")
  val webRTC = Value("webRTC")
  val ua     = Value("ua")

  def lineToLineType(line: Line): LineType = {
    if (line.ua) LineType.ua
    else if (line.webRTC) LineType.webRTC
    else LineType.phone
  }
}

case class AuthenticationRequest(
    login: String,
    password: String,
    expiration: Option[Int],
    entity: Option[String],
    softwareType: Option[SoftwareType]
)

object AuthenticationRequest {
  implicit val format = Json.format[AuthenticationRequest]
  implicit def enumWrites: Writes[SoftwareType.SoftwareType] =
    (value: SoftwareType.SoftwareType) => JsString(value.toString)
}

sealed trait AuthenticationResult {
  val log = Logger("AuthenticationResult")
  def toResult: Result
}

object AuthenticationSuccess {
  implicit val format = Json.format[AuthenticationSuccess]
}

case class AuthenticationSuccess(login: String, token: String, TTL: Long)
    extends AuthenticationResult {
  def toResult = {
    log.debug("Successful authentication")
    Results.Ok(Json.toJson(this))
  }
}

object AuthenticationError extends Enumeration {
  type AuthenticationError = Value
  val EmptyLogin                        = Value
  val Forbidden                         = Value
  val UserNotFound                      = Value
  val InvalidCredentials                = Value
  val InvalidToken                      = Value
  val InvalidJson                       = Value
  val BearerNotFound                    = Value
  val AuthorizationHeaderNotFound       = Value
  val TokenExpired                      = Value
  val UnhandledError                    = Value
  val SsoAuthenticationFailed           = Value
  val CasServerUrlNotSet                = Value
  val CasServerInvalidResponse          = Value
  val CasServerInvalidParameter         = Value
  val CasServerInvalidRequest           = Value
  val CasServerInvalidTicketSpec        = Value
  val CasServerUnauthorizedServiceProxy = Value
  val CasServerInvalidProxyCallback     = Value
  val CasServerInvalidTicket            = Value
  val CasServerInvalidService           = Value
  val CasServerInternalError            = Value
  val OidcNotEnabled                    = Value
  val OidcInvalidParameter              = Value
  val OidcAuthenticationFailed          = Value
  val WrongMobileSetup                  = Value
  val WrongRequester                    = Value

  implicit val format: Format[AuthenticationError] =
    new Format[AuthenticationError] {
      def writes(o: AuthenticationError): JsValue = JsString(o.toString)
      def reads(json: JsValue): JsResult[AuthenticationError] =
        JsSuccess(AuthenticationError.withName(json.as[String]))
    }
}

class AuthenticationException(
    val error: AuthenticationError.AuthenticationError,
    message: String
) extends Exception(message)

object AuthenticationFailure {
  implicit val format = Json.format[AuthenticationFailure]
  def from(t: Throwable): AuthenticationFailure =
    t match {
      case e: AuthenticationException =>
        AuthenticationFailure(e.error, e.getMessage)
      case _ =>
        AuthenticationFailure(AuthenticationError.UnhandledError, t.getMessage)
    }
}

case class AuthenticationFailure(
    error: AuthenticationError.AuthenticationError,
    message: String
) extends AuthenticationResult {
  import AuthenticationError._
  def toResult: Result = {
    log.warn(s"Failed authentication: $this")
    val body = Json.toJson(this)
    error match {
      case BearerNotFound | AuthorizationHeaderNotFound | InvalidJson |
          EmptyLogin | OidcInvalidParameter =>
        Results.BadRequest(body)
      case UnhandledError => Results.InternalServerError(body)
      case SsoAuthenticationFailed =>
        Results
          .Unauthorized(body)
          .withHeaders(Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate")
      case InvalidToken | InvalidCredentials => Results.Unauthorized(body)
      case _                                 => Results.Forbidden(body)
    }
  }
}

case class AuthenticationInformation(
    login: String,
    expiresAt: Long,
    issuedAt: Long,
    userType: String,
    acls: List[String],
    softwareType: Option[SoftwareType]
) {
  def refresh(expires: Long, acls: List[String]): AuthenticationInformation = {
    val now = AuthenticatedAction.now()
    this.copy(expiresAt = now + expires, issuedAt = now, acls = acls)
  }
  def encode(secret: String): String =
    AuthenticationInformation.encode(this, secret)
  def validate: Try[AuthenticationInformation] =
    if (expiresAt > AuthenticatedAction.now())
      Success(this)
    else
      Failure(
        new AuthenticationException(
          AuthenticationError.TokenExpired,
          "Token expired"
        )
      )

}

object AuthenticationInformation extends JwtJsonImplicits {
  implicit val format = Json.format[AuthenticationInformation]

  def encode(
      info: AuthenticationInformation,
      secret: String
  ): String =
    JwtJson.encode(Json.toJson(info).as[JsObject], secret, JwtAlgorithm.HS256)
  def decode(
      token: String,
      secret: String
  ): Try[AuthenticationInformation] =
    JwtJson
      .decodeJson(token, secret, Seq(JwtAlgorithm.HS256))
      .map(_.as[AuthenticationInformation])
      .recoverWith({ case t =>
        Failure(
          new AuthenticationException(
            AuthenticationError.InvalidToken,
            t.getMessage
          )
        )
      })

  def encodeFromLogin(
      login: String,
      secret: String,
      expires: Long,
      softwareType: Option[SoftwareType],
      acls: List[String]
  ) = {
    val now = AuthenticatedAction.now()
    encode(
      AuthenticationInformation(
        login,
        now + expires,
        now,
        AuthenticatedAction.ctiUserType,
        AuthenticatedAction.getAliasesFromAcls(acls),
        softwareType
      ),
      secret
    )
  }
}
