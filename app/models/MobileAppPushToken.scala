package models

import models.ws.{ErrorResult, ErrorType}
import play.api.libs.json.{Json, Reads, Writes}
import play.api.mvc.{Result, Results}

case class MobileAppPushToken(token: Option[String])

object MobileAppPushToken {
  implicit val reads: Reads[MobileAppPushToken] = Json.reads[MobileAppPushToken]
  implicit val writes: Writes[MobileAppPushToken] =
    Json.writes[MobileAppPushToken]
}

case class MobileAppError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case _ => Results.BadRequest(body)
    }
  }
}
object MobileAppError {
  implicit val mobileAppErrorWrites: Writes[MobileAppError] =
    Json.writes[MobileAppError]
}
