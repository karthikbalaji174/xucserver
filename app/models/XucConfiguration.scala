package models

import play.api.libs.json.{JsValue, Json, Writes}

sealed trait XucConfiguration

case class ConfigurationEntry(name: String, value: String)
    extends XucConfiguration
case class ConfigurationIntSetEntry(name: String, value: Set[Int])
    extends XucConfiguration

object ConfigurationEntry {
  implicit val format = Json.format[ConfigurationEntry]
}

object ConfigurationIntSetEntry {
  implicit val format = Json.writes[ConfigurationIntSetEntry]
}

object XucConfiguration {
  implicit val writes = new Writes[XucConfiguration] {
    override def writes(conf: XucConfiguration): JsValue = {
      val (_, data) = conf match {
        case c: ConfigurationEntry =>
          (c, Json.toJson(c)(ConfigurationEntry.format))
        case c: ConfigurationIntSetEntry =>
          (c, Json.toJson(c)(ConfigurationIntSetEntry.format))
      }
      data
    }
  }
}
