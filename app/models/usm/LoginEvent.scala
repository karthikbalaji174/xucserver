package models.usm

import play.api.libs.json.{Json, Writes}

case class LoginEvent(
    softwareType: String,
    applicationType: String,
    lineType: String,
    userId: String,
    eventTime: String
)

object LoginEvent {
  implicit val loginEventWrites: Writes[LoginEvent] = Json.writes[LoginEvent]
}
