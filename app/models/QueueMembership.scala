package models

import play.api.libs.json._

case class QueueMembership(queueId: Long, penalty: Int)

object QueueMembership {
  implicit val queueMembershipWrites = Json.writes[QueueMembership]
  implicit val queueMembershipReads  = Json.reads[QueueMembership]
}

case class UserQueueDefaultMembership(
    userId: Long,
    membership: List[QueueMembership]
)

object UserQueueDefaultMembership {
  implicit val userQueueMembershipWrites =
    Json.writes[UserQueueDefaultMembership]
  implicit val userQueueMembershipReads = Json.reads[UserQueueDefaultMembership]
}

case class UsersQueueDefaultMembership(
    memberships: List[UserQueueDefaultMembership]
)

object UsersQueueDefaultMembership {
  implicit val usersQueueDefaultMembership =
    Json.writes[UsersQueueDefaultMembership]
  implicit val usersQueueMembershipReads =
    Json.reads[UsersQueueDefaultMembership]
}
