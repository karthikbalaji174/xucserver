package models

import play.api.libs.json.{Json, Writes}

case class Queue(id: Option[Int], name: String, displayName: String)

object Queue {
  implicit val writes = new Writes[Queue] {
    def writes(queue: Queue) =
      Json.obj(
        "id"          -> queue.id,
        "name"        -> queue.name,
        "displayName" -> queue.displayName
      )
  }
}
