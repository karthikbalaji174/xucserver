package models

import play.api.libs.json.{Json, Reads, Writes}

case class TurnConfig(
    urls: List[String],
    username: String,
    credential: String,
    ttl: Long
)

object TurnConfig {
  implicit val reads: Reads[TurnConfig]   = Json.reads[TurnConfig]
  implicit val writes: Writes[TurnConfig] = Json.writes[TurnConfig]
}
