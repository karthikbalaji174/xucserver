function callHistoryHandler(callHistory) {
    console.log('Call history handler ' + JSON.stringify(callHistory));
    $('#call_history').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callHistory) + '</code></pre></li>');
}

$('#xuc_find_customer_call_history').click(function() {
    Cti.findCustomerCallHistory(1, [{"field": $("#xuc_cus_field").val(), "operator": $("#xuc_cus_operator").val(), "value": $("#xuc_cus_value").val()}], 7);
});
$('#xuc_get_user_call_history').click(function() {
    var days = Number($("#xuc_uch_days").val());
    var size = Number($("#xuc_uch_size").val());

    if(!size && !days) Cti.getUserCallHistory(7);
    else if(size && !days) Cti.getUserCallHistory(size);
    else if(!size && days) Cti.getUserCallHistoryByDays(days);
    else generateMessage("Cannot set size and days at the same time for the user call history", true, true);
});

$('#xuc_get_queue_call_history').click(function() {
    Cti.getQueueCallHistory($("#xuc_queue").val(), 7);
});


$('#xuc_get_agent_call_history').click(function() {
    Cti.getAgentCallHistory(7);
});

$('#xuc_clean_call_history').click(function(event) {
     $('#call_history').empty();
});


function initHistory() {
  Cti.setHandler(Cti.MessageType.CALLHISTORY, callHistoryHandler);
  Cti.setHandler(Cti.MessageType.CALLHISTORYBYDAYS, callHistoryHandler);
  Cti.setHandler(Cti.MessageType.CUSTOMERCALLHISTORY, callHistoryHandler);
}