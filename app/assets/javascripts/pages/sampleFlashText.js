var _seq = 0;
var _seq_hist = 0;

function FlashTextEventHandler(event) {
  console.log("Flash Text Event Handler " + JSON.stringify(event));
  $('#flashTextEvents').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(event) + '</code></pre></li>');
}
function displayNameHandler(queueMember) {
  $('#display_name').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueMember) + '</code></pre></li>');
}

$('#xuc_send_flash_text').click(function(event) {
  _seq = _seq + 1;

  Cti.sendFlashTextMessage($("#xuc_flash_username").val(), _seq, $("#xuc_flash_message").val());
});

$('#xuc_get_flash_text_history').click(function(event) {
  _seq_hist = _seq_hist + 1;

  Cti.getFlashTextPartyHistory($("#xuc_flash_username_history").val(), _seq_hist);
});

$('#xuc_mark_as_read_flash_text').click(function(event) {
  _seq_hist = _seq_hist + 1;

  Cti.markAsReadFlashText($("#xuc_read_flash_text").val(), _seq_hist);
});

$('#xuc_clean_FlashTextEvent').click(function(event) {
  $('#flashTextEvents').empty();
});

$('#xuc_get_display_name_by_username').click(function(event) {
  Cti.displayNameLookup($("#xuc_display_name_by_username").val());
});

$('#xuc_clean_display_name').click(function(event) {
 $('#display_name').empty();
});

function initFlashText() {
  Cti.setHandler(Cti.MessageType.FLASHTEXTEVENT, FlashTextEventHandler);
  Cti.setHandler(Cti.MessageType.USERDISPLAYNAME, displayNameHandler);
}