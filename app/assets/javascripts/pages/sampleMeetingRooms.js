function meetingroomsEvent(meetingrooms) {
  console.log("meeting rooms Event Handler " + JSON.stringify(meetingrooms));
  $('#meetingroomsEvents').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(meetingrooms) + '</code></pre></li>');
}

function meetingRoomsInvitationEvent(meetingRooms) {
  console.log("Meeting room invitation event handler " + JSON.stringify(meetingRooms));
  $('#meeting-room-invitation-events').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(meetingRooms) + '</code></pre></li>');
}

$('#xuc_subscribe_to_video_status_events').click(function(event) {
  Cti.subscribeToVideoStatus(localStorage.getItem('sampleUsername').split(","));
});

$('#xuc_unsubscribe_from_video_status_events').click(function(event) {
  Cti.unsubscribeFromAllVideoStatus();
});

$('#xuc_clean_meetingroomsEvent').click(function(event) {
  $('#meetingroomsEvents').empty();
});

$('#xuc_start_meeting_room').click(function(event) {
  Cti.videoEvent("videoStart");
});

$('#xuc_end_meeting_room').click(function(event) {
  Cti.videoEvent("videoEnd");
});

$('#btn-invite-meeting-room').click(function(event) {
  var token = $('#meeting-room-invitee-token').val();
  var sequence = parseInt($('#meeting-room-invitee-sequence').val());
  var inviteeUsername = $('#meeting-room-invitee-username').val();
  Cti.inviteToMeetingRoom(sequence, token, inviteeUsername);
});

$('#btn-ack-meeting-room').click(function(event) {
  var sequence = parseInt($('#meeting-room-invitee-sequence').val());
  var destUsername = $('#meeting-room-invitee-username').val();
  Cti.meetingRoomInviteAck(sequence, destUsername);
});

$('#btn-accept-meeting-room').click(function(event) {
  var sequence = parseInt($('#meeting-room-invitee-sequence').val());
  var destUsername = $('#meeting-room-invitee-username').val();
  Cti.meetingRoomInviteAccept(sequence, destUsername);
});

$('#btn-refuse-meeting-room').click(function(event) {
  var sequence = parseInt($('#meeting-room-invitee-sequence').val());
  var destUsername = $('#meeting-room-invitee-username').val();
  Cti.meetingRoomInviteReject(sequence, destUsername);
});

$('#btn-clean-meeting-room').click(function(event) {
 $('#meeting-room-invitation-events').empty();
});


function initMeetingRooms() {
  Cti.setHandler(Cti.MessageType.VIDEOEVENT, meetingroomsEvent);
  Cti.setHandler(Cti.MessageType.MEETINGROOMINVITE, meetingRoomsInvitationEvent);
  Cti.setHandler(Cti.MessageType.MEETINGROOMACK, meetingRoomsInvitationEvent);
  Cti.setHandler(Cti.MessageType.MEETINGROOMRESPONSE, meetingRoomsInvitationEvent);
}
